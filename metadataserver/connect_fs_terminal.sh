#!/bin/bash
#
# Connects to the metadataserver container through a file. This script assumes
# that the FILE_SHELL_ variables in docker-compose.yml have not changed.
#

cd remoteshelloverfs
./file_shell_client.sh shared/command.txt shared/output.txt