#bin/bash

if [ -z ${FILE_SHELL_ENABLE} ]
then
    echo "Not starting the file shell."
else
    echo "Starting file_shell"
    $FILE_SHELL_DIR/wrapper_script.sh &
fi