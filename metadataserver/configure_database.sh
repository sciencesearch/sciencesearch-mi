#!/bin/bash
#
# Script to create and configure the postgress database used by the
# medatadata server.

export POSTGRES_USER="metadataserver"         # default "postgres"
export POSTGRES_PASSWORD="p4ssw3rd"     # default empty

echo "Starting services..."
docker-compose up -d
echo "Waiting for DB to be ready"
sleep 5
docker-compose down
docker-compose up -d
sleep 5
echo "Creating database (If already existing, it will fail, but the script"\
"will continue."
docker-compose exec db psql -U metadataserver -c "CREATE DATABASE metadataserver;"
sleep 5
docker-compose down
docker-compose up -d
sleep 5
rm ncem/migrations/*.py
rm ncemreldata/migrations/*.py
rm searchengine/migrations/*.py
rm papermanager/migrations/*.py

echo "About to do the django migrations: configure db and server."
docker-compose run web-backend python manage.py makemigrations ncem
docker-compose run web-backend python manage.py makemigrations ncemreldata
docker-compose run web-backend python manage.py makemigrations searchengine
docker-compose run web-backend python manage.py makemigrations papermanager
docker-compose run web-backend python manage.py migrate

echo "About to create the superuser to administrate the django server."
echo "Note down the user and password and use [site]/admin to create new users."
docker-compose run web-backend python manage.py createsuperuser

echo "Stopping services..."
docker-compose down
