from django.test import TestCase
from metadataserver.seminfer.textanalysis.nlp import tokenize


class TestTestNLP(TestCase):
    text = ("That's a great starting point for developing custom search, content"
            " recommenders, and even AI applications.")

    def test_tokenize(self):
        tokens = ["That",
                  "'s",
                  "a",
                  "great",
                  "starting",
                  "point",
                  "for",
                  "developing",
                  "custom",
                  "search",
                  ",",
                  "content",
                  "recommenders",
                  ",",
                  "and",
                  "even",
                  "AI",
                  "applications",
                  "."]
        self.assertEqual([str(t) for t in tokenize(self.text)],
                         tokens)
