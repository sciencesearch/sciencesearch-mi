from django.test import TestCase

from metadataserver.seminfer.tagging import (
    _get_new_rank, correct_tfidf_dic, adjust_textrank_with_tfidf,
    simplify_expressions
    )


class TestAdjustTextrank(TestCase):

    def test_adjust_textrank_with_tfidf(self):
        text_rank = [dict(text="green house effect",
                          rank=0.3),
                     dict(text="green dogs",
                          rank=0.2),
                     dict(text="big concrete house",
                          rank=0.1)]

        tf_idf = dict(house=2,
                      dog=1,
                      concrete=-1,
                      effect=1.5)
        new_text_rank = adjust_textrank_with_tfidf(text_rank,
                                                   tf_idf)
        self.assertEqual(new_text_rank,
                         [
                             dict(text="green house effect",
                                  rank=0.3 * 1.75),
                             dict(text="green dogs",
                                  rank=0.2),
                             dict(text="big concrete house",
                                  rank=0.2)])

        tf_idf = dict(house=2,
                      dog=1,
                      concrete=-1,
                      effect=1.5)
        new_text_rank = adjust_textrank_with_tfidf(text_rank,
                                                   tf_idf,
                                                   discard_negatives=False)
        self.assertEqual(new_text_rank,
                         [
                             dict(text="green house effect",
                                  rank=0.3 * 1.75),
                             dict(text="green dogs",
                                  rank=0.2),
                             dict(text="big concrete house",
                                  rank=0.05)])

    def test_adjust_textrank_with_tfidf_filter(self):
        text_rank = [dict(text="green house effect",
                          rank=0.3),
                     dict(text="green dogs",
                          rank=0.2),
                     dict(text="big concrete house",
                          rank=0.1),
                     dict(text="dog",
                          rank=0.1),
                     dict(text="cat",
                          rank=0.1)]

        tf_idf = dict(house=2,
                      dog=1,
                      concrete=-1,
                      effect=1.5)
        new_text_rank = adjust_textrank_with_tfidf(text_rank,
                                                   tf_idf,
                                                   filter_non_tf_words=True)
        self.assertEqual(new_text_rank,
                         [
                             dict(text="green house effect",
                                  rank=0.3 * 1.75),
                             dict(text="green dogs",
                                  rank=0.2),
                             dict(text="big concrete house",
                                  rank=0.2),
                             dict(text="dog",
                                  rank=0.1)])

    def test_get_new_rank(self):
        tf_idf = dict(house=2,
                      dog=1,
                      concrete=0.5,
                      effect=1.5)

        self.assertEqual(_get_new_rank("green house effect", 0.3,
                                       tf_idf),
                         0.3 * 1.75
                         )

    def test_get_new_rank_filter(self):
        tf_idf = dict(house=2,
                      dog=1,
                      concrete=0.5,
                      effect=1.5)

        self.assertEqual(_get_new_rank("my cat is rich", 0.3,
                                       tf_idf, filter_non_tf_words=True),
                         0.3
                         )
        self.assertEqual(_get_new_rank("cat", 0.3,
                                       tf_idf, filter_non_tf_words=True),
                         None
                         )

    def test_correct_tfidf_dic(self):
        tf_idf = dict(house=2,
                      dog=1,
                      concrete=0.5,
                      effect=-1.5)
        self.assertEqual(correct_tfidf_dic(tf_idf),
                         dict(house=3,
                              dog=2,
                              concrete=1.5,
                              effect=-2.5))

    def test_simplify_expressions(self):
        text_rank = [dict(text="green house effect",
                          rank=0.3),
                     dict(text="green house",
                          rank=0.2),
                     dict(text="house effect",
                          rank=0.1)]

        new_text_rank = simplify_expressions(text_rank)
        self.assertEqual(new_text_rank,
                         [dict(text="green house",
                               rank=0.2),
                          dict(text="house effect",
                               rank=0.1)])

        text_rank = [dict(text="green house effect",
                          rank=0.3),
                     dict(text="house",
                          rank=0.2),
                     dict(text="house effect",
                          rank=0.1)]

        new_text_rank = simplify_expressions(text_rank)
        self.assertEqual(new_text_rank,
                         [dict(text="green house effect",
                               rank=0.3),
                          dict(text="house",
                               rank=0.2),
                          dict(text="house effect",
                               rank=0.1)])

        text_rank = [dict(text="green house effect",
                          rank=0.3),
                     dict(text="house",
                          rank=0.2),
                     dict(text="house effect",
                          rank=0.1),
                     dict(text="dog",
                          rank=0.1)]

        new_text_rank = simplify_expressions(text_rank)
        self.assertEqual(new_text_rank,
                         [dict(text="green house effect",
                               rank=0.3),
                          dict(text="house",
                               rank=0.2),
                          dict(text="house effect",
                               rank=0.1),
                          dict(text="dog",
                               rank=0.1)])

        text_rank = [dict(text="green house",
                          rank=0.3),
                     dict(text="house effect",
                          rank=0.2),
                     dict(text="green house effect",
                          rank=0.1)]

        new_text_rank = simplify_expressions(text_rank)
        self.assertEqual(new_text_rank,
                         [dict(text="green house",
                               rank=0.3),
                          dict(text="house effect",
                               rank=0.2)])
