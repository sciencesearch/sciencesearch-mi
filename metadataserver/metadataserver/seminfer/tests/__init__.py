import os
import shutil

from django.test import TestCase, TransactionTestCase
from metadataserver.seminfer.fileanalysis import simplify_route, ensure_folder


class TestFileGenericAbstract(object):

    def _create_test_file(self, file_name, content):
        file_route = os.path.join(self._tmp_folder, file_name)
        self._register_tmp_file(file_route)

        cvs_file = open(file_route, 'w')
        cvs_file.write(content)
        cvs_file.close()

        return file_route

    def _register_tmp_file(self, tmp_file):
        self.addCleanup(os.remove, tmp_file)
        self.assertFalse(os.path.isfile(tmp_file), "Temporary file {} exists"
                                                   " before starting!".format(tmp_file))

    def _register_tmp_folder(self, tmp_file):
        self.addCleanup(shutil.rmtree, tmp_file)
        self.assertFalse(os.path.isfile(tmp_file), "Temporary file {} exists"
                                                   " before starting!".format(tmp_file))
        self.assertFalse(os.path.exists(tmp_file), "Temporary folder {} exists"
                                                   " before starting!".format(tmp_file))

    def _create_empty(self, file_name, is_folder=False):
        if not is_folder:
            with open(file_name, 'a'):
                os.utime(file_name)
        else:
            os.makedirs(file_name)


class TestFileGeneric(TestCase, TestFileGenericAbstract):
    pass


class TestFileGenericTransaction(TransactionTestCase, TestFileGenericAbstract):
    pass


class TestEnsureFolder(TestFileGeneric):

    def test_ensure_folder(self):
        self._register_tmp_folder("tmp/test_crawl/afolder")
        ensure_folder("tmp/test_crawl/afolder/file")
        self.assertTrue(ensure_folder("tmp/test_crawl/afolder/file"))


class TestSimplifyRotue(TestCase):
    def test_simplify_route(self):
        self.assertEqual("file",
                         simplify_route(
                             "/base/",
                             "/base/file"))

        self.assertEqual("file",
                         simplify_route(
                             "/base",
                             "/base/file"))
        self.assertEqual("dir",
                         simplify_route(
                             "/base",
                             "/base/dir/"))
        self.assertEqual("dir/file",
                         simplify_route(
                             "/base",
                             "/base/dir/file"))
        self.assertEqual("file",
                         simplify_route(
                             "/base/dir",
                             "/base/dir/file"))
        self.assertEqual("/base/dir/file",
                         simplify_route(
                             "",
                             "/base/dir/file"))
