from metadataserver.seminfer.fileanalysis.filefinders import (
    get_all_files, _filter_extensions, do_on_all_files
    )
from metadataserver.seminfer.tests import TestFileGeneric


class TestFileListing(TestFileGeneric):

    def test_filter_extensions(self):
        file_list = ["tmp_dir/file1",
                     "tmp_dir/file2.a",
                     "tmp_dir/file3.md3",
                     "tmp_dir/file4.md4",
                     "tmp_dir/file5.md4"]
        f_list = _filter_extensions(file_list, "*")
        self.assertEqual(file_list, f_list)

        f_list = _filter_extensions(file_list, ["*", "a"])
        self.assertEqual(file_list, f_list)

        f_list = _filter_extensions(file_list, "a")
        self.assertEqual(f_list, ["tmp_dir/file2.a"])

        f_list = _filter_extensions(file_list, ["a", "md3"])
        self.assertEqual(f_list, ["tmp_dir/file2.a",
                                  "tmp_dir/file3.md3"])
        f_list = _filter_extensions(file_list, ["md3", "md4"])
        self.assertEqual(f_list, ["tmp_dir/file3.md3",
                                  "tmp_dir/file4.md4",
                                  "tmp_dir/file5.md4"])

        f_list = _filter_extensions(file_list, ["md3", "md4"], folder="dir")
        self.assertEqual(f_list, ["dir/tmp_dir/file3.md3",
                                  "dir/tmp_dir/file4.md4",
                                  "dir/tmp_dir/file5.md4"])

        f_list = _filter_extensions(["tmp_dir/file3.md3",
                                     "tmp_dir/file4.md4",
                                     "tmp_dir/file5.md4"],
                                    "*", folder="dir")
        self.assertEqual(f_list, ["dir/tmp_dir/file3.md3",
                                  "dir/tmp_dir/file4.md4",
                                  "dir/tmp_dir/file5.md4"])

    def test_non_recusive(self):
        self._register_tmp_folder("/tmp/tmp_dir")
        self._create_empty("/tmp/tmp_dir", True)
        self._create_empty("/tmp/tmp_dir/file1")
        self._create_empty("/tmp/tmp_dir/file2.a")
        self._create_empty("/tmp/tmp_dir/file3.md3")
        self._create_empty("/tmp/tmp_dir/file4.md4")
        self._create_empty("/tmp/tmp_dir/file5.md4")

        file_list = get_all_files("/tmp/tmp_dir", "*")
        self.assertEqual(set(file_list), set(["/tmp/tmp_dir/file1",
                                              "/tmp/tmp_dir/file2.a",
                                              "/tmp/tmp_dir/file3.md3",
                                              "/tmp/tmp_dir/file4.md4",
                                              "/tmp/tmp_dir/file5.md4"]))

        file_list = get_all_files("/tmp/tmp_dir", "a")
        self.assertEqual(file_list, ["/tmp/tmp_dir/file2.a"])

        file_list = get_all_files("/tmp/tmp_dir", ["md3", "md4"])
        self.assertEqual(set(file_list), set(["/tmp/tmp_dir/file3.md3",
                                              "/tmp/tmp_dir/file4.md4",
                                              "/tmp/tmp_dir/file5.md4"]))

    def test_recursive(self):
        self._register_tmp_folder("/tmp/tmp_dir")
        self._create_empty("/tmp/tmp_dir", True)
        self._create_empty("/tmp/tmp_dir/f1", True)
        self._create_empty("/tmp/tmp_dir/f2", True)
        self._create_empty("/tmp/tmp_dir/f2/f3", True)
        self._create_empty("/tmp/tmp_dir/f1/file1")
        self._create_empty("/tmp/tmp_dir/f1/file2.a")
        self._create_empty("/tmp/tmp_dir/f2/file3.md3")
        self._create_empty("/tmp/tmp_dir/f2/file4.md4")
        self._create_empty("/tmp/tmp_dir/f2/f3/file5.md4")
        file_list = get_all_files("/tmp/tmp_dir", "*", recursive=True)
        self.assertEqual(set(file_list),
                         set(["/tmp/tmp_dir/f1/file1",
                              "/tmp/tmp_dir/f1/file2.a",
                              "/tmp/tmp_dir/f2/file3.md3",
                              "/tmp/tmp_dir/f2/file4.md4",
                              "/tmp/tmp_dir/f2/f3/file5.md4"]))

        file_list = get_all_files("/tmp/tmp_dir", "a", recursive=True)
        self.assertEqual(set(file_list),
                         set(["/tmp/tmp_dir/f1/file2.a"]))

        file_list = get_all_files("/tmp/tmp_dir", ["a", "md3"], recursive=True)
        self.assertEqual(set(file_list),
                         set(["/tmp/tmp_dir/f1/file2.a",
                              "/tmp/tmp_dir/f2/file3.md3"]))

        file_list = get_all_files("/tmp/tmp_dir", ["md3", "md4"], recursive=True)
        self.assertEqual(set(file_list),
                         set(["/tmp/tmp_dir/f2/file3.md3",
                              "/tmp/tmp_dir/f2/file4.md4",
                              "/tmp/tmp_dir/f2/f3/file5.md4"]))

    def _aux_call(self, file_name, arg1, arg2="arg2", arg3=""):
        return "{}-{}-{}-{}".format(file_name, arg1, arg2, arg3)

    def test_do_on_all_files(self):
        self._register_tmp_folder("/tmp/tmp_dir")
        self._create_empty("/tmp/tmp_dir", True)
        self._create_empty("/tmp/tmp_dir/f1", True)
        self._create_empty("/tmp/tmp_dir/f2", True)
        self._create_empty("/tmp/tmp_dir/f2/f3", True)
        self._create_empty("/tmp/tmp_dir/f1/file1")
        self._create_empty("/tmp/tmp_dir/f1/file2.a")
        self._create_empty("/tmp/tmp_dir/f2/file3.md3")
        self._create_empty("/tmp/tmp_dir/f2/file4.md4")
        self._create_empty("/tmp/tmp_dir/f2/f3/file5.md4")

        list_output = do_on_all_files("/tmp/tmp_dir",
                                      ["md3", "md4"],
                                      self._aux_call,
                                      True,
                                      "arg1",
                                      arg3="arg3")
        self.assertEqual(set(list_output),
                         set(["/tmp/tmp_dir/f2/file3.md3-arg1-arg2-arg3",
                              "/tmp/tmp_dir/f2/file4.md4-arg1-arg2-arg3",
                              "/tmp/tmp_dir/f2/f3/file5.md4-arg1-arg2-arg3"]))

        list_output = do_on_all_files("/tmp/tmp_dir/f2",
                                      ["md3", "md4"],
                                      self._aux_call,
                                      False,
                                      "arg1",
                                      arg3="arg3")
        self.assertEqual(set(list_output),
                         set(["/tmp/tmp_dir/f2/file3.md3-arg1-arg2-arg3",
                              "/tmp/tmp_dir/f2/file4.md4-arg1-arg2-arg3"]))
