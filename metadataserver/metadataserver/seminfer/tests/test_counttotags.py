from django.test import TestCase

from metadataserver.seminfer.tagging.counttotags import (
    get_tags_uniform_fixed, get_tags_progresive_fixed, get_tags_relative,
    flatten_dict_count, select_tags, combine_tag_lists_simple)


class TestTagselection(TestCase):
    def test_flatten_dict_count(self):
        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)

        self.assertEqual(flatten_dict_count(word_count_dic),
                         ["e", "d", "c", "b", "a"])

        self.assertEqual(flatten_dict_count(word_count_dic, decreasing=False),
                         ["a", "b", "c", "d", "e"])

        self.assertEqual(flatten_dict_count(word_count_dic,
                                            include_count=True),
                         ["e", 15, "d", 10, "c", 5, "b", 3, "a", 1.0])

    def test_get_tags_uniform_fixed(self):
        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)

        importance_dict = get_tags_uniform_fixed(word_count_dic,
                                                 4,
                                                 importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(e=4, d=3, c=2, b=1))

        word_count_dic = dict(a=1.0)
        importance_dict = get_tags_uniform_fixed(word_count_dic,
                                                 4,
                                                 importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(a=4))

        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)
        importance_dict = get_tags_uniform_fixed(word_count_dic,
                                                 1,
                                                 importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(e=4))

    def test_get_tags_uniform_fixed_selected(self):
        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)

        importance_dict = select_tags("uni_fixed", word_count_dic,
                                      4,
                                      importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(e=4, d=3, c=2, b=1))

        word_count_dic = dict(a=1.0)
        importance_dict = select_tags("uni_fixed", word_count_dic,
                                      4,
                                      importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(a=4))

        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)
        importance_dict = select_tags("uni_fixed", word_count_dic,
                                      1,
                                      importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(e=4))

    def test_get_tags_progresive_fixed(self):
        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)

        importance_dict = get_tags_progresive_fixed(word_count_dic, 4,
                                                    importance_limits=(1, 4))

        self.assertEqual(importance_dict,
                         dict(e=4, d=3, c=2, b=1))

        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)
        importance_dict = get_tags_progresive_fixed(word_count_dic, 10,
                                                    importance_limits=(0, 10))
        self.assertEqual(importance_dict,
                         dict(e=10, d=7.5, c=5, b=2.5, a=0))

    def test_get_tags_progresive_fixed_selected(self):
        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)

        importance_dict = select_tags("progressive_fixed", word_count_dic, 4,
                                      importance_limits=(1, 4))

        self.assertEqual(importance_dict,
                         dict(e=4, d=3, c=2, b=1))

        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)
        importance_dict = select_tags("progressive_fixed", word_count_dic, 10,
                                      importance_limits=(0, 10))
        self.assertEqual(importance_dict,
                         dict(e=10, d=7.5, c=5, b=2.5, a=0))

    def test_get_tags_relative(self):
        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)

        importance_dict = get_tags_relative(word_count_dic,
                                            share_of_tags=0.5,
                                            importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(e=4, d=1))

        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15, f=20)

        importance_dict = get_tags_relative(word_count_dic,
                                            share_of_tags=0.5,
                                            importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(f=4, e=2.5, d=1))

        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15, f=20)

        importance_dict = get_tags_relative(word_count_dic,
                                            share_of_tags=0.5,
                                            max_number_of_tags=2,
                                            importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(f=4, e=1))

    def test_get_tags_relative_selected(self):
        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15)

        importance_dict = select_tags("relative", word_count_dic,
                                      share_of_tags=0.5,
                                      importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(e=4, d=1))

        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15, f=20)

        importance_dict = select_tags("relative", word_count_dic,
                                      share_of_tags=0.5,
                                      importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(f=4, e=2.5, d=1))

        word_count_dic = dict(a=1.0, b=3, c=5, d=10, e=15, f=20)

        importance_dict = select_tags("relative", word_count_dic,
                                      share_of_tags=0.5,
                                      max_number_of_tags=2,
                                      importance_limits=(1, 4))
        self.assertEqual(importance_dict,
                         dict(f=4, e=1))


class TestCombineTagLists(TestCase):

    def test_combine_tag_lists_simple(self):
        tags = [{"word1": 1, "word2": 2, "word3": 3},
                {"word1": 2, "word2": 0, "word4": 4, }]

        self.assertEqual(combine_tag_lists_simple(tags),
                         {"word1": 2, "word2": 2, "word3": 3, "word4": 4})
