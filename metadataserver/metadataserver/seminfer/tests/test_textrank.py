from metadataserver.seminfer.tests import TestFileGeneric
from metadataserver.seminfer.textanalysis import textrank as tr


class TestTextRank(TestFileGeneric):
    "Abstract taken from http://pubs.acs.org/doi/full/10.1021/acsnano.5b05556"
    sample_text = """The isolation of graphene in 2004 from graphite was a
defining moment for the “birth” of a field: two-dimensional (2D) materials. In
recent years, there hasbeen a rapidly increasing number of papers focusing on
non-graphene layered materials, including transition-metal dichalcogenides
(TMDs), because of the new properties and applications that emerge upon 2D
confinement. Here, we review significant recent advances and important
new developments in 2D materials “beyond graphene”.
We provide insight into the theoretical modeling and understanding of the van
der Waals (vdW) forces that hold together the 2D layers in bulk solids, as
well as their excitonic properties and growth morphologies. Additionally, we
highlight recent breakthroughs in TMD synthesis and characterization and discuss
the newest families of 2D materials, including monoelement 2D materials (i.e.,
silicene, phosphorene, etc.) and transition metal carbide- and carbon
nitride-based MXenes. We then discuss the doping and functionalization of 2D
materials beyond graphene that enable device applications, followed by advances
in electronic, optoelectronic, and magnetic devices and theory. Finally, we
provide perspectives on the future of 2D materials beyond graphene."""

    def setUp(self):
        self._register_tmp_folder("tr_tmp")
        self._create_empty("tr_tmp", is_folder=True)

    def test_process_text(self):
        (keywords, sentences, normalized_key_phrases_as_dict
         ) = tr.process_text(self.sample_text, debug=False)
        self.assertTrue("graphene" in keywords)
