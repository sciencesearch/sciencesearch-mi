"""
Funtions to find files are call functions on them
"""

import os


def _filter_extensions(file_list, extensions, folder=""):
    if type(extensions) is str:
        extensions = [extensions]
    if '*' in extensions:
        return [os.path.join(folder, x)
                for x in file_list]
    else:
        exts = []
        for ext in extensions:
            if ext.startswith('.'):
                exts.append(ext)
            else:
                exts.append('.{}'.format(ext))
        return [os.path.join(folder, x)
                for x in file_list if x.endswith(tuple(exts))]


def get_all_files(folder_route, extensions, recursive=False):
    """ Returns a list of the routes to files under a folder ending with certain
    list of extensions.
    Args:
        - folder_router: string pointing to a file system directory to search
          in.
        - extensions: list of strings (not started with ".") containing the
          extensions which files should end with. If set to "*" it returns
          all files.
        - recursive: if true, get_all_files returns matching files in
          subdirectories of folder_router.
    Returns:
        A list of filesystem routes to the files that are under folder_route
        and end with extensions.
    """
    if not recursive:
        file_list = os.listdir(folder_route)
        file_list = _filter_extensions(file_list, extensions,
                                       folder=folder_route)
    else:
        file_list = []
        for root, dirs, files in os.walk(folder_route):
            for file in files:
                file_list.append(os.path.join(root, file))
        file_list = _filter_extensions(file_list, extensions)
    return file_list


def do_on_all_files(folder_route, extensions,
                    function_call, recursive, *args, **kwargs):
    """Calls function_call(filer_route, *args, **kwargs) on all the files under
    folder_route (recursively).
    Args:
        folder_route: folder under which route will be passed to function_call
        extension: only the route of files which extension is in extensions
          will be considered. If "*" is present, all files are procesed.
        function_call: function to be called. 
        *args, **kwargs: extra arguments to be passed to function_call.
        
    """
    return_list = []
    if not recursive:
        file_names = get_all_files(folder_route, extensions)
        for file_name in file_names:
            return_list.append(function_call(file_name, *args, **kwargs))
    else:
        for root, dirs, file_names in os.walk(folder_route):
            file_names = _filter_extensions(file_names, extensions)
            for file_name in file_names:
                return_list.append(function_call(os.path.join(root, file_name),
                                                 *args, **kwargs))
    return return_list
