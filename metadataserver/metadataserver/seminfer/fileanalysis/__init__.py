import os


def ensure_folder(file_route):
    folder = os.path.dirname(file_route)
    if not os.path.exists(folder):
        try:
            os.makedirs(folder)
        except OSError as e:
            if e.errno != os.errno.EEXIST:
                raise
            print("Warning, folder existed: {}".format)
    return True


def simplify_route(base_route, file_route):
    """Returns a string of a file route removing base_route from it"""
    if not file_route.startswith(base_route):
        raise ValueError('{} is not a subfolder of {}'.format(file_route,
                                                              base_route))
    base_tokens = base_route.split('/')
    route_tokens = file_route.split('/')
    if (base_tokens[-1] == ''):
        base_tokens = base_tokens[:-1]
    if (route_tokens[-1] == ''):
        route_tokens = route_tokens[:-1]
    return '/'.join(route_tokens[len(base_tokens):])
