"""
This module provides functions to produce tags from a word count
dictionary. Results must express the relative importance of tags 
"""


def order_by_count(count_dic, decreasing=True):
    """ Transforms a dicinary {word:number} into a orderd list of (number,word)
    ordered by number.
    
    Args:
        count_dic: Dictionary of the format word:number.
        decreasing: booleans, if True, ordering is in decreasing size. 
    
    returns: a list of (number, workd) ordered by the number component.
    """
    l = [(count_dic[x], x) for x in count_dic]
    l.sort(key=lambda tup: tup[0], reverse=decreasing)
    return l


def flatten_dict_count(word_count_dic, decreasing=True, include_count=False):
    """Returns an ordered list of keys from a dic: {"key":number}."""
    ordered_words = order_by_count(word_count_dic, decreasing=decreasing)
    if include_count:
        l = []
        for x in ordered_words:
            l.append(x[1])
            l.append(x[0])
        return l
    else:
        return [x[1] for x in ordered_words]


def get_tags_uniform_fixed(word_count_dic, max_number_of_tags,
                           importance_limits=(0, 10), free_mem=True):
    """ Transforms a word count dictionary into a word importance index
    dictionary. Policy:
      - Two words will not have the same importance.
      - It only consider the number_of_tags most counted words.
      - The most counted word has the maximum importance.
      - Difference between any two consecutive words is constant and equal
        to |importance_limits|/number_of_tags.
            
    Args:
      word_count_dic: dict of the format {"word":number}. Contains the words
        to be ranked and their counts.
      max_number_of_tags: maximum number of tags to be returned.
      importance_limit: tuple (min, max) indicating the maximum and minimum
        desired importance indexes for the words.
      free_mem: if True, word_count_dic input is deleted as soon is not
        needed. This might have side effects.
    
    Return: a word importance dict of the format {"word": importance}. It will
      contain no more than max_num_of_tags elements, and the importance values
      will be unique and in the importance_limits range.    
    """
    tags_dict = {}

    if max_number_of_tags == 0:
        raise ValueError("max_number_of_tags cannot be 0.")

    ordered_words = order_by_count(word_count_dic)
    if free_mem:
        del word_count_dic
    if (max_number_of_tags == 1):
        if len(ordered_words):
            return {ordered_words[0][1]: importance_limits[1]}
        return dict()

    importance_step = (float(importance_limits[1] - importance_limits[0]) /
                       float(max_number_of_tags - 1.0))

    importance = importance_limits[1]
    for (word, i) in zip(ordered_words, range(max_number_of_tags)):
        tags_dict[word[1]] = importance
        importance -= importance_step
    if free_mem:
        del ordered_words
    return tags_dict


def get_tags_progresive_fixed(word_count_dic, max_number_of_tags,
                              importance_limits=(0, 10)):
    """ Transforms a word count dictionary into a word importance index
    dictionary. Policy:
      - Two words will not have the same importance.
      - It will only consider max_number_of_tags of the dict.
      - The most important tag will always have the max importance limit.
      - The least important tag will always have the min importance limit.
      - In-between values are calculated linearly.
    
    Args:
      word_count_dic: dict of the format {"word":number}. Contains the words
        to be ranked and their counts.
      max_number_of_tags: maximum number of tags to be returned.
      importance_limit: tuple (min, max) indicating the maximum and minimum
        desired importance indexes for the words.
    
    Return: a word importance dict of the format {"word": importance}. It will
      contain no more than max_num_of_tags elements, and the importance values
      will be unique and in the importance_limits range.    
    """
    max_number_of_tags = min(max_number_of_tags, len(word_count_dic))
    return get_tags_uniform_fixed(word_count_dic, max_number_of_tags,
                                  importance_limits=importance_limits)


def get_tags_relative(word_count_dic, share_of_tags=0.5,
                      max_number_of_tags=None, importance_limits=(0, 10)):
    """ Transforms a word count dictionary into a word importance index
    dictionary. Policy:
      - Two words will not have the same importance.
      - It will only consider share_of_tags*num_tags of the dictionary.
      - It will limit the number of tags to max_number_of_tags if set
      - The most important tag will always have the max importance limit.
      - The least important tag will always have the min importance limit.
      - In-between values are calculated linearly.
    
    Args:
      word_count_dic: dict of the format {"word":number}. Contains the words
        to be ranked and their counts.
      share_of_tags: float indicating the share of the tags to be considered
        at most.
      max_number_of_tags: maximum number of tags to be returned.
      importance_limit: tuple (min, max) indicating the maximum and minimum
        desired importance indexes for the words.
    
    Return: a word importance dict of the format {"word": importance}. It will
      contain no more than max_num_of_tags elements, and the importance values
      will be unique and in the importance_limits range.    
    """
    if max_number_of_tags is None:
        max_number_of_tags = len(word_count_dic)
    max_number_of_tags = min(int(share_of_tags * float(len(word_count_dic))),
                             max_number_of_tags)

    return get_tags_progresive_fixed(word_count_dic, max_number_of_tags,
                                     importance_limits=importance_limits)


def select_tags(selection_method, word_count_dic, max_number_of_tags=10,
                importance_limits=(0, 10), share_of_tags=0.5):
    """ Transforms a word count dictionary into a word importance index 
    dictionary. Policy can be selected by the selection_method argument.
    
    Args:
      word_count_dic: dict of the format {"word":number}. Contains the words
        to be ranked and their counts.
      selection_method: string set to "uni_fixed", "progressive_fixed", 
        "relative". Selects which of the selection mehods of this module
        to use. 
      max_number_of_tags: maximum number of tags to be returned.
      importance_limit: tuple (min, max) indicating the maximum and minimum
        desired importance indexes for the words.
      share_of_tags: If the relative method is selected, float indicating the
        share of the tags to be considered at most.
    
    Return: a word importance dict of the format {"word": importance}. 
    
    """
    if selection_method == "uni_fixed":
        return get_tags_uniform_fixed(word_count_dic, max_number_of_tags,
                                      importance_limits=importance_limits)
    elif selection_method == "progressive_fixed":
        return get_tags_progresive_fixed(word_count_dic, max_number_of_tags,
                                         importance_limits=importance_limits)
    elif selection_method == "relative":
        return get_tags_relative(word_count_dic, share_of_tags=share_of_tags,
                                 max_number_of_tags=max_number_of_tags,
                                 importance_limits=importance_limits)
    else:
        raise ValueError("Selection method {} not supported.".format(
            selection_method))


def combine_tag_lists_simple(tags_list, func=max):
    """ Combines tag importance dictionaries in a single one, applying func
    when values appear in more than one dictionary
    
    Args:
        tag_list: list of  {"tag1": importance_value, ...}
        func: function that takes at least two numeric arguments and combine
          them. By default max.
    Returns: {"tag1": importance_value, ...}, which is a merge of the dicts
      in tag list. If values overlap, the final value is the func (max by
      default) of the observed ones.
    
    """
    tags_importance = {}
    for tag_dic in tags_list:
        for (key, value) in tag_dic.items():
            if key in tags_importance.keys():
                tags_importance[key] = func(tags_importance[key], value)
            else:
                tags_importance[key] = value
    return tags_importance


def combine_tag_lists_simple_efficient(tags_importance, tags_list, func=max):
    """ Combines tag importance dictionaries in a single one, applying func
    when values appear in more than one dictionary.
    
    Args:
        tags_importance: dict {"tag1": importance_value, ...}, results are
          written on it. Input dictionary is modifed. 
        tag_list: list of  {"tag1": importance_value, ...}
        func: function that takes at least two numeric arguments and combine
          them. By default max.
    Returns: {"tag1": importance_value, ...}, which is a merge of the dicts
      in tag list. If values overlap, the final value is the func (max by
      default) of the observed ones.
    
    """
    for tag_dic in tags_list:
        for (key, value) in tag_dic.items():
            if key in tags_importance.keys():
                tags_importance[key] = func(tags_importance[key], value)
            else:
                tags_importance[key] = value
    return tags_importance
