import numpy as np

from metadataserver.seminfer.textanalysis.nlp import tokenize, annotate_text


def order_by_rank(ranked_list, decreasing=True):
    ranked_list.sort(key=lambda tup: tup["rank"], reverse=decreasing)
    return ranked_list


def adjust_textrank_with_tfidf(normalized_expressions, tfidf_dic,
                               language="en",
                               filter_non_tf_words=False,
                               discard_negatives=True):
    """ Adjusts the rank of textrank expressions depedning on the tfidf indices
    of its composing words: rank is multiplied by the mean of the tfidf indices
    of the words with tfidf.
    
    Args:
        normalized_expressions: textrank list. Each expression in the list is a
          dict of the format:
          {"text": "The expresion", "rank":importance_As_float}
        language: string identifying the language of the text.
        discard_negatives: discard negative td_idf values to calculate the
          mean adjust factor.
    Returns a textrank list. Each expression in the list is a dict of the
      format {"text": "The expresion", "rank":importance_As_float}.    
    """
    new_normalized_exps = []
    for exp in normalized_expressions:
        new_exp = dict(exp)
        new_exp["rank"] = _get_new_rank(exp["text"], exp["rank"],
                                        tfidf_dic, language=language,
                                        discard_negatives=discard_negatives,
                                        filter_non_tf_words=filter_non_tf_words)
        if new_exp["rank"] is not None:
            new_normalized_exps.append(new_exp)
    return order_by_rank(new_normalized_exps)


def _get_new_rank(expression, rank, tfidf_dic, language="en",
                  discard_negatives=True, filter_non_tf_words=False):
    ann_text = annotate_text(tokenize(expression, language=language))
    tfidf_indices = []
    for sentence in ann_text:
        if filter_non_tf_words and (
                len(sentence) == 1 and not (sentence[0].root in tfidf_dic.keys())):
            return None
        for word in sentence:
            if word.root in tfidf_dic.keys():
                if discard_negatives and tfidf_dic[word.root] < 0:
                    continue
                tfidf_indices.append(tfidf_dic[word.root])
    if tfidf_indices:
        rank *= np.mean(tfidf_indices, dtype=float)
    return rank


def correct_tfidf_dic(tfidf_dic):
    """Returns a tfidf_dic with no value in (-1, 1) range"""
    new_dic = {}
    for (key, value) in tfidf_dic.items():
        if value >= 0:
            new_dic[key] = value + 1
        else:
            new_dic[key] = value - 1
    return new_dic


def is_sublist(lst1, lst2):
    """ Returns True if lst1 is in lst2 """
    ls1 = [element for element in lst1 if element in lst2]
    return ls1 == lst1


def simplify_expressions(ranked_list):
    """ Eliminates expressions from the ranked_list if: the expression can be
    composed by combining other multiword expressions in ranked_list, e.g., 
    "red car with lights" will be eliminated if "red car" and "car with lights"
    are present. Single word expressions are not considered.
        
    Args:
        ranked_list: textrank list. Each expression in the list is a dict of the
          format: {"text": "The expresion", "rank": importance_As_float}
    Returns a ranked list without the superfluous terms.
    """
    new_list = []
    for (exp, i) in zip(ranked_list, range(len(ranked_list))):
        words = exp["text"].split(" ")
        if len(words) > 1:
            new_list.append(dict(words=words,
                                 word_count=[1] * len(words),
                                 position=i))
    for exp in new_list:
        if len(exp["words"]) == 1:
            continue
        for other_exp in new_list:
            if exp != other_exp and (is_sublist(exp["words"], other_exp["words"])):
                for word in exp["words"]:
                    pos = other_exp["words"].index(word)
                    other_exp["word_count"][pos] -= 1
                    other_exp["word_count"][pos] = max(
                        other_exp["word_count"][pos], 0)
    new_list.reverse()
    for exp in new_list:
        if sum(exp["word_count"]) == 0:
            position = exp["position"]
            ranked_list = ranked_list[:position] + ranked_list[position + 1:]
    return ranked_list
