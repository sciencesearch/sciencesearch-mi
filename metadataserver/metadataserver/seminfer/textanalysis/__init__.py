from bs4 import BeautifulSoup


def extract_html_content(raw_text):
    """ Extracts flat text from HTML text. Eliminates new_lines. """
    soup = BeautifulSoup(raw_text, "html.parser")
    for tag in soup():
        for attribute in ["class", "id", "name", "style"]:
            del tag[attribute]
    for script in soup(["script", "style"]):
        script.extract()
    text_only = soup.get_text()
    text_only = text_only.replace("\n", " ")
    return text_only


def clean_string(cad):
    if cad == "(null)" or cad == "null" or cad == "Blob(0)":
        return ""
    return cad
