""" This module provide basic NLP functions to detect importance of words
in text. 

It is largely based in the NLP course: 
https://github.com/ceteri/a41124835ed0/

"""

import math
import unicodedata
from collections import namedtuple, defaultdict

import spacy

WordNode = namedtuple('WordNode', 'raw, root, pos')


def cleanup_text(text):
    """
    clean up (normalize) the unicode in a raw text
    """
    x = " ".join(map(lambda x: x.strip(), text.split("\n"))).strip()

    x = x.replace('“', '"').replace('”', '"')
    x = x.replace("‘", "'").replace("’", "'").replace("`", "'")
    x = x.replace("`` ", '"').replace("''", '"')
    x = x.replace('…', '...').replace('–', '-')
    x = x.replace("\\u00a0", " ").replace("\\u2014", " - "
                                          ).replace("\\u2022", "*")
    x = x.replace("\\u2019", "'").replace("\\u201c", '"'
                                          ).replace("\\u201d", '"')

    x = unicodedata.normalize('NFKD', x).encode('ascii', 'ignore').decode('utf-8')

    return x


def load_stopwords(stop_file):
    """
    load the stopwords list. Stopwords are discarded in text analysis.
    """
    stopwords = set([])

    with open(stop_file) as f:
        for line in f.readlines():
            stopwords.add(line.strip().lower())

    return stopwords


def annotate(doc, span, debug=False):
    """
    Annotote each word in a sentence of a doc
    
    Args:
        doc: doc object produced by Tokenize
        span: subsection of the doc to be annotated.
        debug: if true prints each annotated word.
    
    returns: a list of WordNode objects, one per annotated word. Such
      object indicates the word, its semantic root, and position (function
      in the sentence. For available position values read about tag_ in 
      https://spacy.io/docs/api/token
        
    """
    for i in range(span.start, span.end):
        token = doc[i]

        if debug:
            print(token.text, token.tag_, token.pos_, token.lemma_)

        if token.pos_[0] in ["N", "V"]:
            root = token.lemma_
        else:
            root = token.text.lower()

        if token.pos_ in ["HYPH", "PUNCT"]:
            pos = "."
        else:
            pos = token.tag_

        yield WordNode(raw=token.text, root=root, pos=pos)


NLP = None
SPACY_NLP = None


def keep_annonotated_words_type(annottated_words, word_types=["V", "N"]):
    """ Removes words from a lists of setences if they are not of the
    desired types.
    
    Args:
        annottated_words: list of lists of WordNode.
        word_types: list of first character of the WordNode POS types that will
          be kept.
    
    returns: list of list of words which POS starts with the characters
      specified  in word_types.
    
    """
    return [[word for word in sentence if word.pos[0] in word_types]
            for sentence in annottated_words]


def tokenize(text, language="en"):
    """ returns an nlpobject that contains the word tokens in the text.
    For details: read https://spacy.io/docs/api/token"""
    global NLP, SPACY_NLP
    text = cleanup_text(text)
    if NLP is None:
        if SPACY_NLP is not None:
            NLP = SPACY_NLP
        else:
            NLP = spacy.load(language)
            SPACY_NLP = NLP
    doc = NLP(text, parse=True)
    return doc


def annotate_text(doc):
    """ Returns a list of annotated sentences from the tokens contained in
    doc."""
    anottated_list = []
    for span in doc.sents:
        anottated_list.append([lex for lex in annotate(doc, span)])
    return anottated_list


def count_words(annotated_text, stopwords=[], df=defaultdict(int)):
    """ Count words in annotated text which root is not in stopwords.
    
    Args:
        annotated_text: list of annotated sentences.
        stopwrods: list of words to be discarded in the count.
        df: if set to word count dictionary, the counts on annotated_text are
          aggregated to the ones contained in it.
    
    returns: dictionary (word_root: ocurrences) of word ocurrences in text.
    """
    tf = defaultdict(int)
    for sentence in annotated_text:
        for lex in sentence:
            if (lex.pos != ".") and (lex.root not in stopwords):
                tf[lex.root] += 1
        if not df is None:
            for word in tf.keys():
                df[word] += 1
    return tf


def add_to_dic(source, dest={}):
    for key in source.keys():
        if key in dest:
            dest[key] += source[key]
        else:
            dest[key] = source[key]
    return dest


def calc_tfidf(text_id, work_count_single, num_texts, word_count_general,
               docs_per_word, as_dict=True):
    """ Calculates term frequency–inverse document frequency of a particular
        wordcount dictionary. More info at
        https://en.wikipedia.org/wiki/Tf%E2%80%93idf
        
        Args:
            text_id: term to identify the dictionary in work_count_single
            work_count_single: wordcount dictionary of the shape
              term:occurrences.
            num_texts: number of total wordcount that compose
              word_count_general.
            word_count_general: word count dictionary that aggregates multiple
              word count dictionaries of inter-relevant texts.
            as_dict: booleand that controls the returned format.
        
        returns: if as_dict is True, it returns dictionaty of word:tfid.
          if False, it returns a list of (text_id, word, tfidf) for each
          detected word.
    """
    keywords = []
    if as_dict:
        keywords = {}
    for word, count in work_count_single.items():
        tfidf = (float(count) * math.log((num_texts + 1.0)
                                         / (docs_per_word[word] + 1.0)))
        if as_dict:
            keywords[word] = tfidf
        else:
            keywords.append((text_id, tfidf, word,))
    return keywords
