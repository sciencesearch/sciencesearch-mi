"""
Textrank analysis tools. Based on
https://github.com/ceteri/pytextrank/blob/master/example.ipynb

Textrank analysis allows to detect expressions in a text and their relevance.

More details:
https://web.eecs.umich.edu/~mihalcea/papers/mihalcea.emnlp04.pdf
https://en.wikipedia.org/wiki/Automatic_summarization#Unsupervised_approach:_TextRank
"""

import networkx as nx
import pytextrank
from pytextrank.pytextrank import build_graph


def process_text(text_blob, phrase_limit=12, word_limit=150, debug=False):
    """ Produces a Textrank analysis on the string in text_blob. The analysis
    results include a list expressions and their relevance rank, a list
    of the phrase_limit most relevant expressions, and a summary of word_limit
    words.
    
    Args:
        text_blob: Flat text as a string.
        phrase_limit: number of relevant expression to return.
        word_limit: maximum number of words in the summary of the text.
    
    Returns: 
        keywords: a list or the phrase_limit more relevant expressions present
          in textblob, ordered from most to least importnat.
        sentences: List of sentences that summarize the content of text_blb.
        normalized_key_phrases_as_dict: list of expressions present in the
          text and a rank of their importance. Each expression is a dict of the
          format: {"text": "The expresion", "rank": importance_As_float}
    
    """

    text_dict_list = [dict(id=1, text=text_blob)]

    graphs_s1 = [g._asdict() for g in pytextrank.parse_doc(text_dict_list)]
    if debug:
        print("Word Graphs ***********")
        for graf in graphs_s1:
            print(pytextrank.pretty_print(graf))

    ranks = nx.pagerank(build_graph(graphs_s1))
    # pytextrank.render_ranks(graph_s2, ranks)
    normalized_key_phrases = [phrase for phrase
                              in pytextrank.normalize_key_phrases(graphs_s1,
                                                                  ranks)]
    normalized_key_phrases_as_dict = [phrase._asdict() for phrase in
                                      normalized_key_phrases]
    if debug:
        print("NORMALIZED KEY PHRASES ***********")
        for rl in normalized_key_phrases_as_dict:
            print(pytextrank.pretty_print(rl))

    kernel = pytextrank.rank_kernel(normalized_key_phrases_as_dict)
    top_sentences = [ts._asdict() for ts in
                     pytextrank.top_sentences(kernel, graphs_s1)]
    if debug:
        print("TOP SENTENCES ***********")
        for s in top_sentences:
            print(pytextrank.pretty_print(s))

    keywords = [k for k in pytextrank.limit_keyphrases(normalized_key_phrases,
                                                       phrase_limit=phrase_limit)]

    sentences = [sentence for sentence in
                 pytextrank.limit_sentences(top_sentences,
                                            word_limit=word_limit)]
    if debug:
        phrases = ", ".join(set([p for p in keywords]))
        sent_iter = sorted(sentences, key=lambda x: x[1])
        s = []

        for sent_text, idx in sent_iter:
            s.append(pytextrank.make_sentence(sent_text))
        graf_text = " ".join(s)
        print("**excerpts:** %s\n\n**keywords:** %s" % (graf_text, phrases,))

    return keywords, sentences, normalized_key_phrases_as_dict
