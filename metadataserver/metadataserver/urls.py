"""metadataserver URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from .apps.searchengine.datamodel.transform.indexing import do_once_checks

from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.authtoken import views


# from rest_framework import routers
ncemreldata_urls = include('metadataserver.apps.ncemreldata.urls')
root_url=settings.DJANGO_BASE_URI
baser_url_format = (r'api/')
ncem_urls=include('metadataserver.apps.ncem.urls')
papermanager_urls=include('metadataserver.apps.papermanager.urls')
search_urls=include('metadataserver.apps.searchengine.urls')

urlpatterns = [
    url(root_url+r'admin/', admin.site.urls),
    url(root_url+baser_url_format+r'image-tag/', ncem_urls),
    url(root_url+r'api-token-auth/', views.obtain_auth_token),
    url(root_url+r'api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
    url(root_url+baser_url_format+r'rel-data/', ncemreldata_urls),
    url(root_url+baser_url_format+r'search/', search_urls),
    url(root_url+baser_url_format+r'papermanager/', papermanager_urls),
]

do_once_checks()