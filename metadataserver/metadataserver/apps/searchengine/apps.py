from django.apps import AppConfig


class SearchengineConfig(AppConfig):
    name = 'metadataserver.apps.searchengine'
    label = 'searchengine'
