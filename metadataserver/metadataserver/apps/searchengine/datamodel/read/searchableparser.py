import logging
from django.db import transaction

from metadataserver.apps.ncem.datamodel.read.tagparser import FlatNCEMDataExtractor

logger = logging.getLogger(__name__)

class SearchableParser(FlatNCEMDataExtractor):
    REMOVE_DUPLICATES = False

    def __init__(self, no_score=False, default_score=1.0):
        self._no_score = no_score
        self._default_score = default_score
        super(SearchableParser, self).__init__()

    def _create_dict_from_row(self, row, file_route, the_tags):
        if self._no_score:
            return self._create_dict_from_row_no_scores(row, file_route,
                                                        the_tags,
                                                        self._default_score)
        else:
            return self._create_dict_from_row_with_scores(row, file_route,
                                                          the_tags)

    def _create_dict_from_row_with_scores(self, row, file_route, the_tags):
        if len(the_tags) % 2 != 0:
            raise ValueError("The list of tags an scores has to contain an even"
                             " number of items: {}".format(row))
        the_tags_by_pairs = list(zip(the_tags[0::2],
                                     [float(x) for x in the_tags[1::2]]))
        return super(SearchableParser, self)._create_dict_from_row(row,
                                                                   file_route,
                                                                   the_tags_by_pairs)

    def _create_dict_from_row_no_scores(self, row, file_route, the_tags,
                                        default_score):
        the_tags_by_pairs = list(zip(the_tags,
                                     [default_score] * len(the_tags)))
        return super(SearchableParser, self)._create_dict_from_row(row,
                                                                   file_route,
                                                                   the_tags_by_pairs)


def extract_and_save_searchable_tags(file_route, class_type, metadata_type,
                                     base_route=None, list_only=True,
                                     no_score=False, default_score=1.0):
    """ Extracts searchable tags data present in the file_route CSV file and 
    stores them in the database. Tags for non existent objects are not added.
        Args:
            - file_route: string pointing to the file system location where
            the CSV file is.
            - class_type: Class of the object that the tags point to. It must
              be a subclass of SearchableObject
            - metadata_type: type searcahble object. Must be METADATA_TYPES 
            - base_route: string that should substracted from object ids
              in the CSV file to identiy an objet. e.g. base_route="/foo/",
              id "/foo/foo2/File1" will be parsed as "foo2/File1".
            - list_only: if True, extract_tags only list the files and tags
              detected and does not store the data.
            - no_score: if True, the input file will be expected to not have
              scores and default_score will be used for them,
            - default_score: float to be used as tags confidence score if the 
              input file do not include such scores.
        """
    extractor = SearchableParser(no_score=no_score,
                                 default_score=default_score)

    extractor.extract_tags(file_route, base_route=base_route
                           )
    data_dict = extractor.get_data_dic()
    if list_only:
        logger.info("Detected files")
        for file_name in extractor.get_file_list():
            logger.info(file_name)
        logger.info("")
        logger.info("Files and tags")
        for item in data_dict.keys():
            logger.info("{}: {}".format(item, data_dict[item]))
    else:
        with transaction.atomic(None, False):
            logger.info("Storing data")
            for related_id, list_of_tags in data_dict.items():
                logger.info("Detected tags for id {}".format(related_id))
                related_object = class_type.get_related_model_object_by_id(
                    related_id)
                if not related_object:
                    logger.info("Object not present in DB, skipping.")
                    continue

                for tag_item in list_of_tags:
                    class_type.create_entry(text_content=tag_item[0],
                                            score=tag_item[1],
                                            related_object=related_object,
                                            metadata_type=metadata_type)
