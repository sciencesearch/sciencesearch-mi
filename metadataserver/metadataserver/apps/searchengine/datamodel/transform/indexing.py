import logging

from metadataserver.apps.searchengine.configuration import GenericConfigurer
from metadataserver.apps.searchengine.models import SearchableObject, DEFAULT_FILTER

logger = logging.getLogger(__name__)


class SearchIndexer:
    """
    This class populates metadata for searchable objects. Filtering queries
    need one metadata entty to be made per each searchable object.
    """

    def gen_default_tags(self, metadata_cls, pointed_cls):
        """Creates metadata filter entries in the metadata_cls model for
        pointed_cls objects.
        
        Args:
            metadata_cls: a Searchable object subclass
            pointed_cls: class if the object pointed by the metadata of the
              type metadata_cls.    
        """
        for obj in pointed_cls.objects.all():
            md, created = metadata_cls.objects.get_or_create(
                text_content="DEFAULT", score=1.0,
                metadata_type=DEFAULT_FILTER,
                related_object=obj)
            if created:
                md.save()

    def gen_default_tags_all_classes(self):
        """Creates metadata filter entries for all th subclasses of
        SearchableObject."""

        for cls in SearchableObject.__subclasses__():
            if cls.get_related_model():
                self.gen_default_tags(cls, cls.get_related_model())

    def check_need_create_tags_all_classes(self):
        """Checks if metadata needs to be created for any metadatatype"""
        for cls in SearchableObject.__subclasses__():
            if (cls.get_related_model() is not None and
                    self.check_need_create_tags(cls, cls.get_related_model())):
                return True
        return False

    def check_need_create_tags(self, metadata_cls, pointed_cls):
        return (pointed_cls.objects.all().count() !=
                metadata_cls.objects.filter(metadata_type=DEFAULT_FILTER,
                                            text_content="DEFAULT").count()
                )

    def do_once_checks(self):
        """Checks if metadata is needed to be created and creates it if
        necessary."""
        if self.check_need_create_tags_all_classes():
            logger.info("Metadata for filtering incomplete: CREATING!"
                  " (This may take a few seconds)")
            self.gen_default_tags_all_classes()
            logger.info("Metadata created successfully.")
        else:
            logger.info("Metadata for filtering complete: No further action")


class IndexConfig(GenericConfigurer):

    def __init__(self, prefix="SEARCH_INDEX", custom_settings=None):
        super(IndexConfig, self).__init__(prefix=prefix,
                                          custom_settings=custom_settings)

    def get_default_settings(self):
        return dict(do_check=True)


def do_once_checks():
    config = IndexConfig.get_config()
    if config.get_settings_value("do_check"):
        si = SearchIndexer()
        si.do_once_checks()
