import datetime
import json

from django.test import TestCase
from django.utils import timezone

from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.searchengine.algebra.search import HitListScorer
from metadataserver.apps.searchengine.cache import SearchCacheManager, SearchCacheConfig
from metadataserver.apps.searchengine.models import (
    ProposalMetadata, PROPOSAL_FROM_TEXT, SearchResult, SearchResultSummary,
    SearchResultArchive, SearchResultSummaryArchive
    )
from metadataserver.apps.searchengine.serializers import (
    ProposalMetadataSerializer, SearchResultSerializer, SearchResultSummarySerializer,
    SearchCacheManagerSerializer, SearchResultArchiveSerializer,
    SearchResultSummaryArchiveSerializer
    )
from rest_framework.renderers import JSONRenderer


class TestProposalMetadataSerializer(TestCase):

    def test_serialization(self):
        p1 = Proposal(serial_number="1234")
        p1.save()

        pm = ProposalMetadata(text_content="text1", score=25.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p1)

        pm.save()

        serializer = ProposalMetadataSerializer(pm)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)

        candidate_data = json.loads(
            """{"text_content":"text1",
            "score":25.0,
            "metadata_type":""" + "\"" + PROPOSAL_FROM_TEXT + "\"" + """,
            "related_object":"1234"
            }
            """)

        self.assertEqual(candidate_data, produced_data)


class TestSearchResultSerializer(TestCase):

    def test_serialization_proposal_result(self):
        sr = SearchResult(score=0.5, result_position=5)
        p1 = Proposal(serial_number="1234")

        sr.set_result_obj(p1)
        sr.set_hit_list([(0.1, ProposalMetadata(score=2.2,
                                                metadata_type="pt",
                                                text_content="etch")),
                         (0.2, ProposalMetadata(score=2.1, metadata_type="pt",
                                                text_content="material")),
                         (0.3, ProposalMetadata(score=1.2, metadata_type="si",
                                                text_content="other"))])

        serializer = SearchResultSerializer(sr)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)
        self.maxDiff = None

        candidate_data = json.loads(
            """{"archive_id":"N/A",
                "result_object":{"serial_number":"1234","status":"ACTIVE",
                "title":"","interaction_type":-1,"project_leader":"N",
                "primary_researcher":"N","co_researcher":"N","first_name":"",
                "last_name":"","email":"","bin":"",
                "date_project_started":"1970-01-01T00:00:00Z",
                "date_project_end":"1970-01-01T00:00:00Z",
                "assigned_scientist":"","pi_name":"",
                "primary_researcher_name":"","contact_with_staff_name":"",
                "significance":"","foundry_capabilities":"",
                "work_description":"","project_duration":"",
                "relevant_experience":"","project_summary":"",
                "follow_on_progress_effort":""},
            "score":0.5,
            "result_position":5,
            "hit_list":[[0.1, 2.2,"pt","etch"],[0.2, 2.1,"pt","material"],
            [0.3, 1.2,"si","other"]]
            }
            """)

        self.assertEqual(candidate_data, produced_data)

    def test_serialization_proposal_result_with_archive_id(self):
        sr = SearchResult(score=0.5, result_position=5)
        p1 = Proposal(serial_number="1234")

        sr.set_result_obj(p1)
        sr.set_hit_list([(0.1, ProposalMetadata(score=2.2,
                                                metadata_type="pt",
                                                text_content="etch")),
                         (0.2, ProposalMetadata(score=2.1, metadata_type="pt",
                                                text_content="material")),
                         (0.3, ProposalMetadata(score=1.2, metadata_type="si",
                                                text_content="other"))])
        sr.set_archive_id("id1")
        serializer = SearchResultSerializer(sr)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)
        self.maxDiff = None

        self.assertEqual(produced_data["archive_id"], "id1")


class TestSearchResultSummarySerializer(TestCase):

    def test_serialization(self):
        srs = SearchResultSummary(date=datetime.datetime(year=2010, month=1,
                                                         day=1, tzinfo=datetime.timezone.utc),
                                  page_start=2, page_end=4,
                                  query="my query",
                                  search_latency_s=1.34,
                                  total_results_number=5,
                                  query_type="query_t")

        sr = SearchResult(score=0.5, result_position=5)
        p1 = Proposal(serial_number="1234")
        sr.set_result_obj(p1)
        sr.set_hit_list([(0.1, ProposalMetadata(score=2.2,
                                                metadata_type="pt",
                                                text_content="etch")),
                         (0.2, ProposalMetadata(score=2.1, metadata_type="pt",
                                                text_content="material")),
                         (0.3, ProposalMetadata(score=1.2, metadata_type="si",
                                                text_content="other"))])
        srs.set_results([sr])

        hls = HitListScorer()
        hls.register_mark_properties("si", 1.0, -2.0, 4.0)
        hls.register_mark_properties("pt", 2.0, -3.0, 5.0)
        srs.set_hit_list_scorer(hls)

        serializer = SearchResultSummarySerializer(srs)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)
        self.maxDiff = None

        candidate_data = json.loads(
            """{"query":"my query","date":"2010-01-01T00:00:00Z",
            "archive_id":"N/A",
            "query_type":"query_t",
            "search_latency_s":1.34,
            "page_start":2,"page_end":4,
            "cached": false,
            "search_timed_out": false,
            "search_weights":
                {"si":{"weight":1.0,"min_input":-2.0,"max_input":4.0},
                "pt":{"weight":2.0,"min_input":-3.0,"max_input":5.0}},
            "total_results_number":5,
            "results_list":[{
            "archive_id":"N/A",
            "result_object":{"serial_number":"1234","status":"ACTIVE",
                "title":"","interaction_type":-1,"project_leader":"N",
                "primary_researcher":"N","co_researcher":"N","first_name":"",
                "last_name":"","email":"","bin":"",
                "date_project_started":"1970-01-01T00:00:00Z",
                "date_project_end":"1970-01-01T00:00:00Z",
                "assigned_scientist":"","pi_name":"",
                "primary_researcher_name":"","contact_with_staff_name":"",
                "significance":"","foundry_capabilities":"",
                "work_description":"","project_duration":"",
                "relevant_experience":"","project_summary":"",
                "follow_on_progress_effort":""},
            "score":0.5,
            "result_position":5,
            "hit_list":[[0.1, 2.2,"pt","etch"],[0.2, 2.1,"pt","material"],
            [0.3, 1.2,"si","other"]]
            }]}
            """)

        self.assertEqual(candidate_data, produced_data)

    def test_serialization_with_archive_id(self):
        srs = SearchResultSummary(date=datetime.datetime(year=2010, month=1,
                                                         day=1, tzinfo=datetime.timezone.utc),
                                  page_start=2, page_end=4,
                                  query="my query",
                                  search_latency_s=1.34,
                                  total_results_number=5,
                                  query_type="query_t")

        sr = SearchResult(score=0.5, result_position=5)
        srs.set_archive_id("id1")
        p1 = Proposal(serial_number="1234")
        sr.set_result_obj(p1)
        sr.set_hit_list([(0.1, ProposalMetadata(score=2.2,
                                                metadata_type="pt",
                                                text_content="etch")),
                         (0.2, ProposalMetadata(score=2.1, metadata_type="pt",
                                                text_content="material")),
                         (0.3, ProposalMetadata(score=1.2, metadata_type="si",
                                                text_content="other"))])
        srs.set_results([sr])

        hls = HitListScorer()
        hls.register_mark_properties("si", 1.0, -2.0, 4.0)
        hls.register_mark_properties("pt", 2.0, -3.0, 5.0)
        srs.set_hit_list_scorer(hls)

        serializer = SearchResultSummarySerializer(srs)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)
        self.maxDiff = None
        self.assertEqual(produced_data["archive_id"],
                         "id1")


class TestSearchCacheManagerSerializer(TestCase):
    def tearDown(self):
        SearchCacheManager.clear_cache()
        SearchCacheConfig.get_config(force_read=True)

    def setUp(self):
        SearchCacheManager.clear_cache()
        SearchCacheConfig.get_config(force_read=True)

    def test_decode(self):
        self.maxDiff = None
        srs_1 = SearchResultSummary(date=timezone.now(),
                                    query="1234",
                                    query_type="type1",
                                    search_latency_s=1.0,
                                    total_results_number=1)
        SearchCacheManager.save_result_cache(srs_1)
        srs_2 = SearchResultSummary(date=timezone.now(),
                                    query="1234",
                                    query_type="type2",
                                    search_latency_s=1.0,
                                    total_results_number=1)
        SearchCacheManager.save_result_cache(srs_2)
        serializer = SearchCacheManagerSerializer(SearchCacheManager())
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)

        candidate_data = json.loads(
            """
            {
                "cache_config": {
                    "ASYNC_STORE": false,
                    "NUM_WORKERS": 4,
                    "ENABLED": true,
                    "CACHE_SIZE": 100,
                    "MAX_AGE_S": 86400
                },
                "cache_size": 2,
                "cached_queries": [
                    {
                        "query": "1234",
                        "query_type": "type1",
                        "date": "2018-01-30T01:42:35.696878Z",
                        "id": 1
                    },
                    {
                        "query": "1234",
                        "query_type": "type2",
                        "date": "2018-01-31T23:52:26.436471Z",
                        "id": 2
                    }
                ]
            }
            """)
        d1 = srs_1.date
        d1 = d1.replace(tzinfo=None)
        d2 = srs_2.date
        d2 = d2.replace(tzinfo=None)
        candidate_data["cached_queries"][0]["date"] = d1.isoformat() + "Z"

        candidate_data["cached_queries"][1]["date"] = d2.isoformat() + "Z"
        candidate_data["cached_queries"][0]["id"] = srs_1.id
        candidate_data["cached_queries"][1]["id"] = srs_2.id
        self.assertEqual(candidate_data, produced_data)


class TestSearchResultArchiveSerializer(TestCase):

    def test_serialization_proposal_result(self):
        sr = SearchResultArchive(score=0.5, result_position=5,
                                 user_clicked=3)
        p1 = Proposal(serial_number="1234")
        p1.save()
        sr.set_result_obj(p1)
        sr.set_hit_list([(0.1, ProposalMetadata(score=2.2,
                                                metadata_type="pt",
                                                text_content="etch")),
                         (0.2, ProposalMetadata(score=2.1, metadata_type="pt",
                                                text_content="material")),
                         (0.3, ProposalMetadata(score=1.2, metadata_type="si",
                                                text_content="other"))])

        serializer = SearchResultArchiveSerializer(sr)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)
        self.maxDiff = None

        candidate_data = json.loads(
            """{"result_object":["proposal", "1234"],
            "score":0.5,
            "id": null,
            "result_position":5,
            "hit_list":[[0.1, 2.2,"pt","etch"],[0.2, 2.1,"pt","material"],
            [0.3, 1.2,"si","other"]],
            "user_clicked":3
            }
            """)

        self.assertEqual(candidate_data, produced_data)


class TestSearchResultSummaryArchiveSerializer(TestCase):
    def test_serialization(self):
        srs = SearchResultSummaryArchive(date=datetime.datetime(year=2010, month=1,
                                                                day=1, tzinfo=datetime.timezone.utc),
                                         page_start=2, page_end=4,
                                         query="my query",
                                         search_latency_s=1.34,
                                         total_results_number=5,
                                         query_type="query_t",
                                         session_id="my Session")
        srs.save()

        sr = SearchResultArchive(score=0.5, result_position=5,
                                 user_clicked=3,
                                 search_results_summary_archive=srs)
        p1 = Proposal(serial_number="1234")
        p1.save()
        sr.set_result_obj(p1)
        sr.set_hit_list([(0.1, ProposalMetadata(score=2.2,
                                                metadata_type="pt",
                                                text_content="etch")),
                         (0.2, ProposalMetadata(score=2.1, metadata_type="pt",
                                                text_content="material")),
                         (0.3, ProposalMetadata(score=1.2, metadata_type="si",
                                                text_content="other"))])
        sr.save()

        hls = HitListScorer()
        hls.register_mark_properties("si", 1.0, -2.0, 4.0)
        hls.register_mark_properties("pt", 2.0, -3.0, 5.0)
        srs.set_hit_list_scorer(hls)
        srs.search_date = datetime.datetime(year=2011, month=1,
                                            day=1)
        srs.save()

        serializer = SearchResultSummaryArchiveSerializer(srs)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)
        self.maxDiff = None

        candidate_data = json.loads(
            """{"query":"my query","date":"2010-01-01T00:00:00Z",
            "search_date":"2011-01-01T00:00:00Z",
            "query_type":"query_t",
            "search_latency_s":1.34,
            "page_start":2,"page_end":4,
            "cached": false,
            "session_id": "my Session",
            "search_timed_out": false,
            "search_weights":
                {"si":{"weight":1.0,"min_input":-2.0,"max_input":4.0},
                "pt":{"weight":2.0,"min_input":-3.0,"max_input":5.0}},
            "total_results_number":5,
            "results_list":[{
            "id":""" + str(sr.id) + """,
            "user_clicked": 3,
            "result_object":["proposal","1234"],
            "score":0.5,
            "result_position":5,
            "hit_list":[[0.1, 2.2,"pt","etch"],[0.2, 2.1,"pt","material"],
            [0.3, 1.2,"si","other"]]
            }]}
            """)

        self.assertEqual(candidate_data, produced_data)
