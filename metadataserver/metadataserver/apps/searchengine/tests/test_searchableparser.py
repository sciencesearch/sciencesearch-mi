import os

from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.papermanager.models import Paper
from metadataserver.apps.searchengine.datamodel.read.searchableparser import (
    SearchableParser, extract_and_save_searchable_tags
    )
from metadataserver.apps.searchengine.models import (
    ProposalMetadata, PROPOSAL_FROM_TEXT, SCOPEIMAGE_FROM_FS,
    PaperMetadata, PAPER_FROM_TEXT
    )
from metadataserver.seminfer.tests import TestFileGeneric


class TestSearchableParser(TestFileGeneric):
    _tmp_folder = "/tmp/test_NCEM"

    def setUp(self):
        self._register_tmp_folder(self._tmp_folder)
        self._create_empty(self._tmp_folder, is_folder=True)

    def _create_test_file(self, file_name, content):
        file_route = os.path.join(self._tmp_folder, file_name)
        self._register_tmp_file(file_route)

        cvs_file = open(file_route, 'w')
        cvs_file.write(content)
        cvs_file.close()

        return file_route

    def test_extract_tags_simple(self):
        text = """


'TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser',tag1,2.0,tag2,3.0,tag3,4.0,

'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',tag1a,10.0,tag2a,20.0,tag3a,30.0,

        """
        file_route = self._create_test_file("myFile.csv", text)

        extractor = SearchableParser()

        extractor.extract_tags(file_route)

        data_dict = extractor.get_data_dic()

        self.assertEqual(
            set(data_dict["TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser"]),
            {("tag1", 2.0), ("tag2", 3.0), ("tag3", 4.0)})

        self.assertEqual(
            set(data_dict["TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser"]),
            {("tag1a", 10.0), ("tag2a", 20.0), ("tag3a", 30.0)})

    def test_extract_and_save_searchable_tags(self):
        p1 = Proposal(serial_number="1234")
        p1.save()
        p2 = Proposal(serial_number="4567")
        p2.save()
        text = """

'1234',tag1,2.0,tag2,3.0,tag3,4.0,

'4567',tag1a,10.0,tag2a,20.0,tag3a,30.0,
        """
        file_route = self._create_test_file("myFile.csv", text)
        extract_and_save_searchable_tags(file_route, ProposalMetadata,
                                         PROPOSAL_FROM_TEXT,
                                         list_only=False)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag1"
                                                         ).filter(metadata_type=PROPOSAL_FROM_TEXT
                                                                  ).filter(related_object=p1)[0].score,
                         2.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag2"
                                                         ).filter(metadata_type=PROPOSAL_FROM_TEXT
                                                                  ).filter(related_object=p1)[0].score,
                         3.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag3"
                                                         ).filter(metadata_type=PROPOSAL_FROM_TEXT
                                                                  ).filter(related_object=p1)[0].score,
                         4.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag1a"
                                                         ).filter(metadata_type=PROPOSAL_FROM_TEXT
                                                                  ).filter(related_object=p2)[0].score,
                         10.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag2a"
                                                         ).filter(metadata_type=PROPOSAL_FROM_TEXT
                                                                  ).filter(related_object=p2)[0].score,
                         20.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag3a"
                                                         ).filter(metadata_type=PROPOSAL_FROM_TEXT
                                                                  ).filter(related_object=p2)[0].score,
                         30.0)

    def test_extract_and_save_searchable_tags_missing_object(self):
        p1 = Proposal(serial_number="1234")
        p1.save()
        text = """

'1234',tag1,2.0,tag2,3.0,tag3,4.0,

'4567',tag1a,10.0,tag2a,20.0,tag3a,30.0,
        """
        file_route = self._create_test_file("myFile.csv", text)
        extract_and_save_searchable_tags(file_route, ProposalMetadata,
                                         PROPOSAL_FROM_TEXT,
                                         list_only=False)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag1"
                                                         ).filter(metadata_type=PROPOSAL_FROM_TEXT
                                                                  ).filter(related_object=p1)[0].score,
                         2.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag2"
                                                         ).filter(metadata_type=PROPOSAL_FROM_TEXT
                                                                  ).filter(related_object=p1)[0].score,
                         3.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag3"
                                                         ).filter(metadata_type=PROPOSAL_FROM_TEXT
                                                                  ).filter(related_object=p1)[0].score,
                         4.0)

    def test_extract_and_save_searchable_tags_missing_object_papers(self):
        p1 = Paper(simple_title="1234")
        p1.save()
        text = """

'1234',tag1,2.0,tag2,3.0,tag3,4.0,

'4567',tag1a,10.0,tag2a,20.0,tag3a,30.0,
        """
        file_route = self._create_test_file("myFile.csv", text)
        extract_and_save_searchable_tags(file_route, PaperMetadata,
                                         PAPER_FROM_TEXT,
                                         list_only=False)
        self.assertEqual(PaperMetadata.objects.filter(text_content="tag1"
                                                      ).filter(metadata_type=PAPER_FROM_TEXT
                                                               ).filter(related_object=p1)[0].score,
                         2.0)
        self.assertEqual(PaperMetadata.objects.filter(text_content="tag2"
                                                      ).filter(metadata_type=PAPER_FROM_TEXT
                                                               ).filter(related_object=p1)[0].score,
                         3.0)
        self.assertEqual(PaperMetadata.objects.filter(text_content="tag3"
                                                      ).filter(metadata_type=PAPER_FROM_TEXT
                                                               ).filter(related_object=p1)[0].score,
                         4.0)

    def test_extract_and_save_searchable_tags_no_score(self):
        p1 = Proposal(serial_number="1234")
        p1.save()
        p2 = Proposal(serial_number="4567")
        p2.save()
        text = """

'1234',tag1,tag2,tag3,

'4567',tag1a,tag2a,tag3a,
        """
        file_route = self._create_test_file("myFile.csv", text)
        extract_and_save_searchable_tags(file_route, ProposalMetadata,
                                         SCOPEIMAGE_FROM_FS,
                                         list_only=False,
                                         no_score=True,
                                         default_score=2.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag1"
                                                         ).filter(metadata_type=SCOPEIMAGE_FROM_FS
                                                                  ).filter(related_object=p1)[0].score,
                         2.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag2"
                                                         ).filter(metadata_type=SCOPEIMAGE_FROM_FS
                                                                  ).filter(related_object=p1)[0].score,
                         2.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag3"
                                                         ).filter(metadata_type=SCOPEIMAGE_FROM_FS
                                                                  ).filter(related_object=p1)[0].score,
                         2.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag1a"
                                                         ).filter(metadata_type=SCOPEIMAGE_FROM_FS
                                                                  ).filter(related_object=p2)[0].score,
                         2.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag2a"
                                                         ).filter(metadata_type=SCOPEIMAGE_FROM_FS
                                                                  ).filter(related_object=p2)[0].score,
                         2.0)
        self.assertEqual(ProposalMetadata.objects.filter(text_content="tag3a"
                                                         ).filter(metadata_type=SCOPEIMAGE_FROM_FS
                                                                  ).filter(related_object=p2)[0].score,
                         2.0)
