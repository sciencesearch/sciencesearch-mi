import os
import unittest.mock as mock

from django.test import TestCase

from metadataserver.apps.searchengine.configuration import GenericConfigurer


class FakeSettingsObject(object):

    def __init__(self, data_dict):
        self._data_dict = data_dict

    def get(self, key, default=None):
        if key in self._data_dict:
            return self._data_dict[key]
        return default


class GC1(GenericConfigurer):
    def __init__(self, prefix="GC1", custom_settings=None):
        super(GC1, self).__init__(prefix=prefix,
                                  custom_settings=custom_settings)
        self.create_secondary_objs()

    def get_default_settings(self):
        return dict(key1="value1", key2=2, key3=False,
                    key4=dict(s1="s1"))

    def create_secondary_objs(self):
        self.my_obj = {"a": "b"}


class GC2(GenericConfigurer):
    def __init__(self, prefix="GC2", custom_settings=None):
        super(GC2, self).__init__(prefix=prefix,
                                  custom_settings=custom_settings)

    def get_default_settings(self):
        return dict(key1=False, key2=2)


class TestGenericConfiguration(TestCase):

    def test_init(self):
        gc = GC1()

        self.assertEqual(gc.get_settings(),
                         dict(KEY1="value1", KEY2=2, KEY3=False,
                              KEY4=dict(S1="s1")))
        self.assertEqual(gc.my_obj, {"a": "b"})

        fs = FakeSettingsObject({"GC1_KEY1": "value2"})
        gc = GC1(custom_settings=fs)
        self.assertEqual(gc.get_settings(),
                         dict(KEY1="value2", KEY2=2, KEY3=False,
                              KEY4=dict(S1="s1")))

        fs = FakeSettingsObject({
            "GC1_key1": "value2",
            "GC1_KEY4_S1": "s2"
            })
        gc = GC1(custom_settings=fs)
        self.assertEqual(gc.get_settings(),
                         dict(KEY1="value1", KEY2=2, KEY3=False,
                              KEY4=dict(S1="s2")))

    def test_modified_settings(self):
        with self.settings(GC1_KEY1="value2",
                           GC1_KEY4_S1="s2"):
            gc = GC1()
            self.assertEqual(gc.get_settings(),
                             dict(KEY1="value2", KEY2=2, KEY3=False,
                                  KEY4=dict(S1="s2")))

    def test_modified_settings_environ(self):
        with self.settings(GC1_KEY1="value2",
                           GC1_KEY4_S1="s2"):
            with mock.patch.dict(os.environ, {
                "GC1_KEY1": "value3",
                "GC1_KEY2": "3"
                }):
                gc = GC1()
                self.assertEqual(gc.get_settings(),
                                 dict(KEY1="value3", KEY2=3, KEY3=False,
                                      KEY4=dict(S1="s2")))

    def test_bool_value(self):
        with mock.patch.dict(os.environ, {"GC2_KEY1": "True"}):
            gc = GC2()
            self.assertEqual(gc.get_settings(),
                             dict(KEY1=True, KEY2=2))

    def test_get_value(self):
        gc = GC1()
        self.assertEqual(gc.get_settings_value("key1"), "value1")
        self.assertEqual(gc.get_settings_value("kEy1"), "value1")
        self.assertEqual(gc.get_settings_value("KEY1"), "value1")

    def test_get_config(self):
        gc1 = GC1.get_config()
        gc2 = GC2.get_config()

        gc1_n = GC1.get_config()
        self.assertEqual(gc1_n.get_settings(),
                         dict(KEY1="value1", KEY2=2, KEY3=False,
                              KEY4=dict(S1="s1")))
        self.assertEqual(gc1, gc1_n)
        self.assertEqual(gc1.my_obj, {"a": "b"})
        gc2 = GC2.get_config()
        self.assertEqual(gc2.get_settings(),
                         dict(KEY1=False, KEY2=2))

        fs = FakeSettingsObject({
            "GC1_KEY1": "value2",
            "GC1_KEY4_S1": "s2"
            })
        gc1 = GC1.get_config(custom_settings=fs)
        self.assertEqual(gc1.get_settings(),
                         dict(KEY1="value2", KEY2=2, KEY3=False,
                              KEY4=dict(S1="s2")))
        gc1 = GC1.get_config()
        self.assertEqual(gc1.get_settings(),
                         dict(KEY1="value1", KEY2=2, KEY3=False,
                              KEY4=dict(S1="s1")))
