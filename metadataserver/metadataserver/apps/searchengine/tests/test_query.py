from django.test import TestCase
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.searchengine.algebra.filters import SelectionFilter, OrFilter
from metadataserver.apps.searchengine.algebra.predicates import TextPredicate
from metadataserver.apps.searchengine.algebra.query import (
    ObjectWithPredicate, QueryParser, PredicateQuery, OrQuery, BinaryQuery,
    QueryWithFilters
    )
from metadataserver.apps.searchengine.datamodel.transform.indexing import SearchIndexer
from metadataserver.apps.searchengine.models import (
    ProposalMetadata, PROPOSAL_FROM_TEXT, DEFAULT_FILTER
    )


class PO(ObjectWithPredicate):

    def __init__(self, text):
        self._predicate = TextPredicate(text)

    def get_predicate(self):
        return self._predicate

    def __eq__(self, other):
        return self._predicate == other._predicate

    def __neq__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return str(self._predicate)


class FakeModelObject(object):
    def __init__(self, list_objects):
        self._list_objects = list_objects
        FakeModelObject._list_objects = list_objects

    def all(self):
        return self._list_objects


class TestQueryHits(TestCase):

    def test_simple_query(self):
        sample_objects = [PO("High level"),
                          PO("index"),
                          PO("idea"),
                          PO("concept")]

        fmo = FakeModelObject(sample_objects)

        q = QueryParser.parse("idea")

        hits = q.get_hits(fmo.all(), threshold=0.9)
        self.assertEqual(hits,
                         [(1.0, PO("idea"))]
                         )

    def test_query(self):
        sample_objects = [PO("High level"),
                          PO("index"),
                          PO("idea"),
                          PO("concept")]

        fmo = FakeModelObject(sample_objects)

        q = QueryParser.parse("High level idea")

        hits = q.get_hits(fmo.all(), threshold=0.9)
        self.assertEqual(hits,
                         [(1.0, PO("High level")),
                          (1.0, PO("High level")),
                          (1.0, PO("idea"))]
                         )

        q = QueryParser.parse("High level ideas")
        hits = q.get_hits(fmo.all(), threshold=0.9)
        self.assertEqual(hits,
                         [(1.0, PO("High level")),
                          (1.0, PO("High level")),
                          (0.96, PO("idea"))]
                         )

        q = QueryParser.parse("High level ideas level")
        hits = q.get_hits(fmo.all(), threshold=0.9)
        self.assertEqual(hits,
                         [(1.0, PO("High level")),
                          (1.0, PO("High level")),
                          (0.96, PO("idea"))]
                         )


class FilterParse(TestCase):
    def test_empty(self):
        self.assertEqual(QueryParser.filter_parse("High level ideas level"),
                         None)

    def test_one(self):
        self.assertEqual(QueryParser.filter_parse("High level ideas level"
                                                  " serial_number=:=1234"),
                         SelectionFilter("serial_number", "1234"))

    def test_many(self):
        self.assertEqual(QueryParser.filter_parse("High level ideas level"
                                                  " serial_number=:=1234 "
                                                  " serial_number=:=4321"),
                         OrFilter(SelectionFilter("serial_number", "1234"),
                                  SelectionFilter("serial_number", "4321")))


class TestEqual(TestCase):
    def test_predicate_query_equal(self):
        q1 = PredicateQuery("hola")
        q2 = PredicateQuery("hola")
        q3 = PredicateQuery("hola", strict=True)
        q4 = PredicateQuery("adios")

        self.assertTrue(q1.equal(q2))
        self.assertFalse(q1.equal(q3))
        self.assertFalse(q1.equal(q4))

        self.assertTrue(q1 == q2)
        self.assertTrue(q1 != q3)
        self.assertTrue(q1 != q4)

    def test_binary_query_equal(self):
        q1 = BinaryQuery(BinaryQuery(PredicateQuery("hola"),
                                     PredicateQuery("adios")),
                         PredicateQuery("luego"))

        q2 = BinaryQuery(BinaryQuery(PredicateQuery("hola"),
                                     PredicateQuery("adios")),
                         PredicateQuery("luego"))

        self.assertEqual(q1, q2)

        q3 = BinaryQuery(OrQuery(PredicateQuery("hola"),
                                 PredicateQuery("adios")),
                         PredicateQuery("luego"))
        self.assertNotEqual(q1, q3)

        q4 = BinaryQuery(BinaryQuery(PredicateQuery("hola2"),
                                     PredicateQuery("adios")),
                         PredicateQuery("luego"))

        self.assertNotEqual(q1, q4)


class TestQueryParser(TestCase):

    def test_simple_text(self):
        query = QueryParser.parse("word")
        self.assertEqual(query, PredicateQuery("word"))

    def test_sentence_or(self):
        query = QueryParser.parse("this is a sentence")

        self.assertEqual(query,
                         OrQuery(
                             OrQuery(
                                 OrQuery(
                                     PredicateQuery("this"),
                                     PredicateQuery("is")),
                                 PredicateQuery("a")),
                             PredicateQuery("sentence"))
                         )


class TestQueryWithFiltersParse(TestCase):

    def test_simple_text(self):
        query = QueryParser.parse_filtered_query("word")
        self.assertEqual(query._query, PredicateQuery("word"))

        query = QueryParser.parse_filtered_query("word serial_number=:=1234")
        self.assertEqual(query._query, PredicateQuery("word"))
        self.assertEqual(query._filter,
                         SelectionFilter("serial_number", "1234"))

    def test_sentence_or(self):
        query = QueryParser.parse_filtered_query("this is a sentence")

        self.assertEqual(query._query,
                         OrQuery(
                             OrQuery(
                                 OrQuery(
                                     PredicateQuery("this"),
                                     PredicateQuery("is")),
                                 PredicateQuery("a")),
                             PredicateQuery("sentence"))
                         )
        self.assertEqual(query._filter, None)
        query = QueryParser.parse_filtered_query("this is a sentence"
                                                 " serial_number=:=1234"
                                                 " serial_number=:=4321")

        self.assertEqual(query._query,
                         OrQuery(
                             OrQuery(
                                 OrQuery(
                                     PredicateQuery("this"),
                                     PredicateQuery("is")),
                                 PredicateQuery("a")),
                             PredicateQuery("sentence"))
                         )
        self.assertEqual(query._filter,
                         OrFilter(SelectionFilter("serial_number", "1234"),
                                  SelectionFilter("serial_number", "4321")))


class TestQueryWithFilters(TestCase):

    def test_equal(self):
        q1 = BinaryQuery(BinaryQuery(PredicateQuery("hola"),
                                     PredicateQuery("adios")),
                         PredicateQuery("luego"))
        f1 = OrFilter(SelectionFilter("serial_number", "1234"),
                      SelectionFilter("serial_number", "4321"))

        q2 = BinaryQuery(BinaryQuery(PredicateQuery("hola"),
                                     PredicateQuery("adios")),
                         PredicateQuery("luego"))
        f2 = OrFilter(SelectionFilter("serial_number", "1234"),
                      SelectionFilter("serial_number", "4321"))

        q3 = BinaryQuery(BinaryQuery(PredicateQuery("hola2"),
                                     PredicateQuery("adios")),
                         PredicateQuery("luego"))
        f3 = OrFilter(SelectionFilter("serial_number", "1234"),
                      SelectionFilter("serial_number", "4321A"))

        self.assertEqual(QueryWithFilters(q1), QueryWithFilters(q2))
        self.assertNotEqual(QueryWithFilters(q1), QueryWithFilters(q3))
        self.assertEqual(QueryWithFilters(q1, f1), QueryWithFilters(q2, f2))
        self.assertNotEqual(QueryWithFilters(q1, f1), QueryWithFilters(q2))
        self.assertNotEqual(QueryWithFilters(q1, f1), QueryWithFilters(q2, f3))
        self.assertNotEqual(QueryWithFilters(q1, f1), QueryWithFilters(q3, f2))

    def test_hits(self):
        p1 = Proposal(serial_number="1234")
        p1.save()

        p2 = Proposal(serial_number="4321")
        p2.save()

        p3 = Proposal(serial_number="ABCD")
        p3.save()

        pm_1 = ProposalMetadata(text_content="High level", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_1.save()

        pm_2 = ProposalMetadata(text_content="idea", score=0.96,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_2.save()

        pm_3 = ProposalMetadata(text_content="High level", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_3.save()

        pm_4 = ProposalMetadata(text_content="house", score=0.96,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_4.save()

        pm_5 = ProposalMetadata(text_content="High level", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p3)
        pm_5.save()

        pm_6 = ProposalMetadata(text_content="idea", score=0.96,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p3)
        pm_6.save()

        all_queryset = ProposalMetadata.objects.all()

        qf = QueryParser.parse_filtered_query("high")
        self.assertEqual(len(qf.get_hits(all_queryset, 0.5)),
                         3)

        qf = QueryParser.parse_filtered_query("high serial_number=:=1234")
        self.assertEqual(len(qf.get_hits(all_queryset, 0.5)),
                         1)

        qf = QueryParser.parse_filtered_query("high level idea")
        self.assertEqual(len(qf.get_hits(all_queryset, 0.99)),
                         8)

        qf = QueryParser.parse_filtered_query("high level idea"
                                              " serial_number=:=1234")
        self.assertEqual(len(qf.get_hits(all_queryset, 0.99)),
                         3)

        qf = QueryParser.parse_filtered_query("high level idea"
                                              " serial_number=:=1234"
                                              " serial_number=:=4321")
        self.assertEqual(len(qf.get_hits(all_queryset, 0.99)),
                         5)
        # no query case
        si = SearchIndexer()
        si.gen_default_tags_all_classes()
        qf = QueryParser.parse_filtered_query(
            " serial_number=:=1234"
            " serial_number=:=4321")
        self.assertEqual(len(qf.get_hits(all_queryset, 0.99,
                                         DEFAULT_FILTER)),
                         2)
