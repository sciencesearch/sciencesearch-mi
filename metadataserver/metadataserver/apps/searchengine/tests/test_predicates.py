from metadataserver.apps.searchengine.algebra.predicates import TextPredicate

from django.test import TestCase


class TestTextPredicates(TestCase):
    
    def test_match_strict(self):
        
        p1 = TextPredicate("House")
        p2 = TextPredicate("House")
        p3 = TextPredicate("house")
        
        self.assertEqual(p1.match(p2, strict=True), 1.0)
        self.assertEqual(p1.match(p3, strict=True), 1.0)
        self.assertEqual(p1.match(p3, strict=True, case_sensitive=True), 0.0)
        
    def test_match(self):
        
        p1 = TextPredicate("House")
        p2 = TextPredicate("House")
        p3 = TextPredicate("louse")
        p4 = TextPredicate("House1")
        
        self.assertAlmostEqual(p1.match(p2), 1.0)
        self.assertAlmostEqual(p1.match(p3), 0.866666666666)
        self.assertAlmostEqual(p1.match(p4), 0.966666666666)
    
    def test_match_multiword(self):
        p1 = TextPredicate("dog house")
        p2 = TextPredicate("dog house")
        p3 = TextPredicate("dogs house")
        p4 = TextPredicate("dog")
        p5 = TextPredicate("dog house house")
        self.assertAlmostEqual(p1.match(p4, threshold=0.9), 1.0)
        self.assertAlmostEqual(p1.match(p2, threshold=0.9), 2.0)
        self.assertAlmostEqual(p1.match(p3, threshold=0.9), 1.9166666666666)
        self.assertAlmostEqual(p1.match(p5, threshold=0.9), 2.0)
        