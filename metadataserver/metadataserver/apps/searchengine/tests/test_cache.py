import datetime
import time

from django.test import TestCase, TransactionTestCase
from django.utils import timezone

from metadataserver.apps.searchengine.cache import SearchCacheConfig, SearchCacheManager
from metadataserver.apps.searchengine.models import (
    SearchResultSummary, HitListScorer, SearchResult
    )


class TestSearchCacheConfig(TestCase):
    def setUp(self):
        SearchCacheConfig.del_config()

    def test_default(self):
        self.assertEqual(SearchCacheConfig.get_config().get_settings(),
                         dict(ENABLED=True,
                              CACHE_SIZE=100,
                              MAX_AGE_S=86400,
                              ASYNC_STORE=False,
                              NUM_WORKERS=4))


class TestSearchCacheManager(TransactionTestCase):
    def setUp(self):
        SearchCacheConfig.del_config()
        import django.db.transaction as transaction
        transaction.set_autocommit(True)

    def test_clean_cache(self):
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1",
                                  search_latency_s=1.0,
                                  total_results_number=1)
        SearchCacheManager.save_result_cache(srs)
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1",
                                  search_latency_s=1.0,
                                  total_results_number=1)
        SearchCacheManager.save_result_cache(srs)
        self.assertEqual(SearchResultSummary.objects.count(),
                         2)
        SearchCacheManager.clear_cache()
        self.assertEqual(SearchResultSummary.objects.count(),
                         0)

    def test_get_cache_size(self):
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1",
                                  search_latency_s=1.0,
                                  total_results_number=1,
                                  page_start=0,
                                  page_end=1)
        SearchCacheManager.save_result_cache(srs)
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1",
                                  search_latency_s=1.0,
                                  total_results_number=1,
                                  page_start=0,
                                  page_end=1)
        SearchCacheManager.save_result_cache(srs)
        self.assertEqual(SearchCacheManager.get_cache_size(),
                         2)

    def test_save_result(self):
        with self.settings(SEARCH_CACHE_ENABLED=True,
                           SEARCH_CACHE_CACHE_SIZE=2):
            srs = SearchResultSummary(date=timezone.now(),
                                      query="1234",
                                      search_latency_s=1.0,
                                      total_results_number=1)

            sr2 = SearchResult(score=0.5, result_position=1,
                               search_results_summary=srs)

            sr1 = SearchResult(score=0.3, result_position=0,
                               search_results_summary=srs)

            srs.set_results([sr1, sr2])

            SearchCacheManager.save_result_cache(srs)
            self.assertEqual(srs,
                             SearchResultSummary.objects.get(id=srs.id))
            self.assertEqual(sr1, SearchResult.objects.get(id=sr1.id))
            self.assertEqual(sr2, SearchResult.objects.get(id=sr2.id))

            srs = SearchResultSummary(date=timezone.now(),
                                      query="1",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            SearchCacheManager.save_result_cache(srs)

            srs = SearchResultSummary(date=timezone.now(),
                                      query="2",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            SearchCacheManager.save_result_cache(srs)
            self.assertEqual(SearchResultSummary.objects.count(),
                             2)
            self.assertEqual([x.query
                              for x in SearchResultSummary.objects.all()],
                             ["1", "2"])
            self.assertEqual([],
                             list(SearchResult.objects.filter(id=sr1.id)))
            self.assertEqual([],
                             list(SearchResult.objects.filter(id=sr2.id)))

    def test_get_cached_query(self):
        with self.settings(SEARCH_CACHE_ENABLED=True,
                           SEARCH_CACHE_CACHE_SIZE=2):
            srs = SearchResultSummary(date=timezone.now(),
                                      query="1234",
                                      search_latency_s=1.0,
                                      total_results_number=1)

            sr2 = SearchResult(score=0.5, result_position=1,
                               search_results_summary=srs)

            sr1 = SearchResult(score=0.3, result_position=0,
                               search_results_summary=srs)

            srs.set_results([sr1, sr2])

            SearchCacheManager.save_result_cache(srs)
            start_time = time.time()

            srs_n = SearchCacheManager.get_cached_query("1234",
                                                        start_time)

            self.assertLess(srs_n.search_latency_s, 1.0)
            self.assertEqual(srs_n.date,
                             srs.date)

            self.assertEqual([x.id for x in srs_n.get_results()],
                             [x.id for x in srs.get_results()])

            start_time = time.time()

            srs_n = SearchCacheManager.get_cached_query("1234",
                                                        start_time,
                                                        first_index=1,
                                                        last_index=2)

            self.assertLess(srs_n.search_latency_s, 1.0)
            self.assertEqual(srs_n.date,
                             srs.date)
            self.assertEqual(srs_n.page_start, 1)
            self.assertEqual(srs_n.page_end, 2)

            self.assertEqual([x.id for x in srs_n.get_results()],
                             [sr2.id])

    def test_get_cached_query_concurrent(self):
        with self.settings(SEARCH_CACHE_ENABLED=True,
                           SEARCH_CACHE_CACHE_SIZE=2,
                           SEARCH_CACHE_ASYNC_STORE=True):
            srs = SearchResultSummary(date=timezone.now(),
                                      query="1234",
                                      search_latency_s=1.0,
                                      total_results_number=1)

            sr2 = SearchResult(score=0.5, result_position=1,
                               search_results_summary=srs)

            sr1 = SearchResult(score=0.3, result_position=0,
                               search_results_summary=srs)

            srs.set_results([sr1, sr2])

            SearchCacheManager.save_result_cache(srs)
            SearchCacheManager.shutdown_and_wait()
            start_time = time.time()

            srs_n = SearchCacheManager.get_cached_query("1234",
                                                        start_time)

            self.assertLess(srs_n.search_latency_s, 1.0)
            self.assertEqual(srs_n.date,
                             srs.date)

            self.assertEqual([x.score for x in srs_n.get_results()],
                             [x.score for x in srs.get_results()])

            start_time = time.time()

            srs_n = SearchCacheManager.get_cached_query("1234",
                                                        start_time,
                                                        first_index=1,
                                                        last_index=2)

            self.assertLess(srs_n.search_latency_s, 1.0)
            self.assertEqual(srs_n.date,
                             srs.date)
            self.assertEqual(srs_n.page_start, 1)
            self.assertEqual(srs_n.page_end, 2)

            self.assertEqual([x.score for x in srs_n.get_results()],
                             [sr2.score])

    def test_check_cache_size(self):
        with self.settings(SEARCH_CACHE_ENABLED=True,
                           SEARCH_CACHE_CACHE_SIZE=2):
            srs = SearchResultSummary(date=timezone.now(),
                                      query="1234",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            srs.save()
            srs = SearchResultSummary(date=timezone.now(),
                                      query="1",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            srs.save()

            srs = SearchResultSummary(date=timezone.now(),
                                      query="2",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            srs.save()
            srs = SearchResultSummary(date=timezone.now(),
                                      query="3",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            srs.save()
            self.assertEqual(SearchResultSummary.objects.count(),
                             4)

            SearchCacheManager.check_cache_size()
            self.assertEqual(SearchResultSummary.objects.count(),
                             2)

            self.assertEqual([x.query
                              for x in SearchResultSummary.objects.all()],
                             ["2", "3"])

    def test_check_cache_size_age(self):
        with self.settings(SEARCH_CACHE_ENABLED=True,
                           SEARCH_CACHE_CACHE_SIZE=3,
                           SEARCH_CACHE_MAX_AGE_S=50):
            srs = SearchResultSummary(date=timezone.now(),
                                      query="1234",
                                      search_latency_s=1.0,
                                      total_results_number=1)

            srs.save()
            srs.date = timezone.now() - datetime.timedelta(seconds=100)
            srs.save()
            srs = SearchResultSummary(date=timezone.now(),
                                      query="1",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            srs.save()

            srs = SearchResultSummary(date=timezone.now(),
                                      query="2",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            srs.save()
            srs.date = timezone.now() - datetime.timedelta(seconds=100)
            srs.save()
            srs = SearchResultSummary(date=timezone.now(),
                                      query="3",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            srs.save()
            self.assertEqual(SearchResultSummary.objects.count(),
                             4)

            SearchCacheManager.check_cache_size()
            self.assertEqual(SearchResultSummary.objects.count(),
                             2)

            self.assertEqual([x.query
                              for x in SearchResultSummary.objects.all()],
                             ["1", "3"])

    def test_query_catching(self):
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1234",
                                  search_latency_s=1.0,
                                  total_results_number=1)
        srs.save()
        self.assertEqual(SearchCacheManager._get_result_summary("1234",
                                                                max_age_s=1),
                         srs)
        time.sleep(2)
        self.assertEqual(SearchCacheManager._get_result_summary("1234",
                                                                max_age_s=1),
                         None)
        self.assertEqual(SearchCacheManager._get_result_summary("1234"),
                         srs)

        srsn = SearchResultSummary(date=timezone.now(),
                                   query="1234",
                                   search_latency_s=1.0,
                                   total_results_number=1)
        srsn.save()

        self.assertEqual(SearchCacheManager._get_result_summary("1234",
                                                                max_age_s=1).id,
                         srsn.id)

        self.assertTrue(SearchCacheManager._get_result_summary("1234",
                                                               max_age_s=1).cached)

    def test_query_catching_type(self):

        srs_1 = SearchResultSummary(date=timezone.now(),
                                    query="1234",
                                    query_type="type1",
                                    search_latency_s=1.0,
                                    total_results_number=1,
                                    page_start=0,
                                    page_end=1)
        srs_1.save()
        srs_2 = SearchResultSummary(date=timezone.now(),
                                    query="1234",
                                    query_type="type2",
                                    search_latency_s=1.0,
                                    total_results_number=1,
                                    page_start=0,
                                    page_end=1)
        srs_2.save()
        self.assertEqual(SearchCacheManager.get_cached_query("1234",
                                                             time.time(),
                                                             query_type="type1"),
                         srs_1)

        self.assertEqual(SearchCacheManager.get_cached_query("1234",
                                                             time.time(),
                                                             query_type="type2"),
                         srs_2)

    def test_query_catching_hit_list_weights(self):

        hls = HitListScorer()
        config_dict = dict(m1=dict(weight=1.0, min_input=-1, max_input=1),
                           m2=dict(weight=2.0, min_input=-2, max_input=2))

        for (key, value) in config_dict.items():
            hls.register_mark_properties(key, value["weight"],
                                         value["min_input"],
                                         value["max_input"])

        srs = SearchResultSummary(date=timezone.now(),
                                  query="1234",
                                  search_latency_s=1.0,
                                  total_results_number=1,
                                  hit_list_scorer_as_json=hls.get_weights_dict())

        srs.save_for_cache()

        hls2 = HitListScorer()
        config_dict2 = dict(m1=dict(weight=2.0, min_input=-1, max_input=1),
                            m2=dict(weight=2.0, min_input=-2, max_input=2))

        for (key, value) in config_dict2.items():
            hls2.register_mark_properties(key, value["weight"],
                                          value["min_input"],
                                          value["max_input"])

        srs2 = SearchResultSummary(date=timezone.now(),
                                   query="1234",
                                   search_latency_s=1.0,
                                   total_results_number=1,
                                   hit_list_scorer_as_json=hls2.get_weights_dict())

        srs2.save_for_cache()
        self.assertEqual(SearchCacheManager._get_result_summary("1234"),
                         srs2)

        self.assertEqual(SearchCacheManager._get_result_summary("1234",
                                                                hit_list_weights=hls.get_weights_dict()),
                         srs)
