from unittest.mock import patch

from django.test import TestCase

from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.papermanager.models import Paper
from metadataserver.apps.searchengine.datamodel.transform.indexing import (
    SearchIndexer, do_once_checks, IndexConfig
    )
from metadataserver.apps.searchengine.models import (
    ProposalMetadata, ScopeImageMetadata, PaperMetadata, DEFAULT_FILTER
    )


class TestSearchIndexer(TestCase):

    def test_gen_default_tags(self):
        p1 = Proposal(serial_number="1234")
        p1.save()

        p2 = Proposal(serial_number="4321")
        p2.save()

        si = SearchIndexer()

        si.gen_default_tags(ProposalMetadata, Proposal)
        si.gen_default_tags(ProposalMetadata, Proposal)

        md_qs = ProposalMetadata.objects.all()
        self.assertEqual(len(md_qs), 2)

        self.assertEqual(md_qs[0].related_object, p1)
        self.assertEqual(md_qs[1].related_object, p2)

    def test_gen_default_tags_all_classes(self):
        p1 = Proposal(serial_number="1234")
        p1.save()

        p2 = Proposal(serial_number="4321")
        p2.save()

        im1 = ScopeImage(original_route="/1.ser")
        im1.save()
        im2 = ScopeImage(original_route="/2.ser")
        im2.save()

        pa1 = Paper.paper_creator("Title1")
        pa2 = Paper.paper_creator("Title2")

        si = SearchIndexer()
        si.gen_default_tags_all_classes()

        md_qs = ProposalMetadata.objects.all()
        self.assertEqual(len(md_qs), 2)
        self.assertEqual(md_qs[0].related_object, p1)
        self.assertEqual(md_qs[1].related_object, p2)

        md_qs = ScopeImageMetadata.objects.all()
        self.assertEqual(len(md_qs), 2)
        self.assertEqual(md_qs[0].related_object, im1)
        self.assertEqual(md_qs[1].related_object, im2)

        md_qs = PaperMetadata.objects.all()
        self.assertEqual(len(md_qs), 2)
        self.assertEqual(md_qs[0].related_object, pa1)
        self.assertEqual(md_qs[1].related_object, pa2)

    def test_check(self):
        with patch.dict("os.environ", {'SEARCH_INDEX_DO_CHECK': "true"}):
            with self.settings(SEARCH_INDEX_DO_CHECK=True):
                self.assertTrue(True,
                                IndexConfig.get_config(True).get_settings_value("do_check"))
                p1 = Proposal(serial_number="1234")
                p1.save()

                p2 = Proposal(serial_number="4321")
                p2.save()

                im1 = ScopeImage(original_route="/1.ser")
                im1.save()
                im2 = ScopeImage(original_route="/2.ser")
                im2.save()

                pa1 = Paper.paper_creator("Title1")
                pa2 = Paper.paper_creator("Title2")
                si = SearchIndexer()
                self.assertTrue(si.check_need_create_tags_all_classes())
                do_once_checks()

                md_qs = ProposalMetadata.objects.all()
                self.assertEqual(len(md_qs), 2)
                self.assertEqual(md_qs[0].related_object, p1)
                self.assertEqual(md_qs[1].related_object, p2)

                md_qs = ScopeImageMetadata.objects.all()
                self.assertEqual(len(md_qs), 2)
                self.assertEqual(md_qs[0].related_object, im1)
                self.assertEqual(md_qs[1].related_object, im2)

                md_qs = PaperMetadata.objects.all()
                self.assertEqual(len(md_qs), 2)
                self.assertEqual(md_qs[0].related_object, pa1)
                self.assertEqual(md_qs[1].related_object, pa2)
                self.assertFalse(si.check_need_create_tags_all_classes())

        IndexConfig.get_config(True)

    def test_check_2(self):
        with patch.dict("os.environ", {'SEARCH_INDEX_DO_CHECK': "true"}):
            with self.settings(SEARCH_INDEX_DO_CHECK=True):
                self.assertTrue(True,
                                IndexConfig.get_config(True).get_settings_value("do_check"))
                p1 = Proposal(serial_number="1234")
                p1.save()

                p2 = Proposal(serial_number="4321")
                p2.save()

                pm1 = ProposalMetadata(related_object=p1,
                                       text_content="DEFAULT",
                                       metadata_type=DEFAULT_FILTER,
                                       score=1.0)
                pm1.save()
                im1 = ScopeImage(original_route="/1.ser")
                im1.save()
                im2 = ScopeImage(original_route="/2.ser")
                im2.save()

                pa1 = Paper.paper_creator("Title1")
                pa2 = Paper.paper_creator("Title2")
                si = SearchIndexer()
                self.assertTrue(si.check_need_create_tags_all_classes())
                do_once_checks()

                md_qs = ProposalMetadata.objects.all()
                self.assertEqual(len(md_qs), 2)
                self.assertEqual(md_qs[0].related_object, p1)
                self.assertEqual(md_qs[1].related_object, p2)

                md_qs = ScopeImageMetadata.objects.all()
                self.assertEqual(len(md_qs), 2)
                self.assertEqual(md_qs[0].related_object, im1)
                self.assertEqual(md_qs[1].related_object, im2)

                md_qs = PaperMetadata.objects.all()
                self.assertEqual(len(md_qs), 2)
                self.assertEqual(md_qs[0].related_object, pa1)
                self.assertEqual(md_qs[1].related_object, pa2)
                si = SearchIndexer()
                self.assertFalse(si.check_need_create_tags_all_classes())
        IndexConfig.get_config(True)
