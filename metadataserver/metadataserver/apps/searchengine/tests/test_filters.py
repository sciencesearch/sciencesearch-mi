from django.test import TestCase
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.searchengine.algebra.filters import (
    BaseFilter, SelectionFilter, OrFilter
    )
from metadataserver.apps.searchengine.models import ProposalMetadata, PROPOSAL_FROM_TEXT


class TestBaseFilter(TestCase):

    def test_equal(self):
        bf_1 = BaseFilter()
        bf_2 = BaseFilter()
        sf = SelectionFilter("field", "term")
        self.assertEqual(bf_1, bf_2)
        self.assertNotEqual(bf_1, sf)


class TestSelectionFilter(TestCase):

    def test_create(self):
        sf = SelectionFilter("field", "term", False, related_field_name="myf")

        self.assertEqual(sf._field, "field")
        self.assertEqual(sf._term, "term")
        self.assertFalse(sf._strict_equal)
        self.assertEqual(sf._related_field_name, "myf")

    def test_equal(self):
        sf_1 = SelectionFilter("field", "term", False,
                               related_field_name="myf")

        sf_2 = SelectionFilter("field", "term", False,
                               related_field_name="myf")

        self.assertEqual(sf_1, sf_2)

        sf_3 = SelectionFilter("field1", "term", False,
                               related_field_name="myf")

        self.assertNotEqual(sf_1, sf_3)

        sf_3 = SelectionFilter("field", "term1", False,
                               related_field_name="myf")

        self.assertNotEqual(sf_1, sf_3)

        sf_3 = SelectionFilter("field1", "term", True,
                               related_field_name="myf")

        self.assertNotEqual(sf_1, sf_3)

        sf_3 = SelectionFilter("field1", "term", False,
                               related_field_name="myf2")

        self.assertNotEqual(sf_1, sf_3)

    def test_apply_filter_strict(self):
        p1 = Proposal(serial_number="1234")
        p1.save()

        p2 = Proposal(serial_number="4321")
        p2.save()

        pm_1 = ProposalMetadata(text_content="pm_1", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_1.save()

        pm_2 = ProposalMetadata(text_content="pm_2", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_2.save()

        pm_3 = ProposalMetadata(text_content="pm_3", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_3.save()

        pm_4 = ProposalMetadata(text_content="pm_4", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_4.save()

        all_queryset = ProposalMetadata.objects.all()

        sf = SelectionFilter("serial_number", "1234")

        filtered_queryset = sf.apply_filter(all_queryset)

        self.assertEqual(len(filtered_queryset), 2)
        self.assertEqual(filtered_queryset[0], pm_1)
        self.assertEqual(filtered_queryset[1], pm_2)

        sf = SelectionFilter("serial_number", "4321")

        filtered_queryset = sf.apply_filter(all_queryset)

        self.assertEqual(len(filtered_queryset), 2)
        self.assertEqual(filtered_queryset[0], pm_3)
        self.assertEqual(filtered_queryset[1], pm_4)

    def test_apply_filter_non_strict(self):
        p1 = Proposal(serial_number="1234")
        p1.save()

        p2 = Proposal(serial_number="4321")
        p2.save()

        pm_1 = ProposalMetadata(text_content="pm_1", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_1.save()

        pm_2 = ProposalMetadata(text_content="pm_2", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_2.save()

        pm_3 = ProposalMetadata(text_content="pm_3", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_3.save()

        pm_4 = ProposalMetadata(text_content="pm_4", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_4.save()

        all_queryset = ProposalMetadata.objects.all()

        sf = SelectionFilter("serial_number", "12", strict_equal=False)

        filtered_queryset = sf.apply_filter(all_queryset)

        self.assertEqual(len(filtered_queryset), 2)
        self.assertEqual(filtered_queryset[0], pm_1)
        self.assertEqual(filtered_queryset[1], pm_2)

        sf = SelectionFilter("serial_number", "43", strict_equal=False)

        filtered_queryset = sf.apply_filter(all_queryset)

        self.assertEqual(len(filtered_queryset), 2)
        self.assertEqual(filtered_queryset[0], pm_3)
        self.assertEqual(filtered_queryset[1], pm_4)

        sf = SelectionFilter("serial_number", "4", strict_equal=False)

        filtered_queryset = sf.apply_filter(all_queryset)

        self.assertEqual(len(filtered_queryset), 4)
        self.assertEqual(filtered_queryset[0], pm_1)
        self.assertEqual(filtered_queryset[1], pm_2)
        self.assertEqual(filtered_queryset[2], pm_3)
        self.assertEqual(filtered_queryset[3], pm_4)


class TestOrFilter(TestCase):

    def test_equal(self):
        of_1 = OrFilter(SelectionFilter("serial_number", "1234"),
                        SelectionFilter("serial_number", "4321"))

        of_2 = OrFilter(SelectionFilter("serial_number", "1234"),
                        SelectionFilter("serial_number", "4321"))

        self.assertEqual(of_1, of_2)

        of_3 = OrFilter(SelectionFilter("serial_number", "1234"),
                        SelectionFilter("serial_number", "4321",
                                        False))
        self.assertNotEqual(of_1, of_3)

    def test_apply_filter(self):
        p1 = Proposal(serial_number="1234")
        p1.save()

        p2 = Proposal(serial_number="4321")
        p2.save()

        p3 = Proposal(serial_number="ABCD")
        p3.save()

        pm_1 = ProposalMetadata(text_content="pm_1", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_1.save()

        pm_2 = ProposalMetadata(text_content="pm_2", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_2.save()

        pm_3 = ProposalMetadata(text_content="pm_3", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_3.save()

        pm_4 = ProposalMetadata(text_content="pm_4", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_4.save()

        pm_5 = ProposalMetadata(text_content="pm_5", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p3)
        pm_5.save()

        pm_6 = ProposalMetadata(text_content="pm_6", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p3)
        pm_6.save()

        all_queryset = ProposalMetadata.objects.all()

        of = OrFilter(SelectionFilter("serial_number", "1234"),
                      SelectionFilter("serial_number", "4321"))

        filtered_queryset = of.apply_filter(all_queryset)

        self.assertEqual(len(filtered_queryset), 4)
        self.assertEqual(filtered_queryset[0], pm_1)
        self.assertEqual(filtered_queryset[1], pm_2)
        self.assertEqual(filtered_queryset[2], pm_3)
        self.assertEqual(filtered_queryset[3], pm_4)

    def test_apply_filter_overlap(self):
        p1 = Proposal(serial_number="1234")
        p1.save()

        p2 = Proposal(serial_number="4321")
        p2.save()

        p3 = Proposal(serial_number="ABCD")
        p3.save()

        pm_1 = ProposalMetadata(text_content="pm_1", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_1.save()

        pm_2 = ProposalMetadata(text_content="pm_2", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p1)
        pm_2.save()

        pm_3 = ProposalMetadata(text_content="pm_3", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_3.save()

        pm_4 = ProposalMetadata(text_content="pm_4", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p2)
        pm_4.save()

        pm_5 = ProposalMetadata(text_content="pm_5", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p3)
        pm_5.save()

        pm_6 = ProposalMetadata(text_content="pm_6", score=1.0,
                                metadata_type=PROPOSAL_FROM_TEXT,
                                related_object=p3)
        pm_6.save()

        all_queryset = ProposalMetadata.objects.all()

        of = OrFilter(SelectionFilter("serial_number", "4", strict_equal=False),
                      SelectionFilter("serial_number", "3", strict_equal=False)
                      )

        filtered_queryset = of.apply_filter(all_queryset)

        self.assertEqual(len(filtered_queryset), 4)
        self.assertEqual(filtered_queryset[0], pm_1)
        self.assertEqual(filtered_queryset[1], pm_2)
        self.assertEqual(filtered_queryset[2], pm_3)
        self.assertEqual(filtered_queryset[3], pm_4)
