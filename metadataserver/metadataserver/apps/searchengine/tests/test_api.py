import datetime
import json
import logging

from django.utils import timezone
from rest_framework import status

from metadataserver.apps.ncem.tests.test_api import GenericAPITestTransaction
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.papermanager.models import Paper
from metadataserver.apps.searchengine.cache import SearchCacheManager, SearchCacheConfig
from metadataserver.apps.searchengine.configuration import (
    PROPOSAL_FROM_TEXT, SCOPEIMAGE_FROM_PROPOSAL_TEXT, PAPER_FROM_TEXT
    )
from metadataserver.apps.searchengine.models import (
    ScopeImage, ProposalMetadata, ScopeImageMetadata, SearchResultSummary, SearchResult,
    SearchResultSummaryArchive, SearchResultArchive, HitListScorer, PaperMetadata,
    ScopeImageMetadataIndex, ProposalMetadataIndex, PaperMetadataIndex
    )

SEARCH_API_BASE = '/api/search/'

search_proposals_endpoint = SEARCH_API_BASE + "do_search/proposals/"
search_images_endpoint = SEARCH_API_BASE + "do_search/images/"
search_papers_endpoint = SEARCH_API_BASE + "do_search/papers/"
search_archive_entries_endpoint = SEARCH_API_BASE + "archive/entries/"
search_archive_result_clicked_endpoint = (
        SEARCH_API_BASE + "archive/results/{}/clicked/"
)

logger = logging.getLogger(__name__)


class TestSearchAPI(GenericAPITestTransaction):
    def tearDown(self):
        super(TestSearchAPI, self).tearDown()
        SearchCacheManager.clear_cache()
        SearchCacheConfig.get_config(force_read=True)
        del self._settings

    def setUp(self):
        SearchCacheManager.clear_cache()
        SearchCacheConfig.get_config(force_read=True)
        super(TestSearchAPI, self).setUp()
        self._settings = dict(SEARCH_CONFIG_INDEX_SEARCH=False, SEARCH_CONFIG_DEBUG_DATA=True)

    def _pre_search_readyness(self):
        """This method will create and index in the test subclass to
        test the index features."""
        pass

    def parse_json_date(self, cad):
        """2018-01-23T19:56:29.086422Z"""
        date_format = '%Y-%m-%dT%H:%M:%S.%fZ'
        return datetime.datetime.strptime(cad,
                                          date_format).replace(tzinfo=datetime.timezone.utc)

    def test_client_search_empty(self):
        url = self._configure_url(search_proposals_endpoint)
        self._pre_search_readyness()
        the_now = timezone.now()
        request = {}
        request.update(self._settings)
        request.update({"query": "test hi!"})
        response = self.client.get(url, request)

        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        self.assertEqual(data_from_api["search_weights"],
                         {
                             'pt': {
                                 'max_input': 10.0,
                                 'min_input': -10.0,
                                 'weight': 1.0
                                 },
                             'scrt': {
                                 'max_input': 10.0,
                                 'min_input': -10.0,
                                 'weight': 1.0
                                 },
                             'imfs': {
                                 'max_input': 10.0,
                                 'min_input': -10.0,
                                 'weight': 1.0
                                 },
                             'spt': {
                                 'max_input': 10.0,
                                 'min_input': -10.0,
                                 'weight': 1.0
                                 },
                             'filt': {
                                 'max_input': 10.0,
                                 'min_input': -10.0,
                                 'weight': 1.0
                                 },
                             'scdl': {
                                 'max_input': 1.0,
                                 'min_input': 0.5,
                                 'weight': 1.0
                                 },
                             })

        self.assertEqual(data_from_api["results_list"],
                         [])

        self.assertEqual(data_from_api["total_results_number"],
                         0)

        self.assertEqual(data_from_api["query"],
                         "test hi!")
        self.assertEqual(data_from_api["page_start"],
                         0)
        self.assertEqual(data_from_api["page_end"],
                         10)
        self.assertLess(data_from_api["search_latency_s"], 1.0)
        self.assertLess((self.parse_json_date(data_from_api["date"]) -
                         the_now).total_seconds(), 1.0)

    def test_client_search_results(self):
        url = self._configure_url(search_proposals_endpoint)

        p_1 = Proposal(serial_number="P1")
        p_1.save()

        p_2 = Proposal(serial_number="P2")
        p_2.save()

        pm = ProposalMetadata(text_content="Houses", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="cat", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="dog house", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_2)
        pm.save()
        self._pre_search_readyness()
        request = {}
        request.update(self._settings)
        request.update({"query": "cat house"})
        response = self.client.get(url, request)
        archive_objs = list(SearchResultArchive.objects.all())
        ids = [str(x.id) for x in archive_objs]
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        self.maxDiff = None
        self.assertEqual(data_from_api["results_list"],
                         [{
                             'hit_list': [[1.0, 1.0, 'pt', 'cat'],
                                          [0.9666666666666667, 1.0, 'pt', 'Houses']],
                             'archive_id': ids[0],
                             'result_object': {
                                 'assigned_scientist': '',
                                 'bin': '',
                                 'co_researcher': 'N',
                                 'contact_with_staff_name': '',
                                 'date_project_end': '1970-01-01T00:00:00Z',
                                 'date_project_started': '1970-01-01T00:00:00Z',
                                 'email': '',
                                 'first_name': '',
                                 'follow_on_progress_effort': '',
                                 'foundry_capabilities': '',
                                 'interaction_type': -1,
                                 'last_name': '',
                                 'pi_name': '',
                                 'primary_researcher': 'N',
                                 'primary_researcher_name': '',
                                 'project_duration': '',
                                 'project_leader': 'N',
                                 'project_summary': '',
                                 'relevant_experience': '',
                                 'serial_number': 'P1',
                                 'significance': '',
                                 'status': 'ACTIVE',
                                 'title': '',
                                 'work_description': ''
                                 },
                             'score': 1.0816666666666668,
                             'result_position': 0
                             },
                             {
                                 'hit_list': [[1.0, 1.0, 'pt', 'dog house']],
                                 'archive_id': ids[1],
                                 'result_object': {
                                     'assigned_scientist': '',
                                     'bin': '',
                                     'co_researcher': 'N',
                                     'contact_with_staff_name': '',
                                     'date_project_end': '1970-01-01T00:00:00Z',
                                     'date_project_started': '1970-01-01T00:00:00Z',
                                     'email': '',
                                     'first_name': '',
                                     'follow_on_progress_effort': '',
                                     'foundry_capabilities': '',
                                     'interaction_type': -1,
                                     'last_name': '',
                                     'pi_name': '',
                                     'primary_researcher': 'N',
                                     'primary_researcher_name': '',
                                     'project_duration': '',
                                     'project_leader': 'N',
                                     'project_summary': '',
                                     'relevant_experience': '',
                                     'serial_number': 'P2',
                                     'significance': '',
                                     'status': 'ACTIVE',
                                     'title': '',
                                     'work_description': ''
                                     },
                                 'score': 0.55,
                                 'result_position': 1
                                 }])
        self.assertEqual(data_from_api["total_results_number"],
                         2)
        response = self.client.get(url, dict(
            query="cat house",
            SEARCH_METADATA_PT_WEIGHT=1.0,
            SEARCH_METADATA_PT_MIN_INPUT=0.0,
            SEARCH_METADATA_PT_MAX_INPUT=1.0,
            **self._settings
            ))
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        self.maxDiff = None

        self.assertEqual(data_from_api["search_weights"],
                         {
                             'pt': {
                                 'max_input': 1.0,
                                 'min_input': 0.0,
                                 'weight': 1.0
                                 },
                             'scrt': {
                                 'max_input': 10.0,
                                 'min_input': -10.0,
                                 'weight': 1.0
                                 },
                             'imfs': {
                                 'max_input': 10.0,
                                 'min_input': -10.0,
                                 'weight': 1.0
                                 },
                             'spt': {
                                 'max_input': 10.0,
                                 'min_input': -10.0,
                                 'weight': 1.0
                                 },
                             'filt': {
                                 'max_input': 10.0,
                                 'min_input': -10.0,
                                 'weight': 1.0
                                 },
                             'scdl': {
                                 'max_input': 1.0,
                                 'min_input': 0.5,
                                 'weight': 1.0
                                 }

                             })
        ids = [str(x.id)
               for x in SearchResultArchive.objects.all().order_by("id")]

        self.assertEqual(data_from_api["results_list"],
                         [{
                             'hit_list': [[1.0, 1.0, 'pt', 'cat'],
                                          [0.9666666666666667, 1.0, 'pt', 'Houses']],
                             'archive_id': ids[2],
                             'result_object': {
                                 'assigned_scientist': '',
                                 'bin': '',
                                 'co_researcher': 'N',
                                 'contact_with_staff_name': '',
                                 'date_project_end': '1970-01-01T00:00:00Z',
                                 'date_project_started': '1970-01-01T00:00:00Z',
                                 'email': '',
                                 'first_name': '',
                                 'follow_on_progress_effort': '',
                                 'foundry_capabilities': '',
                                 'interaction_type': -1,
                                 'last_name': '',
                                 'pi_name': '',
                                 'primary_researcher': 'N',
                                 'primary_researcher_name': '',
                                 'project_duration': '',
                                 'project_leader': 'N',
                                 'project_summary': '',
                                 'relevant_experience': '',
                                 'serial_number': 'P1',
                                 'significance': '',
                                 'status': 'ACTIVE',
                                 'title': '',
                                 'work_description': ''
                                 },
                             'score': 1.9666666666666668,
                             'result_position': 0
                             },
                             {
                                 'hit_list': [[1.0, 1.0, 'pt', 'dog house']],
                                 'archive_id': ids[3],
                                 'result_object': {
                                     'assigned_scientist': '',
                                     'bin': '',
                                     'co_researcher': 'N',
                                     'contact_with_staff_name': '',
                                     'date_project_end': '1970-01-01T00:00:00Z',
                                     'date_project_started': '1970-01-01T00:00:00Z',
                                     'email': '',
                                     'first_name': '',
                                     'follow_on_progress_effort': '',
                                     'foundry_capabilities': '',
                                     'interaction_type': -1,
                                     'last_name': '',
                                     'pi_name': '',
                                     'primary_researcher': 'N',
                                     'primary_researcher_name': '',
                                     'project_duration': '',
                                     'project_leader': 'N',
                                     'project_summary': '',
                                     'relevant_experience': '',
                                     'serial_number': 'P2',
                                     'significance': '',
                                     'status': 'ACTIVE',
                                     'title': '',
                                     'work_description': ''
                                     },
                                 'score': 1.0,
                                 'result_position': 1
                                 }])
        self.assertEqual(data_from_api["total_results_number"],
                         2)

    def test_client_search_bad_pagination(self):
        url = self._configure_url(search_proposals_endpoint)
        self._pre_search_readyness()
        request = {}
        request.update(self._settings)
        request.update({
            "query": "test hi!",
            "page_start": -1
            })
        response = self.client.get(url, request)

        self.assertEqual(status.HTTP_404_NOT_FOUND,
                         response.status_code)

        request.update({
            "query": "test hi!",
            "page_start": 0,
            "num_results": -2
            })
        response = self.client.get(url, request)

        self.assertEqual(status.HTTP_404_NOT_FOUND,
                         response.status_code)

        request.update({
            "query": "test hi!",
            "page_start": 0,
            "num_results": 0
            })
        response = self.client.get(url, request)

        self.assertEqual(status.HTTP_404_NOT_FOUND,
                         response.status_code)

        request.update({
            "query": "test hi!",
            "page_start": 0,
            "num_results": "a"
            })
        response = self.client.get(url, request)

        self.assertEqual(status.HTTP_404_NOT_FOUND,
                         response.status_code)

        request.update({
            "query": "test hi!",
            "page_start": "b",
            "num_results": "1"
            })
        response = self.client.get(url, request)

        self.assertEqual(status.HTTP_404_NOT_FOUND,
                         response.status_code)

        request.update({
            "query": "test hi!",
            "page_start": 2,
            "num_results": 4
            })
        response = self.client.get(url, request)
        self.assertEqual(status.HTTP_200_OK,
                         response.status_code)

        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        self.assertEqual(data_from_api["page_start"],
                         2)
        self.assertEqual(data_from_api["page_end"],
                         6)

        request.update({
            "query": "test hi!",
            "page_start": 2,
            "num_results": 10
            })
        response = self.client.get(url, request)

        self.assertEqual(status.HTTP_200_OK,
                         response.status_code)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        self.assertEqual(data_from_api["page_start"],
                         2)
        self.assertEqual(data_from_api["page_end"],
                         12)

    def test_search_pagination(self):
        url = self._configure_url(search_proposals_endpoint)

        p_1 = Proposal(serial_number="P1")
        p_1.save()

        p_2 = Proposal(serial_number="P2")
        p_2.save()

        p_3 = Proposal(serial_number="P3")
        p_3.save()

        pm = ProposalMetadata(text_content="Houses", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="cat", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="dog house", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_2)
        pm.save()

        pm = ProposalMetadata(text_content="cats house", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_3)
        pm.save()
        self._pre_search_readyness()
        request = {}
        request.update(self._settings)
        request.update({"query": "cat house"})
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        last_search_id = str(SearchResultSummaryArchive.objects.last().id)
        self.assertEqual(data_from_api["archive_id"], last_search_id)
        self.assertEqual(data_from_api["total_results_number"],
                         3)
        self.assertEqual(data_from_api["results_list"][0][
                             "result_object"]["serial_number"], "P1")
        self.assertEqual(data_from_api["results_list"][1][
                             "result_object"]["serial_number"], "P3")
        self.assertEqual(data_from_api["results_list"][2][
                             "result_object"]["serial_number"], "P2")

        request.update({
            "query": "cat house",
            "page_start": 1,
            "num_results": 1
            })
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        self.assertEqual(data_from_api["total_results_number"],
                         3)
        self.assertEqual(data_from_api["results_list"][0][
                             "result_object"]["serial_number"], "P3")
        self.assertEqual(len(data_from_api["results_list"]), 1)

        request.update({
            "query": "cat house",
            "page_start": 1,
            "num_results": 2
            })
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        self.assertEqual(data_from_api["total_results_number"],
                         3)
        self.assertEqual(data_from_api["results_list"][0][
                             "result_object"]["serial_number"], "P3")
        self.assertEqual(data_from_api["results_list"][1][
                             "result_object"]["serial_number"], "P2")
        self.assertEqual(len(data_from_api["results_list"]), 2)

        request.update({
            "query": "cat house",
            "page_start": 1,
            "num_results": 100
            })
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        self.assertEqual(data_from_api["total_results_number"],
                         3)
        self.assertEqual(data_from_api["results_list"][0][
                             "result_object"]["serial_number"], "P3")
        self.assertEqual(data_from_api["results_list"][1][
                             "result_object"]["serial_number"], "P2")
        self.assertEqual(len(data_from_api["results_list"]), 2)


    def test_client_search_results_images(self):
        url = self._configure_url(search_images_endpoint)

        p_1 = ScopeImage(original_route="P1")
        p_1.save()

        p_2 = ScopeImage(original_route="P2")
        p_2.save()

        pm = ScopeImageMetadata(text_content="Houses", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_1)
        pm.save()

        pm = ScopeImageMetadata(text_content="cat", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_1)
        pm.save()

        pm = ScopeImageMetadata(text_content="dog house", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_2)
        pm.save()
        self._pre_search_readyness()
        request = {}
        request.update(self._settings)
        request.update({"query": "cat house"})
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        last_search_id = str(SearchResultSummaryArchive.objects.last().id)
        self.assertEqual(data_from_api["archive_id"], last_search_id)
        self.maxDiff = None
        ids = [str(x.id) for x in SearchResultArchive.objects.all()]
        self.assertEqual(data_from_api["results_list"],
                         [{
                             'archive_id': ids[0],
                             'hit_list': [[1.0, 1.0, 'scrt', 'cat'],
                                          [0.9666666666666667, 1.0, 'scrt', 'Houses']],
                             'result_object': {
                                 'id': p_1.id, 'file_name': '',
                                 'dimensions': 0, 'file_path': 'P1',
                                 'original_route': 'P1', 'bitmap_url': '',
                                 'data_url': 'P1',
                                 'file_metadata': '', 'reviewed': False,
                                 'tags': [], 'suggested_tags': [],
                                 'proposal': None
                                 },
                             'score': 1.0816666666666668,
                             'result_position': 0
                             },
                             {
                                 'archive_id': ids[1],
                                 'hit_list': [[1.0, 1.0, 'scrt', 'dog house']],
                                 'result_object': {
                                     'id': p_2.id,
                                     'dimensions': 0,
                                     'file_name': '', 'original_route': 'P2',
                                     'file_path': 'P2',
                                     'data_url': 'P2',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 1
                                 }])
        self.assertEqual(data_from_api["total_results_number"],
                         2)

    def test_client_search_results_images_with_filter(self):
        url = self._configure_url(search_images_endpoint)

        p_1 = ScopeImage(original_route="P1")
        p_1.save()

        p_2 = ScopeImage(original_route="P2/01.dm3")
        p_2.save()

        p_3 = ScopeImage(original_route="P2/02.dm3")
        p_3.save()

        p_4 = ScopeImage(original_route="P2/03.dm3")
        p_4.save()

        p_5 = ScopeImage(original_route="P2/04.dm3")
        p_5.save()

        p_6 = ScopeImage(original_route="P2/05.dm3")
        p_6.save()

        p_7 = ScopeImage(original_route="P3/a.ser")
        p_7.save()

        p_8 = ScopeImage(original_route="P3/b.ser")
        p_8.save()

        p_9 = ScopeImage(original_route="P3/c.ser")
        p_9.save()

        pm = ScopeImageMetadata(text_content="Houses", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_1)
        pm.save()

        pm = ScopeImageMetadata(text_content="cat", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_1)
        pm.save()

        pm = ScopeImageMetadata(text_content="dog house", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_2)
        pm.save()

        pm = ScopeImageMetadata(text_content="chicken", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_3)
        pm.save()

        pm = ScopeImageMetadata(text_content="mouse house", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_4)
        pm.save()

        pm = ScopeImageMetadata(text_content="monkey house", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_5)
        pm.save()

        pm = ScopeImageMetadata(text_content="pig house", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_6)
        pm.save()

        pm = ScopeImageMetadata(text_content="chicken", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_7)
        pm.save()

        pm = ScopeImageMetadata(text_content="cat", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_8)
        pm.save()

        pm = ScopeImageMetadata(text_content="DEFAULT", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_9)
        pm.save()

        self._pre_search_readyness()
        request = {}
        request.update(self._settings)
        request.update({"query": "house file_path=:=P2"})
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        last_search_id = str(SearchResultSummaryArchive.objects.last().id)
        self.assertEqual(data_from_api["archive_id"], last_search_id)
        self.maxDiff = None
        ids = [str(x.id) for x in SearchResultArchive.objects.all()]

        self.assertEqual(data_from_api["results_list"],
                         [
                             {
                                 'archive_id': ids[0],
                                 'hit_list': [[1.0, 1.0, 'scrt', 'dog house']],
                                 'result_object': {
                                     'id': p_2.id,
                                     'dimensions': 0,
                                     'file_name': '', 'original_route': 'P2/01.dm3',
                                     'file_path': 'P2',
                                     'data_url': 'P2/01.dm3',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 0
                             },
                             {
                                 'archive_id': ids[1],
                                 'hit_list': [[1.0, 1.0, 'scrt', 'mouse house']],
                                 'result_object': {
                                     'id': p_4.id,
                                     'dimensions': 0,
                                     'file_name': '', 'original_route': 'P2/03.dm3',
                                     'file_path': 'P2',
                                     'data_url': 'P2/03.dm3',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 1

                                 },
                             {
                                 'archive_id': ids[2],
                                 'hit_list': [[1.0, 1.0, 'scrt', 'monkey house']],
                                 'result_object': {
                                     'id': p_5.id,
                                     'dimensions': 0,
                                     'file_name': '', 'original_route': 'P2/04.dm3',
                                     'file_path': 'P2',
                                     'data_url': 'P2/04.dm3',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 2

                                 },
                             {
                                 'archive_id': ids[3],
                                 'hit_list': [[1.0, 1.0, 'scrt', 'pig house']],
                                 'result_object': {
                                     'id': p_6.id,
                                     'dimensions': 0,
                                     'file_name': '', 'original_route': 'P2/05.dm3',
                                     'file_path': 'P2',
                                     'data_url': 'P2/05.dm3',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 3
                                 }
                         ])
        self.assertEqual(data_from_api["total_results_number"],
                         4)

        request.update({"query": "cat"})
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        last_search_id = str(SearchResultSummaryArchive.objects.last().id)
        self.assertEqual(data_from_api["archive_id"], last_search_id)
        ids = [str(x.id) for x in SearchResultArchive.objects.all()]

        self.assertEqual(data_from_api["results_list"],
                         [
                             {
                                 'archive_id': ids[1],
                                 'hit_list': [[1.0, 1.0, 'scrt', 'cat']],
                                 'result_object': {
                                     'id': p_1.id,
                                     'dimensions': 0,
                                     'file_name': '', 'original_route': 'P1',
                                     'file_path': 'P1',
                                     'data_url': 'P1',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 0

                                 },
                             {
                                 'archive_id': ids[3],
                                 'hit_list': [[1.0, 1.0, 'scrt', 'cat']],
                                 'result_object': {
                                     'id': p_8.id,
                                     'dimensions': 0,
                                     'file_name': '', 'original_route': 'P3/b.ser',
                                     'file_path': 'P3',
                                     'data_url': 'P3/b.ser',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 1
                             }
                         ])
        self.assertEqual(data_from_api["total_results_number"],
                         2)

        request.update({"query": "cat file_path=:=P3"})
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        last_search_id = str(SearchResultSummaryArchive.objects.last().id)
        self.assertEqual(data_from_api["archive_id"], last_search_id)
        ids = [str(x.id) for x in SearchResultArchive.objects.all()]

        self.assertEqual(data_from_api["results_list"],
                         [
                             {
                                 'archive_id': ids[0],
                                 'hit_list': [[1.0, 1.0, 'scrt', 'cat']],
                                 'result_object': {
                                     'id': p_8.id,
                                     'dimensions': 0,
                                     'file_name': '', 'original_route': 'P3/b.ser',
                                     'file_path': 'P3',
                                     'data_url': 'P3/b.ser',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 0
                             }
                         ])
        self.assertEqual(data_from_api["total_results_number"],
                         1)

        request.update({"query": "file_path=:=P3"})
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        last_search_id = str(SearchResultSummaryArchive.objects.last().id)
        self.assertEqual(data_from_api["archive_id"], last_search_id)
        ids = [str(x.id) for x in SearchResultArchive.objects.all()]

        self.assertEqual(data_from_api["results_list"],
                         [
                            {
                                 'archive_id': ids[0],
                                 'hit_list': [[1.0, 1.0, 'scrt', 'DEFAULT']],
                                 'result_object': {
                                     'id': p_9.id,
                                     'dimensions': 0,
                                     'file_name': '', 'original_route': 'P3/c.ser',
                                     'file_path': 'P3',
                                     'data_url': 'P3/c.ser',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 0
                             }
                         ])
        self.assertEqual(data_from_api["total_results_number"],
                         1)


    def test_client_search_results_papers(self):
        url = self._configure_url(search_papers_endpoint)

        p_1 = Paper(simple_title="P1", text_body="body1",
                    url="http://url.com")
        p_1.save()

        p_2 = Paper(simple_title="P2", text_body="body2")
        p_2.save()

        pm = PaperMetadata(text_content="Houses", score=1.0,
                           metadata_type=PAPER_FROM_TEXT,
                           related_object=p_1)
        pm.save()

        pm = PaperMetadata(text_content="cat", score=1.0,
                           metadata_type=PAPER_FROM_TEXT,
                           related_object=p_1)
        pm.save()

        pm = PaperMetadata(text_content="dog house", score=1.0,
                           metadata_type=PAPER_FROM_TEXT,
                           related_object=p_2)
        pm.save()
        self._pre_search_readyness()
        request = {}
        request.update(self._settings)
        request.update({"query": "cat house"})
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        last_search_id = str(SearchResultSummaryArchive.objects.last().id)
        self.assertEqual(data_from_api["archive_id"], last_search_id)
        self.maxDiff = None
        ids = [str(x.id) for x in SearchResultArchive.objects.all()]
        self.assertEqual(data_from_api["results_list"],
                         [{
                             'archive_id': ids[0],
                             'hit_list': [[1.0, 1.0, 'spt', 'cat'],
                                          [0.9666666666666667, 1.0, 'spt', 'Houses']],
                             'result_object': {
                                 'simple_title': p_1.simple_title,
                                 'file_name': '',
                                 'authors': [],
                                 'title': None,
                                 'citation': '',
                                 'pub_date': None,
                                 'pub_issue': '',
                                 'pub_pages': '',
                                 'pub_title': '',
                                 'url': "http://url.com"
                                 },
                             'score': 1.0816666666666668,
                             'result_position': 0
                             },
                             {
                                 'archive_id': ids[1],
                                 'hit_list': [[1.0, 1.0, 'spt', 'dog house']],
                                 'result_object': {
                                     'simple_title': p_2.simple_title,
                                     'file_name': '',
                                     'authors': [],
                                     'title': None,
                                     'citation': '',
                                     'pub_date': None,
                                     'pub_issue': '',
                                     'pub_pages': '',
                                     'pub_title': '',
                                     'url': ''
                                     },
                                 'score': 0.55,
                                 'result_position': 1
                                 }])
        self.assertEqual(data_from_api["total_results_number"],
                         2)

    def test_cache(self):
        url = self._configure_url(search_images_endpoint)

        p_1 = ScopeImage(original_route="P1")
        p_1.save()

        p_2 = ScopeImage(original_route="P2")
        p_2.save()

        pm = ScopeImageMetadata(text_content="Houses", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_1)
        pm.save()

        pm = ScopeImageMetadata(text_content="cat", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_1)
        pm.save()

        pm = ScopeImageMetadata(text_content="dog house", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_2)
        pm.save()

        self.assertEqual(len(SearchResultSummary.objects.all()), 0)
        self._pre_search_readyness()
        request = {}
        request.update(self._settings)
        request.update({"query": "cat house"})
        response = self.client.get(url, request)

        self.assertEqual(len(SearchResultSummary.objects.all()), 1)
        self.assertEqual(len(SearchResult.objects.all()), 2)

        data_from_api = json.loads(response.content.decode('utf-8'))[0]
        self.maxDiff = None
        ids = [str(x.id)
               for x in SearchResultArchive.objects.all().order_by("id")]
        last_search_id = str(SearchResultSummaryArchive.objects.last().id)
        self.assertEqual(data_from_api["archive_id"], last_search_id)
        self.assertEqual(data_from_api["results_list"],
                         [{
                             'hit_list': [[1.0, 1.0, 'scrt', 'cat'],
                                          [0.9666666666666667, 1.0, 'scrt', 'Houses']],
                             "archive_id": ids[-2],
                             'result_object': {
                                 'id': p_1.id, 'file_name': '',
                                 'dimensions': 0, 'file_path': 'P1',
                                 'original_route': 'P1', 'bitmap_url': '',
                                 'data_url': 'P1',
                                 'file_metadata': '', 'reviewed': False,
                                 'tags': [], 'suggested_tags': [],
                                 'proposal': None
                                 },
                             'score': 1.0816666666666668,
                             'result_position': 0
                             },
                             {
                                 'hit_list': [[1.0, 1.0, 'scrt', 'dog house']],
                                 "archive_id": ids[-1],
                                 'result_object': {
                                     'id': p_2.id,
                                     'dimensions': 0, 'file_path': 'P2',
                                     'file_name': '', 'original_route': 'P2',
                                     'data_url': 'P2',
                                     'bitmap_url': '', 'file_metadata': '',
                                     'reviewed': False, 'tags': [],
                                     'suggested_tags': [],
                                     'proposal': None
                                     },
                                 'score': 0.55,
                                 'result_position': 1
                                 }])
        self.assertEqual(data_from_api["total_results_number"],
                         2)

        self.assertFalse(data_from_api["cached"])

        response = self.client.get(url, request)
        cached_search_data = json.loads(response.content.decode('utf-8'))[0]
        last_search_id = str(SearchResultSummaryArchive.objects.last().id)
        self.assertEqual(cached_search_data["archive_id"], last_search_id)
        self.assertEqual(len(SearchResultSummary.objects.all()), 1)
        self.assertTrue(cached_search_data["cached"])

        cached_search_data["search_latency_s"] = 0

        data_from_api["cached"] = True
        data_from_api["archive_id"] = last_search_id
        data_from_api["search_latency_s"] = 0
        data_from_api["results_list"][0]["archive_id"] = "N/A"
        data_from_api["results_list"][1]["archive_id"] = "N/A"
        data_from_api["date"] = cached_search_data["date"]

        self.assertAlmostEqualDeep(cached_search_data, data_from_api)
        with self.settings(SEARCH_CACHE_MAX_AGE_S=1):
            import time
            time.sleep(2)
            response = self.client.get(url, request)
            cached_search_data = json.loads(response.content.decode('utf-8'))[0]
            self.assertEqual(len(SearchResultSummary.objects.all()), 2)
            self.assertFalse(cached_search_data["cached"])

    def test_log(self):
        self._do_login_token(is_admin=True, username="user2")

        url = self._configure_url(search_archive_entries_endpoint)

        srs = SearchResultSummaryArchive(date=datetime.datetime(year=2010,
                                                                month=1,
                                                                day=1,
                                                                tzinfo=datetime.timezone.utc),
                                         search_date=datetime.datetime(year=2011,
                                                                       month=1,
                                                                       day=1,
                                                                       tzinfo=datetime.timezone.utc),
                                         page_start=2, page_end=4,
                                         query="my query",
                                         search_latency_s=1.34,
                                         total_results_number=5,
                                         query_type="query_t",
                                         session_id="my Session")
        hls = HitListScorer()
        hls.register_mark_properties("si", 1.0, -2.0, 4.0)
        hls.register_mark_properties("pt", 2.0, -3.0, 5.0)
        srs.set_hit_list_scorer(hls)
        srs.save()
        srs.search_date = datetime.datetime(year=2011, month=1, day=1, tzinfo=datetime.timezone.utc)
        srs.save()
        sr = SearchResultArchive(score=0.5, result_position=5,
                                 user_clicked=3,
                                 search_results_summary_archive=srs)

        p1 = Proposal(serial_number="1234")
        p1.save()
        sr.set_result_obj(p1)
        sr.set_hit_list([(0.1, ProposalMetadata(score=2.2,
                                                metadata_type="pt",
                                                text_content="etch")),
                         (0.2, ProposalMetadata(score=2.1, metadata_type="pt",
                                                text_content="material")),
                         (0.3, ProposalMetadata(score=1.2, metadata_type="si",
                                                text_content="other"))])
        srs.set_results([sr])
        sr.save()
        self._pre_search_readyness()
        request = {}
        request.update(self._settings)
        response = self.client.get(url, request)
        data_from_api = json.loads(response.content.decode('utf-8'))
        self.maxDiff = None

        candidate_data = json.loads(
            """[{"query":"my query","date":"2010-01-01T00:00:00Z",
            "search_date": "2011-01-01T00:00:00Z",
            "query_type":"query_t",
            "search_latency_s":1.34,
            "page_start":2,"page_end":4,
            "cached": false,
            "search_timed_out": false,
            "session_id": "my Session",
            "search_weights":
                {"si":{"weight":1.0,"min_input":-2.0,"max_input":4.0},
                "pt":{"weight":2.0,"min_input":-3.0,"max_input":5.0}},
            "total_results_number":5,
            "results_list":[{
            "id":""" + str(sr.id) + """,
            "user_clicked": 3,
            "result_object":["proposal","1234"],
            "score":0.5,
            "result_position":5,
            "hit_list":[[0.1, 2.2,"pt","etch"],[0.2, 2.1,"pt","material"],
            [0.3, 1.2,"si","other"]]
            }]}]
            """)
        self.assertEqual(data_from_api, candidate_data)

    def test_clicked_result(self):
        srs = SearchResultSummaryArchive(date=datetime.datetime(year=2010,
                                                                month=1,
                                                                day=1,
                                                                tzinfo=datetime.timezone.utc),
                                         search_date=datetime.datetime(year=2011,
                                                                       month=1,
                                                                       day=1,
                                                                       tzinfo=datetime.timezone.utc),
                                         page_start=2, page_end=4,
                                         query="my query",
                                         search_latency_s=1.34,
                                         total_results_number=5,
                                         query_type="query_t",
                                         session_id="my Session")
        hls = HitListScorer()
        hls.register_mark_properties("si", 1.0, -2.0, 4.0)
        hls.register_mark_properties("pt", 2.0, -3.0, 5.0)
        srs.set_hit_list_scorer(hls)
        srs.save()
        srs.search_date = datetime.datetime(year=2011, month=1, day=1, tzinfo=datetime.timezone.utc)
        srs.save()
        sr = SearchResultArchive(score=0.5, result_position=5,
                                 user_clicked=3,
                                 search_results_summary_archive=srs)

        p1 = Proposal(serial_number="1234")
        p1.save()
        sr.set_result_obj(p1)
        sr.set_hit_list([(0.1, ProposalMetadata(score=2.2,
                                                metadata_type="pt",
                                                text_content="etch")),
                         (0.2, ProposalMetadata(score=2.1, metadata_type="pt",
                                                text_content="material")),
                         (0.3, ProposalMetadata(score=1.2, metadata_type="si",
                                                text_content="other"))])
        srs.set_results([sr])
        sr.save()
        self._pre_search_readyness()
        url = self._configure_url(search_archive_result_clicked_endpoint, sr.id)
        response = self.client.post(url)
        data_from_api = json.loads(response.content.decode('utf-8'))
        self.maxDiff = None

        candidate_data = json.loads(
            """{
            "id":""" + str(sr.id) + """,
            "user_clicked": 4,
            "result_object":["proposal","1234"],
            "score":0.5,
            "result_position":5,
            "hit_list":[[0.1, 2.2,"pt","etch"],[0.2, 2.1,"pt","material"],
            [0.3, 1.2,"si","other"]]
            }
            """)
        self.assertEqual(data_from_api, candidate_data)
        n_srs = SearchResultArchive.objects.get(id=sr.id)
        self.assertEqual(n_srs.user_clicked, 4)

    def assertAlmostEqualDeep(self, d1, d2):
        self.assertEqual(type(d1), type(d2))
        if type(d1) is dict:
            for (key) in d1:
                self.assertTrue(key in d2, "Missing key in D2")
                self.assertAlmostEqualDeep(d1[key], d2[key])
            for key in d2:
                self.assertTrue(key in d1, "Missing key in D1")
        elif type(d1) is list:
            self.assertEqual(len(d1), len(d2))
            for (s1, s2) in zip(d1, d2):
                self.assertAlmostEqualDeep(s1, s2)
        elif type(d1) is float:
            self.assertAlmostEqual(d1, d2)
        else:
            self.assertEqual(d1, d2)


class TestSearchIndexAPI(TestSearchAPI):
    def setUp(self):
        super(TestSearchIndexAPI, self).setUp()
        self._settings = dict(SEARCH_CONFIG_INDEX_SEARCH=True, SEARCH_CONFIG_DEBUG_DATA=True)

    def tearDown(self):
        super(TestSearchIndexAPI, self).tearDown()

    def _pre_search_readyness(self):
        for class_type in [ProposalMetadataIndex, ScopeImageMetadataIndex,
                           PaperMetadataIndex]:
            class_type.create_index()
