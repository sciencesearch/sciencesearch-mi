import time

from django.test import TestCase
from metadataserver.apps.searchengine.algebra.predicates import TextPredicate
from metadataserver.apps.searchengine.algebra.query import QueryParser
from metadataserver.apps.searchengine.algebra.search import SearchableMark, HitListScorer


class FakeSM(SearchableMark):

    @classmethod
    def reg_obj(cls, object_id, obj):
        if not getattr(cls, "objects", None):
            cls.clear_objs()
        cls.objects[object_id] = obj

    @classmethod
    def get_obj(cls, object_id):
        if not getattr(cls, "objects", None):
            cls.clear_objs()
        if object_id not in cls.objects:
            return None
        return cls.objects[object_id]

    @classmethod
    def clear_objs(cls):
        cls.objects = {}

    def __init__(self, score=None, object_id=None, type_id=None, predicate=None,
                 pointed_object=None, text_content=None):
        self._score = score
        self._object_id = object_id
        self._type_id = type_id
        self._predicate = predicate
        self._pointed_object = pointed_object
        if self._predicate:
            self._text_content = self._predicate
        if text_content:
            self._predicate = text_content
            self._text_content = text_content
        FakeSM.reg_obj(self._object_id, self._pointed_object)

    def value(self):
        return dict(score=self._score, object_id=self._object_id,
                    type_id=self._type_id, predicate=self._predicate,
                    pointed_object=self._pointed_object,
                    text_content=self._text_content)

    def get_score(self):
        return self._score

    def get_object_id(self):
        return self._object_id

    def get_type_id(self):
        return self._type_id

    def get_predicate(self):
        return self._predicate

    def get_pointed_object(self):
        return self._pointed_object

    def __repr__(self):
        return str(self._predicate)

    @classmethod
    def get_pointed_objs_list(cls, list_ids, filter_list=None):
        obj_dict = {}
        for object_id in list_ids:
            obj = FakeSM.get_obj(object_id)
            if obj:
                obj_dict[object_id] = obj
        return obj_dict


class FakeSearchableMark(FakeSM):
    def get_default_search_object_list(self):
        return self._default_objs

    def set_default_search_object_list(self, objs):
        self._default_objs = objs


class FakeSearchableMarkSlow(FakeSearchableMark):

    def set_wait_time(self, wait_time):
        self._wait_time = wait_time

    def search_worker(self, *args, **kwargs):
        time.sleep(self._wait_time)

        (scores_by_id, extra_items) = super(FakeSearchableMarkSlow,
                                            self).search_worker(*args,
                                                                **kwargs)

        return (scores_by_id, extra_items)


class TestSearch(TestCase):
    def setUp(self):
        FakeSM.clear_objs()

    def test_flatten_by_score(self):
        adict = {
            "id1": 2.0,
            "id2": 1.5,
            "id3": 1.7
            }

        self.assertEqual(SearchableMark.flatten_by_score(adict, do_short=True,
                                                         more_to_less=True),
                         [(2.0, "id1"),
                          (1.7, "id3"),
                          (1.5, "id2")])

        self.assertEqual(SearchableMark.flatten_by_score(adict, do_short=True,
                                                         more_to_less=False),
                         [(1.5, "id2"),
                          (1.7, "id3"),
                          (2.0, "id1")])

        self.assertEqual(sorted(SearchableMark.flatten_by_score(adict,
                                                                do_short=False,
                                                                more_to_less=False)),
                         sorted([(1.5, "id2"),
                                 (1.7, "id3"),
                                 (2.0, "id1")]))

    def test_flatten_by_score_extra_values(self):
        adict = {
            "id1": 2.0,
            "id2": 1.5,
            "id3": 1.7
            }

        extra_items = {
            "id1": "a",
            "id2": [1, 2, 3]
            }
        self.assertEqual(SearchableMark.flatten_by_score(adict, do_short=True,
                                                         more_to_less=True,
                                                         extra_items=extra_items
                                                         ),
                         [(2.0, "id1", "a"),
                          (1.7, "id3"),
                          (1.5, "id2", 1, 2, 3)])

        self.assertEqual(SearchableMark.flatten_by_score(adict, do_short=True,
                                                         more_to_less=False,
                                                         extra_items=extra_items),
                         [(1.5, "id2", 1, 2, 3),
                          (1.7, "id3"),
                          (2.0, "id1", "a")])

        self.assertEqual(sorted(SearchableMark.flatten_by_score(adict,
                                                                do_short=False,
                                                                more_to_less=False,
                                                                extra_items=extra_items)),
                         sorted([(1.5, "id2", 1, 2, 3),
                                 (1.7, "id3"),
                                 (2.0, "id1", "a")]))

    def test_group_hits_by_id(self):
        fs1 = FakeSM(3.0, "id1", "T1", "predicate1")
        fs2 = FakeSM(4.0, "id1", "T1", "predicate2")
        fs3 = FakeSM(2.0, "id2", "T2", "predicate3")
        hits_list = [(0.96, fs1),
                     (0.95, fs2),
                     (0.92, fs3)]

        self.assertEqual(SearchableMark.group_hits_by_id(hits_list),
                         {
                             "id1": [(0.96, fs1), (0.95, fs2)],
                             "id2": [(0.92, fs3)]
                             }
                         )

    def test_calc_hit_tuple_score(self):
        fs1 = FakeSM(3.0, "id1", "T1", "predicate1")
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties("T1", weight=0.2,
                                                  min_input=-10.0,
                                                  max_input=10.0)
        hit_tuple = (0.96, fs1)
        self.assertEqual(SearchableMark.calc_hit_tuple_score(
            hit_tuple,
            hist_list_scorer),
            1.248)

        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties("T1", weight=0.2,
                                                  min_input=0.0,
                                                  max_input=20.0)
        hit_tuple = (0.96, fs1)
        self.assertAlmostEqual(SearchableMark.calc_hit_tuple_score(
            hit_tuple,
            hist_list_scorer,
            min_individual_score=2.0,
            max_individual_score=4.0),
            0.4416)

    def test_calc_aggregated_score(self):
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties("T1", weight=0.2,
                                                  min_input=0.0,
                                                  max_input=20.0)
        hist_list_scorer.register_mark_properties("T2", weight=2.0,
                                                  min_input=2.0,
                                                  max_input=20.0)

        fs1 = FakeSM(3.0, "id1", "T1", "predicate1")
        fs2 = FakeSM(4.0, "id1", "T2", "predicate2")
        hits_list = [(0.96, fs1),
                     (0.95, fs2)]
        self.assertAlmostEqual(SearchableMark.calc_aggregated_score(
            hits_list,
            hist_list_scorer,
            min_individual_score=2.0,
            max_individual_score=4.0),
            0.4416 + 4.222222222)

    def test_rank_hits(self):
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties("T1", weight=0.2,
                                                  min_input=0.0,
                                                  max_input=20.0)
        hist_list_scorer.register_mark_properties("T2", weight=2.0,
                                                  min_input=2.0,
                                                  max_input=20.0)

        fs1 = FakeSM(3.0, "id1", "T1", "predicate1", "o1")
        fs2 = FakeSM(4.0, "id1", "T1", "predicate2", "o1")
        fs3 = FakeSM(2.0, "id2", "T2", "predicate3", "o2")
        hits_list = [(0.96, fs1),
                     (0.95, fs2),
                     (0.92, fs3)]
        self.assertEqual(SearchableMark.rank_hits(hits_list,
                                                  hist_list_scorer,
                                                  min_individual_score=2.0,
                                                  max_individual_score=4.0),
                         [(3.68, "id2", "o2"),
                          (0.4416 + 0.456, "id1", "o1")
                          ])

    def test_rank_hits_debug_data(self):
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties("T1", weight=0.2,
                                                  min_input=0.0,
                                                  max_input=20.0)
        hist_list_scorer.register_mark_properties("T2", weight=2.0,
                                                  min_input=2.0,
                                                  max_input=20.0)

        fs1 = FakeSM(3.0, "id1", "T1", "predicate1", "o1")
        fs2 = FakeSM(4.0, "id1", "T1", "predicate2", "o1")
        fs3 = FakeSM(2.0, "id2", "T2", "predicate3", "o2")
        hits_list = [(0.96, fs1),
                     (0.95, fs2),
                     (0.92, fs3)]
        self.assertEqual(SearchableMark.rank_hits(hits_list,
                                                  hist_list_scorer,
                                                  min_individual_score=2.0,
                                                  max_individual_score=4.0,
                                                  debug_data=True),
                         [(3.68, "id2", "o2", [(0.92, fs3)]),
                          (0.4416 + 0.456, "id1", "o1", [(0.96, fs1), (0.95, fs2)])
                          ])

    def test_search(self):
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties("T1", weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)
        hist_list_scorer.register_mark_properties("T2", weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)

        fs1 = FakeSM(1.0, "id1", "T1", TextPredicate("Houses"),
                     pointed_object="o1")
        fs2 = FakeSM(1.0, "id1", "T1", TextPredicate("cat"), pointed_object="o1")
        fs3 = FakeSM(1.0, "id2", "T2", TextPredicate("dog house"),
                     pointed_object="o2")

        sm = SearchableMark()
        query = QueryParser.parse("cat house")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)

        self.assertEqual(results,
                         [(1.9666666666666668, "id1", "o1"),
                          (1.0, "id2", "o2")])
        self.assertEqual(count, 2)

        query = QueryParser.parse("dog houses")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)

        self.assertEqual(results,
                         [(1.9666666666666668, "id2", "o2"),
                          (1.0, "id1", "o1")])
        self.assertEqual(count, 2)

        query = QueryParser.parse("horse stables")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)
        self.assertEqual(results,
                         [])
        self.assertEqual(count, 0)

    def test_search_serial_vs_parallel(self):
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties("T1", weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)
        hist_list_scorer.register_mark_properties("T2", weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)

        fs1 = FakeSM(1.0, "id1", "T1", TextPredicate("Houses"),
                     pointed_object="o1")
        fs2 = FakeSM(1.0, "id1", "T1", TextPredicate("cat"), pointed_object="o1")
        fs3 = FakeSM(1.0, "id2", "T2", TextPredicate("dog house"),
                     pointed_object="o2")

        sm = FakeSearchableMark()
        sm.set_default_search_object_list([fs1, fs2, fs3])
        query = QueryParser.parse("cat house")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)
        self.assertEqual(results,
                         [(1.9666666666666668, "id1", "o1"),
                          (1.0, "id2", "o2")])
        self.assertEqual(count, 2)

        self.assertEqual((sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0) + (False,)),
                         sm.search_parallel(query,
                                            hist_list_scorer,
                                            min_individual_score=0.0,
                                            max_individual_score=1.0,
                                            num_workers=2))

        query = QueryParser.parse("dog houses")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)
        self.assertEqual(results,
                         [(1.9666666666666668, "id2", "o2"),
                          (1.0, "id1", "o1")])
        self.assertEqual(count, 2)

        self.assertEqual((sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0) + (False,)),
                         sm.search_parallel(query,
                                            hist_list_scorer,
                                            min_individual_score=0.0,
                                            max_individual_score=1.0,
                                            num_workers=2))

        query = QueryParser.parse("horse stables")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)
        self.assertEqual(results,
                         [])
        self.assertEqual(count, 0)

        self.assertEqual((sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0) + (False,)),
                         sm.search_parallel(query,
                                            hist_list_scorer,
                                            min_individual_score=0.0,
                                            max_individual_score=1.0,
                                            num_workers=2))

    def test_search_streaming(self):
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties("T1", weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)
        hist_list_scorer.register_mark_properties("T2", weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)

        fs1 = FakeSM(1.0, "id1", "T1", TextPredicate("Houses"),
                     pointed_object="o1")
        fs2 = FakeSM(1.0, "id1", "T1", TextPredicate("cat"), pointed_object="o1")
        fs3 = FakeSM(1.0, "id2", "T2", TextPredicate("dog house"),
                     pointed_object="o2")

        sm = FakeSearchableMark()
        sm.set_default_search_object_list([fs1, fs2, fs3])
        query = QueryParser.parse("cat house")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)

        self.assertEqual(results,
                         [(1.9666666666666668, "id1", "o1"),
                          (1.0, "id2", "o2")])
        self.assertEqual(count, 2)

        query = QueryParser.parse("dog houses")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   object_list=[fs1, fs2, fs3],
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)

        self.assertEqual(results,
                         [(1.9666666666666668, "id2", "o2"),
                          (1.0, "id1", "o1")])
        self.assertEqual(count, 2)

        query = QueryParser.parse("horse stables")
        count, results, timeout = sm.search_parallel(query,
                                                     hist_list_scorer,
                                                     min_individual_score=0.0,
                                                     max_individual_score=1.0,
                                                     num_workers=2)
        self.assertEqual(results, [])
        self.assertEqual(count, 0)
        self.assertEqual(timeout, False)

    def test_search_timeout(self):
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties("T1", weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)
        hist_list_scorer.register_mark_properties("T2", weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)

        fs1 = FakeSM(1.0, "id1", "T1", TextPredicate("Houses"),
                     pointed_object="o1")
        fs2 = FakeSM(1.0, "id1", "T1", TextPredicate("cat"), pointed_object="o1")
        fs3 = FakeSM(1.0, "id2", "T2", TextPredicate("dog house"),
                     pointed_object="o2")

        sm = FakeSearchableMarkSlow()
        sm.set_wait_time(3)
        sm.set_default_search_object_list([fs1.value(), fs2.value(),
                                           fs3.value()])
        query = QueryParser.parse("cat house")
        count, results, timeout = sm.search_parallel(query,
                                                     hist_list_scorer,
                                                     min_individual_score=0.0,
                                                     max_individual_score=1.0,
                                                     num_workers=2,
                                                     worker_timeout=1)

        self.assertEqual(results, [])
        self.assertEqual(count, 0)
        self.assertEqual(timeout, True)

        query = QueryParser.parse("dog houses")
        count, results, timeout = sm.search_parallel(query,
                                                     hist_list_scorer,
                                                     min_individual_score=0.0,
                                                     max_individual_score=1.0,
                                                     num_workers=2,
                                                     worker_timeout=1)

        self.assertEqual(results, [])
        self.assertEqual(count, 0)
        self.assertEqual(timeout, True)

        query = QueryParser.parse("horse stables")
        count, results, timeout = sm.search_parallel(query,
                                                     hist_list_scorer,
                                                     min_individual_score=0.0,
                                                     max_individual_score=1.0,
                                                     num_workers=2,
                                                     worker_timeout=1)
        self.assertEqual(results, [])
        self.assertEqual(count, 0)
        self.assertEqual(timeout, True)
