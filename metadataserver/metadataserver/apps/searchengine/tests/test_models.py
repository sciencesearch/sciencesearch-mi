import datetime

from django.test import TestCase
from django.utils import timezone

from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.searchengine.algebra.predicates import TextPredicate
from metadataserver.apps.searchengine.algebra.query import QueryParser
from metadataserver.apps.searchengine.configuration import (
    PROPOSAL_FROM_TEXT, SCOPEIMAGE_FROM_PROPOSAL_TEXT
    )
from metadataserver.apps.searchengine.models import (
    ProposalMetadata, ScopeImageMetadata, SearchResult, SearchResultSummary, SearchResultSummaryArchive,
    SearchWeightsConfig, HitListScorer, ProposalMetadataIndex
    )
from metadataserver.apps.searchengine.tests.test_configuration import FakeSettingsObject


class TestSearchResults(TestCase):

    def test_create(self):
        si = ScopeImage(original_route="F1")

        si.save()
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1234",
                                  search_latency_s=1.0,
                                  total_results_number=1)
        srs.save()
        sr = SearchResult(score=0.1,
                          search_results_summary=srs)

        sr.set_result_obj(si)
        sr.save()

        nsr = SearchResult.objects.get(id=sr.id)

        self.assertEqual(nsr.get_result_obj(), si)
        self.assertEqual(nsr.search_results_summary, srs)

    def test_create_proposal(self):
        si = Proposal(serial_number="1234")

        si.save()
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1234",
                                  search_latency_s=1.0,
                                  total_results_number=1)
        srs.save()
        sr = SearchResult(score=0.1,
                          search_results_summary=srs)

        sr.set_result_obj(si)
        sr.save()
        # TODO(gonzalo): continue with this

        nsr = SearchResult.objects.get(id=sr.id)

        self.assertEqual(nsr.get_result_obj(), si)
        self.assertEqual(nsr.search_results_summary, srs)


class TestSearchResultsSummary(TestCase):

    def test_create(self):
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1234",
                                  search_latency_s=1.0,
                                  total_results_number=1)

        hls = HitListScorer()
        config_dict = dict(m1=dict(weight=1.0, min_input=-1, max_input=1),
                           m2=dict(weight=2.0, min_input=-2, max_input=2))

        for (key, value) in config_dict.items():
            hls.register_mark_properties(key, value["weight"],
                                         value["min_input"],
                                         value["max_input"])
        srs.set_hit_list_scorer(hls)
        srs.save()

        nsrs = SearchResultSummary.objects.get(id=srs.id)
        self.assertEqual(nsrs.hit_list_scorer_as_json, config_dict)

    def test_get_cached_results(self):
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1234",
                                  search_latency_s=1.0,
                                  total_results_number=1)
        srs.save()
        sr2 = SearchResult(score=0.5, result_position=1,
                           search_results_summary=srs)
        sr2.save()

        sr1 = SearchResult(score=0.3, result_position=0,
                           search_results_summary=srs)
        sr1.save()

        srs_n = SearchResultSummary.objects.get(id=srs.id)

        self.assertEqual([x.id for x in srs_n.get_cached_results()],
                         [sr1.id, sr2.id])

        self.assertEqual([x.score for x in srs_n.get_cached_results()],
                         [0.3, 0.5])

        self.assertEqual([x.score for x in srs_n.get_cached_results(1)],
                         [0.5])

        self.assertEqual([x.score for x in srs_n.get_cached_results(1, 2)],
                         [0.5])

        self.assertEqual([x.score for x in srs_n.get_cached_results(0, 1)],
                         [0.3])

    def test_save_results(self):
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1234",
                                  search_latency_s=1.0,
                                  total_results_number=1)

        sr2 = SearchResult(score=0.5, result_position=1,
                           search_results_summary=srs)

        sr1 = SearchResult(score=0.3, result_position=0,
                           search_results_summary=srs)

        srs.set_results([sr1, sr2])
        srs.save_for_cache()

        srs_n = SearchResultSummary.objects.get(id=srs.id)

        self.assertEqual([x.id for x in srs_n.get_cached_results()],
                         [sr1.id, sr2.id])

        self.assertEqual([x.score for x in srs_n.get_cached_results()],
                         [0.3, 0.5])

        self.assertEqual([x.score for x in srs_n.get_cached_results(1)],
                         [0.5])

        self.assertEqual([x.score for x in srs_n.get_cached_results(1, 2)],
                         [0.5])

        self.assertEqual([x.score for x in srs_n.get_cached_results(0, 1)],
                         [0.3])


class TestSearchResultsSummaryArchive(TestCase):

    def test_create(self):
        self.assertEqual(len(SearchResultSummaryArchive.objects.all()), 0)
        self.assertEqual(len(SearchResultSummary.objects.all()), 0)
        srs = SearchResultSummaryArchive(date=timezone.now(),
                                         query="1234",
                                         search_latency_s=1.0,
                                         total_results_number=1)
        srs.save()
        self.assertEqual(len(SearchResultSummaryArchive.objects.all()), 1)
        self.assertEqual(len(SearchResultSummary.objects.all()), 0)


class TestProposalMetadata(TestCase):

    def test_create(self):
        p = Proposal(serial_number="ABC")
        p.save()
        pm = ProposalMetadata(text_content="MyText", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p)
        pm.save()

        pm_n = ProposalMetadata.objects.get(id=pm.id)

        self.assertEqual(pm_n.get_pointed_object(), p)
        self.assertEqual(pm_n.get_object_id(), p.serial_number)
        self.assertEqual(pm_n.get_type_id(), PROPOSAL_FROM_TEXT)
        self.assertEqual(pm_n.get_predicate(), TextPredicate("MyText"))
        self.assertEqual(pm_n.get_tuple(), (1.0, PROPOSAL_FROM_TEXT,
                                            "MyText"))

    def test_simple_search(self):
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties(PROPOSAL_FROM_TEXT,
                                                  weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)

        p_1 = Proposal(serial_number="P1")
        p_1.save()

        p_2 = Proposal(serial_number="P2")
        p_2.save()

        pm = ProposalMetadata(text_content="Houses", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="cat", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="dog house", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_2)
        pm.save()

        sm = ProposalMetadata()
        query = QueryParser.parse("cat house")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)

        self.assertEqual(results,
                         [(1.9666666666666668, p_1.serial_number, p_1),
                          (1.0, p_2.serial_number, p_2)])
        self.assertEqual(count, 2)

        query = QueryParser.parse("dog houses")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)

        self.assertEqual(results,
                         [(1.9666666666666668, p_2.serial_number, p_2),
                          (1.0, p_1.serial_number, p_1)])
        self.assertEqual(count, 2)

        query = QueryParser.parse("horse stables")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)
        self.assertEqual(results, [])
        self.assertEqual(count, 0)

    def test_simple_search_images(self):
        hist_list_scorer = HitListScorer()
        hist_list_scorer.register_mark_properties(SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                                  weight=1.0,
                                                  min_input=0.0,
                                                  max_input=1.0)
        p_1 = ScopeImage(original_route="P1")
        p_1.save()

        p_2 = ScopeImage(original_route="P2")
        p_2.save()

        pm = ScopeImageMetadata(text_content="Houses", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_1)
        pm.save()

        pm = ScopeImageMetadata(text_content="cat", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_1)
        pm.save()

        pm = ScopeImageMetadata(text_content="dog house", score=1.0,
                                metadata_type=SCOPEIMAGE_FROM_PROPOSAL_TEXT,
                                related_object=p_2)
        pm.save()

        sm = ScopeImageMetadata()
        query = QueryParser.parse("cat house")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)

        self.assertEqual(results,
                         [(1.9666666666666668, p_1.id, p_1),
                          (1.0, p_2.id, p_2)])

        query = QueryParser.parse("dog houses")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)

        self.assertEqual(results,
                         [(1.9666666666666668, p_2.id, p_2),
                          (1.0, p_1.id, p_1)])

        query = QueryParser.parse("horse stables")
        count, results = sm.search(query,
                                   hist_list_scorer,
                                   min_individual_score=0.0,
                                   max_individual_score=1.0)
        self.assertEqual(results, [])


class TestSearchWeightsConfig(TestCase):

    def test_default(self):
        sc = SearchWeightsConfig().get_config()
        self.assertEqual(SearchWeightsConfig().get_config().get_settings(),
                         dict(PT=dict(WEIGHT=1.0,
                                      MIN_INPUT=-10.0,
                                      MAX_INPUT=10.0),
                              SCRT=dict(WEIGHT=1.0,
                                        MIN_INPUT=-10.0,
                                        MAX_INPUT=10.0),
                              IMFS=dict(WEIGHT=1.0,
                                        MIN_INPUT=-10.0,
                                        MAX_INPUT=10.0),
                              SPT=dict(WEIGHT=1.0,
                                       MIN_INPUT=-10.0,
                                       MAX_INPUT=10.0),
                              FILT=dict(WEIGHT=1.0,
                                        MIN_INPUT=-10.0,
                                        MAX_INPUT=10.0),
                              SCDL=dict(WEIGHT=1.0,
                                        MIN_INPUT=0.5,
                                        MAX_INPUT=1.0)))
        self.assertEqual(sc.hls.get_mark_properties("PT"),
                         (1.0,
                          -10.0,
                          10.0))
        self.assertEqual(sc.hls.get_mark_properties("pt"),
                         (1.0,
                          -10.0,
                          10.0))

    def test_hls(self):
        fs = FakeSettingsObject({
                                    "SEARCH_METADATA_PT_WEIGHT": 2.0,
                                    "SEARCH_METADATA_PT_MIN_INPUT": -1.0
                                })
        sc = SearchWeightsConfig().get_config(custom_settings=fs)
        self.assertEqual(sc.hls.get_mark_properties("PT"),
                         (2.0,
                          -1.0,
                          10.0))


class TestIndexObjects(TestCase):

    def test_create_index(self):
        p_1 = Proposal(serial_number="P1")
        p_1.save()

        p_2 = Proposal(serial_number="P2")
        p_2.save()

        pm = ProposalMetadata(text_content="Houses", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="cat", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="dog", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_2)
        pm.save()

        pm = ProposalMetadata(text_content="Houses", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_2)
        pm.save()

        ProposalMetadataIndex.create_index()

        self.assertEqual(ProposalMetadataIndex.objects.count(), 3)

        self.assertEqual(ProposalMetadataIndex.objects.get(text_content=
                                                           "Houses").metadata_type,
                         PROPOSAL_FROM_TEXT)

        self.assertEqual(ProposalMetadataIndex.objects.get(text_content=
                                                           "dog").metadata_type,
                         PROPOSAL_FROM_TEXT)
        self.assertEqual(ProposalMetadataIndex.objects.get(text_content=
                                                           "cat").metadata_type,
                         PROPOSAL_FROM_TEXT)

    def test_recreate(self):
        p_1 = Proposal(serial_number="P1")
        p_1.save()

        p_2 = Proposal(serial_number="P2")
        p_2.save()

        pm = ProposalMetadata(text_content="Houses", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="cat", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_1)
        pm.save()

        pm = ProposalMetadata(text_content="dog", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_2)
        pm.save()

        pm = ProposalMetadata(text_content="Houses", score=1.0,
                              metadata_type=PROPOSAL_FROM_TEXT,
                              related_object=p_2)
        pm.save()

        ProposalMetadataIndex.create_index()

        index_hits = [(0.5, ProposalMetadataIndex.objects.get(text_content=
                                                              "Houses")),
                      (1.0, ProposalMetadataIndex.objects.get(text_content=
                                                              "dog"))
                      ]

        good_hits = ProposalMetadataIndex().recreate_metadata(index_hits)

        self.assertEqual(len(good_hits), 3)
