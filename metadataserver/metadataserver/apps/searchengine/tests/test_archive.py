import datetime

from django.test import TestCase
from django.utils import timezone

from metadataserver.apps.searchengine.archive import (
    SearchArchiveConfigurer, SearchArchiveManager
    )
from metadataserver.apps.searchengine.models import (
    SearchResultSummary, SearchResult, SearchResultSummaryArchive,
    SearchResultArchive
    )


class TestArchive(TestCase):
    def setUp(self):
        SearchArchiveConfigurer.del_config()

    def tearDown(self):
        SearchArchiveConfigurer.del_config()

    def test_config(self):
        with self.settings(SEARCH_ARCHIVE_ENABLED=False,
                           SEARCH_ARCHIVE_RESULTS_PER_SEARCH=2,
                           SEARCH_ARCHIVE_REGISTER_SEARCH_RESULTS=False):
            config = SearchArchiveConfigurer.get_config()
            self.assertFalse(config.get_settings_value("enabled"))
            self.assertEqual(config.get_settings_value("results_per_search"),
                             2)
            self.assertFalse(config.get_settings_value(
                "register_search_results"))
            sam = SearchArchiveManager()
            config = sam._config
            self.assertFalse(config.get_settings_value("enabled"))
            self.assertEqual(config.get_settings_value("results_per_search"),
                             2)
            self.assertFalse(config.get_settings_value(
                "register_search_results"))

    def test_copy_results(self):
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1234",
                                  search_latency_s=1.0,
                                  total_results_number=1)
        sr2 = SearchResult(score=0.5, result_position=1,
                           search_results_summary=srs)

        sr1 = SearchResult(score=0.3, result_position=0,
                           search_results_summary=srs)

        srs.set_results([sr1, sr2])
        srs.save()

        a_srs = SearchResultSummaryArchive(date=timezone.now(),
                                           query="1234",
                                           search_latency_s=1.0,
                                           total_results_number=1,
                                           session_id="MySession")
        a_srs.save()

        sam = SearchArchiveManager()
        new_results = sam.copy_results(srs, a_srs)
        srs.delete()

        the_new_res = SearchResultArchive.objects.filter(
            search_results_summary_archive=a_srs)
        self.assertEqual(len(the_new_res), 2)

        self.assertEqual([x.score for x in the_new_res],
                         [0.3, 0.5]
                         )
        self.assertEqual(sr1.get_archive_id(), new_results[0].id)
        self.assertEqual(sr2.get_archive_id(), new_results[1].id)

    def test_copy_results_config(self):
        with self.settings(SEARCH_ARCHIVE_RESULTS_PER_SEARCH=1):
            srs = SearchResultSummary(date=timezone.now(),
                                      query="1234",
                                      search_latency_s=1.0,
                                      total_results_number=1)
            sr2 = SearchResult(score=0.5, result_position=1,
                               search_results_summary=srs)

            sr1 = SearchResult(score=0.3, result_position=0,
                               search_results_summary=srs)

            srs.set_results([sr1, sr2])
            srs.save()

            a_srs = SearchResultSummaryArchive(date=timezone.now(),
                                               query="1234",
                                               search_latency_s=1.0,
                                               total_results_number=1,
                                               session_id="MySession")
            a_srs.save()

            sam = SearchArchiveManager()
            new_results = sam.copy_results(srs, a_srs)
            srs.delete()

            the_new_res = SearchResultArchive.objects.filter(
                search_results_summary_archive=a_srs)
            self.assertEqual(len(the_new_res), 1)

            self.assertEqual([x.score for x in the_new_res],
                             [0.3]
                             )

    def test_query_happens(self):
        srs = SearchResultSummary(date=timezone.now(),
                                  query="1234",
                                  search_latency_s=1.0,
                                  total_results_number=1,
                                  page_start=1,
                                  page_end=4,
                                  cached=True)
        sr2 = SearchResult(score=0.5, result_position=1,
                           search_results_summary=srs,
                           hit_list_as_json={"a": 1})

        sr1 = SearchResult(score=0.3, result_position=0,
                           search_results_summary=srs)

        srs.save()
        srs = SearchResultSummary.objects.get(id=srs.id)
        srs.set_results([sr1, sr2])

        sam = SearchArchiveManager()
        sam.query_happens(srs, "session1")
        sam.query_happens(srs, "session1")
        sam.query_happens(srs, "session3")
        self.assertEqual(len(SearchResultSummaryArchive.objects.all()),
                         3)
        self.assertEqual(len(SearchResultArchive.objects.all()),
                         6)

        a_srs = SearchResultSummaryArchive.objects.filter(query="1234",
                                                          session_id="session3").last()
        self.assertEqual(srs.get_archive_id(), a_srs.id)
        self.assertEqual(a_srs.date, srs.date)
        self.assertEqual(a_srs.search_latency_s, 1.0)
        self.assertEqual(a_srs.total_results_number, 1)
        self.assertEqual(a_srs.page_start, 1)
        self.assertEqual(a_srs.page_end, 4)
        self.assertEqual(a_srs.query, "1234")

        the_new_res = SearchResultArchive.objects.filter(
            search_results_summary_archive=a_srs)
        self.assertEqual(len(the_new_res), 2)

        self.assertEqual([x.score for x in the_new_res],
                         [0.3, 0.5]
                         )
        self.assertEqual(the_new_res[1].hit_list_as_json,
                         {"a": 1})

    def test_clear_archive(self):
        srs = SearchResultSummary(
            query="1234",
            search_latency_s=1.0,
            total_results_number=1,
            page_start=1,
            page_end=4,
            cached=True,
            date=datetime.datetime(2017, 1, 2, tzinfo=datetime.timezone.utc))

        sam = SearchArchiveManager()
        sam.query_happens(srs)

        srs = SearchResultSummary(
            query="1235",
            search_latency_s=1.0,
            total_results_number=1,
            page_start=1,
            page_end=4,
            cached=True,
            date=datetime.datetime(2017, 1, 5, tzinfo=datetime.timezone.utc))

        sam.query_happens(srs)

        self.assertEqual(SearchResultSummaryArchive.objects.count(), 2)
        sam.clear_archive()
        self.assertEqual(SearchResultSummary.objects.count(), 0)

    def test_clear_archive_dates(self):
        sam = SearchArchiveManager()
        srs = SearchResultSummary(
            query="1234",
            search_latency_s=1.0,
            total_results_number=1,
            page_start=1,
            page_end=4,
            cached=True,
            date=datetime.datetime(2017, 1, 2, tzinfo=datetime.timezone.utc))

        sam.query_happens(srs)

        srs = SearchResultSummary(
            query="1235",
            search_latency_s=1.0,
            total_results_number=1,
            page_start=1,
            page_end=4,
            cached=True,
            date=datetime.datetime(2017, 1, 5, tzinfo=datetime.timezone.utc))

        sam.query_happens(srs)

        self.assertEqual(SearchResultSummaryArchive.objects.count(), 2)
        sam.clear_archive(start_date=datetime.datetime(2017, 1, 3))
        self.assertEqual(SearchResultSummaryArchive.objects.count(), 1)

        self.assertEqual(SearchResultSummaryArchive.objects.all()[0].query,
                         "1234")

        sam.clear_archive()
        self.assertEqual(SearchResultSummaryArchive.objects.count(), 0)
        srs = SearchResultSummary(
            query="1234",
            search_latency_s=1.0,
            total_results_number=1,
            page_start=1,
            page_end=4,
            cached=True,
            date=datetime.datetime(2017, 1, 2, tzinfo=datetime.timezone.utc))

        sam.query_happens(srs)

        srs = SearchResultSummary(
            query="1235",
            search_latency_s=1.0,
            total_results_number=1,
            page_start=1,
            page_end=4,
            cached=True,
            date=datetime.datetime(2017, 1, 5, tzinfo=datetime.timezone.utc))

        sam.query_happens(srs)
        sam.clear_archive(stop_date=datetime.datetime(2017, 1, 3))
        self.assertEqual(SearchResultSummaryArchive.objects.count(), 1)

        self.assertEqual(SearchResultSummaryArchive.objects.all()[0].query,
                         "1235")

        sam.clear_archive()
        self.assertEqual(SearchResultSummaryArchive.objects.count(), 0)
        srs = SearchResultSummary(
            query="1234",
            search_latency_s=1.0,
            total_results_number=1,
            page_start=1,
            page_end=4,
            cached=True,
            date=datetime.datetime(2017, 1, 2, tzinfo=datetime.timezone.utc))

        sam.query_happens(srs)

        srs = SearchResultSummary(
            query="1235",
            search_latency_s=1.0,
            total_results_number=1,
            page_start=1,
            page_end=4,
            cached=True,
            date=datetime.datetime(2017, 1, 5, tzinfo=datetime.timezone.utc))

        sam.query_happens(srs)

        srs = SearchResultSummary(
            query="1236",
            search_latency_s=1.0,
            total_results_number=1,
            page_start=1,
            page_end=4,
            cached=True,
            date=datetime.datetime(2017, 1, 7, tzinfo=datetime.timezone.utc))

        sam.query_happens(srs)
        sam.clear_archive(start_date=datetime.datetime(2017, 1, 3),
                          stop_date=datetime.datetime(2017, 1, 6))
        self.assertEqual(SearchResultSummaryArchive.objects.count(), 2)

        self.assertEqual([x.query for x in
                          SearchResultSummaryArchive.objects.all()],
                         ["1234", "1236"])
