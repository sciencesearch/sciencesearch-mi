import datetime
import time
from concurrent.futures import ProcessPoolExecutor
import logging

from django.db import models
from django.utils import timezone

from metadataserver.apps.searchengine.configuration import GenericConfigurer
from metadataserver.apps.searchengine.models import SearchResultSummary

logger = logging.getLogger(__name__)


class SearchCacheConfig(GenericConfigurer):
    """Specific configuration of the query caching behavior
    Values:
    - SEARCH_CACHE_ENABLED: if True, caching is enabled. If False caching is disabled.
      Default True.
    - SEARCH_CACHE_CACHE_SIZE: Set to a positive integer n. Cache system will store N
      last queries. Default 100.
    - SEARCH_CACHE_MAX_AGE_S: cached results older than SEARCH_CACHE_MAX_AGE_S seconds
      will be ignored and purged from cache. Default 3600*24.
    - SEARCH_CACHE_ASYNC_STORE: if True (default false), the cache data is
      stored after serving a search request, reducing the latency of the
      searches but requiring the use of a background process.
    - SEARCH_CACHE_NUM_WORKERS: set to a number (default 4), it sets the
      maximum number of background process to store cache values.
    """

    def __init__(self, prefix="SEARCH_CACHE", custom_settings=None):
        super(SearchCacheConfig, self).__init__(prefix=prefix,
                                                custom_settings=custom_settings)

    def get_default_settings(self):
        return dict(enabled=True,
                    cache_size=100,
                    max_age_s=3600 * 24,
                    async_store=False,
                    num_workers=4)


class SearchCacheManager(models.Model):
    """Class to manage cache entries. Cache entries are stored in a the
    database, subsets of results of a cached query can be retrieved for
    pagination."""

    class Meta:
        pass

    @classmethod
    def get_list_of_cached_queries(cls):
        """Returns the SearchResultSummary objects stored in the cache."""
        return SearchResultSummary.objects.all()

    @classmethod
    def get_cache_size(cls):
        """Returns the number of entries in the cache."""
        return SearchResultSummary.objects.count()

    @classmethod
    def get_cache_config_dict(cls):
        """Returns the cache configuration as a dictionary."""
        return SearchCacheConfig.get_config().get_settings()

    @classmethod
    def get_cached_query(cls, query, start_time,
                         query_type=None,
                         first_index=0,
                         last_index=None,
                         hit_list_weights=None,
                         config=None):
        """Returns a cache entry that match query. Returns None otherwise.
        Args:
        - query: string with a text search query.
        - start_time: datetime object. Considered as the start time of the
          search in the search elapsed time calculation.
        - query_type: string that identifies the type of query: e.g., images, 
          proposals.
        - first_index: index of the first result of the list of cached
          results to be returned.
        - last_index: index of the last result of the list of cached
          results to be returned. If set to None, no limit is set.
        """
        if config is None:
            config = SearchCacheConfig.get_config()
        if not config.get_settings_value("enabled"):
            return None

        cached_result = cls._get_result_summary(query, query_type=query_type,
                                                hit_list_weights=hit_list_weights,
                                                max_age_s=config.get_settings_value("max_age_s"),
                                                page_start=first_index)
        if cached_result:
            # TODO store the size of the results cache so we don't need to fetch this
            results_size = len(cached_result.get_cached_results())
            # compute correct start and end index, the cached results will be stored in a list that begins at 0
            # but the start and end requested will be outside of the list index range
            if results_size > 0 and first_index >= results_size:
                start = int(first_index % results_size)
                end = start + (last_index - first_index)
            else:
                start = first_index
                end = last_index

            cached_result.set_results(cached_result.get_cached_results(
                first_index=start,
                last_index=end))

            elapsed_time = (time.time() - start_time)
            cached_result.search_latency_s = elapsed_time
            cached_result.page_start = first_index
            cached_result.page_end = last_index
        return cached_result

    @classmethod
    def _get_result_summary(cls, query, query_type=None,
                            hit_list_weights=None,
                            max_age_s=None,
                            page_start=None,
                            page_end=None):
        matched_queries = SearchResultSummary.objects.filter(query=query)

        if hit_list_weights:
            matched_queries = matched_queries.filter(
                hit_list_scorer_as_json=hit_list_weights)

        if query_type:
            matched_queries = matched_queries.filter(
                query_type=query_type)

        # find the cached results that contain requested pages
        # TODO remove default page_end of -1, force explicit page_end values to perform better range searches
        if page_start is not None:
            matched_queries_open = matched_queries.filter(
                page_start__lte=page_start, page_end=-1)

            matched_queries_range = matched_queries.filter(
                page_start__lte=page_start, page_end__gte=page_start)

            if matched_queries_range:
                matched_queries = matched_queries_range
            elif matched_queries_open:
                matched_queries = matched_queries_open

        if not matched_queries:
            return None
        else:
            last_query = matched_queries.last()
            if max_age_s:
                if (timezone.now() -
                        last_query.date).total_seconds() > max_age_s:
                    return None
            last_query.cached = True
            return last_query

    @classmethod
    def get_executor(cls, config=None):
        """ Returns a ProcessPoolExecutor to be used to process cache writes 
        asynchronously. Number of workers depends on CacheConfiguration.
        If it was called before, the executor returned will be the same.
        Args:
          config: CacheConfiguration object
        Returns:
          SearchCacheConfig object
        """
        if config is None:
            config = SearchCacheConfig.get_config()
        if not hasattr(cls, "_executor") or cls._executor is None:
            cls._executor = ProcessPoolExecutor(
                max_workers=config.get_settings_value("num_workers"))
        return cls._executor

    @classmethod
    def save_result_cache(cls, srs, config=None):
        """Saves a SearchResultSummary to the cache."""
        if config is None:
            config = SearchCacheConfig.get_config()
        if config.get_settings_value("async_store"):
            from django.db import connection
            connection.close()
            executor = cls.get_executor(config)
            executor.submit(SearchCacheManager.save_result_cache_op,
                            srs, config=config, async_call=True)
        else:
            SearchCacheManager.save_result_cache_op(srs, config=config)

    @classmethod
    def shutdown_and_wait(cls, future=None, config=None):
        """Stops current executor waiting for its work to be completed"""
        logger.info("Waiting for Cache to be done")
        cls.get_executor(config=config).shutdown(wait=True)
        cls._executor = None
        logger.info("Wait Cache done!")

    @classmethod
    def save_result_cache_op(cls, srs, config=None, async_call=False):
        """ Saves the SearchResultSummary object to the database.
        Args:
          srs: SearchResultSummary object to store.
          config: CacheConfigure object
          async_call: must be True if called from a subprocess.
        """
        if config.get_settings_value("enabled"):
            if async_call:
                from django.db import connection
                connection.close()
            srs.save_for_cache()
            cls.check_cache_size(config=config)
            logger.info("Cache store completed")

    @classmethod
    def clean_old_entries(cls, config=None):
        """Removes cache entries older than the configured mad age."""
        if config is None:
            config = SearchCacheConfig.get_config()
        SearchResultSummary.objects.filter(
            date__lt=timezone.now() - datetime.timedelta(
                seconds=config.get_settings_value("max_age_s"))).delete()

    @classmethod
    def check_cache_size(cls, config=None):
        """Checks if the cache size is within the configured size and deletes
        entries until it is (FIFO)."""
        if config is None:
            config = SearchCacheConfig.get_config()
        max_cache_size = config.get_settings_value("cache_size")
        last_count = 0
        if SearchResultSummary.objects.count() > max_cache_size:
            cls.clean_old_entries(config=config)
        while SearchResultSummary.objects.count() > max_cache_size:
            if last_count == SearchResultSummary.objects.count():
                raise Exception("Error, potential infinite loop in cache"
                                " element deletion.")
            SearchResultSummary.objects.first().delete()

    @classmethod
    def clear_cache(cls):
        SearchResultSummary.objects.all().delete()
