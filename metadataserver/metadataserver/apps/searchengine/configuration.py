"""This module includes a default configuration for the search engine and
provides an interface to custom config.

The search configuration can be set in the django settings (dominant value)
or in enviroment variables. For metadata search parameters identified
by IDMETADATA the format is:

SEARCH_METADATAID_METADATA_WEIGHT, SEARCH_METADATAID_METADATA_MIN,
SEARCH_METADATAID_METADATA_MAX for the weight, min expected value,
and max expected value. 
"""
import copy
import os

from django.conf import settings

SCOPEIMAGE_FROM_PROPOSAL_TEXT = "scrt"
PROPOSAL_FROM_TEXT = "pt"
SCOPEIMAGE_FROM_FS = "imfs"
PAPER_FROM_TEXT = "spt"
DEFAULT_FILTER = "filt"
SCOPEIMAGE_DL = "scdl"
USER_PROVIDED = "user"
UNKNOWN_SOURCE = "unk"

METADATA_TYPES = [(SCOPEIMAGE_FROM_PROPOSAL_TEXT, "text_related_proposal"),
                  (PROPOSAL_FROM_TEXT, "text_proposal"),
                  (SCOPEIMAGE_FROM_FS, "filesystem"),
                  (PAPER_FROM_TEXT, "text_paper"),
                  (DEFAULT_FILTER, "filter"),
                  (SCOPEIMAGE_DL, "images_dl"),
                  (USER_PROVIDED, "user"),
                  (UNKNOWN_SOURCE, "unknown")]


class GenericConfigurer(object):
    """ Generic class that reads settings values from DJANGO config,
    environment variables, or a request.GET type of object.
      
    Default values are defined as a dictionary returned by the
    "get_default_settings" function. Only values present in default values
    can be set. The prefix value and the ids involved to access a setting
    in the dictionary are used to construct a canonical name for the setting.
    
    1) Format for default values: it must be a dictionary of Python standard
       types.
    2) Naming conventions: For a prefix="MYPREFIX", and given the following
       default settings:
        { "setting_integer": 1.0,
          "setting_dict": { "setting_string": "cad"}
        }
        The following canonical setting names are available:
        MYPREFIX_SETTINGS_INTEGER, MYPREFIX_SETTINGS_DICT_SETTING_STRING.
        
    The canonical names can be used as a DJANGO setting, an env var, or
    passed in a request.get object to the custtom_settings in the Configurer
    creation.
    """

    def __init__(self, prefix="GENERIC", custom_settings=None):
        """Returns a GenericConfigurer object.
        Args:
        - prefix: String to append to the canonical name of settings.
        - custsom_settings: requeste.get object. If it contains values
        indexed by the canonical names of settings, the created configuraiton
        object will have such value over-writeng with whatever custom_settings
        contains.
        """
        self._prefix = prefix
        self.update_settings(custom_settings)
        #self.create_secondary_objs()

    def get_prefix(self):
        """Returns the prefix used in the canonical settings of this object"""
        return self._prefix

    @staticmethod
    def get_default_settings():
        """Returns a dictionary with the default settings. These method has to
        be overridden by specific implementations."""
        return {}

    def set_settings(self, st):
        """Sets the dictionary containing the settings of the object"""
        self._actual_settings = st

    def get_settings(self):
        """Returns the structure containing the settings of the object"""
        return self._actual_settings

    def get_settings_value(self, value_name):
        """Gets the value of a setting indexed by its canonical name."""
        value_name = self.canonize_setting_name(value_name)
        return self._actual_settings[value_name]

    def canonize_setting_name(self, set_name):
        return set_name.upper()

    def canonize_dicts(self, a_dict):
        new_dict = {}
        for (dic_id, value) in a_dict.items():
            cannon_id = self.canonize_setting_name(dic_id)
            if type(value) is dict:
                new_dict[cannon_id] = (
                    self.canonize_dicts(value))
            else:
                new_dict[cannon_id] = value
        return new_dict

    def _read_settings_value(self, setting_name, custom_settings=None,
                             the_type=str):
        setting_name = self.canonize_setting_name(setting_name)
        read_value = None
        if (custom_settings and
                custom_settings.get(setting_name, None) is not None):
            read_value = custom_settings.get(setting_name, None)
            if the_type is bool:
                read_value = read_value.lower() == "true"
            else:
                read_value = the_type(read_value)
        elif os.getenv(setting_name, None) is not None:
            read_value = os.getenv(setting_name, None)
            if read_value is not None:
                if the_type is bool:
                    read_value = read_value.lower() == "true"
                else:
                    read_value = the_type(read_value)
        elif hasattr(settings, setting_name):
            read_value = getattr(settings, setting_name)

        return read_value

    def _create_settings_dict(self, dict_obj, base_name="",
                              custom_settings=None):
        for (set_id, set_value) in dict_obj.items():
            if type(set_value) is dict:
                self._create_settings_dict(set_value,
                                           base_name="{}{}_".format(base_name,
                                                                    set_id),
                                           custom_settings=custom_settings)
            else:
                setting_name = "{}{}".format(base_name,
                                             set_id)
                read_value = self._read_settings_value(setting_name,
                                                       custom_settings=custom_settings,
                                                       the_type=type(set_value))
                if read_value is not None:
                    dict_obj[set_id] = read_value

    def _create_settings(self, custom_settings=None):
        new_settings = copy.deepcopy(self.canonize_dicts(
            self.get_default_settings()))
        self._create_settings_dict(new_settings,
                                   base_name="{}_".format(self.get_prefix()),
                                   custom_settings=custom_settings)
        return new_settings

    def update_settings(self, custom_settings=None):
        """Resets the settings of the object reading Django config, env vars,
        and the passed argument custom_settings request.get object."""
        self.set_settings(self._create_settings(custom_settings=custom_settings))

    def create_secondary_objs(self):
        """Creates internal objects that must be created after writing the
        internal settings.  
        """
        pass

    @classmethod
    def get_config(cls, force_read=False, custom_settings=None):
        """Returns a GenericConfigurer
        Args:
          force_read: if False, the method will returns a cached copy generated
            in previous calls of the same method. If True, the method will
            recalculate the HitListScorer from defaults, settings, and
            custom_settings.
          custom_settings: object that behaves like a dictionary in which
            items are read with get(id_obj, default) method. 
        Returns: a configured GenericConfigurer
        """
        if (not hasattr(cls, "_cached_config") or force_read or
                custom_settings is not None or cls._cached_config is None):
            sc = cls(custom_settings=custom_settings)
            if not (custom_settings):
                cls._cached_config = sc
            else:
                return sc
        return cls._cached_config

    @classmethod
    def del_config(cls):
        cls._cached_config = None
