from django.conf.urls import url, include
from rest_framework import routers

from metadataserver.apps.searchengine.views import archive as archive_browsable, browsable as search_browsable, cache as cache_browsable

metadata_router = routers.DefaultRouter()
metadata_router.register(r'proposals',
                         search_browsable.SearchableProposalMetadatata,
                         base_name='metadata/proposals')
metadata_router.register(r'images',
                         search_browsable.SearchableScopeImageMetadata,
                         base_name='metadata/images')

metadata_router.register(r'papers',
                         search_browsable.SearchableScopePaperMetadata,
                         base_name='metadata/papers')

search_router = routers.DefaultRouter()
search_router.register(r'proposals',
                       search_browsable.SearchProposals,
                       base_name='do_search/proposals')

search_router.register(r'images',
                       search_browsable.SearchImages,
                       base_name='do_search/images')

search_router.register(r'papers',
                       search_browsable.SearchPapers,
                       base_name='do_search/papers')

cache_router = routers.DefaultRouter()
cache_router.register(r'state',
                      cache_browsable.CacheState,
                      base_name='cache/state')
cache_router.register(r'entries',
                      cache_browsable.CacheContent,
                      base_name='cache/entries')

archive_router = routers.DefaultRouter()
# archive_router.register(r'state',
#                        cache_browsable.CacheState,
#                      base_name='cache/state')
archive_router.register(r'entries',
                        archive_browsable.ArchiveContent,
                        base_name='archive/entries')
archive_router.register(r'results',
                        archive_browsable.SearchResultContent,
                        base_name='archive/results')

urlpatterns = [
    url(r'metadata/', include(metadata_router.urls)),
    url(r'do_search/', include(search_router.urls)),
    url(r'cache/', include(cache_router.urls)),
    url(r'archive/', include(archive_router.urls)),
    ]
