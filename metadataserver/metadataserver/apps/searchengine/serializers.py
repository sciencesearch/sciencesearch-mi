from rest_framework import serializers

from metadataserver.apps.searchengine.cache import SearchCacheManager
from metadataserver.apps.searchengine.models import (
    ProposalMetadata, ScopeImageMetadata, PaperMetadata,
    SearchResult, SearchResultSummary, SearchResultArchive,
    SearchResultSummaryArchive,
    )
from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.apps.ncem.serializers import ScopeImageSerializer
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.ncemreldata.serializers import ProposalSerializer
from metadataserver.apps.papermanager.models import Paper
from metadataserver.apps.papermanager.serializers import PaperSerializer


class ProposalMetadataSerializer(serializers.ModelSerializer):
    """
    Serializer for the searchengine.models.ProposalMetadata. 
    """

    class Meta:
        model = ProposalMetadata
        fields = ('text_content', 'score', 'metadata_type', 'related_object')


class ScopeImageMetadataSerializer(serializers.ModelSerializer):
    """
    Serializer for the searchengine.models.ScopeImageMetadata. 
    """

    class Meta:
        model = ScopeImageMetadata
        fields = ('text_content', 'score', 'metadata_type', 'related_object')


class PaperMetadataSerializer(serializers.ModelSerializer):
    """
    Serializer for the searchengine.models.PaperMetadata. 
    """

    class Meta:
        model = PaperMetadata
        fields = ('text_content', 'score', 'metadata_type', 'related_object')


class SearchResultSerializerAbstract(serializers.ModelSerializer):
    """
    Serializer for searchengine.models.SearchResult.
    It includes two extra fields:
        - result_object: serialized version of the object that the result
          refers to.
        - hit_list: list of serialized version of the SearchableObject hits
          that produced this result.
    JSON Example:
        {"result_object":{"serial_number":"1234","status":"ACTIVE",
                "title":"","interaction_type":-1,"project_leader":"N",
                "primary_researcher":"N","co_researcher":"N","first_name":"",
                "last_name":"","email":"","bin":"",
                "date_project_started":"1970-01-01T00:00:00Z",
                "date_project_end":"1970-01-01T00:00:00Z",
                "assigned_scientist":"","pi_name":"",
                "primary_researcher_name":"","contact_with_staff_name":"",
                "significance":"","foundry_capabilities":"",
                "work_description":"","project_duration":"",
                "relevant_experience":"","project_summary":"",
                "follow_on_progress_effort":""},
            "score":0.5,
            "hit_list":[[0.1, 2.2,"pt","etch"],[0.2, 2.1,"pt","material"],
            [0.3, 1.2,"si","other"]]
            }
    """
    result_object = serializers.SerializerMethodField("_get_result_object")
    hit_list = serializers.SerializerMethodField("_get_hit_list")

    class Meta:
        model = SearchResult
        fields = ("result_object", "score", "result_position", "hit_list")
        abstract = True

    @staticmethod
    def _get_hit_list(search_result):
        return search_result.get_hit_list()

    def _get_result_object(self, search_result):
        result_object = search_result.get_result_obj()
        if result_object is None:
            return "None"
        if type(result_object) is ScopeImage:
            return ScopeImageSerializer(result_object).data
        elif type(result_object) is Proposal:
            return ProposalSerializer(result_object).data
        elif type(result_object) is Paper:
            return PaperSerializer(result_object).data
        else:
            return "Result object type unknown: {}".format(type(result_object))


class SearchResultSerializer(SearchResultSerializerAbstract):
    class Meta(SearchResultSerializerAbstract.Meta):
        model = SearchResultArchive
        fields = SearchResultSerializerAbstract.Meta.fields + ('archive_id',)

    archive_id = serializers.SerializerMethodField("_get_archive_id")

    @staticmethod
    def _get_archive_id(search_result):
        archive_id = search_result.get_archive_id()
        if archive_id is not None:
            return str(archive_id)
        return "N/A"

    pass


class SearchResultArchiveSerializer(SearchResultSerializerAbstract):
    class Meta(SearchResultSerializerAbstract.Meta):
        model = SearchResultArchive
        fields = SearchResultSerializerAbstract.Meta.fields + ('id',
                                                               'user_clicked',)

    def _get_result_object(self, search_result):
        result_object = search_result.get_result_obj()
        if result_object is None:
            return ["None"]
        elif type(result_object) is ScopeImage:
            return ["scopeimage", str(result_object.id)]
        elif type(result_object) is Proposal:
            return ["proposal", str(result_object.serial_number)]
        elif type(result_object) is Paper:
            return ["paper", str(result_object.simple_title)]
        else:
            return ["UNK({})".format(type(
                result_object))]


class SearchResultSummarySerializerAbstract(serializers.ModelSerializer):
    """Serializer for a SearchResultSummary object.
    It includes two extra fields:
        - resulst_list: list serialized version of SearchResults
        - search_weights: seralization of the dictionary that configures the
          HitListScorer used in the search.
    JSON Example:
        {"query":"my query","date":"2010-01-01T00:00:00Z",
            "search_latency_s":1.34,
            "page_start":2,"page_end":4,
            "search_weights":
                {"si":{"weight":1.0,"min_input":-2.0,"max_input":4.0},
                "pt":{"weight":2.0,"min_input":-3.0,"max_input":5.0}},
            "results_list":[{
            "result_object":{"serial_number":"1234","status":"ACTIVE",
                "title":"","interaction_type":-1,"project_leader":"N",
                "primary_researcher":"N","co_researcher":"N","first_name":"",
                "last_name":"","email":"","bin":"",
                "date_project_started":"1970-01-01T00:00:00Z",
                "date_project_end":"1970-01-01T00:00:00Z",
                "assigned_scientist":"","pi_name":"",
                "primary_researcher_name":"","contact_with_staff_name":"",
                "significance":"","foundry_capabilities":"",
                "work_description":"","project_duration":"",
                "relevant_experience":"","project_summary":"",
                "follow_on_progress_effort":""},
            "score":0.5,
            "hit_list":[[0.1, 2.2,"pt","etch"],[0.2, 2.1,"pt","material"],
            [0.3, 1.2,"si","other"]]
            }]}
    """
    results_list = serializers.SerializerMethodField("_get_result_list")
    search_weights = serializers.SerializerMethodField("_get_search_weights")

    class Meta:
        model = SearchResultSummary
        fields = ("query", "query_type", "date", "cached", "search_latency_s",
                  "page_start",
                  "page_end", "search_weights", "total_results_number",
                  "results_list", "search_timed_out")

    def _get_serializer(self):
        return SearchResultSerializer

    def _get_result_list(self, search_summary):
        result_list = search_summary.get_results()
        serializer_class = self._get_serializer()
        return [serializer_class(re).data for re in result_list]

    def _get_search_weights(self, search_summary):
        return search_summary.hit_list_scorer_as_json


class SearchResultSummarySerializer(SearchResultSummarySerializerAbstract):
    class Meta:
        model = SearchResultSummary
        fields = SearchResultSummarySerializerAbstract.Meta.fields + (
            "archive_id",)

    archive_id = serializers.SerializerMethodField("_get_archive_id")

    def _get_archive_id(self, search_result):
        archive_id = search_result.get_archive_id()
        if archive_id is not None:
            return str(archive_id)
        return "N/A"

    pass


class SearchResultSummaryArchiveSerializer(SearchResultSummarySerializerAbstract):
    class Meta(SearchResultSummarySerializer.Meta):
        model = SearchResultSummaryArchive
        fields = SearchResultSummarySerializerAbstract.Meta.fields + (
            "session_id", "search_date")

    def _get_serializer(self):
        return SearchResultArchiveSerializer


class SearchCacheManagerSerializer(serializers.ModelSerializer):
    cached_queries = serializers.SerializerMethodField(
        "_get_list_of_cached_queries")
    cache_size = serializers.SerializerMethodField("_get_cache_size")
    cache_config = serializers.SerializerMethodField("_get_cache_config")

    class Meta:
        model = SearchCacheManager
        fields = ("cache_config", "cache_size", "cached_queries")

    def _get_list_of_cached_queries(self, cache_m):
        return [{
                    "query": x.query, "query_type": x.query_type,
                    "date": x.date, "id": x.id
                }
                for x in cache_m.get_list_of_cached_queries()]

    def _get_cache_size(self, cache_m):
        return cache_m.get_cache_size()

    def _get_cache_config(self, cache_m):
        return cache_m.get_cache_config_dict()
