"""This package provides objects and classes to archive search queries and
their results.
"""

from rest_framework.renderers import JSONRenderer

from metadataserver.apps.searchengine.configuration import GenericConfigurer
from metadataserver.apps.searchengine.models import (
    SearchResultArchive, SearchResultSummaryArchive
    )
from metadataserver.apps.searchengine.serializers import SearchResultSummaryArchiveSerializer


class SearchArchiveConfigurer(GenericConfigurer):
    """Specific configuration object for the search archiving. It defines
    three values to be set:
    - SEARCH_ARCHIVE_ENABLED: search archiving is enabled if set to True. 
      Default=True
    - SEARCH_ARCHIVE_RESULTS_PER_SEARCH: set to an positive number it will
      limit to maximum number of results per query that will be logged.
      Default=40
    - SEARCH_ARCHIVE_REGISTER_SEARCH_RESULTS: If set to False, only the queries
      and not the results are archived. Default=True
    """

    def __init__(self, prefix="SEARCH_ARCHIVE", custom_settings=None):
        super(SearchArchiveConfigurer, self).__init__(prefix=prefix,
                                                      custom_settings=custom_settings)

    def get_default_settings(self):
        return dict(enabled=True,
                    results_per_search=40,
                    register_search_results=True)


class SearchArchiveManager():
    """Class to manage the archive, retrieve, and eport data from 
    SearchResultSummary and SearchResult objects."""

    def __init__(self, config=None, custom_settings=None):
        if config:
            self._config = config
        else:
            self._config = SearchArchiveConfigurer.get_config(
                custom_settings=custom_settings)

    def query_happens(self, srs, session_id=""):
        """Archives a SearchResultSummary  as SearchResultSummaryArchive
        persistent object.
        Args: 
        - srs: SearchResultSummary object containing a search result.
        If configured in the SearchArchiveConfig, the results pending from
        the query will be stored too.
        - session_id: string that identifies the the browser sessions.        
        """
        if self._config.get_settings_value("enabled"):
            a_srs = SearchResultSummaryArchive.objects.create(
                **SearchArchiveManager.to_dict(srs, ["id"]))
            a_srs.session_id = session_id
            a_srs.save()
            srs.set_archive_id(a_srs.id)
            self.copy_results(srs, a_srs)

    def copy_results(self, srs, a_srs):
        """if SEARCH_ARCHIVE_REGISTER_SEARCH_RESULTS is True, it copies
        SearchResult objects from srs as SearchResultArchive objects
        associated to a_srs.
        Args:
        - srs: SearchResultSummary obj
        - a_srs: SearchResultSummaryArchive obj
        Returns: A list of the SearchResultArchive instances associated with
        a_srs.
        """
        new_results = []
        for result in srs.get_results()[:
        self._config.get_settings_value("results_per_search")]:
            n_result = SearchResultArchive.objects.create(
                **SearchArchiveManager.to_dict(result, ["id",
                                                        "image",
                                                        "proposal",
                                                        "paper",
                                                        "search_results_summary"]))
            n_result.set_result_obj(result.get_result_obj())
            n_result.search_results_summary = None
            n_result.search_results_summary_archive = a_srs
            new_results.append(n_result)
            n_result.save()
            result.set_archive_id(n_result.id)
        return new_results

    def get_archive(self, start_date=None, stop_date=None):
        """Returns the a query list of stored SearchResultSummary between
        start_date and stop_date.
        Args:
        - start_date: Datatime.datetime object. If set to None, start_date is
          considered the beginning of times. Method returns only archives
          entries created at of after start_date.
        - stop_date: Datatime.datetime object. If set to None, stop_date is
          considered the end of times. Method returns only archives
          entries as created at or before stop_time.
        Returns: querylist of  SearchResultSummary objs.
        """
        qs = SearchResultSummaryArchive.objects.all()
        if start_date:
            qs = qs.filter(date__gte=start_date)
        if stop_date:
            qs = qs.filter(date__lte=stop_date)
        return qs

    def clear_archive(self, start_date=None, stop_date=None):
        """Selectively deletes archive entries between
        start_date and stop_date.
        Args:
        - start_date: Datatime.datetime object. If set to None, start_date is
          considered the beginning of times. Method deletes only archives
          entries created at of after start_date.
        - stop_date: Datatime.datetime object. If set to None, stop_date is
          considered the end of times. Method deletes only archives
          entries as created at or before stop_time.
        """
        qs = self.get_archive(start_date=start_date, stop_date=stop_date)
        qs.delete()

    def dump_to_file(self, f, start_date=None, stop_date=None):
        """Selectively dumps archive entries between
        start_date and stop_date in JSON format.
        Args:
        - f: open textfile to write to.
        - start_date: datetime.datetime object. If set to None, start_date is
          considered the beginning of times. Method dumps only archives
          entries created at of after start_date.
        - stop_date: datetime.datetime object. If set to None, stop_date is
          considered the end of times. Method dumps only archives
          entries as created at or before stop_time.
        """
        qs = self.get_archive(start_date=start_date, stop_date=stop_date)
        serializer = SearchResultSummaryArchiveSerializer(qs, many=True)
        f.write(str(JSONRenderer().render(serializer.data)))

    @classmethod
    def to_dict(cls, instance, fields_to_remove=[]):
        new_dict = {field.name: field.value_from_object(instance)
                    for field in instance._meta.fields}
        for field in fields_to_remove:
            del (new_dict[field])
        return new_dict
