"""
Filter module include classes to select SearchableObject(s) according to values
of fields of the objects they point.
"""
import logging
import django.core.exceptions

logger = logging.getLogger(__name__)

class BaseFilter(object):
    """Base class for the filters. Includes the methods needed by the
    interacting classes.
    
    """

    def apply_filter(self, query_set):
        """Applies the filter policy to query_set and returns only the entries
        that comply with the policy. 
        """
        return query_set

    def equal(self, q):
        """Returns True if q and this filter would return the same subset
        of a queryset."""
        return type(self) == type(q)

    def __eq__(self, other):
        return self.equal(other)

    def __ne__(self, other):
        result = self.__eq__(other)
        return not result

    def __repr__(self):
        return ""


class SelectionFilter(BaseFilter):

    def __init__(self, field, term, strict_equal=True,
                 related_field_name="related_object"):
        """
        Filter based on the value of an object pointed by the SearchableObject(s)
        in the a query set. 
        Args:
            field(str): field in the point object to check.
            term(str): value that the field in the pointed object has to have
              to comply with the filtering policy.
            strict_equal(bool): If true, the policy is satisfied if value
              in the field is exactly term. If False, policy is satisfied
              if term is substring of the value in the field.
            related_field_name(str): name of the attribute of the
              SearchableObject that points to the object which field will be
              read.            
        """
        self._field = field
        self._term = term
        self._strict_equal = strict_equal
        self._related_field_name = related_field_name

    def apply_filter(self, query_set):
        """Applies the filter policy to query_set and returns only the entries
        that comply with the policy.
        Args:
            query_set(QuerySet): List of searchable objects.
        Returns: query_set
        """
        filter_args = {}
        if self._strict_equal:
            if self._related_field_name is not None:
                filter_args["{}__{}__exact".format(self._related_field_name, self._field)] = self._term
            else:
                filter_args["{}__exact".format(self._field)] = self._term
        else:
            if self._related_field_name is not None:
                filter_args["{}__{}__icontains".format(self._related_field_name, self._field)] = self._term
            else:
                filter_args["{}__icontains".format(self._field)] = self._term

        try:
            return query_set.filter(**filter_args)
        except django.core.exceptions.FieldError as e:
            logger.warning(e)
            return query_set

    def equal(self, q):
        return (super().equal(q) and
                self._field == q._field and
                self._term == q._term and
                self._strict_equal == q._strict_equal and
                self._related_field_name == q._related_field_name)

    def __repr__(self):
        if self._strict_equal:
            return "{}=:={}".format(self._field, self._term)
        else:
            return "{}=:>{}".format(self._field, self._term)


class BinaryFilter(BaseFilter):

    def __init__(self, filter1, filter2):
        """
        Filter which policy depends on a combination the policies filter1 
        and filter2.
        """
        self._filter1 = filter1
        self._filter2 = filter2

    def equal(self, q):
        return (super().equal(q) and
                self._filter1 == q._filter1 and
                self._filter2 == q._filter2)

    def apply_filter(self, query_set):
        raise NotImplementedError

    def __repr__(self):
        return "{} {}".format(self._filter1, self._filter2)


class OrFilter(BinaryFilter):

    def apply_filter(self, query_set):
        """
        Return entries that satisfy filter1 or filter2. It does not reutrn
        repeated entries. 
        Args:
            query_set(QuerySet): List of searchable objects.
        Returns: query_set
        """
        q1 = self._filter1.apply_filter(query_set)
        q2 = self._filter2.apply_filter(query_set)
        return q1 | q2

    def __repr__(self):
        return "{} \or {}".format(self._filter1, self._filter2)
