import jellyfish


class Predicate(object):
    """ This object represents a piece of semantic that can be compared to
    other pieces of semantic and represent their similarity with a float in
    [0.0, 1.0]
    """

    def __init__(self, content):
        """
        Args:
          content: object containing the piece of semantic.
        """
        self._content = content

    def match(self, p, strict=False, case_sensitive=False, threshold=0.0):
        """ Returns a numeric expression of the similarity between this
        predicate and p.
        
        Args:
          p: Predicate to compare to. It must be of the same type.
          strict: if true, match returns 1.0 if p is identical to this
            predicate. Returns 0.0 otherwise.
          case_sensitive: if false, use lowercase strings
          threshold: lower bound on match scores, anything below this
            gets a score of 0.0
        Returns: Float between 0.0 and 1.0 where 1.0 represents maximum
          similarity and 0.0 represents no similarity at all.
        """
        raise NotImplementedError

    def get_content(self):
        """Returns the content of this predicate."""
        return self._content

    def __repr__(self):
        return str(self._content)

    def __eq__(self, other):
        return self._content == other._content

    def __neq__(self, other):
        return not self.__eq__(other)


class TextPredicate(Predicate):
    def __init__(self, content):
        super().__init__(
            [TextPredicateSingleWord(w) for w in list(set(content.split(" ")))]
            )

    def match(self, p, strict=False, case_sensitive=False, threshold=0.0):
        total_score = 0.0

        if isinstance(p, str):
            p = TextPredicate(p)

        if issubclass(type(p), TextPredicate):
            p_content = p.get_content()

            for own_pred in self.get_content():
                for p_pred in p_content:
                    total_score += own_pred.match(
                        p_pred,
                        strict=strict,
                        case_sensitive=case_sensitive,
                        threshold=threshold)
        else:
            for own_pred in self.get_content():
                total_score += own_pred.match(
                    p,
                    strict=strict,
                    case_sensitive=case_sensitive,
                    threshold=threshold)

        return total_score


class TextPredicateSingleWord(Predicate):
    """Predicate which semantic is a string. Similarity between predicates
    is calculated using the Jaro distance between two strings. More details:
    https://en.wikipedia.org/wiki/Jaro%E2%80%93Winkler_distance 
    
    Distance is larger is more edits are needed to go from one to the other 
    (jaro). However, distance is reduced if both words start with the same
    substring (winkler).
    """

    def match(self, p, strict=False, case_sensitive=False, threshold=0.0):
        t1 = p.get_content()
        t2 = self.get_content()

        if not case_sensitive:
            t1 = t1.lower()
            t2 = t2.lower()

        if strict:
            if t1 == t2:
                return 1.0
            else:
                return 0.0

        score = jellyfish.jaro_winkler(t1, t2)

        if score <= threshold:
            return 0.0

        return score
