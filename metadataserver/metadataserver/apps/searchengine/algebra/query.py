""" This module include the objects to define and execute queries."""
import re
import logging

from metadataserver.apps.searchengine.algebra.filters import SelectionFilter, OrFilter
from metadataserver.apps.searchengine.algebra.predicates import TextPredicate

logger = logging.getLogger(__name__)
filter_pattern = r'\s*(\S+)\=\:\=(?:[\"\']([^\"\']*)[\"\']\s*|(\S+.*?))'


class ObjectWithPredicate(object):
    """Class of objects that can be matched with a Query through get_hits"""

    def get_predicate(self):
        """Returns a Predicate object that will be matched with a query."""
        raise NotImplementedError


class QueryParser(object):

    @classmethod
    def parse_filtered_query(cls, query_string, related_field_name="related_object"):
        return QueryWithFilters(cls.parse(query_string),
                                cls.filter_parse(query_string, related_field_name))

    @classmethod
    def filter_parse(cls, query_string, related_field_name="related_object"):
        """Parses a string and produces a filter object. It returns an OrFilter
        of SelectionFilters. For each space separated
        "field_name=:=field_value" it will add a SelectionFilter for the
        field_value term in the field_name field. Words with no "=:=" are
        ignored.
        
        Args: 
          query_string(str): string to produce the filter
                  
        Returns(BaseFilter subclass)
        """

        filter_list = None
        if "=:=" in query_string:
            p = re.compile(filter_pattern)
            matches = re.findall(p, query_string)

            for m in matches:
                groups = [g for g in m if len(g.strip()) > 0]
                if len(groups) == 2:
                    name = groups[0]
                    value = groups[1]
                    new_f = SelectionFilter(name,
                                            value,
                                            strict_equal=True,
                                            related_field_name=related_field_name)
                    if filter_list is None:
                        filter_list = new_f
                    else:
                        filter_list = OrFilter(filter_list, new_f)

        return filter_list

    @classmethod
    def parse(cls, query_string):
        """Parses a string and produces a query object. Supported syntax:
            - A word: query will match words.
            - words separated by spaces: it will match predicates that contain
              any of its words.
        word.
        
        Args:
          query_string: string to be converted into a query object.
        Returns: a query object,
        """

        # ignore filters
        p = re.compile(filter_pattern)
        clean_query_string = re.sub(p, "", query_string).strip()

        words = []
        for word in clean_query_string.split(" "):
            if len(word.strip()) > 0 and word not in words:
                words.append(word)

        query = None
        for word in words:
            if query is None:
                query = PredicateQuery(word)
            else:
                query = OrQuery(query, PredicateQuery(word))
        return query


class QueryWithFilters(object):

    def __init__(self, query, filter_list=None):
        """Class that combines a Query and Filter object to select 
        searchable objects that match the query bu which pointed objects
        match the filter.
        Args:
            query (PredicateQuery)
            filter_list (BaseFilter)
        """

        self._query = query
        self._filter = filter_list
        if self._query is None and self._filter is not None:
            self._filter_only = True
        else:
            self._filter_only = False
        if self._filter_only:
            self._query = self.gen_default_query()

    @staticmethod
    def gen_default_query():
        return QueryParser.parse("DEFAULT")

    def pre_do_filter(self, mark_list, default_metadata=""):
        """ Returns a subset of mark_list that complies with the filter of the
        query. 
        
        Args:
          mark_list: QuerySet of ObjectWithPredicate to match.
          default_metadata: metadata type used for filtering.
        Returns: QuerySet of ObjectWithPredicates.
        """

        if self._filter:
            mark_list = self._filter.apply_filter(mark_list)
        if self._filter_only:
            mark_list = mark_list.filter(metadata_type=default_metadata)
        else:
            mark_list = mark_list.exclude(metadata_type=default_metadata)
        return mark_list

    def get_hits(self, mark_list, threshold=0.0, default_metadata="",
                 filter_done=False):
        """Returns all the objects that match with score over threshold from
        the ObjectWithPredicate objects in mark_list and thir pointed
        objects match the filter_list..
        
        Args:
          mark_list: list of ObjectWithPredicate to match.
          threshold: minimum score achieved in an individual word match that
            will be considered.
          default_metadata: metadata type used for filtering.
          filter_done: If True, no filtering is applied in the query matching.
           (Because either it is not needed or has been done before).
        Returns: a list of tuples (score, ObjectWithPredicate). score indicates
          the matching level of ObjectWithPredicate with the query. If the
          predicate has n words, score can range (threshold, n*1.0). Each
          word in the predicate is matched individually.        
        """
        if not filter_done:
            mark_list = self.pre_do_filter(mark_list,
                                           default_metadata=default_metadata)
        return self._query.get_hits(mark_list, threshold=threshold)

    def __repr__(self):
        return "\tquery {} \tfilter {}".format(self._query, self._filter)

    def equal(self, q):
        """Returns if q and this query match the same predicate"""
        return self._query == q._query and self._filter == q._filter

    def __eq__(self, other):
        return self.equal(other)

    def __ne__(self, other):
        result = self.__eq__(other)
        return not result


class PredicateQuery(TextPredicate):
    """Most basic query object that represents a text to be matched."""

    def __init__(self, content, strict=False, filter_list=None):
        super().__init__(content)
        self._strict = strict

    def get_hits(self, mark_list, threshold=0.0, default_metadata=None,
                 filter_done=False):
        """Returns all the objects that match with score over threshold from
        the ObjectWithPredicate objects in mark_list.
        
        Args:
          mark_list: list of ObjectWithPredicate to match.
          threshold: minimum score achieved in an individual word match that
            will be considered.
          default_metadata: placeholder for subclasses
          filter_done: placeholder for subclasses
        Returns: a list of tuples (score, ObjectWithPredicate). score indicates
          the matching level of ObjectWithPredicate with the query. If the
          predicate has n words, score can range (threshold, n*1.0). Each
          word in the predicate is matched individually.        
        """
        hits = []
        for obj in mark_list:
            if type(obj) is dict:
                score = self.match(obj["text_content"], strict=self._strict,
                                   threshold=threshold)

            else:
                score = self.match(obj.get_predicate(), strict=self._strict,
                                   threshold=threshold)
            if score > threshold:
                hits.append((score, obj))
        return hits

    def equal(self, q):
        """Returns if q and this query match the same predicate"""
        return (type(self) == type(q) and
                q._strict == self._strict and self.match(q) == 1.0)

    def __eq__(self, other):
        return self.equal(other)

    def __ne__(self, other):
        result = self.__eq__(other)
        return not result


class BinaryQuery(object):
    """ A query that is composed of two sub-queries."""

    def __init__(self, q1, q2):
        self._q1 = q1
        self._q2 = q2

    def get_hits(self, p, threshold=0.0, default_metadata=None,
                 filter_done=False):
        raise NotImplementedError

    def __eq__(self, other):
        return self.equal(other)

    def __ne__(self, other):
        return not self.equal(other)

    def equal(self, q):
        return (type(self) == type(q) and self._q1 == q._q1 and
                self._q2 == q._q2)

    def __repr__(self):
        return "{} {}".format(self._q1, self._q2)


class OrQuery(BinaryQuery):
    def get_hits(self, mark_list, threshold=0.0, default_metadata=None,
                 filter_done=False):
        """Returns the sum of the scores of mark_list for the two subqueries.
        This induces a behavior of OR betweent he two.
        
        Args:
          mark_list: list of ObjectWithPredicate to match.
          threshold: minimum score achieved in an individual word match that
            will be considered.
          default_metadata: unused
          filter_done: unused
        Returns: a list of tuples (score, ObjectWithPredicate). score indicates
          the matching level of ObjectWithPredicate with the query. If the
          predicate has n words, score can range (threshold, n*1.0). Each
          word in the predicate is matched individually.        
        """
        return (self._q1.get_hits(mark_list, threshold=threshold)
                + self._q2.get_hits(mark_list, threshold=threshold))

    def __repr__(self):
        return "{} \or {}".format(self._q1, self._q2)

    def pre_do_filter(self, mark_list, default_metadata=""):
        start = mark_list[:]

        try:
            left_out = self._q1.pre_do_filter(start)
        except AttributeError:
            left_out = start

        try:
            right_out = self._q2.pre_do_filter(start)
        except AttributeError:
            right_out = start

        if left_out == right_out:
            out = left_out
        else:
            out = left_out[:]
            for x in right_out:
                if x not in left_out:
                    out.append(x)

        return out
