""" This module includes the highlevel functions to search among a list of
objects with predicates.

**Search Model**

The search method receives a query. That query is matched against a list of
SearchableMark objects, producing a rank of matching for each one of them. Then
the SearchableMark objects are grouped by the object that they point to. Each
group is then transformed into a score of the relationship between the original
query and the pointed object.

The per object score takes into account:
- SearchableMark expresses its level of relationship towards an object through
its score level. However, SearchableMark can be of different types, and each
type can have different ranges of scores. Also, a weight is assigned to each
type which alters the contribution of each type of SearchableMark to the
score of the objects.
- Level of matching of each SearchableMark with the query (hit level). There
is a configurable thresholds to discard hits low score that could alter
the results because they are many,
- Internal score of the SearchableMark for the object: Larger if
the SearchableMark is very relevant for the pointed object.
- Type of SearchableMark for the object an the configured weights/min/max.
values associated with that type. This is expressed in the HitListScorer
object passed to the search.

"""
import multiprocessing
from concurrent.futures import ProcessPoolExecutor, wait
import logging
import os

logger = logging.getLogger(__name__)

from django.db.models import Max, Min
from django.db.models.query import QuerySet
from django.core.cache import cache

from metadataserver.tools.perf import TM
from metadataserver.apps.searchengine.algebra.query import ObjectWithPredicate, QueryWithFilters
from metadataserver.apps.searchengine.algebra.filters import SelectionFilter


def combine_dicts(dict_orig, dig_dest):
    """Combines dicts of Int without making a new copy and writing the result
    in one of them to save memory.
    Args:
      dict_orig(dict(int)): Dictionary of integers which values will be added
        to dict_dest. If the key exists in dict_dest, values are added. If
        not value is set.
      dig_dest(dict(int)): Dictionary of integers that will receive the values
        of dict_origin.
    Returns: dict_dest
    """

    for key, value in dict_orig.items():
        if key in dig_dest:
            dig_dest[key] += value
        else:
            dig_dest[key] = value
    return dig_dest


class SearchableMark(ObjectWithPredicate):
    """Meta class which objects represent a searchable text data (a mark that
    can be hit) and points to an object. For example, if we have tags
    associated to images, the tag class will be a subclass of SearchableMark.

    Methods raising NotImplementedError must be implemented by subclasses.
    """

    def get_default_search_object_list(self):
        """Returns a list of SearchableMark to perform search if "search"
        does not receive one.

        Returns: A list of SearchableMark objects.
        """
        raise NotImplementedError

    def search(self, query, hit_list_scorer, object_list=None, threshold=0.90,
               min_individual_score=0.0, max_individual_score=10.0,
               debug_data=False, default_metadata=None, num_results=10, start=0):
        """ Returns a list of tuples composed by a score, and the id of objects
        pointed by SearchableMark objects in object_list. The score reflects
        the degree of matching of the query with the SearchableMarkers that
        point to the object in the tuple.

        Args:
          query: Query object to match against SearchableMark predicates.
          hit_list_scorer: HitListScorer that expresses the weight and value
            range for the internal scores of the types of SearchableMark
            in the list.
          object_list: list of SearchableMark objects to perform the search
            over. If set to None, the search defaults to whatever
            get_default_search_object_list returns.
          threshold: minimum matching score that a SearchableMark must obtain
            against the query to be considered in the search process.
          min_individual_score: Minimum values to be given  to each individual
            SearchableMark after its normalization by type.
          max_individual_score: Maximum values to be given to each individual
            SearchableMark after its normalization by type.
            min_individual_score and max_individual_score are useful to
            control the dimension of the final sources in particular search.
          debug_data: if True, the returned tuples include its list of mark
            hits.
          default_metadata: id of the metadata type used for filtering
        Returns: a list of tuples (float_score, object_id, object), where the
          score represents the fitness of an object to the query, object_id
          the id of the object, and object that corresponding object. The list
          is ordered from higher to lower scores. If debug_data=True, the tuples
          include an extra element with a list of SearchableMark objects
          associated with the result object.
        """
        tt = TM("Full Search Serial").start()
        t = TM("Data retrieval").start()
        if object_list is None:
            object_list = self.get_default_search_object_list()
            logger.info("DO retrieval")
        t.stop()

        # if field filters have been applied, keep those until we can apply them
        # to the correct table
        filter_query = None
        if isinstance(query, QueryWithFilters):
            filter_query = query._filter
            query = QueryWithFilters(query._query)

        # execute query for all matching rows
        t = TM("Actual Data Retrieval").start()
        obj_list = None
        if cache:
            logger.info("Checking cache")
            if isinstance(object_list, list):
                cache_key = "list_" + type(object_list[0]).__name__ + "_all_rows"
            else:
                cache_key = "qset_" + type(object_list.first()).__name__ + "_all_rows"

            try:
                obj_list = cache.get(cache_key)
            except Exception as e:
                logger.warning("Exception thrown while getting Redis Cache entry {}".format(e))
                obj_list = None

        if obj_list is None:
            if isinstance(object_list, list):
                obj_list = [x.value() for x in object_list]
                if cache:
                    try:
                        cache.set(cache_key, obj_list)
                    except Exception as e:
                        logger.warning("Exception thrown while setting Redis Cache entry {}".format(e))
            else:
                obj_list = list(object_list.values())
                # Data is retrieved without creating django objects via .values() to save time and memory.
                # for v in object_list.values().iterator():
                #    obj_list.append(v)
                if cache:
                    try:
                        cache.set(cache_key, obj_list)
                    except Exception as e:
                        logger.warning("Exception thrown while setting Redis Cache entry {}".format(e))
        t.stop()
        logger.info("Retrieved {} rows".format(len(obj_list)))

        if len(obj_list) == 0:
            return 0, []

        t = TM("get_hits").start()
        hits_list = query.get_hits(obj_list, threshold=threshold,
                                   default_metadata=default_metadata,
                                   filter_done=True)
        t.stop()

        logger.info("hits_list {}".format(len(hits_list)))

        # create tag table objects for all tags that matched the query
        t = TM("Recreate hits").start()
        # After keeping only the metadata that has hits, we re-create those
        # metadata objects.
        try:
            target_cls = type(object_list[0])
            m_hits_list = target_cls().recreate_metadata(hits_list)
        except Exception as e:
            logger.exception(e)
            logger.info("\n\n\n")
            logger.info("hits_list: {}".format(hits_list))
            logger.info("\n\n\n")
            raise
        t.stop()

        logger.info("expanded hits_list {}".format(len(m_hits_list)))

        t = TM("Ranking").start()
        score_id_pairs = SearchableMark.rank_hits(m_hits_list,
                                                  hit_list_scorer,
                                                  filter_query,
                                                  min_individual_score=min_individual_score,
                                                  max_individual_score=max_individual_score,
                                                  debug_data=debug_data)
        total_count = len(score_id_pairs)
        t.stop()

        if num_results > 0 and total_count > num_results:
            if start > 0 and start < total_count:
                score_id_pairs = score_id_pairs[start:num_results]
            else:
                score_id_pairs = score_id_pairs[:num_results]

        logger.info("Total result objects {}".format(len(score_id_pairs)))

        tt.stop()
        return total_count, score_id_pairs

    def get_max_min_index(self, query, default_metadata=None):
        """ returns the max and min id of a metadata objects that might be hit
        by the query.
        Args:
          query (Query object): query which "potentially hit" metadata objects
            must be checked.
          default_metadata(str): id of the metadata type used for field
            filtering.
        Returns:
          end_index: maximum id of the metadata used to answer to the query.
          start_index: minimum id of the metadata used to answer to the query.
        """
        t = TM("Pre-count").start()
        object_list = self.get_default_search_object_list()
        object_list = query.pre_do_filter(object_list,
                                          default_metadata=default_metadata)
        start_index = 0
        if type(object_list) is QuerySet:
            end_index = object_list.aggregate(Max('id'))['id__max']
            start_index = object_list.aggregate(Min('id'))['id__min']
            if end_index is None:
                end_index = 0
                start_index = 0
        else:
            end_index = len(object_list)
        t.stop()
        return end_index, start_index

    def search_parallel(self, query, hit_list_scorer, threshold=0.90,
                        min_individual_score=0.0, max_individual_score=10.0,
                        debug_data=False, default_metadata=None, num_workers=2,
                        worker_timeout=None, num_results=10, start=0):
        """ Returns a list of tuples composed by a score, and the id of objects
        pointed by SearchableMark objects in object_list. The score reflects
        the degree of matching of the query with the SearchableMarkers that
        point to the object in the tuple.

        It uses num_worker sub processes to perform the data retrieval and
        hit calculation in parallel to reduce search latency.

        Args:
          query: Query object to match against SearchableMark predicates.
          hit_list_scorer: HitListScorer that expresses the weight and value
            range for the internal scores of the types of SearchableMark
            in the list.
          threshold: minimum matching score that a SearchableMark must obtain
            against the query to be considered in the search process.
          min_individual_score: Minimum values to be given  to each individual
            SearchableMark after its normalization by type.
          max_individual_score: Maximum values to be given to each individual
            SearchableMark after its normalization by type.
            min_individual_score and max_individual_score are useful to
            control the dimension of the final sources in particular search.
          debug_data: if True, the returned tuples include its list of mark
            hits.
          default_metadata: metadata type used for filtering.
          num_workers(int): number of sub processed to be used to calculate
            the search results. A higher number should reduce the search
            latency but increase resource requirements.
          worker_timeout: number of seconds to wait for worker to complete
        Returns: a list of tuples (float_score, object_id, object), where the
          score represents the fitness of an object to the query, object_id
          the id of the object, and object that corresponding object. The list
          is ordered from higher to lower scores. If debug_data=True, the tuples
          include an extra element with a list of SearchableMark objects
          associated with the result object.
        """
        #with open() as pid_file:
        #    write()

        logger.info("Parallel Search for query:{}".format(query))
        tt = TM("Full Search Parallel").start()

        t = TM("Workers Section query:{}".format(query)).start()
        scores_by_id = {}
        extra_items = {}

        # Count all only once, then we pass the min, max indexes, number
        # of workers and worker_id to the worker, so it calculates what
        # sub-set of the data should be processed.
        max_index, min_index = self.get_max_min_index(
            query,
            default_metadata=default_metadata)
        from django.db import connection, connections
        connections.close_all()
        #connection.close()
        timeout_happend = False
        with ProcessPoolExecutor(max_workers=num_workers) as executor:
            futures = []
            stopping_worker = multiprocessing.Manager().Event()
            for worker_id in range(num_workers):
                futures.append(executor.submit(
                    self.search_worker,
                    worker_id,
                    num_workers,
                    min_index,
                    max_index,
                    query,
                    hit_list_scorer,
                    threshold=threshold,
                    min_individual_score=min_individual_score,
                    max_individual_score=max_individual_score,
                    debug_data=debug_data,
                    default_metadata=default_metadata,
                    stopping_worker=stopping_worker)
                    )
                logger.info("Worker {} created for query:{}.".format(
                    worker_id, query))

            logger.info("Waiting for workers to end for query:{}.".format(
                query))
            wait(futures, timeout=worker_timeout)
            logger.info("Signaling workers to end for query:{}.".format(
                query))
            stopping_worker.set()
            logger.info("Waiting for workers to end for real for query:{}.".format(
                query))
            wait(futures)
            logger.info("Workers work completed for query:{}.".format(
                query))
            t.stop()
            t = TM("Combine results for query:{}".format(
                query)).start()

            for f in futures:
                if f.done():
                    error = f.exception()
                    if error:
                        logger.error("Exception in search worker for query:{}".format(
                            query))
                        logger.exception(error)
                        with open('/tmp/exception_futures', 'w') as exc_out:
                            exc_out.write(error)
                    (succ, (one_scores_by_id, one_extra_items)) = f.result()

                    if not succ:
                        timeout_happend = True
                    else:
                        logger.info("I have {} results from search worker for query:{}".format(
                            len(one_scores_by_id), query))
                        combine_dicts(one_scores_by_id, scores_by_id)
                        SearchableMark.combine_extra_items(one_extra_items,
                                                           extra_items)
            t.stop()

        t = TM("Flatten for query:{}".format(query)).start()
        score_id_pairs = SearchableMark.flatten_by_score(scores_by_id,
                                                         extra_items=extra_items)
        total_count = len(score_id_pairs)
        t.stop()

        if num_results > 0:
            t = TM("Applying limit to score_id_pairs, current_size: {}, start: {}, limit: {} for query:{}".format(
                total_count, start, num_results, query)).start()
            if start > 0 and start < total_count:
                score_id_pairs = score_id_pairs[start:start+num_results]
            elif start > total_count:
                logger.error("Requested start value {} is larger than available results {} for query:{}".format(
                    start, total_count, query))
                score_id_pairs = []
            else:
                score_id_pairs = score_id_pairs[:num_results]
            t.stop()

        tt.stop()
        connection.close()
        return total_count, score_id_pairs, timeout_happend

    def search_worker(self, worker_id, num_workers, min_index, max_index,
                      query, hit_list_scorer, threshold=0.90,
                      min_individual_score=0.0, max_individual_score=10.0,
                      debug_data=False, default_metadata=None,
                      stopping_worker=None):
        """ Task code for the workers in parallel search. Retrieves a subset
        of the search metadata (calculated from the worker_id), and calculates
        the associated scores by id and required debug data.

        Args:
          worker_id(int): between 0 and number_workers-1, identifies the worker
            int he query and allows to calculate the subset of the metedata
            to use.
          num_workers(int): number of processes that are working in the query.
          min_index: first row of the table, used to calculate data chunk
          max_index: last row of the table, used to calculate data chunk
          query: Query object to match against SearchableMark predicates.
          hit_list_scorer: HitListScorer that expresses the weight and value
            range for the internal scores of the types of SearchableMark
            in the list.
          threshold: minimum matching score that a SearchableMark must obtain
            against the query to be considered in the search process.
          min_individual_score: Minimum values to be given  to each individual
            SearchableMark after its normalization by type.
          max_individual_score: Maximum values to be given to each individual
            SearchableMark after its normalization by type.
            min_individual_score and max_individual_score are useful to
            control the dimension of the final sources in particular search.
          debug_data: if True, the returned tuples include its list of mark
            hits.
          default_metadata: metadata type used for filtering.
          stopping_worker: event from the master process to signal that the
            worker should stop early

        Returns (scores_by_id, extra_items):
          score_by_id: dict of scores indexed by associated object id
            calculated from the subset of metadata after calculation with the
            query.
          extra_item: dictionary of list of tags and scores triggered by the
            query for eachif in score_by_id. (if debug_data is False, it
            just includes an empty list)

        """
        from django.db import connection
        connection.close()

        try:
            scores_by_id = {}
            extra_items = {}
            timeout_happened = False

            logger.info("Worker {} Starts for query:{}".format(
                worker_id, query))

            if hasattr(query, "_filter_only") and query._filter_only:
                # TODO move this behavior to an API endpoint for fetching table data
                scores_by_id, extra_items = self.get_directory_data(query._filter)
                return True, (scores_by_id, extra_items)

            t = TM("Retrieve data for query:{}".format(
                query), worker_id).start()

            # start forming the query using objects.all()
            t_1 = TM("GET", worker_id).start()
            object_list = self.get_default_search_object_list()
            t_1.stop()

            # if field filters have been applied, keep those until we can apply them
            # to the correct table
            filter_query = None
            if isinstance(query, QueryWithFilters):
                filter_query = query._filter
                query = QueryWithFilters(query._query)

            # remove results that include metadata type 'filt'
            t_1 = TM("Filter for query:{}".format(
                query), worker_id).start()
            object_list = query.pre_do_filter(object_list,
                                              default_metadata=default_metadata)
            t_1.stop()

            # calculate a chunk size to retrieve database rows
            t_1 = TM("Mix for query:{}".format(
                query), worker_id).start()
            chunk_size = int((max_index - min_index) / num_workers)
            first = min_index + worker_id * chunk_size
            if worker_id == num_workers - 1:
                last = max_index + 1
            else:
                last = first + chunk_size
            t_1.stop()

            object_list_is_type_list = isinstance(object_list, list)

            # select only rows within this chunk
            t_1 = TM("Slice for query:{}".format(
                query), worker_id).start()
            if object_list_is_type_list:
                object_list = object_list[first:last]
            else:
                object_list = object_list.filter(id__gte=first, id__lt=last)
                if not object_list.exists():
                    t_1.stop()
                    t.stop()
                    connection.close()
                    return True, ({}, {})
            t_1.stop()
            t.stop()

            if stopping_worker.is_set():
                connection.close()
                return False, ({}, {})

            # execute query for all matching rows
            t = TM("Actual Data Retrieval: {} rows for query:{}".format(
                last - first, query), worker_id).start()
            obj_list = None
            if cache:
                logger.info("W({}) Checking cache for query:{}".format(
                    worker_id, query))
                if object_list_is_type_list:
                    cache_key = "list" + type(object_list[0]).__name__ + "_" + str(first) + "_" + str(last)
                else:
                    cache_key = type(object_list.first()).__name__ + "_" + str(first) + "_" + str(last)

                t1 = TM("Attempting to fetch {} from cache for query:{}".format(
                    cache_key, query), worker_id).start()
                try:
                    obj_list = cache.get(cache_key)
                except Exception as e:
                    logger.warning("W({}) Exception thrown while getting Redis Cache entry {}".format(worker_id, e))
                    obj_list = None
                t1.stop()

            if obj_list is None:
                logger.warning("W({}) CACHE MISS for {} for query:{}".format(
                    worker_id, cache_key, query))
                if object_list_is_type_list:
                    obj_list = [x.value() for x in object_list]
                    if cache:
                        try:
                            cache.set(cache_key, obj_list, None)
                        except Exception as e:
                            logger.warning("W({}) Exception thrown while setting".format(worker_id) +
                                           "Redis Cache entry {} for query:{}".format(
                                               e, query))
                else:
                    obj_list = list(object_list.values())

                    if cache:
                        t_1 = TM("Set CACHE {} for query:{}".format(
                            cache_key, query), worker_id).start()
                        try:
                            cache.set(cache_key, obj_list, None)
                        except Exception as e:
                            logger.warning("W({}) Exception thrown while setting".format(worker_id) +
                                           "Redis Cache entry {} for query:{}".format(
                                               e, query))
                        t_1.stop()
            t.stop()

            logger.debug("W({}) obj_list {} for query:{}".format(
                worker_id, len(object_list), query))
            if stopping_worker.is_set():
                connection.close()
                return False, ({}, {})
            elif len(obj_list) == 0:
                connection.close()
                return True, ({}, {})

            # find all tags that match the query text
            t = TM("Query.get_hits for query:{}".format(
                query), worker_id).start()
            hits_list = query.get_hits(obj_list, threshold=threshold,
                                       default_metadata=default_metadata,
                                       filter_done=True)
            t.stop()

            logger.info("W({}) hits_list {} for query:{}".format(
                worker_id, len(hits_list), query))

            if stopping_worker.is_set():
                connection.close()
                return False, ({}, {})
            # end worker if no hits to evaluate
            elif len(hits_list) == 0:
                connection.close()
                return True, ({}, {})

            # create tag table objects for all tags that matched the query
            t = TM("Recreate hits for query:{}".format(
                query), worker_id).start()
            # After keeping only the metadata that has hits, we re-create those
            # metadata objects.
            target_cls = type(object_list[0])
            m_hits_list = target_cls().recreate_metadata(hits_list)
            t.stop()

            logger.info("W({}) expanded hits_list: {} for query:{}".format(
                worker_id, len(m_hits_list), query))

            if stopping_worker.is_set():
                connection.close()
                return False, ({}, {})

            # rank the results per entity based on the score of all tags for that entity
            t = TM("SearchableMark.rank_hits (hits: {}) for query:{}".format(
                len(m_hits_list), query), worker_id).start()

            # Limit is static now we need to make it adaptable
            if len(m_hits_list) > 150000:
                logger.info('I m here NOW')
                logger.info("The hits are more than the threshold you have to parallelize more")
                num_subworkers = 10
                subworker_timeout = 300
                process_part = int(len(m_hits_list) / num_subworkers)
                logger.info("Number of results to rank per subworker {}".format(process_part))

                with ProcessPoolExecutor(max_workers=num_subworkers) as executor:
                    futures_2 = []
                    stopping_subworker = multiprocessing.Manager().Event()
                    for subworker_id in range(0, num_subworkers):
                        first = 0 + process_part * subworker_id
                        logger.info("First index: {} to process for subworker: {}".format(first, subworker_id))
                        last = first + process_part
                        logger.info("Last index: {} to process for subworker: {}".format(last, subworker_id))
                        futures_2.append(executor.submit(
                            self.sub_worker, subworker_id, worker_id,
                            num_subworkers,
                            m_hits_list[first:last],
                            hit_list_scorer,
                            filter_query,
                            threshold=threshold,
                            min_individual_score=min_individual_score,
                            max_individual_score=max_individual_score,
                            debug_data=debug_data,
                            stopping_subworker=stopping_subworker))
                        logger.info("SubWorker {} created.".format(subworker_id))

                    logger.info("Waiting for subworkers to end.")
                    wait(futures_2, timeout=subworker_timeout)
                    logger.info("Signaling subworkers to end.")
                    stopping_subworker.set()
                    logger.info("Waiting for subworkers to end for real.")
                    wait(futures_2)
                    logger.info("SubWorkers work completed.")
                    for f in futures_2:
                        if f.done():
                            if f.exception():
                                logger.info("Exception in subworker")
                                logger.exception(f.exception())

                            (succ, (one_scores_by_id, one_extra_items)) = f.result()

                            if not succ:
                                logger.info('There is an issue with your subworker')
                                timeout_happened = True
                            else:
                                logger.info('Subworker complete')
                                combine_dicts(one_scores_by_id, scores_by_id)
                                SearchableMark.combine_extra_items(one_extra_items,
                                                                   extra_items)
            else:
                logger.info('No need for subworkers I will proceed with normal opperations')
                if stopping_worker.is_set():  # May not be needed
                    connection.close()
                    return False, ({}, {})

                (scores_by_id, extra_items) = SearchableMark.rank_hits(m_hits_list,
                                                                       hit_list_scorer,
                                                                       filter_query,
                                                                       min_individual_score=min_individual_score,
                                                                       max_individual_score=max_individual_score,
                                                                       debug_data=debug_data, worker_id=worker_id,
                                                                       no_flatten=True)

                if stopping_worker.is_set():
                    connection.close()
                    return False, ({}, {})

            t.stop()
            logger.info("Worker {} END for query:{}".format(
                worker_id, query))
            connection.close()
            logger.info("W({}) Total objects {} for query:{}".format(
                worker_id, len(scores_by_id), query))
            return True, (scores_by_id, extra_items)
        except Exception as e:
            logger.info("W({}) Exception thrown from search worker".format(worker_id) +
                        "{} for query:{}".format(e, query))
            logger.exception(e)
            connection.close()
            return False, ({}, {})

    def sub_worker(self, subworker_id, worker_id, num_subworkers, hits_list,
                   hist_list_scorer, filter_query, threshold=0.90,
                   min_individual_score=0.0, max_individual_score=10.0,
                   debug_data=False, stopping_subworker=None):
        """This is to be called only in extreme cases"""
        from django.db import connection
        connection.close()

        logger.info("Sub Worker({}) Starts".format(subworker_id))
        logger.info("I belong to Worker({})".format(worker_id))
        logger.info("You called me because the number of results exceeds the threshold")
        if stopping_subworker.is_set():
            connection.close()
            return False, ({}, {})
        logger.info("Here")
        t_2 = TM("SearchableMark.rank_hits (hits: {})".format(
            len(hits_list)), subworker_id).start()
        (scores_by_id, extra_items) = SearchableMark.rank_hits(hits_list,
                                                               hist_list_scorer,
                                                               filter_query,
                                                               min_individual_score=min_individual_score,
                                                               max_individual_score=max_individual_score,
                                                               debug_data=debug_data, worker_id=subworker_id,
                                                               no_flatten=True)
        logger.info("Operation finished")
        logger.info("Length of extra items: {}".format(len(extra_items.keys())))
        n = int(len(scores_by_id) / 10)

        scores_by_id_2 = {k: scores_by_id[k] for k in list(scores_by_id)[:n]}

        n = int(len(extra_items) / 10)
        extra_items_2 = {k: extra_items[k] for k in list(extra_items)[:n]}

        t_2.stop()
        if stopping_subworker.is_set():
            connection.close()
            return False, ({}, {})

        logger.info("SubWorker {} END".format(subworker_id))
        connection.close()
        return True, (scores_by_id_2, extra_items_2)

    def get_directory_data(self, filters):
        """
        Fetch image directory data based on a path filter.

        Args:
           filters: assumed to be a filter of type file_path=:='image_path'
        Returns(tuple): (scores_by_id, extra_items)

        """
        # TODO move this to an API endpoint instead
        # check to make sure that the file_path is in filters
        if isinstance(filters, SelectionFilter) and filters._field == "file_path":
            # get the current Django model, which for this case will be ScopeImageMetadataIndex
            cls = type(self)
            # get the ScopeImage model
            tbl_type = cls.get_metadata_model().get_related_model()
            # start the QuerySet
            query_set = tbl_type.objects.all()
            # apply our file_path filter to the table
            data = list(filters.apply_filter(query_set))
            # create the expected score dictionary with all scores set to 0
            scores_by_id = {data[i].id: 0 for i in range(len(data))}
            # store the data only, since we do not have matching tags
            extra_items = {data[i].id: [data[i], []] for i in range(len(data))}
            return scores_by_id, extra_items
        else:
            # raise an exception if someone is trying to run this in a different context
            raise Exception("Must include query terms for this filter type {}".format(filters))

    @classmethod
    def recreate_metadata(cls, hits_list):
        """Creates a list the django model object from a list of hits that
        container dictionaries created from a querySet.values() call.
        Args:
            hits_list(list): list of tuples of the format (hit_score, dict)
        Returns(list): list of tuples (hit_score, SearchableMark)
        """
        return [(x[0], cls(**x[1])) for x in hits_list]

    @classmethod
    def combine_extra_items(cls, orig, dest):
        for (item_id, value) in orig.items():
            if item_id in dest:
                dest[item_id] += value[1:]
            else:
                dest[item_id] = value
        return dest

    @classmethod
    def rank_hits(cls, hits_list, hit_list_scorer, filter_list=None, min_individual_score=0.0,
                  max_individual_score=10.0, debug_data=False, worker_id=None,
                  no_flatten=False):
        """ Ranks a list of SearchableMark hits according to their hit score,
        object score, and  hit_list_scorer configuration (weight and value
        range of each type of SearchableMark).

        Args:
          hits_list: list of tuples of the format (hit_score, SearchableMark).
            the hit_score represents the matching score of the SearchableMark
            to a query.
          hit_list_scorer: HitListScorer that expresses the weight and value
            range for the internal scores of the types of SearchableMark
            in the list.
            filter_list: a list of SelectionFilters to apply before pulling objects
          min_individual_score: Minimum values to be given  to each individual
            SearchableMark after its normalization by type.
          max_individual_score: Maximum values to be given to each individual
            SearchableMark after its normalization by type.
            min_individual_score and max_individual_score are useful to
            control the dimension of the final sources in particular search
          debug_data: bool to include extra debugging info in the results
          worker_id: int of the current worker running
          no_flatten: bool that can be used to cancel flattening the results
        Returns: a list of tuples of the format (rank, object_id). Rank
         is a score coming from weighing a MarkerObject score with its hit
         score. The object_id is the object to which the MarkerObject pointed
         to. If debug_data=True, the tuples include an extra element with a list
         of SearchableMark objects associated with the result object.
        """
        try:
            t = TM("group_hits_by_id", worker_id).start()
            hits_by_id = cls.group_hits_by_id(hits_list)
            t.stop()

            if len(hits_by_id) == 0:
                if no_flatten:
                    return {}, {}
                else:
                    return []

            logger.info("W({}) Op({}) started".format(worker_id, "Get pointed objs"))
            t = TM("Get pointed objs: {}".format(len(hits_by_id)),
                   worker_id).start()
            if len(hits_list) > 2000:
                logger.info('Your hits are too many need to do something'.format(len(hits_list)))
                n = 1000
                hits_by_id = {k: hits_by_id[k] for k in list(hits_by_id)[:n]} 
            cls_type = type(next(iter(hits_by_id.values()))[0][1])
            pointed_objs = cls_type.get_pointed_objs_list(hits_by_id.keys(), filter_list=filter_list)

            with open('/tmp/still_alive_{}'.format(worker_id), 'w') as test_out:
                test_out.write("Still running, size pointed_objs_list {}".format(len(pointed_objs)))

            logger.info("W({}) Op({}) ended".format(worker_id, "Get pointed objs {}".format(len(pointed_objs))))
            t.stop()

            logger.info("W({}) Op({}) started".format(worker_id, "extra items"))
            t = TM("extra_items: {}".format(len(hits_by_id)), worker_id).start()
            extra_items = {}
            if debug_data:
                for res_id in pointed_objs:
                    extra_items[res_id] = [pointed_objs[res_id], hits_by_id[res_id]]
            else:
                for res_id in pointed_objs:
                    extra_items[res_id] = [pointed_objs[res_id]]
            logger.info("W({}) Op({}) ended".format(worker_id, "extra items"))
            t.stop()

            logger.info("W({}) Op({}) started".format(worker_id, "scores_by_id"))
            t = TM("scores_by_id", worker_id).start()
            scores_by_id = {res_id: SearchableMark.calc_aggregated_score(
                hits_by_id[res_id],
                hit_list_scorer,
                min_individual_score=min_individual_score,
                max_individual_score=max_individual_score)
                for res_id in pointed_objs}
            logger.info("W({}) Op({}) ended".format(worker_id, "scores_by_id"))
            t.stop()

            if no_flatten:
                return scores_by_id, extra_items
            else:
                logger.info("W({}) Op({}) started".format(worker_id, "Flatten"))
                t = TM("Flatten", worker_id).start()
                sm = SearchableMark.flatten_by_score(scores_by_id,
                                                     extra_items=extra_items)
                logger.info("W({}) Op({}) ended".format(worker_id, "Flatten"))
                t.stop()
                return sm
        except Exception as e:
            logger.info("Exception for worker {}".format(worker_id))
            logger.info(e)
            import traceback
            import datetime
            traceback.print_exc()
            traceback.print_exc(file=open('/tmp/exception_{}_{}'.format(worker_id, datetime.datetime.utcnow().isoformat()), 'w'))
            raise

    @classmethod
    def group_hits_by_id(cls, results_list):
        """ Groups a list of hits (hit_score, MarkerObject) by the id of the
        objects pointed by the MakerObjects.
        Args:
          results_list: list of MarkerObject hits, i.e., tuple (hit_rank,
            MarkerObject).
        Returns: a dictionary of lists of MarkerObject hits indexed by
          the id of the object pointed by the MarkerObjects in the list.
          {"object_id":[(hit_rank, MarkerObject)]}
        """
        results_dic = {}
        for result in results_list:
            obj_id = result[1].get_object_id()
            if obj_id not in results_dic:
                results_dic[obj_id] = []
            results_dic[obj_id].append(result)
        return results_dic

    @classmethod
    def calc_aggregated_score(cls, hits_list, hit_list_scorer, total_score=0.0,
                              min_individual_score=0.0,
                              max_individual_score=10.0):
        """ Calculates the final object scores of a hits_list corresponding to
        an object and sum them all.

        Args:
          hits_list: list of MarkerObject hits, i.e., tuple (hit_rank,
            MarkerObject)
          hit_list_scorer:  HitListScorer that expresses the weight and value
            range for the internal scores of the types of SearchableMark.
          total_score: initial value to use for aggregated score
          min_individual_score: Minimum values to be given  to each individual
            SearchableMark after its normalization by type.
          max_individual_score: Maximum values to be given to each individual
            SearchableMark after its normalization by type.
            min_individual_score and max_individual_score are useful to
            control the dimension of the final sources in particular search.
        Returns: a float.
        """
        for hit_tuple in hits_list:
            total_score += SearchableMark.calc_hit_tuple_score(hit_tuple,
                                                               hit_list_scorer,
                                                               min_individual_score=min_individual_score,
                                                               max_individual_score=max_individual_score)
        return total_score

    @classmethod
    def calc_hit_tuple_score(cls, hit_tuple, hit_list_scorer,
                             min_individual_score=0.0,
                             max_individual_score=10.0):
        """ Produces a score for a MarkerObject hit according to the hit score
        and the MarkerObject object score (level of connection of the
         pointed object by the MarkerObject). The final score depends on
        the weight associated to the Type Of MarkerObject.

        The formula is:
        score = (((object_score-min_input_type_score) /
                      (max_input_type_score min_input_type_score)) *
                    (max_individual_score-min_individual_score) +
                     min_individual_score)
                  * type_weight * hit_Score
        Args:
          hit_tuple: tuple of the format (hit_score, MarkerObject), where
            hit_score is a float representing how well MarkerObject's predicate
            matches a query.
          hit_list_scorer:  HitListScorer that expresses the weight and value
            range for the internal scores of the types of SearchableMark.
          min_individual_score: Minimum values to be given  to each individual
            SearchableMark after its normalization by type.
          max_individual_score: Maximum values to be given to each individual
            SearchableMark after its normalization by type.
            min_individual_score and max_individual_score are useful to
            control the dimension of the final sources in particular search
        Returns: a float
        """
        (hit_score, mark_obj) = hit_tuple
        type_id = mark_obj.get_type_id()
        (weight, min_input,
         max_input) = hit_list_scorer.get_mark_properties(type_id)

        individual_score = ((mark_obj.get_score() - min_input) /
                            (max_input - min_input))
        individual_score *= (max_individual_score - min_individual_score)
        individual_score += min_individual_score
        individual_score *= weight
        individual_score *= hit_score
        return individual_score

    @classmethod
    def flatten_by_score(cls, adict, do_short=True, more_to_less=True,
                         extra_items=None):
        """ Transforms a dictionary of floats into an ordered
        list of tuples (float, id).

        Args:
          adict: dictionary of floats indexed by stings.
          do_short: bool, if True, the returned list is ordered by the values
            of the float.
          more_to_less: If true, the order is descending.
          extra_items: dictionary of tuples with the same keys as "adict".
            Result tuples will include the values in extra_items.
        Returns: a list of tuples (float, string) + tuple from extra items
        """
        if extra_items is None:
            extra_items_tuple = {k: () for k in adict}
        else:
            extra_items_tuple = {}
            for k in adict:
                if k not in extra_items:
                    extra_items_tuple[k] = ()
                else:
                    extra_items_tuple[k] = tuple(extra_items[k])

        if do_short:
            pairs = [(adict[k], k) + extra_items_tuple[k]
                     for k in sorted(adict, key=adict.get, reverse=more_to_less)]
        else:
            pairs = [(adict[k], k) + extra_items_tuple[k] for k in adict]
        return pairs

    def get_pointed_object(self):
        """ Returns the object that this MarkerObject refers to."""
        raise NotImplementedError

    def get_object_id(self):
        """ Returns a string id of object that this MarkerObject refers to."""
        raise NotImplementedError

    def get_type_id(self):
        """ Returns a string id of the type of the MarkerObject."""
        raise NotImplementedError

    def get_score(self):
        """ Returns the object score of the MarkerObject: strenght of the
        relationship between the MarkerObejct and its associated object."""
        raise NotImplementedError

    def get_predicate(self):
        raise NotImplementedError


class HitListScorer(object):
    """Object to store the weight and ranges for each type of MarkerObject"""

    def __init__(self):
        self._mark_types = {}
        self._default_filter = None

    @staticmethod
    def canonize_ids(an_id):
        return an_id.lower()

    def register_mark_properties(self, type_id, weight=1.0, min_input=-10.0,
                                 max_input=10.0, default_filter=False):
        """ Method to register a type of MarkerObject and its properties.
        Args:
          type_id: string identifying the MarkerObject type.
          weight: float to be used to weight hits of this type.
          min_input: min object score that a MarkerObject of this type can have.
          max_input: max object score that a MarkerObject of this type can have.
          default_filter: set this type_id as the default filter
        """
        type_id = self.canonize_ids(type_id)
        self._mark_types[type_id] = dict(weight=weight,
                                         min_input=min_input,
                                         max_input=max_input)
        if default_filter:
            self._default_filter = type_id

    def get_default_filter(self):
        return self._default_filter

    def get_weights_dict(self):
        """Return the dictionary of values with the format:
        {"(type_id_string)":{"weight":float, "min_input": float,
                             "max_input":float"}}
        """
        return self._mark_types

    def get_mark_properties(self, rank_id):
        """ Gets the properties of a MarkerRank type.
        Args:
          rank_id: string identifying the MarkerObject type
        Returns: three values weight, min_input, and max_input
        """
        rank_id = self.canonize_ids(rank_id)
        prop = self._mark_types[rank_id]
        return prop["weight"], prop["min_input"], prop["max_input"]
