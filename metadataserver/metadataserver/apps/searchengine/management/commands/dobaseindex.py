import logging
from django.core.management.base import BaseCommand

from metadataserver.apps.searchengine.datamodel.transform.indexing import SearchIndexer

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    """ Creates metadata needed for the filtering function.
    
    Usage:
        
    python manage.py dobaseindex

    Options:
        
    """
    help = 'Creates metadata needed for the filtering function.'

    def handle(self, *args, **options):
        logger.info("Creating base search metadata for objects")
        si = SearchIndexer()
        si.gen_default_tags_all_classes()
        logger.info("Done!")
