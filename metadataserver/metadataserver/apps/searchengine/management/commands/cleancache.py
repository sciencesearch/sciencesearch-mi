import logging
from django.core.management.base import BaseCommand

from metadataserver.apps.searchengine.cache import SearchCacheManager, SearchCacheConfig

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    """ Command to clean the search cache.
    
    Usage:
        
    python manage.py cleancache [options] 

    Options:
        --onlyold: elminates only entries older than max_age_s.
    """
    help = 'Imports tags from a file'

    def add_arguments(self, parser):
        parser.add_argument(
            '--onlyold', '-l',
            action='store_true',
            dest='only_old',
            default=False,
            help='If present,only cache entries older than max_age_s are'
                 'removes.',
            )

    def handle(self, *args, **options):
        self.stdout.write(
            """Cache clean command:
            - Only old: {}
            """.format(options["only_old"]))
        config = SearchCacheConfig.get_config()
        if options["only_old"]:
            logger.info("Cleaning only entries older than: {}".format(
                config.get_settings_value("max_age_s")))
            SearchCacheManager.clean_old_entries()
        else:
            logger.info("Cleaning all cache")
            SearchCacheManager.clear_cache()
