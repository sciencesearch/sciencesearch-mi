from django.core.management.base import BaseCommand, CommandError

from metadataserver.apps.searchengine.datamodel.read.searchableparser import (
    extract_and_save_searchable_tags)
from metadataserver.apps.searchengine.models import (
    METADATA_TYPES, ProposalMetadata, ScopeImageMetadata, PaperMetadata)


class Command(BaseCommand):
    """ Commend to parase searchable tags from proposals and images from a
    csv file.
    
    Usage:
        
    python manage.py searchtagparse [options] (object_type) (metadata_type) (csv_tags_file)
        
    Args:
        object_type: object that the searchable metadata refers to. Supports
          proposal, image 
        metadata_type: type of metadat accepted. supported_values:
          text_related_proposal, text_proposal
        csv_tags_file: csv file containing tags for files. Foramat is:
        id_obj,tag1,score1,tag2,score2,tag3,score3,...
    
    Options:
        --listonly: if present, tags are not imported but a list of the image
          full routes is printed.
        --basefolder (router): file system route to be considered the root of
          the files referenced in the CSV file. This route will be subtracted
          when producing the image id to matach with an image in the DB.
        --noscore: if set, the input file will be expected to not have scores
          and default_score will be used for them,
        --defaultscore (float): float to be used as tags confidence score if
          the input file do not include such scores.
    """
    help = 'Imports tags from a file'

    def add_arguments(self, parser):
        parser.add_argument(
            '--listonly', '-l',
            action='store_true',
            dest='list_only',
            default=False,
            help='If present the tags are not inserted in the database but'
                 ' a list of referenced files is produced.',
            )

        parser.add_argument(
            '--basefolder', '-b',
            dest='base_folder',
            default="",
            help='Folder to be considered "root" of the files in the csv.'
                 ' It has to be a parent route to the location in the csv. It will'
                 ' be substracted from the parsed routes to identify the images in'
                 ' the db,'
            )

        parser.add_argument(
            '--noscore', '-n',
            dest='no_score',
            action='store_true',
            default=False,
            help='If present, the input file will not include a score per tag.'
            )

        parser.add_argument(
            '--defaultscore', '-d',
            dest='default_score',
            default=1.0,
            help='Only applicable with "--no_score". It sets the score to be'
                 ' assigned to each tag. Default 1.0'
            )

        parser.add_argument('object_type', help="Type of object that the"
                                                " search data is related to.",
                            choices=["proposal", "image", "paper"])
        parser.add_argument('metadata_type', help="Type metadata that the "
                                                  " search data is comes from.",
                            choices=[x[1] for x in METADATA_TYPES])
        parser.add_argument('csv_tags_file', help="File system"
                                                  " route where the tags csv file is.")

    def handle(self, *args, **options):
        self.stdout.write(
            """"NCEM crawling script:
            - Object related: {}
            - Type of metadata: {}
            - CSV tags file: {}
            - List only: {}
            - Base folder: {}
            - File include scores: {}
            - Forced score if no score: {}
            """.format(options["object_type"],
                       options["metadata_type"],
                       options["csv_tags_file"],
                       options["list_only"],
                       options["base_folder"],
                       not options["no_score"],
                       options["default_score"]))

        metadata_type_processed = None
        for m in METADATA_TYPES:
            if m[1] == options["metadata_type"]:
                metadata_type_processed = m[0]
        if metadata_type_processed is None:
            raise CommandError("Poblem parsing the metadata type: {}".format(
                options["metadata_type"]))
        class_name = options["object_type"]
        if class_name == "proposal":
            class_type = ProposalMetadata
        elif class_name == "image":
            class_type = ScopeImageMetadata
        elif class_name == "paper":
            class_type = PaperMetadata
        else:
            raise CommandError("Unknown class_name: {}".format(class_name))

        extract_and_save_searchable_tags(options["csv_tags_file"],
                                         class_type,
                                         metadata_type_processed,
                                         base_route=options["base_folder"],
                                         list_only=options["list_only"],
                                         no_score=options["no_score"],
                                         default_score=options["default_score"])
