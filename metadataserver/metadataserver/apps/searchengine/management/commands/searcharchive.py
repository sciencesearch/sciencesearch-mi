import datetime

from django.core.management.base import BaseCommand, CommandError

from metadataserver.apps.searchengine.archive import SearchArchiveManager


class Command(BaseCommand):
    """ Command to read and manipulate the search archive. 
    
    Usage:
        
    python manage.py searcharchive (clear/dump) 

    Arguments:
        operation: if set to "clear", the archive log is cleared. If set to
          dump the log is printed in a machine readable format.
    Options:
        --startdate: if set to a date it clears/dumps from that date.
        --stopdate: if set to a date it clears/dumps until that date.
    """
    help = 'Imports tags from a file'

    def add_arguments(self, parser):
        parser.add_argument('operation', help="",
                            choices=["clear", "dump"])

        parser.add_argument(
            '--startdate', '-s',
            dest='start_date',
            default="",
            help=('Date from which to start the operation. Format:'
                  'YYYY[MM][DD][-HHMMSS]')
            )

        parser.add_argument(
            '--stopdate', '-S',
            dest='stop_date',
            default="",
            help=('Date to which operation will be performed.. Format:'
                  'YYYY[MM][DD][-HHMMSS]')
            )

        parser.add_argument(
            '--dryrun', '-d',
            action='store_true',
            dest='dry_run',
            default=False,
            help='If present, the command actions are shown but not executed.',
            )

    @staticmethod
    def curate_date(cad):
        """ Returns a datetime object if cad is a string with
        supported format. Returns a datetime of epoch 0 otherwise.
        In call cases, the date is timezone aware and set to UTC."""
        if type(cad) is str:
            suppoted_formats = ['%Y', '%Y%m',
                                '%Y%m%d',
                                '%Y%m%d-%H:%M:%S',
                                '%Y%m%d-%H%M%S'
                                ]

            for date_format in suppoted_formats:
                try:
                    return datetime.datetime.strptime(cad,
                                                      date_format).replace(
                        tzinfo=datetime.timezone.utc)
                except ValueError:
                    pass
        return None

    def handle(self, *args, **options):
        start_date = self.curate_date(options["start_date"])
        stop_date = self.curate_date(options["stop_date"])
        operation = options["operation"].lower()
        dry_run = options["dry_run"]

        log_str = "Log Archive action to be performed: {}".format(operation)
        if start_date:
            log_str += "\nfrom {}".format(start_date)
        if stop_date:
            log_str += "\nto {}".format(stop_date)
        if dry_run:
            log_str += "\n(Dry run, no operations performed)"
        self.stdout.write(log_str)

        if not dry_run:
            sam = SearchArchiveManager()
            if operation == "clear":
                sam.clear_archive(start_date, stop_date)
            elif operation == "dump":
                sam.dump_to_file(self.stdout, start_date=start_date,
                                 stop_date=stop_date)
            else:
                raise CommandError("Operations {} not supported".format(
                    operation))
