from django.core.management.base import BaseCommand, CommandError

from metadataserver.apps.searchengine.models import (
    ScopeImageMetadataIndex, ProposalMetadataIndex, PaperMetadataIndex)


class Command(BaseCommand):
    """ Creates metadata needed for the filtering function.
    
    Usage:    
    python manage.py createfastsearchindex [options] (model) 
      Args:
        model: object to create the index about: proposal, image, paper
      Options:
        --numworkers n: if set, n workers will process the index.
        
    """
    help = 'Creates metadata needed for the filtering function.'

    def add_arguments(self, parser):
        parser.add_argument('object_type', help="Type of object that the"
                                                " search data is related to.",
                            choices=["proposal", "image", "paper"])
        parser.add_argument(
            '--numworkers', '-w',
            dest='num_workers',
            default=1,
            help='Number of parallel workers to create the index'
            )

    def handle(self, *args, **options):
        self.stdout.write(
            """NCEM search index script:
            - Object related: {}
            - Num workers: {}
            """.format(options["object_type"],
                       options["num_workers"]))
        class_name = options["object_type"]
        if class_name == "proposal":
            class_type = ProposalMetadataIndex
        elif class_name == "image":
            class_type = ScopeImageMetadataIndex
        elif class_name == "paper":
            class_type = PaperMetadataIndex
        else:
            raise CommandError("Unknown class_name: {}".format(class_name))
        num_workers = int(options["num_workers"])
        if num_workers > 1:
            class_type.create_index_parallel(num_workers=num_workers)
        else:
            class_type.create_index()
