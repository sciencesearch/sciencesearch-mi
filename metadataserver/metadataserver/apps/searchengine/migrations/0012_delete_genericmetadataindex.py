# Generated by Django 2.0.4 on 2018-06-11 05:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('searchengine', '0011_auto_20180611_0443'),
    ]

    operations = [
        migrations.DeleteModel(
            name='GenericMetadataIndex',
        ),
    ]
