"""This model includes:
- Objects that are the specific implementation of search algebra meta classes
  to search proposals and images.
- Encapsulate search results.

"""
import logging
from concurrent.futures import ProcessPoolExecutor, wait

from django.utils import timezone
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Count
from django.db import transaction

from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.papermanager.models import Paper
from metadataserver.apps.searchengine.algebra.predicates import TextPredicate
from metadataserver.apps.searchengine.algebra.search import HitListScorer
from metadataserver.apps.searchengine.algebra.search import SearchableMark
from metadataserver.apps.searchengine.configuration import (
    GenericConfigurer, SCOPEIMAGE_FROM_PROPOSAL_TEXT, PROPOSAL_FROM_TEXT,
    SCOPEIMAGE_FROM_FS, PAPER_FROM_TEXT, DEFAULT_FILTER, SCOPEIMAGE_DL,
    USER_PROVIDED, UNKNOWN_SOURCE, METADATA_TYPES
    )
from metadataserver.tools.perf import TM

logger = logging.getLogger(__name__)

# Create your models here.

"""
Search config object
"""


class SearchWeightsConfig(GenericConfigurer):
    """Specific configuration object for the weights used in search score
    calculation."""

    def __init__(self, prefix="SEARCH_METADATA", custom_settings=None):
        super().__init__(prefix=prefix, custom_settings=custom_settings)
        self.hls = HitListScorer()
        self.create_secondary_objs()

    @staticmethod
    def get_default_settings():
        return {
            PROPOSAL_FROM_TEXT: dict(weight=1.0,
                                     min_input=-10.0,
                                     max_input=10.0),
            SCOPEIMAGE_FROM_PROPOSAL_TEXT: dict(weight=1.0,
                                                min_input=-10.0,
                                                max_input=10.0),
            SCOPEIMAGE_FROM_FS: dict(weight=1.0,
                                     min_input=-10.0,
                                     max_input=10.0),
            PAPER_FROM_TEXT: dict(weight=1.0,
                                  min_input=-10.0,
                                  max_input=10.0),
            DEFAULT_FILTER: dict(weight=1.0,
                                 min_input=-10.0,
                                 max_input=10.0),
            SCOPEIMAGE_DL: dict(weight=1.0,
                                min_input=0.50,
                                max_input=1.0)
            }

    def create_secondary_objs(self):
        for (key, value) in self.get_settings().items():
            self.hls.register_mark_properties(key,
                                              value[self.canonize_setting_name("weight")],
                                              value[self.canonize_setting_name("min_input")],
                                              value[self.canonize_setting_name("max_input")])


class SearchConfig(GenericConfigurer):
    def __init__(self, prefix="SEARCH_CONFIG", custom_settings=None):
        super(SearchConfig, self).__init__(prefix=prefix,
                                           custom_settings=custom_settings)

    @staticmethod
    def get_default_settings():
        return dict(parallel_enabled=False,
                    num_workers=4,
                    num_results=-1,
                    timeout_s=3600,
                    index_search=False,
                    debug_data=False)


"""
Search results objects 
"""


class AssociatedWithArchive(object):
    """Meta class for classes that are related to a Archive type class and
    they need to store their id."""
    def __init__(self):
        self._archive_id = None

    def set_archive_id(self, archive_id):
        self._archive_id = archive_id

    def get_archive_id(self):
        return self._archive_id


class SearchResultSummaryAbstract(models.Model):
    """Object that contains all the results from a query."""

    """Datetime when the search was performed."""
    date = models.DateTimeField(blank=True, default=timezone.now)
    """Index of the first result of the list of all the results of the query."""
    page_start = models.IntegerField(blank=False, default=0)
    """Index of the last result of the list of all the results of the query."""
    page_end = models.IntegerField(blank=False, default=-1)
    """Text of the query producing the result."""
    query = models.TextField()
    """Number of seconds that the search processing took."""
    search_latency_s = models.FloatField()
    """Number of total results"""
    total_results_number = models.IntegerField(blank=False, default=0)

    cached = models.BooleanField(default=False)
    query_type = models.CharField(default="", max_length=50)

    hit_list_scorer_as_json = JSONField(blank=True, default='')

    search_timed_out = models.BooleanField(blank=True, default=False)

    class Meta:
        ordering = ('date',)
        abstract = True

    def set_results(self, results_list):
        """Sets the list of SearchResult objects to present to the user."""
        self._results_list = results_list

    def get_results(self):
        """Returns the list of SearchResult objects to present to the user."""
        if hasattr(self, "_results_list"):
            return self._results_list
        else:
            return []

    def set_hit_list_scorer(self, hl_scorer):
        """Sets the HitListScorer used to calculate the result scores."""
        self._hl_scorer = hl_scorer
        self.hit_list_scorer_as_json = self._hl_scorer.get_weights_dict()

    def get_hit_list_scorer(self):
        """Returns the HitListScorer used to calculate the result scores."""
        return self._hl_scorer

    def get_cached_results(self, first_index=0, last_index=None):
        res = SearchResult.objects.filter(search_results_summary=self,
                                          result_position__gte=first_index)
        if last_index is not None and last_index != -1:
            res = res.filter(result_position__lt=last_index)
        return res

    def save_for_cache(self):
        self.save()
        for result in self.get_results():
            result.search_results_summary = self
            result.save()


class SearchResultSummary(SearchResultSummaryAbstract, AssociatedWithArchive):
    pass


class SearchResultAbstract(models.Model):
    """Object that contains an individual result information for a query."""

    class Meta:
        ordering = ('result_position',)
        abstract = True

    """Overall score obtained by the object in the search."""
    score = models.FloatField()

    image = models.ForeignKey(
        ScopeImage,
        models.CASCADE,
        blank=True,
        null=True,
        )

    proposal = models.ForeignKey(
        Proposal,
        models.CASCADE,
        blank=True,
        null=True,
        )

    paper = models.ForeignKey(
        Paper,
        models.CASCADE,
        blank=True,
        null=True,
        )

    search_results_summary = models.ForeignKey(SearchResultSummary,
                                               models.CASCADE,
                                               null=True)

    hit_list_as_json = JSONField(blank=True, default='')

    result_position = models.IntegerField(default=0)

    def set_result_obj(self, result_obj):
        """Sets the object pointed by the individual result."""
        self._result_obj = result_obj
        r_obj = result_obj

        if type(r_obj) is ScopeImage:
            self.image = r_obj
        elif type(r_obj) is Proposal:
            self.proposal = r_obj
        elif type(r_obj) is Paper:
            self.paper = r_obj
        return self

    def get_result_obj(self):
        """Returns the object  pointed by the individual result."""
        if self.image is not None:
            return self.image
        if self.proposal is not None:
            return self.proposal
        if self.paper is not None:
            return self.paper
        return None

    def set_hit_list(self, hit_list):
        """Sets the list of hit (SearchableObject, SearchableMark) objects
        used to calculate the score of this result."""
        self._hit_list = hit_list
        self.hit_list_as_json = self.hit_list_to_tuples(hit_list)
        return self

    def get_hit_list(self):
        return self.hit_list_as_json

    @staticmethod
    def hit_list_to_tuples(hit_list):
        if hit_list is None:
            return []
        else:
            return [(x[0],) + x[1].get_tuple() for x in hit_list]


class SearchResult(SearchResultAbstract, AssociatedWithArchive):
    pass


"""
Search archiving objects
"""


class SearchResultSummaryArchive(SearchResultSummaryAbstract):
    session_id = models.CharField(max_length=1024, blank=True, default='')
    search_date = models.DateTimeField(blank=True, default=timezone.now)

    class Meta:
        ordering = ('search_date',)

    def get_results(self):
        queryset = SearchResultArchive.objects.all().filter(
            search_results_summary_archive=self)
        res = [x for x in queryset.iterator()]
        return res


class SearchResultArchive(SearchResultAbstract):
    class Meta:
        ordering = ('result_position',)

    search_results_summary_archive = models.ForeignKey(
        SearchResultSummaryArchive,
        models.CASCADE,
        null=True,
        related_name="archived_results")
    user_clicked = models.IntegerField(default=0)

    def mark_clicked(self):
        self.user_clicked += 1
        self.save()


"""
Searchable metadata 
"""


class SearchableObject(models.Model):
    """Abstract class to store searchable text associated to an object."""

    """Text to be used as search target."""
    text_content = models.TextField(blank=False)
    """Confidence score, higher value indicates that this piece of metadata
    is very relevant for its associated object."""
    score = models.FloatField(default=0)
    """Type according to the algebra search metadata types."""
    metadata_type = models.CharField(choices=METADATA_TYPES, max_length=4,
                                     blank=False, db_index=True)

    class Meta:
        abstract = True

    def get_type_id(self):
        return self.metadata_type

    def get_score(self):
        return self.score

    def get_predicate(self):
        """Returns a searchable object from the text,"""
        return TextPredicate(self.text_content)

    def get_object_id(self):
        """Returns the string id of the object that the metadata refers to"""
        raise NotImplementedError

    def get_default_search_object_list(self):
        """Returns the list of metadata objects to be used in a search by
        default."""
        raise NotImplementedError

    def get_pointed_object(self):
        """Returns the object that the metadata refers to"""
        raise NotImplementedError

    def get_tuple(self):
        """Returns some of the object data as a tuple."""
        return self.score, str(self.metadata_type), str(self.text_content)

    @classmethod
    def get_related_model(cls):
        """Returns the class of the object pointed by the metadata."""
        return None

    @classmethod
    def get_related_model_object_by_id(cls, id_str):
        return None

    @classmethod
    def get_related_model_object_id_field_name(cls):
        return "id"

    @classmethod
    def get_pointed_objs_list(cls, list_ids, filter_list=None):
        """ Returns a dictionary of objects which ids are in list_ids.
        Args:
          list_ids: list of ids of the object "cls". If no object exists with
            any given id, it will just not appear in the result.
          filter_list: list of SelectionFilters to apply
        Returns:
          dict(cls): dictionary indexed by ids of the cls objects
        """
        filter_dict = {}
        id_field_name = cls.get_related_model_object_id_field_name()
        filter_att = "{}__in".format(id_field_name)
        filter_dict[filter_att] = list_ids
        obj_list = cls.get_related_model().objects.filter(**filter_dict)

        logger.info(obj_list.query)
        import os

        if filter_list is not None:
            obj_list = filter_list.apply_filter(obj_list)

        obj_dict = {}
        if obj_list.exists():
            logger.info("pulling object data")
            pos = 0
            for obj in obj_list.iterator(chunk_size=100):
                logger.info("ppid: {}, pid: {} chunk position: {}".format(os.getppid(), os.getpid(), pos))
                obj_dict[getattr(obj, id_field_name)] = obj
                pos += 100
            logger.info("pulled all related object data")
        return obj_dict

    @classmethod
    def get_related_object_by_id(cls, id_str):
        raise NotImplementedError

    def set_related_object_by_id(self, id_str):
        """Sets the related object by id."""
        obj = self.get_related_object_by_id(id_str)
        if not obj:
            return False
        self.related_object = obj
        return True

    @classmethod
    def create_entry(cls, text_content, score, metadata_type, related_object):
        """Creates and stores a SearchableObject or just retrieves if it exists.
         Args:
           text_content(str): text tag of the search metadata
           score(float): relevance score of the search matadata,
           metadata_type(str): string of METADATA_TYPES indicating the type
             of metadata.
           related_object(object): object associated with the metadata
        Returns: True if the object was created. False otherwise.
        """
        md, created = cls.objects.get_or_create(
            text_content=text_content,
            score=score,
            related_object=related_object,
            metadata_type=metadata_type)
        if created:
            md.save()
        else:
            logger.info("Not inserting duplicated entry: {},{},{},{}"
                  "".format(text_content, score,
                            metadata_type, related_object))
        cls.get_index_model().create_index_entry(text_content,
                                                 metadata_type)
        return created

    @classmethod
    def get_index_model(cls):
        """Returns the fast search index class that corresponds to this
        SearchableObject."""
        return None


class GenericMetadataIndex(SearchableObject, SearchableMark):
    """ Base class for the fast search indexes for SearchableObjects. It
    stores all the individual text strings present in the metadata. Search
    is perform over the GenericMetadataIndex class instead of its associated 
    SearchableObject. It calculates hits over the strings in it and only
    retrieves the metadata for the hits over a certain threshold. 
    GenericMetadataIndex class will return the final SearchableObjects as
    results. To extend this class it is needed to implement:
    get_metadata_model, get_related_model 
    
    """

    class Meta(SearchableObject.Meta):
        unique_together = (('text_content', 'metadata_type'),)
        abstract = True
        indexes = [
            models.Index(fields=['text_content']),
            ]

    @classmethod
    def get_metadata_model(cls):
        """Return the SearchableObject (sarch metadata()associated with this
        class"""
        return None

    @classmethod
    def get_related_model(cls):
        """Return the pointed object (target of saerch) associated with this
        class"""
        return None

    def get_metadata_entries(self):
        """Returns a QuerySet with all the potential metadatada objects
        associated with this object: i.e., have the same text_content"""

        metadata_model = self.get_metadata_model()
        return metadata_model.objects.filter(text_content=self.text_content,
                                             metadata_type=self.metadata_type)

    @classmethod
    def get_metadata_entries_as_dict(cls, text_content_list):
        """Returns a dictionary indexed by text_content_list. Each item
        is a list of objects (class returned by get_metadata_model) which 
        text_content matches the index.
        Args:
          text_content_list(list(str)): list of strings
        Returns(dict[str]:list(object)): Dictionary indexed by strings. Each
          item is a list of objects (class whatever get_metadata_model returns)
          which text_content matches the index.
        """
        metadata_model = cls.get_metadata_model()
        entries = list(metadata_model.objects.filter(
            text_content__in=text_content_list))
        entries_dict = {}
        for entry in entries:
            key = "{}-{}".format(entry.text_content,
                                 entry.metadata_type)
            if key not in entries_dict:
                entries_dict[key] = []
            entries_dict[key].append(entry)
        return entries_dict

    @classmethod
    def get_metadata_entries_bulk(cls, hits_list):
        """Recreates a list metadata objects from a list of hits
        that include only the hit score and text_string. 
        Args:
          hits_list(list): list of hits with text strings. A hit is a tuple
            of the format (float, str). Str is unique.
        Returns(list): list of metadata hits. The hits are tuples of the format
          (float, metadata_model), were metadata model is the class returned by
          get_metadata_mode. Tuples are calculated as all the metadata
          with text_content equal to the an entry in hits_list and hit_score
          the one of that particular hit in hits_list.
        """
        with transaction.atomic(None, False):
            is_value = hits_list and type(hits_list[0][1]) is dict
            if is_value:
                text_entries = [hit[1]["text_content"] for hit in hits_list]
            else:
                text_entries = [hit[1].text_content for hit in hits_list]
            entries_dict = cls.get_metadata_entries_as_dict(text_entries)

            all_entries = []

            for hit in hits_list:
                if is_value:
                    text_content = hit[1]["text_content"]
                    metadata_type = hit[1]["metadata_type"]
                else:
                    text_content = hit[1].text_content
                    metadata_type = hit[1].metadata_type
                for entry in entries_dict[
                    "{}-{}".format(text_content,
                                   metadata_type)]:
                    all_entries.append((hit[0], entry))
        return all_entries

    @classmethod
    def create_index(cls, worker_id=None, num_workers=None):
        """Fill the model with all the unique text_content values in its
        associated metadata class. If worker_id and num_workers are specified
        it will only create the index based on an slice of associated metadata.
        
        Args:
          worker_id(int): if the process worker running this method.
            If only one process is creating the index in parallel, worker_id
            is set to None.
          num_workers(int): number of  workers creating the index in parallel.
            If set to 0 or None, only one worker is creating the index.
        """
        metadata_model = cls.get_metadata_model()

        for metadata_type in [x[0] for x in METADATA_TYPES]:
            logger.info("Grouping entries for metadata: {}".format(metadata_type))
            entries = metadata_model.objects.filter(
                metadata_type=metadata_type)
            entries = entries.values('text_content').annotate(
                Count('text_content'))
            logger.info("Found different text content: {}".format(
                entries.count()))
            if worker_id is not None and num_workers:
                num_entries = entries.count()
                chunk_size = int(num_entries / num_workers)
                start_index = worker_id * chunk_size

                if worker_id < num_workers - 1:
                    end_index = start_index + chunk_size
                else:
                    end_index = num_entries
                entries = entries[start_index:end_index]
                logger.info("W({}): Creating index entries: {}-{}".format(
                    worker_id, start_index, end_index))
                logger.info("W({}) len: {}".format(worker_id, len(entries)))
                cls.create_index_op(worker_id, entries, metadata_type)
            else:
                with transaction.atomic(None, False):
                    logger.info("Creating index entries")
                    cls.create_index_op(None, entries, metadata_type)
                    logger.info("Index entries created")

    @classmethod
    def create_index_op(cls, worker_id, entries, metadata_type):
        """Creates index entry for each metadata object in entries.
        Args:
          worker_id(int): if the process worker running this method.
            If only one process is creating the index in parallel, worker_id
            is set to None.
          entries: list of metadata objects. Their text_content fields will
            be inserted as index entries.
          metadata_type: type of metadata for the entries.  
        """
        entries_to_process = len(entries)
        one_percent = max(int(entries_to_process / 100), 1)
        tm = TM("Creating index entries: Start process".format(1),
                w=worker_id).start()

        for (i, entry) in zip(range(entries_to_process), entries):
            if i % one_percent == 0:
                tm.stop()
                percent = i / one_percent
                tm = TM("Processed {}%".format(percent + 1),
                        w=worker_id).start()
            cls.create_index_entry(entry['text_content'],
                                   metadata_type)

    @classmethod
    def create_index_parallel(cls, num_workers=2):
        """Creates index for the class by running num_worker processes in
        parallel.
        """
        with ProcessPoolExecutor(max_workers=num_workers) as executor:
            futures = []
            logger.info("Creating workers for index creation")
            for worker_id in range(num_workers):
                futures.append(executor.submit(cls.create_index,
                                               worker_id,
                                               num_workers))
            logger.info("Waiting for workers to be done")
            wait(futures)

    @classmethod
    def create_index_entry(cls, text_content, metadata_type):
        """Creates an entry in the index. Reads it otherwise."""
        md, created = cls.objects.get_or_create(
            text_content=text_content,
            metadata_type=metadata_type)
        if created:
            md.save()
        else:
            logger.info("Index for {}-{} already exists, not creating"
                  "".format(text_content, metadata_type))
        return created

    def recreate_metadata(self, hits_list):
        return self.get_metadata_entries_bulk(hits_list)

    def get_default_search_object_list(self):
        raise NotImplementedError

    def get_object_id(self):
        raise NotImplementedError

    def get_pointed_object(self):
        raise NotImplementedError

    @classmethod
    def get_related_object_by_id(cls, id_str):
        raise NotImplementedError


class ScopeImageMetadataIndex(GenericMetadataIndex):
    class Meta(GenericMetadataIndex.Meta):
        indexes = [
            models.Index(fields=["text_content"]),
            models.Index(fields=["metadata_type"])
            ]

    @classmethod
    def get_metadata_model(cls):
        return ScopeImageMetadata

    def get_object_id(self):
        raise NotImplementedError

    def get_pointed_object(self):
        raise NotImplementedError

    def get_default_search_object_list(self):
        return ScopeImageMetadataIndex.objects.all()

    @classmethod
    def get_related_object_by_id(cls, id_str):
        raise NotImplementedError


class ScopeImageMetadata(SearchableObject, SearchableMark):
    """Implementation of searchable metadata that is associated to an 
    ScopeImage object."""

    related_object = models.ForeignKey(ScopeImage,
                                       on_delete=models.CASCADE,
                                       related_name="related_object_id",
                                       related_query_name="related_object")

    class Meta(SearchableObject.Meta):
        unique_together = (('related_object', 'text_content', 'metadata_type'),)

    def get_pointed_object(self):
        return self.related_object

    def get_object_id(self):
        return self.related_object_id

    def get_default_search_object_list(self):
        return ScopeImageMetadata.objects.all()

    @classmethod
    def get_related_object_by_id(cls, id_str):
        return ScopeImageMetadata.get_related_model_object_by_id(id_str)

    @classmethod
    def get_related_model_object_by_id(cls, id_str):
        try:
            return ScopeImage.objects.get(original_route=id_str)
        except ObjectDoesNotExist:
            return None

    @classmethod
    def get_related_model(cls):
        return ScopeImage

    @classmethod
    def get_index_model(cls):
        return ScopeImageMetadataIndex


class ProposalMetadata(SearchableObject, SearchableMark):
    """Implementation of searchable metadata that is associated to a 
    Proposal object."""
    related_object = models.ForeignKey(Proposal, on_delete=models.CASCADE)

    class Meta(SearchableObject.Meta):
        unique_together = (('related_object', 'text_content', 'metadata_type'),)

    def get_default_search_object_list(self):
        return ProposalMetadata.objects.all()

    def get_pointed_object(self):
        return self.related_object

    def get_object_id(self):
        return self.related_object_id

    @classmethod
    def get_related_model_object_by_id(cls, id_str):
        try:
            return Proposal.objects.get(serial_number=id_str)
        except ObjectDoesNotExist:
            return None

    @classmethod
    def get_related_object_by_id(cls, id_str):
        return ProposalMetadata.get_related_model_object_by_id(id_str)

    @classmethod
    def get_related_model(cls):
        return Proposal

    @classmethod
    def get_related_model_object_id_field_name(cls):
        return "serial_number"

    @classmethod
    def get_index_model(cls):
        return ProposalMetadataIndex


class ProposalMetadataIndex(GenericMetadataIndex):

    @classmethod
    def get_metadata_model(cls):
        return ProposalMetadata

    def get_object_id(self):
        raise NotImplementedError

    def get_pointed_object(self):
        raise NotImplementedError

    def get_default_search_object_list(self):
        return ProposalMetadataIndex.objects.all()

    @classmethod
    def get_related_object_by_id(cls, id_str):
        raise NotImplementedError


class PaperMetadata(SearchableObject, SearchableMark):
    """Implementation of searchable metadata that is associated to a 
    Paper object."""
    related_object = models.ForeignKey(Paper, on_delete=models.CASCADE)

    class Meta(SearchableObject.Meta):
        unique_together = (('related_object', 'text_content', 'metadata_type'),)

    def get_default_search_object_list(self):
        return PaperMetadata.objects.all()

    def get_pointed_object(self):
        return self.related_object

    def get_object_id(self):
        return self.related_object_id

    @classmethod
    def get_related_model_object_by_id(cls, id_str):
        try:
            return Paper.objects.get(simple_title=id_str)
        except ObjectDoesNotExist:
            return None

    @classmethod
    def get_related_model(cls):
        return Paper

    @classmethod
    def get_related_model_object_id_field_name(cls):
        return "simple_title"

    @classmethod
    def get_related_object_by_id(cls, id_str):
        return PaperMetadata.get_related_model_object_by_id(id_str)

    @classmethod
    def get_index_model(cls):
        return PaperMetadataIndex


class PaperMetadataIndex(GenericMetadataIndex):

    @classmethod
    def get_metadata_model(cls):
        return PaperMetadata

    def get_object_id(self):
        raise NotImplementedError

    def get_pointed_object(self):
        raise NotImplementedError

    def get_default_search_object_list(self):
        return PaperMetadataIndex.objects.all()

    @classmethod
    def get_related_object_by_id(cls, id_str):
        raise NotImplementedError
