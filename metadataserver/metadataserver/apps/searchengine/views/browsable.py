"""This file provides views to browse the metadata used in search. It also
provides access to the search functionalities."""
""" Debug views to review the metadata in the datbase."""
import time
import logging

from django.http import Http404
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.authentication import (TokenAuthentication,
                                           SessionAuthentication)
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.searchengine.algebra.query import QueryParser
from metadataserver.apps.searchengine.archive import SearchArchiveManager
from metadataserver.apps.searchengine.cache import SearchCacheManager, SearchCacheConfig
from metadataserver.apps.searchengine.models import (
    ProposalMetadata, ScopeImageMetadata, PaperMetadata, SearchResult,
    SearchResultSummary, SearchWeightsConfig, SearchConfig, DEFAULT_FILTER
    )
from metadataserver.apps.searchengine.serializers import (
    ProposalMetadataSerializer, ScopeImageMetadataSerializer, PaperMetadataSerializer,
    SearchResultSummarySerializer
    )
from metadataserver.tools.perf import TM

logger = logging.getLogger(__name__)


class SearchableProposalMetadatata(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)

    queryset = ProposalMetadata.objects.all()
    serializer_class = ProposalMetadataSerializer


class SearchableScopeImageMetadata(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)

    queryset = ScopeImageMetadata.objects.all()
    serializer_class = ScopeImageMetadataSerializer


class SearchableScopePaperMetadata(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)

    queryset = PaperMetadata.objects.all()
    serializer_class = PaperMetadataSerializer


class SearchProposals(viewsets.ModelViewSet):
    """Search view, to produce queries. 
    Get Args:
      - query: urlencoded string with the text query.
      - Any SEARCH_METADATA_METADATAID__WEIGHT, SEARCH_METADATA_METADATAID_MIN,
        SEARCH_METADATA_METADATAID_MAX: will set the three values used in
        the search score calculation for the metadata indentified by METADATAID.
    
    Returns a serialized object of the type SearchResultSummarySerializer,
    in the format:
    {"query":"text query",
            "date":"2010-01-01T00:00:00Z",
            "search_latency_s":1.34,
            "page_start":2,"page_end":4,
            "search_weights":
                {"si":{"weight":1.0,"min_input":-2.0,"max_input":4.0},
                "pt":{"weight":2.0,"min_input":-3.0,"max_input":5.0}},
            "results_list":[{
            "result_object":{"serial_number":"1234","status":"ACTIVE",
                "title":"","interaction_type":-1,"project_leader":"N",
                "primary_researcher":"N","co_researcher":"N","first_name":"",
                "last_name":"","email":"","bin":"",
                "date_project_started":"1970-01-01T00:00:00Z",
                "date_project_end":"1970-01-01T00:00:00Z",
                "assigned_scientist":"","pi_name":"",
                "primary_researcher_name":"","contact_with_staff_name":"",
                "significance":"","foundry_capabilities":"",
                "work_description":"","project_duration":"",
                "relevant_experience":"","project_summary":"",
                "follow_on_progress_effort":""},
            "score":0.5,
            "hit_list":[[0.1, 2.2,"pt","etch"],[0.2, 2.1,"pt","material"],
            [0.3, 1.2,"si","other"]]
            }]}
    """

    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = Proposal.objects.all()
    serializer_class = SearchResultSummarySerializer

    def get_session_id(self):
        """Makes sure that the request has a session and returns its id string"""
        if not self.request.session.session_key:
            self.request.session.create()
        return self.request.session.session_key

    def dispatch(self, request, *args, **kwargs):
        self.query_text = request.GET.get('query', "")
        self.page_start = request.GET.get('page_start', 0)
        self.num_results = request.GET.get('num_results', 10)

        try:
            self.page_start = int(self.page_start)
        except ValueError:
            raise Http404("page_start({}) has to be a number.".format(
                self.page_start))

        if self.page_start < 0:
            raise Http404("page_start({}) cannot be negative.".format(
                self.page_start))
        try:
            self.num_results = int(self.num_results)
        except ValueError:
            raise Http404("num_results({}) has to be a number.".format(
                self.num_results))

        if self.num_results <= 0:
            raise Http404("num_results({}) cannot be negative or 0.".format(
                self.page_start))

        self.page_end = self.page_start + self.num_results

        return super(SearchProposals, self).dispatch(
            request, *args, **kwargs)

    def get_search_object(self):
        return ProposalMetadata()

    def get_query_type(self):
        return "proposal"

    def get_queryset(self):
        """ To test:
        
        http://127.0.0.1:8000/api/search/do_search/proposals/?query=atomic%20resolution
        Args:
          query: urlencoded string that will be parsed as query.
          Any value named like a search config setting: will be used to
            produce a search overwritting settings with those settings. 
          page_start: index of the first result to be returns. If not set, the
            results start for the first.
          num_results: if set to a integer, the search will only return
            num_results values. If not set, only 100 results are returned.
        """

        try:
            if not self.query_text:
                return []
            start_time = time.time()

            search_weights = SearchWeightsConfig.get_config(
                custom_settings=self.request.GET)
            cache_config = SearchCacheConfig.get_config(
                custom_settings=self.request.GET)
            search_config = SearchConfig.get_config(
                custom_settings=self.request.GET)

            debug_data = search_config.get_settings_value("debug_data")
            parallel_search_enabled = search_config.get_settings_value(
                "parallel_enabled")
            parallel_search_num_workers = search_config.get_settings_value(
                "num_workers")
            search_worker_timeout = search_config.get_settings_value(
                "timeout_s")
            index_search = search_config.get_settings_value("index_search")
            num_results = search_config.get_settings_value("num_results")

            if parallel_search_enabled:
                logger.info("Using parallel search")

            pm = self.get_search_object()
            if index_search:
                index_class = type(pm).get_index_model()
                if index_class:
                    logger.info("Using index search!")
                    pm = index_class()

            query = QueryParser.parse_filtered_query(self.query_text, related_field_name=None)
            hl_scorer = search_weights.hls

            cached_result = SearchCacheManager.get_cached_query(
                self.query_text,
                query_type=self.get_query_type(),
                start_time=start_time,
                first_index=self.page_start,
                last_index=self.page_end,
                hit_list_weights=hl_scorer.get_weights_dict(),
                config=cache_config)
            logger.info("Inside VIEW for query: {}".format(self.query_text))
            if cached_result:
                srs = cached_result
            else:
                t = TM("Search action for query: {}".format(self.query_text), "VIEW").start()
                search_timed_out = False
                if parallel_search_enabled:
                    total_results_number, result, search_timed_out = pm.search_parallel(
                        query,
                        hl_scorer,
                        min_individual_score=0.0,
                        max_individual_score=1.0,
                        debug_data=debug_data,
                        default_metadata=DEFAULT_FILTER,
                        num_workers=parallel_search_num_workers,
                        worker_timeout=search_worker_timeout,
                        num_results=num_results,
                        start=self.page_start)
                else:
                    total_results_number, result = pm.search(
                        query,
                        hl_scorer,
                        min_individual_score=0.0,
                        max_individual_score=1.0,
                        debug_data=debug_data,
                        default_metadata=DEFAULT_FILTER,
                        num_results=num_results,
                        start=self.page_start)
                elapsed_time = (time.time() - start_time)
                t.stop()

                results_list = []
                t = TM("Results List Creation for query: {}".format(self.query_text), "VIEW").start()
                if isinstance(result, list) and len(result) > 0:
                    for (one_result, position) in zip(result, range(len(result))):
                        r_obj = SearchResult(result_position=position,
                                             score=one_result[0])
                        r_obj.set_result_obj(one_result[2])
                        if debug_data:
                            r_obj.set_hit_list(one_result[3])
                        results_list.append(r_obj)
                t.stop()
                t1 = TM("Result Summary Creation for query: {}".format(self.query_text), "VIEW").start()

                srs = SearchResultSummary(page_start=self.page_start,
                                          page_end=self.page_end,
                                          query=self.query_text,
                                          search_latency_s=elapsed_time,
                                          date=timezone.now(),
                                          total_results_number=total_results_number,
                                          query_type=self.get_query_type(),
                                          search_timed_out=search_timed_out)
                srs.set_hit_list_scorer(hl_scorer)

                srs_c = SearchResultSummary(page_start=self.page_start,
                                            page_end=self.page_start + num_results,
                                            query=self.query_text,
                                            search_latency_s=elapsed_time,
                                            date=timezone.now(),
                                            total_results_number=total_results_number,
                                            query_type=self.get_query_type(),
                                            search_timed_out=search_timed_out)
                srs_c.set_hit_list_scorer(hl_scorer)
                srs_c.set_results(results_list)
                t1.stop()

                t2 = TM("Store Cache for query: {}".format(self.query_text), "VIEW").start()
                SearchCacheManager.save_result_cache(srs_c, config=cache_config)
                t2.stop()

                # compute adjusted start and end indices for accessing the results list
                if self.page_start >= num_results:
                    page_start = int(self.page_start % num_results)
                    page_end = page_start + self.num_results
                else:
                    page_start = self.page_start
                    page_end = self.page_end

                # use the computed start and end to extract a slice from the result set for the client
                if page_end <= len(results_list):
                    results_list_final = results_list[page_start:page_end]
                else:
                    results_list_final = results_list[page_start:]

                srs.set_results(results_list_final)

                if total_results_number > 0:
                    report = "Results total count: {}, backend returned: {}, view returned: {}, " +\
                        "query: {}, start: {}, end: {}"
                    logger.info(report.format(
                        total_results_number, len(results_list), len(results_list_final),
                        self.query_text, self.page_start, self.page_end))
                else:
                    logger.info("Empty result from search action")

            session_id = self.get_session_id()
            t = TM("Archiving", "VIEW").start()
            sam = SearchArchiveManager()
            sam.query_happens(srs, session_id)
            t.stop()

            return [srs]
        except Exception as e:
            logger.exception(e)
            return []


class SearchImages(SearchProposals):

    def get_search_object(self):
        return ScopeImageMetadata()

    def get_query_type(self):
        return "scopeimage"


class SearchPapers(SearchProposals):

    def get_search_object(self):
        return PaperMetadata()

    def get_query_type(self):
        return "paper"
