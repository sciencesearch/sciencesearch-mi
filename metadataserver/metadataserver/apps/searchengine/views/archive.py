from rest_framework import viewsets
from rest_framework.authentication import (TokenAuthentication,
                                           SessionAuthentication)
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response

from metadataserver.apps.searchengine.models import (
    SearchResultSummaryArchive, SearchResultArchive
    )
from metadataserver.apps.searchengine.serializers import (
    SearchResultSummaryArchiveSerializer, SearchResultArchiveSerializer
    )


class ArchiveContent(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)

    queryset = SearchResultSummaryArchive.objects.all()
    serializer_class = SearchResultSummaryArchiveSerializer


class SearchResultContent(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = SearchResultArchive.objects.all()
    serializer_class = SearchResultArchiveSerializer

    @detail_route(methods=['post'], url_path='clicked')
    def clicked(self, request, pk):
        result = self.get_object()
        result.mark_clicked()
        serialized_result = SearchResultArchiveSerializer(result)
        return Response(serialized_result.data)
