from rest_framework import viewsets
from rest_framework.authentication import (
    TokenAuthentication, SessionAuthentication
    )
from rest_framework.permissions import IsAdminUser

from metadataserver.apps.searchengine.cache import SearchCacheManager
from metadataserver.apps.searchengine.models import SearchResultSummary
from metadataserver.apps.searchengine.serializers import (
    SearchCacheManagerSerializer, SearchResultSummarySerializer
    )


class CacheState(viewsets.ModelViewSet):
    
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)
    
    queryset =  SearchCacheManager.objects.all()
    serializer_class = SearchCacheManagerSerializer
    
    def get_queryset(self):
        return [SearchCacheManager()]
    
    
class CacheContent(viewsets.ModelViewSet):
    
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)
    
    queryset =  SearchResultSummary.objects.all()
    serializer_class = SearchResultSummarySerializer
    
