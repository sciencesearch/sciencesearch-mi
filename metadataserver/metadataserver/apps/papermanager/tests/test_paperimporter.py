import os
import logging

from metadataserver.apps.papermanager.datamodel.read.paperimporter import paper_import
from metadataserver.apps.papermanager.models import Author, Paper
from metadataserver.seminfer.tests import TestFileGeneric

logger = logging.getLogger(__name__)


class TestPaperImport(TestFileGeneric):
    _tmp_folder = "/tmp/test_PAPERMANAGER"

    def setUp(self):
        self._register_tmp_folder(self._tmp_folder)
        self._create_empty(self._tmp_folder, is_folder=True)

    def _create_test_file(self, file_name, content):
        file_route = os.path.join(self._tmp_folder, file_name)
        self._register_tmp_file(file_route)

        cvs_file = open(file_route, 'w')
        cvs_file.write(content)
        cvs_file.close()

        return file_route

    def test_import_paper(self):
        text = """
{
  "sections": [{
    "text": "This is text1."
  }, {
    "heading": "Title 1",
    "text": "Text 2"
  }],
  "year": 2018,
  "references": [],
  "id": "SP:751ea687e6c5c2be8fa9a07c3b4d90abc2d462f1",
  "authors": [{
    "name": "Gonzalo Rodrigo",
    "affiliations": []
  }, {
    "name": "Juan Perez",
    "affiliations": []
  }],
  "abstractText": "The Abstract",
  "title": "The Title"
}
        """
        file_route = self._create_test_file("myPaper.pdf.json", text)

        paper_import(file_route, "/tmp/test_PAPERMANAGER")

        p = Paper.objects.get(file_name="myPaper.pdf.json")

        self.assertEqual(p.title, "The Title")
        self.assertEqual(p.simple_title, "TheTitle")
        self.assertEqual(p.text_body, "This is text1.\nTitle 1\nText 2\n")
        self.assertEqual(p.authors.count(), 2)
        self.assertEqual(list(p.authors.all())[0].matching_string, "Gonzalo Rodrigo")
        self.assertEqual(list(p.authors.all())[1].matching_string, "Juan Perez")

    def test_import_paper_dir(self):
        text = """
{
  "sections": [{
    "text": "This is text1."
  }, {
    "heading": "Title 1",
    "text": "Text 2"
  }],
  "year": 2018,
  "references": [],
  "id": "SP:751ea687e6c5c2be8fa9a07c3b4d90abc2d462f1",
  "authors": [{
    "name": "Gonzalo Rodrigo",
    "affiliations": []
  }, {
    "name": "Juan Perez",
    "affiliations": []
  }],
  "abstractText": "The Abstract",
  "title": "The Title A"
}
        """
        file_route = self._create_test_file("myPaper1.pdf.json", text)

        text = """
{
  "sections": [{
    "text": "This is text1."
  }, {
    "heading": "Title 1",
    "text": "Text 2"
  }],
  "year": 2018,
  "references": [],
  "id": "SP:751ea687e6c5c2be8fa9a07c3b4d90abc2d462f1",
  "authors": [{
    "name": "Gonzalo Rodrigo",
    "affiliations": []
  }, {
    "name": "Juan Perez",
    "affiliations": []
  }],
  "abstractText": "The Abstract",
  "title": "The Title B"
}
        """
        file_route = self._create_test_file("myPaper2.pdf.json", text)

        logger.info("FR {}".format(file_route))
        paper_import("/tmp/test_PAPERMANAGER/", "/tmp/test_PAPERMANAGER")

        p = Paper.objects.get(file_name="myPaper1.pdf.json")

        self.assertEqual(p.title, "The Title A")
        self.assertEqual(p.simple_title, "TheTitleA")
        self.assertEqual(p.text_body, "This is text1.\nTitle 1\nText 2\n")
        self.assertEqual(p.authors.count(), 2)
        self.assertEqual(list(p.authors.all())[0].matching_string, "Gonzalo Rodrigo")
        self.assertEqual(list(p.authors.all())[1].matching_string, "Juan Perez")

        p = Paper.objects.get(file_name="myPaper2.pdf.json")

        self.assertEqual(p.title, "The Title B")
        self.assertEqual(p.simple_title, "TheTitleB")
        self.assertEqual(p.text_body, "This is text1.\nTitle 1\nText 2\n")
        self.assertEqual(p.authors.count(), 2)
        self.assertEqual(list(p.authors.all())[0].matching_string, "Gonzalo Rodrigo")
        self.assertEqual(list(p.authors.all())[1].matching_string, "Juan Perez")

        a = Author.objects.get(matching_string="Gonzalo Rodrigo")
        papers = list(a.papers.all())
        self.assertEqual(len(papers), 2)
        self.assertEqual(papers[0].simple_title, "TheTitleB")
        self.assertEqual(papers[1].simple_title, "TheTitleA")
