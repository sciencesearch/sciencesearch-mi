import datetime
import os

from django.core.management import call_command
from metadataserver.apps.papermanager.datamodel.read.paperimporter import (
    load_single_paper_data, load_paper_data_from_json, update_single_paper_url,
    get_simple_title_url, load_paper_urls_file
    )
from metadataserver.apps.papermanager.models import Paper
from metadataserver.seminfer.tests import TestFileGeneric


class TestLoadPapersUrl(TestFileGeneric):
    _tmp_folder = "/tmp/test_PAPERMANAGER"

    def setUp(self):
        self._register_tmp_folder(self._tmp_folder)
        self._create_empty(self._tmp_folder, is_folder=True)

    def _create_test_file(self, file_name, content):
        file_route = os.path.join(self._tmp_folder, file_name)
        self._register_tmp_file(file_route)

        cvs_file = open(file_route, 'w')
        cvs_file.write(content)
        cvs_file.close()

        return file_route

    def test_update_single_paper_url(self):
        p = Paper(simple_title="Title",
                  file_name="title.pdf")
        p.save()

        update_single_paper_url("Title", "http://url1.com")
        p_t = Paper.objects.get(simple_title="Title")
        self.assertEqual(p_t.url, "http://url1.com")

        p = Paper(simple_title="Title2", url="http://url.old",
                  file_name="title2.pdf")
        p.save()
        self.assertFalse(update_single_paper_url("Title2", "http://url.new"))

        p_t = Paper.objects.get(simple_title="Title2")
        self.assertEqual(p_t.url, "http://url.old")

        self.assertTrue(update_single_paper_url("Title2", "http://url.new",
                                                update=True))

        p_t = Paper.objects.get(simple_title="Title2")
        self.assertEqual(p_t.url, "http://url.new")

        self.assertFalse(update_single_paper_url("Title3", "http://url.new",
                                                 update=True))

    def test_get_simple_title_url(self):

        s_t, url = get_simple_title_url("")
        self.assertEqual(s_t, None)

        self.assertEqual(get_simple_title_url("title | http://url.com"),
                         ("title", "http://url.com"))

        self.assertEqual(get_simple_title_url("title.pdf | http://url.com"),
                         ("title", "http://url.com"))

    def test_load_papers_urls_file_command(self):
        self.test_load_papers_urls_file(True)

    def test_load_papers_urls_file(self, use_command=False):
        p1 = Paper(simple_title="title",
                   file_name="title.pdf")
        p1.save()
        p2 = Paper(simple_title="title2", url="http://url.old",
                   file_name="title2.pdf")
        p2.save()
        text = """
title.pdf | http://url.com
title2.pdf | http://url.new
"""
        file_route = self._create_test_file("urls.list", text)
        if use_command:
            args = [file_route]
            opts = {}
            call_command('loadpaperurls', *args, **opts)
        else:
            self.assertEqual(load_paper_urls_file(file_route),
                             1)

        self.assertEqual(Paper.objects.get(simple_title="title").url,
                         "http://url.com")
        self.assertEqual(Paper.objects.get(simple_title="title2").url,
                         "http://url.old")

        if use_command:
            args = [file_route]
            opts = {"update": True}
            call_command('loadpaperurls', *args, **opts)
        else:
            self.assertEqual(load_paper_urls_file(file_route, update=True),
                             2)

        self.assertEqual(Paper.objects.get(simple_title="title").url,
                         "http://url.com")
        self.assertEqual(Paper.objects.get(simple_title="title2").url,
                         "http://url.new")


class TestPaperLoadExtraData(TestFileGeneric):
    _tmp_folder = "/tmp/test_PAPERMANAGER"

    def setUp(self):
        self._register_tmp_folder(self._tmp_folder)
        self._create_empty(self._tmp_folder, is_folder=True)

    def _create_test_file(self, file_name, content):
        file_route = os.path.join(self._tmp_folder, file_name)
        self._register_tmp_file(file_route)

        cvs_file = open(file_route, 'w')
        cvs_file.write(content)
        cvs_file.close()

        return file_route

    def test_load_single_paper_data(self):

        p = Paper(simple_title="ATitle")
        p.save()

        load_single_paper_data(dict(simple_title="ATitle",
                                    pub_date="(2013)",
                                    pub_issue="Issue Text",
                                    pub_pages="Pages Text",
                                    pub_title="Pub Title"))

        load_single_paper_data(dict(simple_title="ATitle2",
                                    pub_date="(2013)",
                                    pub_issue="Issue Text",
                                    pub_pages="Pages Text",
                                    pub_title="Pub Title"))

        n_p = Paper.objects.get(simple_title="ATitle")

        self.assertEqual(n_p.pub_date, datetime.date(2013, 1, 1))
        self.assertEqual(n_p.pub_issue, "Issue Text")
        self.assertEqual(n_p.pub_pages, "Pages Text")
        self.assertEqual(n_p.pub_title, "Pub Title")

    def test_load_single_paper_data_partial(self):

        p = Paper(simple_title="ATitle")
        p.save()

        load_single_paper_data(dict(simple_title="ATitle",
                                    pub_issue="Issue Text",
                                    pub_title="Pub Title"))

        n_p = Paper.objects.get(simple_title="ATitle")

        self.assertEqual(n_p.pub_issue, "Issue Text")
        self.assertEqual(n_p.pub_title, "Pub Title")

    def test_load_json_file_command(self):
        self.test_load_json_file(True)

    def test_load_json_file(self, as_command=False):
        text = """
{
    "0":{
        "authors":[
            "Hendrik Schlicke",
            "Debraj Ghosh",
            "Lamkiu Fong",
            "Huolin L Xin",
            "Haimei Zheng",
            "Paul Alivisatos"
        ],
        "citation":"Schlicke, Hendrik; Ghosh, Debraj; Fong, Lam-Kiu; Xin, Huolin L; Zheng, Haimei; Alivisatos, A Paul, Selective Placement of Faceted Metal Tips on Semiconductor Nanorods, <i>Angewandte Chemie International Edition</i>, Vol. 52, pp. 980-982, (2012)",
        "file_name":"/metadata/source/20180425-6_Papers/SelectivePlacementOfFacetedMetalTipsOnSemiconductorNanorods.pdf.json",
        "id":"67dbdfcad146113ad30db5827ae14a3e",
        "pub_date":"(2012)",
        "pub_issue":"Vol. 52",
        "pub_pages":"pp. 980-982",
        "pub_title":"Angewandte Chemie International Edition",
        "simple_title":"SelectivePlacementOfFacetedMetalTipsOnSemiconductorNanorods",
        "title":"Selective Placement of Faceted Metal Tips on Semiconductor Nanorods"
    },
    "1":{
        "authors":[
            "Zarah Walsha",
            "Pavel A Levkinb",
            "Brett Paulla",
            "Frantisek Svecb",
            "Mirek Mackaa"
        ],
        "file_name":"/metadata/source/20180425-6_Papers/VisibleLightInitiatedPolymerizationOfStyrenicMonolithicStationaryPhasesUsing470NmLightEmittingDiodeArrays.pdf.json",
        "id":"c786bd20f92d48964a3227520b80a73f",
        "simple_title":"VisibleLightInitiatedPolymerisationOfStyrenicMonolithicStationaryPhasesUsing470NmLightEmittingDiodeArrays",
        "title":"Visible light initiated polymerisation of styrenic monolithic stationary phases using 470 nm light emitting diode arrays",
        "pub_title":"Other Pub Title"
    }
}
        """
        file_route = self._create_test_file("etxra_data.json", text)
        p = Paper(simple_title="SelectivePlacementOfFacetedMetalTipsOnSemiconductorNanorods")
        p.save()
        p = Paper(
            simple_title="VisibleLightInitiatedPolymerisationOfStyrenicMonolithicStationaryPhasesUsing470NmLightEmittingDiodeArrays")
        p.save()

        if as_command:
            args = [file_route]
            opts = {}

            call_command('loadextrapaperdata', *args, **opts)
        else:
            load_paper_data_from_json(file_route)

        n_p = Paper.objects.get(simple_title="SelectivePlacementOfFacetedMetalTipsOnSemiconductorNanorods")
        self.assertEqual(n_p.pub_date, datetime.date(2012, 1, 1))
        self.assertEqual(n_p.pub_issue, "Vol. 52")
        self.assertEqual(n_p.pub_pages, "pp. 980-982")
        self.assertEqual(n_p.pub_title, "Angewandte Chemie International Edition")

        n_p = Paper.objects.get(
            simple_title="VisibleLightInitiatedPolymerisationOfStyrenicMonolithicStationaryPhasesUsing470NmLightEmittingDiodeArrays")
        self.assertEqual(n_p.pub_title, "Other Pub Title")
        self.assertEqual(n_p.pub_issue, "")
