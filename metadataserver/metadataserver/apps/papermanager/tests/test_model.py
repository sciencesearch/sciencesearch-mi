from django.db import models
from django.test import TestCase

from metadataserver.apps.papermanager.models import Paper, Author


class TestPaper(TestCase):

    def test_simplify_title(self):
        self.assertEqual("MastersOfTheUniverse",
                         Paper.simplify_title(" masters of the  uNIVerse   "))

        self.assertEqual("MastersOfThe12Universe",
                         Paper.simplify_title(" masters of the 12 uNIVerse'!-?"))


class TestAuthor(TestCase):

    def test_simplify_matching_string(self):
        self.assertEqual("A J M Johnison",
                         Author.simplify_matching_string("A j. m. !Jo!hNison"))

    def test_get_author(self):
        with self.assertRaises(models.ObjectDoesNotExist):
            Author.objects.get(matching_string="A J M Johnison")

        self.assertIsNone(Author.get_author("A j. m. !Jo!hNison",
                                            create=False))

        obj = Author.get_author("A j. m. !Jo!hNison")

        self.assertEqual(obj.matching_string, "A J M Johnison")

        obj = Author.get_author("A j. m. !Jo!hNison")

        self.assertEqual(obj.matching_string, "A J M Johnison")
        self.assertIsNotNone(Author.objects.get(
            matching_string="A J M Johnison"))

    def test_paper_creator(self):
        with self.assertRaises(models.ObjectDoesNotExist):
            Paper.objects.get(simple_title="My Title")

        obj = Paper.paper_creator("My !!!!! title",
                                  "MyTitle.pdf",
                                  "A lot of cool science",
                                  [" G Rodrigo", "P villuela", "P D L palotes"])

        self.assertEqual(obj.simple_title, "MyTitle")
        self.assertEqual(obj.title, "My !!!!! title")
        self.assertEqual(obj.file_name, "MyTitle.pdf")
        self.assertEqual(obj.text_body, "A lot of cool science")
