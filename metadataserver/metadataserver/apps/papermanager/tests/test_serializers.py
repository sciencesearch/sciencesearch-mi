import json

from django.test import TestCase
from rest_framework.renderers import JSONRenderer

from metadataserver.apps.papermanager.models import Author, Paper
from metadataserver.apps.papermanager.serializers import AuthorSerializer, PaperSerializer


class TestAuthorSerializer(TestCase):

    def test_serialization_author(self):
        a = Author(matching_string="G Rodrigo", name="Gonzalo",
                   surname="Rodrigo", email="gonzalo@rodrigo.com",
                   affiliation="LBNL")
        a.save()

        p = Paper.paper_creator("My Title")

        a.papers.add(p)

        p = Paper.paper_creator("Something About Workflows")

        a.papers.add(p)

        serializer = AuthorSerializer(a)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)

        candidate_data = json.loads(
            """{"affiliation": "LBNL",
                "email": "gonzalo@rodrigo.com",
                "matching_string": "G Rodrigo",
                "name": "Gonzalo",
                "papers": ["SomethingAboutWorkflows","MyTitle"],
                "surname": "Rodrigo"}
            """)
        self.assertEqual(candidate_data, produced_data)

    def test_serialization_paper(self):
        p = Paper(simple_title="SimpleT", title="Simple T",
                  file_name="SimpleT.pdf", url="http://url.com")
        p.save()

        p.authors.add(Author.get_author("G Rodrigo"))
        p.authors.add(Author.get_author("L Ramakrishnan"))

        serializer = PaperSerializer(p)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)
        candidate_data = json.loads(
            """{"simple_title": "SimpleT",
                "title": "Simple T",
                "file_name": "SimpleT.pdf",
                "authors": ["G Rodrigo","L Ramakrishnan"],
                "citation": "",
                "pub_date": null,
                "pub_issue": "",
                "pub_pages": "",
                "pub_title": "",
                "url": "http://url.com"
                }
            """)
        self.assertEqual(candidate_data, produced_data)
