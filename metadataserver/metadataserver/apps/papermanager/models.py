"""
Data model to represent scientific papers and their authors. 
"""

from django.db import models


class Author(models.Model):
    """Object that represents an Author of an article. it might be related to
    one or more articles.
    
    Attributes:
        matching_string (models.Charfield): identifies an author uniquely. For
            good behavior, its content should have been cleaned with
            simplify_amtching_string.
        name (modes.CharField): Name of the author.
        surname (modes.CharField): Surnames of the author separated by spaces.
        email (modes.CharField): email of the author.
        affiliation (modes.CharField): institution that the author is
            affiliated to.
        papers (inverse of ManyToManyField): list of papers authored by this
            author.
    """
    matching_string= models.CharField(max_length=1024, blank=False,
                                      primary_key=True)
    name = models.CharField(max_length=1024, blank=True, default="",
                            null=True)
    surname = models.CharField(max_length=1024, blank=True, default="",
                               null=True)
    email = models.CharField(max_length=1024, blank=True, default="",
                             null=True)
    
    affiliation = models.CharField(max_length=1024, blank=True, default="",
                                   null=True)
    
    
    @classmethod
    def get_author(cls, matching_string, create=True):  
        """Returns an existing author identified by ``matching_string``. If
        such author is not in the database, it might create it and stores if
        with ``matching_string`` as its id and clear fields for the rest.
        
        Args:
            matching_string (str): id of the author.
            create (bool): if True an author will be created  and stored if 
                none with id ``matching_string`` existsi.
                
        Returns:
            Author
        
        """
        matching_string = cls.simplify_matching_string(matching_string)
        if not matching_string:
            return None
        try:
            obj = cls.objects.get(matching_string=matching_string)
        except models.ObjectDoesNotExist:
            obj = None
            
        if not obj and create:
            obj = Author(matching_string=matching_string)
            obj.save()
        
        return obj
                
    @classmethod
    def simplify_matching_string(cls, matching_string):
        """Simplifies an author id to its canonical form: capitalized words with
        only alphanumeric characters.
        
        Args:
            matching_string (str): id representing an Author.
            
        Returns:
            str: canonical versions of the user id after keeping only alpha
                numeric characters and spaces. Repeated spaces are reduced to one.
                Pre-pending and post-pending spaces are trimmed.
        """
        title = matching_string.strip()
        title = "".join([c for c in title if c.isalpha() or c.isdigit() 
                         or c==" "])
        return " ".join([c[:1].capitalize()+c[1:].lower()
                        for c in title.split()])
    
    
    
    
class Paper(models.Model):
    """Object that stores the metadata of a scientific article. 
    
    Attributes:
        simple_title (model.TextField): Canonical representation of the title
            that contains no spaces, each word is capitalized, and only
            includes alpha-numeric characters. It is the primary key of the
            object.
        title (model.TextField): Original title for the article.
        file_name (models.CharField): name of the file from which the metadata
            was read.
        text_body (model.TextField): Main text of the the article including
            abstract but not the references.
        authors (model.ManyToManyField): List of Author objects representing
            the authors of the article.        
    """
    
    simple_title = models.TextField(max_length=1024, blank=False,
                                    primary_key=True) 
    title = models.TextField(max_length=1024, blank=False,
                             null=True)
    file_name = models.CharField(max_length=1024, blank=True,
                                 default="",null=True)
    text_body = models.TextField(blank=True, default="",null=True)
    authors = models.ManyToManyField(Author, related_name="papers")
    
    citation = models.CharField(max_length=2048, blank=True, default="",
                                   null=True)
    pub_date  = models.DateField(blank=True, null=True)
    pub_issue =  models.CharField(max_length=1024, blank=True, default="",
                                   null=True)
    pub_pages =  models.CharField(max_length=1024, blank=True, default="",
                                   null=True)
    pub_title = models.CharField(max_length=1024, blank=True, default="",
                                   null=True)
    
    url = models.CharField(max_length=1024, blank=True, default="",
                           null=True)
    
    @classmethod
    def get_analyzable_fields(cls):
        return ["text_body"]
    
    def __str__(self):
        return str(self.simple_title)
        
    @classmethod
    def simplify_title(cls, title):
        """Simplifies article title to its canonical form: capitalized words
        with only alphanumeric characters and no spaces.
        
        Args:
            title (str): string representing the title of a scientific article.
            
        Returns:
            str: canonical versions of an article title after keeping only alpha
                numeric characters.
        """
        title = title.strip()
        title = "".join([c for c in title if c.isalpha() or c.isdigit() 
                         or c==" "])
        return "".join([c[:1].capitalize()+c[1:].lower()
                        for c in title.split()])
    @classmethod
    def paper_creator(cls, title, file_name=None, text_body=None, 
                      author_strings = [], update = False):
        """Retrieves a paper object that matches title or creates it in case
        it does not exist using the passed arguments to fill its structure.
        It also detects if the ids of the authors in ``authro_strings`` match
        existing Authors (to associate them with the article), otherwise it
        creates them new. 
        
        Args:
            title (str): Title of the article. It is transformed into its
                canonical form before checking it the article exists already.
             file_name (str): Route to the article where the metadata was
                 extracted from.
            text_body (str): text content of the article.
            authors_strings ([str]): list of strings that identify the authors
              of the article.  
        Returns:
            Article: object identified by title, existing in the DB or created.
        
        """
        simple_title  = Paper.simplify_title(title)
        
        try:
            obj = cls.objects.get(simple_title=title)
        except models.ObjectDoesNotExist:
            obj = None
            
        if not obj:
            obj = Paper(simple_title=simple_title, title=title, 
                        file_name=file_name, text_body=text_body)
            authors = [Author.get_author(matching_string)
                       for matching_string in author_strings]
            obj.save()
            for a in authors:
                obj.authors.add(a)
        elif update:
            if file_name:
                obj.file_name=file_name
            if text_body:
                obj.text_body=text_body
            if author_strings:
                authors = [Author.get_author(matching_string)
                       for matching_string in author_strings]
                for a in authors:
                    obj.authors.add(a)

        return obj
        
    
