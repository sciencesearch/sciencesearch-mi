from metadataserver.apps.papermanager.views import browsable as browsable_views

from django.conf.urls import url, include
from rest_framework import routers


papermanager_router = routers.DefaultRouter()
papermanager_router.register(r'authors', browsable_views.AuthorViewSet,
                     base_name='authors')
papermanager_router.register(r'papers', browsable_views.PaperViewSet,
                     base_name='papers')

urlpatterns = [
    url(r'', include(papermanager_router.urls))]