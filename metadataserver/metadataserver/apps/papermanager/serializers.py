from rest_framework import serializers

from metadataserver.apps.papermanager.models import Author, Paper


class AuthorSerializer(serializers.ModelSerializer):
    """Serializer for the the Author model."""
    
    class Meta:
        model = Author
        fields  = ('matching_string', 'name', 'surname', 'email',
                   'affiliation', 'papers')
        
class PaperSerializer(serializers.ModelSerializer):
    """Serializer for the the Paper model."""
    
    class Meta:
        model = Paper
        fields  = ('title', 'file_name', 'authors',
                   'simple_title', 'citation', 'pub_date', 'pub_issue',
                   'pub_pages', 'pub_title', 'url')