from django.core.management.base import BaseCommand
from metadataserver.apps.papermanager.datamodel.read.paperimporter import paper_import


class Command(BaseCommand):
    """Command to import one or more papers into the database.
    
    Usage:
    
    python manage.py loadpapers [options] (location_or_filename)
    
    location_or_filename: valid route to a file or directory. If
            file is pointed, only that file is imported. If directory is
            pointed, all files ending with "extensions" will be processed.
    
    Options:
        --recursive: if locations is provided, crawling continues in
            sub-folders.
        --extension (ext): if location is provided, the files with extesion
            ext will be processed.
        --update: if set, imported papers that are already present in the
            database will be updated with new information.
        --base_folder:Folder to be considered "root" of the import. It has to be
            a parent route to the location of the "importing folder". Default
            is the PWD.
    """

    def _get_default_exts(self):
        return self._get_supported_exts()

    def _get_supported_exts(self):
        return ["json"]

    def add_arguments(self, parser):
        parser.add_argument(
            '--recursive', '-r',
            action='store_true',
            dest='recursive',
            default=False,
            help='If present the load will process sub-directories'
                 ' recursively.',
            )

        parser.add_argument(
            '--update', '-u',
            action='store_true',
            dest='update',
            default=False,
            help='If present the import will allow update the fields of'
                 ' existing entries. Otherwise, pre-existing papers are not'
                 ' imported.',
            )

        parser.add_argument(
            '--extensions', '-e',
            dest='extensions',
            nargs='+',
            choices=self._get_supported_exts(),
            default=self._get_default_exts(),
            help='Extensions of the files to be processed. Default: {}'
                 ''.format(self._get_default_exts())
            )

        parser.add_argument(
            '--basefolder', '-b',
            dest='base_folder',
            default="",
            help='Folder to be considered "root" of the import. It has to be'
                 ' a parent route to the location of the "importing folder". Default'
                 ' is the PWD.'
            )

        parser.add_argument('location_or_filename', help="File system"
                                                         " route the file or location to load.")

    def handle(self, *args, **options):
        paper_import(options["location_or_filename"],
                     base_folder=options["base_folder"],
                     extensions=options["extensions"],
                     recursive=options["recursive"],
                     update=options["update"], )
