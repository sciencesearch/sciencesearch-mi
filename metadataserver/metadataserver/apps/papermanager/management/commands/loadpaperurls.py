import logging

from django.core.management.base import BaseCommand
from metadataserver.apps.papermanager.datamodel.read.paperimporter import load_paper_urls_file

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--update', '-u',
            action='store_true',
            dest='update',
            default=False,
            help='If present the import will allow update the fields of'
                 ' existing entries. Otherwise, pre-existing papers are not'
                 ' imported.',
            )

        parser.add_argument('urls_file', help="File system"
                                              " route of the file to load.")

    def handle(self, *args, **options):
        logger.info("Importing URLs from papers at: {}".format(options["urls_file"]))
        logger.info("Update: {}".format(options["update"]))
        total_count = load_paper_urls_file(options["urls_file"],
                                           update=options["update"], )

        logger.info("Written urls: {}".format(total_count))
