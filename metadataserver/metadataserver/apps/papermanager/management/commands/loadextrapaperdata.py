from django.core.management.base import BaseCommand

from metadataserver.apps.papermanager.datamodel.read.paperimporter import load_paper_data_from_json


class Command(BaseCommand):
    """Command to import medata data for papers.
    
    Usage:
    
    python manage.py loadpapers [options] (location_or_filename)
    
    json_file: valid route to a  json file with the format produced by
    https://bitbucket.org/sciencesearch/map_citations/. 
    
    Options:
        --recursive: if locations is provided, crawling continues in
            sub-folders.
        --extension (ext): if location is provided, the files with extesion
            ext will be processed.
        --update: if set, imported papers that are already present in the
            database will be updated with new information.
        --base_folder:Folder to be considered "root" of the import. It has to be
            a parent route to the location of the "importing folder". Default
            is the PWD.
    """

    def add_arguments(self, parser):
        parser.add_argument('json_file', help="File system"
                                              " route the file or location to load.")

    def handle(self, *args, **options):
        load_paper_data_from_json(options["json_file"])
