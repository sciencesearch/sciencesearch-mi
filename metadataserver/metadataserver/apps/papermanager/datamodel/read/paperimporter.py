import datetime
import json
import os
import logging

from metadataserver.apps.papermanager.models import Paper
from metadataserver.seminfer.fileanalysis import simplify_route
from metadataserver.seminfer.fileanalysis.filefinders import do_on_all_files

logger = logging.getLogger(__name__)


def import_single_paper(file_name, base_folder=None, update=False):
    """Imports a paper from a JSON file with the format produced by
    science-parse (AllenAI, https://github.com/allenai/science-parse) and
    creates the corresponding Paper and Author objects.
    
    Args:
        file_name (str): file system route pointing to a json file containing
            the a paper metadata.
        base_folder (str): if set, to a file system route that is a sub string
            of file_name, it is substracted from file_name to calculate the
            paper's file_name field.
        update (bool): if False, duplicated paper records are discarded. If
            True, present fields are updated.
    """
    with open(file_name, "r", encoding="utf-8") as f:
        data = json.load(f)

    agg_text = ""
    title = None
    if "title" in data:
        title = data["title"]
    if not title:
        logger.warning("WARNING: Paper {} has no title, it cannot be imported.".format(
            file_name))
    else:
        logger.info("Processing paper: {}".format(title))
    if "abstract" in data:
        agg_text += data["abstract"] + "\n"
    if "sections" in data:
        for sec in data["sections"]:
            if "heading" in sec:
                agg_text += sec["heading"] + "\n"
            if "text" in sec:
                agg_text += sec["text"] + "\n"
    author_list = []
    if "authors" in data:
        for author in data["authors"]:
            if "name" in author:
                author_list.append(author["name"])
    if not agg_text:
        logger.warning("WARNING: Paper {} has no body text.".format(file_name))
    cleaned_route = simplify_route(base_folder, file_name)
    if title:
        Paper.paper_creator(title, cleaned_route, agg_text, author_list,
                            update)


def paper_import(folder_or_filename, base_folder=None, extensions=["json"],
                 recursive=False,
                 update=False):
    """Imports one or many papers from a file system locations and creates
    the corresponding Paper and Author objects.
    
    Args:
        folder_or_filename (str): valid route to a file or directory. If
            file is pointed, only that file is imported. If directory is
            pointed, all files ending with "extensions" will be processed.
        base_folder (str):  if set, to a file system route that is a sub string
            of location_or_filename, it is substracted from files' router to
            calculate the paper's file_name field.
        extensions ([str]): File extensions of the files to process if a
            location_or_filename is set to a folder.
        recursive (bool): if true,  and folder_or_filename set to a dir, it
            seraches for files in subdirectories.
        update (bool): if False, duplicated paper records are discarded. If
            True, present fields are updated.
    
    """
    if os.path.exists(folder_or_filename):
        if not os.path.isdir(folder_or_filename):
            import_single_paper(folder_or_filename, base_folder, update)
        else:
            do_on_all_files(folder_or_filename, extensions,
                            import_single_paper, recursive,
                            base_folder=base_folder, update=update)
    else:
        raise ValueError("Filename or directory does not exist: {}".format(
            folder_or_filename))


def load_paper_data_from_json(file_name):
    """Loads extra data on papers extracted from the NCEM site with the tool 
    at https://bitbucket.org/sciencesearch/map_citations/. It stores the values
    in exiting database paper objects. Data is ignored if the object does not
    exist. Check tests for format. 
    
    Args:
        file_name(str): files system route pointing to the file.
    """

    with open(file_name, "r") as f:
        data = json.load(f)
        for key, value in data.items():
            load_single_paper_data(value)


def curate_pub_date(cad):
    """ Returns a datatime object if cad is a string with format
    suppported foramt. Returns a datetime of epoch 0 otherwise.
    In all cases, the date is timezone aware and set to UTC."""
    if type(cad) is str:
        suppoted_formats = ['(%Y)', '%Y']

        for date_format in suppoted_formats:
            try:
                return datetime.datetime.strptime(cad,
                                                  date_format).replace(
                    tzinfo=datetime.timezone.utc)
            except ValueError:
                pass
    return None


def load_single_paper_data(data_dict, store=True):
    """Stores data in fields of Paper objects. 
    Args:
      data_dict(dict): it must have a "simple_title" key which value should be
        a simple_title of the a Paper object in the database. Data
        keys supported: pub_date, pub_issue, pub_title, pub_pages. It
        overwrites existing values.
      store(bool): if true, the modified paper object is stored.
    Returns (Paper): modified paper object.        
    """
    simple_title = data_dict["simple_title"]
    try:
        the_paper = Paper.objects.get(simple_title=simple_title)
    except Paper.DoesNotExist:
        logger.warning("No paper found with '{}' simple tile. Skipping data".format(
            simple_title))
        return None

    if "pub_date" in data_dict:
        parsed_date = curate_pub_date(data_dict["pub_date"])
        if not parsed_date:
            logger.warning("Incorrect date format for {}:{}".format(simple_title,
                                                           data_dict["pub_date"]))
        else:
            the_paper.pub_date = parsed_date

    for field_name in ["pub_issue", "pub_title", "pub_pages", "citation"]:
        if field_name in data_dict:
            setattr(the_paper, field_name, data_dict[field_name])
    if store:
        the_paper.save()
    return the_paper


def load_paper_urls_file(file_name, update=False):
    """Reads file pointed by file_name. In each line, it extracts a
    simple_title and url, retrive a paper with such title and sets the url
    field. Non existing papers are ignored.
    Lines format: 'simple_title, url' or 'simple_title.pdf, url'
    
    Args:
      file_name(str): pointing to the filename
      update(bool): if True, url values will be overwriten even if set before.
    Returns (int): number of papers set.
    """
    total_count = 0
    with open(file_name, "r") as f:
        for line in f:
            simple_title, url = get_simple_title_url(line)
            if simple_title is None:
                logger.warning("Unknown format in line: {}".format(line))
                continue
            if update_single_paper_url(simple_title, url, update=update):
                total_count += 1
    return total_count


def get_simple_title_url(line):
    """Extracts simple_title and url from a text line of the format:
    Returns simple_title(str), url(str)
     
    """
    words = line.split(" | ")
    if len(words) != 2:
        return None, None
    pdf_file_name = words[0].strip()
    simple_title = pdf_file_name.split(".pdf")[0]
    url = words[1].strip()
    return simple_title, url


def update_single_paper_url(simple_title, url, update=False):
    """Sets the url filed in a paper titled simple_title.
    Args:
      simple_title(str): simple_title of the paper to match.
      url(str): url to set 
      update: if True, the url field will be always written. If False,
        the field will be written only if the original url is "".
    Returns: True if field is set, False otherwise.
    """

    try:
        write = False
        for p in Paper.objects.filter(file_name__icontains=simple_title + ".pdf"):
            if update or p.url == "":
                p.url = url
                p.save()
                write = True
    except Paper.DoesNotExist:
        logger.warning("No paper found with '{}' simple tile. Skipping data".format(
            simple_title))
    return write
