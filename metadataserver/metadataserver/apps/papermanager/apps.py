from django.apps import AppConfig


class PapermanagerConfig(AppConfig):
    name = 'metadataserver.apps.papermanager'
    label = 'papermanager'
