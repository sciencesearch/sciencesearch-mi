# Generated by Django 2.0.4 on 2018-05-02 18:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('papermanager', '0003_auto_20180501_1850'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='url',
            field=models.CharField(blank=True, default='', max_length=1024, null=True),
        ),
    ]
