from rest_framework import viewsets
from rest_framework.authentication import (
    TokenAuthentication, SessionAuthentication
    )
from rest_framework.permissions import IsAuthenticated

from metadataserver.apps.papermanager.models import (Author, Paper)
from metadataserver.apps.papermanager.serializers import (AuthorSerializer, PaperSerializer)


class AuthorViewSet(viewsets.ModelViewSet):
    """ViewSet to list all the Authors or one in detail."""
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class PaperViewSet(viewsets.ModelViewSet):
    """ViewSet to list all the Authors or one in detail."""
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = Paper.objects.all()
    serializer_class = PaperSerializer
