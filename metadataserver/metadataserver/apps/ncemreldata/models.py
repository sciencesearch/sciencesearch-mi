import datetime
from metadataserver.seminfer.textanalysis import (
    extract_html_content, clean_string
    )

from django.db import models


# Create your models here.
PROPOSAL_STATE_ACTIVE = "ACTIVE"
PROPOSAL_STATE_COMPLETED = "COMPLETED"
PROPOSAL_STATE = [(PROPOSAL_STATE_ACTIVE, "Active"),
                  (PROPOSAL_STATE_COMPLETED,"Completed")]

YES = "Y"
NO = "N"
YES_NO = [(YES, "Yes"),
                  (NO,"no")]

INSTRUMENT_TEAM_I = "TEAM_I"
INSTRUMENTS = [(INSTRUMENT_TEAM_I, "Instrumen"),]

def get_null_date():
    """ Returns a NULL date to epoch=0."""
    md= datetime.datetime(1970,1,1,
                             tzinfo=datetime.timezone.utc)
    return md


def curate_date(cad):
        """ Returns a datatime object if cad is a string with format 
        suppported foramt. Returns a datetime of epoch 0 otherwise.
        In call cases, the date is timezone aware and set to UTC."""
        if type(cad) is str: 
            suppoted_formats = ['%m/%d/%Y %I:%M:%S %p',
                                '%m/%d/%Y %H:%M:%S',
                                '%m/%d/%Y',
                                '%a %b %d %H:%M:%S %Y']
                                
            for date_format in suppoted_formats:
                try:
                    return datetime.datetime.strptime(cad,
                                        date_format).replace(
                                            tzinfo=datetime.timezone.utc)
                except ValueError:
                    pass
        return get_null_date()

class Proposal(models.Model):
    """ Represents a grant Proposal to use NCEM instruments."""
    
    """ Fields from the excel table """
    serial_number = models.CharField(max_length=32, primary_key=True) #0
    status = models.CharField(choices=PROPOSAL_STATE,
                              max_length=32,
                              blank=True,
                              default=PROPOSAL_STATE_ACTIVE)
    title = models.TextField(blank=True)
    interaction_type = models.IntegerField(blank=True, default=-1)
    project_leader = models.CharField(max_length=1, choices=YES_NO,
                                      blank=True, default=NO)
    primary_researcher= models.CharField(max_length=1, choices=YES_NO,
                                         blank=True,
                                         default=NO)
    co_researcher= models.CharField(max_length=1, choices=YES_NO,
                                    blank=True,
                                    default=NO)
    first_name = models.CharField(max_length=128, blank=True)
    last_name = models.CharField(max_length=128, blank=True)
    email = models.CharField(max_length=256, blank=True)
    bin = models.TextField(blank=True)
    date_project_started = models.DateTimeField(blank=True,
                                        default=get_null_date())
    date_project_end = models.DateTimeField(blank=True,
                                         default=get_null_date())
    assigned_scientist = models.CharField(max_length=256, blank=True)
    pi_name = models.CharField(max_length=256, blank=True)
    primary_researcher_name  = models.CharField(max_length=256, blank=True)
    contact_with_staff_name = models.CharField(max_length=256, blank=True)
    significance = models.TextField(blank=True) # HTML
    foundry_capabilities = models.TextField(blank=True) # HTML
    work_description = models.TextField(blank=True) # HTML
    project_duration = models.TextField(blank=True)
    relevant_experience = models.TextField(blank=True) # HTML
    project_summary = models.TextField(blank=True)
    follow_on_progress_effort = models.TextField(blank=True) # HTML
    
    class Meta:
        ordering = ('serial_number',)
    
    def __str__(self):
        return str(self.serial_number)
    
    @classmethod
    def clean_xls_record(cls, record_row):
        """ Returns a list of values that correspond to the model fields and
        can be inserted in the database.
        
        Args:
            record_row: list of values that correspond to the model field in
              the same order as they are defined.
        
        Returrns: A list of values that correspond to the model field and
          are database safe.
        """
        new_record = record_row
        
        new_record = [clean_string(r) for r in new_record]
        new_record[0] = str(int(new_record[0]))
        new_record[11] = curate_date(new_record[11])
        new_record[12] = curate_date(new_record[12])
        
        html_fields = [17, 18, 19, 20, 21, 22, 23]
        for field_id in html_fields:
            new_record[field_id]=extract_html_content(new_record[field_id])
        
        
        return new_record
    
    @classmethod
    def get_analyzable_fields(cls):
        return ["significance", "foundry_capabilities", "work_description",
                "project_duration", "relevant_experience",
                "project_summary", "follow_on_progress_effort"]


class CalendarEntry(models.Model):
    """ NCEM Calendar format as exported from NCEM's google Calendar."""    
    
    """ The model includes an extra hidden field to be primary key. """
    """ Fields from the excel table """
    summary = models.TextField(blank=True)
    start = models.DateTimeField(blank=False)
    end = models.DateTimeField(blank=False)
    location = models.CharField(max_length=32, blank=True)
    duration_h = models.IntegerField(blank=True, default=-1)
    created_by = models.CharField(max_length=128, blank=True)
    attendees = models.TextField(blank=True)
    proposal_number = models.CharField(max_length=32, blank=False)
    proposal_type = models.CharField(max_length=128, blank=True)
    lead_facility = models.CharField(max_length=128, blank=True)
    support_facilities = models.CharField(max_length=256, blank=True)
    pi_affiliation = models.TextField(blank=True)
    pi_employer_type = models.CharField(max_length=256, blank=True)
    pi_affiliation_type = models.CharField(max_length=256, blank=True)
    pi_funding_agency = models.CharField(max_length=256, blank=True)
    proposal_submmited = models.DateTimeField(blank=True,
                                         default=get_null_date())
    proposal_last_submmited = models.DateTimeField(blank=True,
                                         default=get_null_date())
    proposal_authorized = models.DateTimeField(blank=True,
                                         default=get_null_date())
    proposal_started = models.DateTimeField(blank=True,
                                         default=get_null_date())
    proposal_ended = models.DateTimeField(blank=True,
                                         default=get_null_date())
    proposal_expires = models.DateTimeField(blank=True,
                                         default=get_null_date())
    
    """ Synhtetic field pointing to the associated proposal """
    proposal = models.ForeignKey(Proposal,
                                 on_delete=models.SET_NULL,
                                 null=True)
    
    """ Synthetic field Indicating what instrument was used."""
    instrument = models.CharField(max_length=128, choices=INSTRUMENTS,
                                  blank=False)

    @classmethod
    def clean_xls_record(cls, record_row):
        """ Returns a list of values that correspond to the model fields and
        can be inserted in the database.
        
        Args:
            record_row: list of values that correspond to the model field in
              the same order as they are defined.
        
        Returrns: A list of values that correspond to the model field and
          are database safe.
        """
        
        new_record = record_row
        try:
            new_record[3] = str(int(new_record[3]))
        except ValueError:
            pass
            
        new_record[7] = str(int(new_record[7]))
        proposal = Proposal.objects.filter(serial_number = new_record[7])
        if proposal:
            proposal = proposal.first().serial_number
        else:
            proposal = None
        
        for i in [1,2,15,16,17,18,19,20]:
            new_record[i] = curate_date(new_record[i])
        new_record = [None] + new_record
        new_record += [proposal]
        return new_record
    
    
    
     
    