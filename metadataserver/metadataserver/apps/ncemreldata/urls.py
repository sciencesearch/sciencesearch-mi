from django.conf.urls import url, include
from rest_framework import routers

from metadataserver.apps.ncemreldata.views import browsable

browsable_router  = routers.DefaultRouter()

browsable_router.register(r'proposals',                     
                      browsable.ProposalViewSet,
                      base_name='browsable-proposals')
browsable_router.register(r'calendar',                     
                      browsable.CalendarEntryViewSet,
                      base_name='browsable-calendar')

urlpatterns = [
    url(r'browsable/', include(browsable_router.urls)), 
]