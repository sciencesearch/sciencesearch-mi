from rest_framework import serializers

from metadataserver.apps.ncemreldata.models import Proposal, CalendarEntry

class ProposalSerializer(serializers.ModelSerializer):
    class Meta:
        model= Proposal
        fields=('serial_number',
                'status',
                'title',
                'interaction_type',
                'project_leader',
                'primary_researcher',
                'co_researcher',
                'first_name' ,
                'last_name' ,
                'email' ,
                'bin' ,
                'date_project_started' ,
                'date_project_end' ,
                'assigned_scientist',
                'pi_name' ,
                'primary_researcher_name'  ,
                'contact_with_staff_name' ,
                'significance' ,
                'foundry_capabilities' ,
                'work_description' ,
                'project_duration' ,
                'relevant_experience' ,
                'project_summary' ,
                'follow_on_progress_effort')
        
class CalendarEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = CalendarEntry
        fields=('instrument' ,
                'summary' ,
                'start',
                'end',
                'location', 
                'duration_h',
                'created_by', 
                'attendees',
                'proposal_number', 
                'proposal_type',
                'lead_facility',
                'support_facilities', 
                'pi_affiliation',
                'pi_employer_type',
                'pi_affiliation_type', 
                'pi_funding_agency',
                'proposal_submmited',
                'proposal_last_submmited', 
                'proposal_authorized',
                'proposal_started',
                'proposal_ended',
                'proposal_expires',
                'proposal')
