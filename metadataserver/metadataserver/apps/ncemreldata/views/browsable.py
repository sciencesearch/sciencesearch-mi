""" This file provides views that list the fields of the models for development
puposes."""

from rest_framework import viewsets
from rest_framework.authentication import (TokenAuthentication,
                                           SessionAuthentication)
from rest_framework.permissions import IsAdminUser
from metadataserver.apps.ncemreldata.models import Proposal, CalendarEntry
from metadataserver.apps.ncemreldata.serializers import ProposalSerializer, CalendarEntrySerializer

class ProposalViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)
    queryset = Proposal.objects.all()
    serializer_class = ProposalSerializer
    
    
class CalendarEntryViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)
    queryset = CalendarEntry.objects.all()
    serializer_class = CalendarEntrySerializer