import datetime
import logging

import xlrd
from xlrd.sheet import ctype_text

logger = logging.getLogger(__name__)


class ModelPopulator(object):
    """ Class to parse an XLS (Excel) file that contains the data of object
    of a django Model and inserts them in the database. The model class must
    include a clean_xls_record that takes the values of a row and prepares
    them to be the input list to the model creation.
    
    XLS format must include:
    - A first row with the names of the model fields (or blank).
    - n rows, one per object to be inserted.
    - In each row, k cells for k fields in the model, in the same order
      as the fields are defined in the model class.
    
    """

    def parse_file(self, model, file_route, extra_fields_dic={}):
        """ Parses XLS in file route and inserts the corresponding objects
        of the model model. Each row corresponds to an object. The 
        
        Args:
            model: Python class that must be subclass of django.db.Model.
            file_route: string pointing to an xls file to be parsed.
            extra_fields_dic:  dictionary of {field__name:field_value} to be
              added to the all the models created from parsing the file.
            
        Returns: the number of inserted objects in the databases.
        """
        num_objs = 0
        with xlrd.open_workbook(file_route) as xl_workbook:
            xl_sheet = xl_workbook.sheet_by_index(0)
            first = True
            logger.info("Processing {}, detected rows: {}".format(file_route,
                                                            xl_sheet.nrows))
            for row_idx in range(0, xl_sheet.nrows):
                if first:
                    first = False
                    continue
                num_cols = xl_sheet.ncols
                data_row = [xl_sheet.cell(row_idx, col_idx)
                            for col_idx in range(0, num_cols)]

                if all(item.value == "" for item in data_row):
                    logger.info("Blank line skipped")
                    continue
                self._populate_model(data_row, model, xl_workbook,
                                     extra_fields_dic=extra_fields_dic)
                num_objs += 1
        return num_objs

    @staticmethod
    def _get_excel_date(value, xl_workbook):
        value_date = datetime.datetime(*xlrd.xldate_as_tuple(value,
                                                             xl_workbook.datemode))
        return value_date.strftime("%m/%d/%Y %H:%M:%S")

    def _populate_model(self, row, model, xl_workbook, extra_fields_dic={}):
        data_row = []

        for cell_obj in row:
            value = cell_obj.value
            cell_type_str = ctype_text.get(cell_obj.ctype, 'unknown type')
            if cell_type_str == "xldate":
                value = self._get_excel_date(value, xl_workbook)
            data_row.append(value)

        data_row = model.clean_xls_record(data_row)

        #         if len(data_row)!=(len(model._meta.get_fields())-len(extra_fields_dic)):
        #             raise ValueError("Row contains incorrect ({}) number of cells."
        #                              " Expected: {}".format(len(data_row),
        #                                                  len(model._meta.get_fields())))
        obj = model(*data_row, **extra_fields_dic)
        obj.save()
