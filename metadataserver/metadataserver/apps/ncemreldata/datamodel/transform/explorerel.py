from datetime import timedelta
from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.apps.ncemreldata.models import CalendarEntry

def do_relation_exploration(grace_h_before=0, grace_h_after=0):
    """ Fills the calendar entry and proposal fileds of the ScopeImages in the
    db. It searches for calendar entries that correspond to the acquiring date
    and same instrument, and completes the image accordingly.    
    Args:
    - grace_h_before: int. During analysis, calendar entries will start
      grace_h_before hours before that their original start time.
    - grace_h_after: int. During analysis, calendar entries will start
      grace_h_before hours after that their original start time.
      
    """
    
    rel_list = []
    for ce in CalendarEntry.objects.all():
        start_date = ce.start - timedelta(hours=grace_h_before)
        end_date  = ce.end + timedelta(hours=grace_h_after)
        
        candidate_images = ScopeImage.objects.filter(calendar_entry=None,
                                                 acquire_date__gte=start_date,
                                                 acquire_date__lte=end_date,
                                                 instrument=ce.instrument)
        for si in candidate_images:
            si.calendar_entry=ce
            si.proposal=ce.proposal
            si.save()
            rel_list.append(dict(scope_image_id=si.id,
                              calendar_entry_id=ce.id,
                              proposal_serial_number=si.proposal.serial_number)
                            )
    
    return rel_list
        
        
            
        
        
        
    
