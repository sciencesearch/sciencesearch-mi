import logging

import metadataserver.apps.ncemreldata.datamodel.write.genreport as logic
from metadataserver.dataexport.csv import transform_values_to_columns, save_to_csv
from metadataserver.seminfer.tagging.counttotags import order_by_count

logger = logging.getLogger(__name__)


def write_word_count_report(csv_file, model, fields=None, word_types=None,
                            include_text_rank=False,
                            include_test_rank_adjusted=False):
    logger.info("Counting words")
    (all_words_count,
     count_per_item,
     count_per_item_normalized) = logic.gen_report(model, fields,
                                                   word_types=word_types)

    all_words_count = order_by_count(all_words_count)
    count_per_item = {x: order_by_count(count_per_item[x])
                      for x in count_per_item.keys()}
    norm_counts_per_item = {x: order_by_count(
        count_per_item_normalized[x])
        for x in count_per_item_normalized.keys()}
    if include_text_rank or include_test_rank_adjusted:
        logger.info("Calculating Textrank")
        normalized_key_phrases_per_item = logic.get_textrank(model, fields)
        norm_list_dic = logic.transform_textrank_to_list(
            normalized_key_phrases_per_item)

    if include_test_rank_adjusted:
        logger.info("Adjusting Textrank values")
        adjusted_key_phrases_per_item = logic.adjust_textrank_tfidf(
            normalized_key_phrases_per_item, count_per_item_normalized,
            filter_non_tf_words=True)
        adjusted_norm_list_dic = logic.transform_textrank_to_list(
            adjusted_key_phrases_per_item)

    data = count_per_item
    for key in norm_counts_per_item.keys():
        data[key + "_N"] = norm_counts_per_item[key]
    if include_text_rank:
        for key in norm_list_dic.keys():
            data[key + "_TR"] = norm_list_dic[key]
    if include_test_rank_adjusted:
        for key in adjusted_norm_list_dic.keys():
            data[key + "_TR_A"] = adjusted_norm_list_dic[key]

    dic_order = ["All"] + sorted(data.keys())

    data["All"] = all_words_count

    logger.info("Transforming word count into CSV rows")
    rows = transform_values_to_columns(data, dic_order=dic_order)
    logger.info("Writing CSV file")
    save_to_csv(rows, csv_file)
    logger.info("Report completed")
