"""
High-level functions to caount words in models and generate the corresponding
CSV report.
"""
import logging
from collections import defaultdict

from metadataserver.seminfer.tagging import adjust_textrank_with_tfidf, correct_tfidf_dic
from metadataserver.seminfer.textanalysis.nlp import (
    add_to_dic, tokenize, annotate_text, count_words, load_stopwords,
    calc_tfidf, keep_annonotated_words_type, cleanup_text
    )
from metadataserver.seminfer.textanalysis.textrank import process_text

logger = logging.getLogger(__name__)


def gen_report(model, fields, word_types=None):
    """ Counts words in the fields of a model and produce dicts including
    all counted word and per object word count and term frequency–inverse
    document frequency analysis.
    
    Args:
        model: Model class which objects are to be analyzed.
        fields: list of strings listing the names of the fields which content
          will be analyzed.
        word_types: if set to a list of single char string, gen_report will
          only consider words which type starts with chapters in the list.
          If set to None, all words are considered.
    
    returns: a tuple of  (all_words_count, count_per_item,
        count_per_item_normalized):
        - all_words_count: dict of word:count listing all the words and
          ocucrrences in all analyzed content.
        - count_per_item: dict of object_id:{word:count}. Word count per object.
        - count_per_item_normalized: dict of object_id:{word:tfidf}. TFIFD
          per word in each object of the model
         
    """

    if fields is None:
        fields = model.get_analyzable_fields()

    all_words_count = defaultdict(int)

    stopwords = load_stopwords("stopwords.txt")

    count_per_item = {}
    count_per_item_normalized = {}
    docs_per_word = {}

    all_items = model.objects.all()
    item_count = 0
    total_item_count = len(all_items)
    for item in all_items:
        item_count += 1
        if item_count % 10 == 0 or item_count == total_item_count:
            logger.info("WC Processing {}/{}".format(item_count, total_item_count))
        item_id = str(item)
        count_per_item[item_id] = defaultdict(int)
        for field_name in fields:
            original_text = str(getattr(item, field_name))

            annotated_tokens = annotate_text(tokenize(original_text))
            if word_types:
                annotated_tokens = keep_annonotated_words_type(
                    annotated_tokens, word_types=word_types)

            count = count_words(annotated_tokens,
                                stopwords=stopwords, df=all_words_count)
            count_per_item[item_id] = add_to_dic(count,
                                                 count_per_item[item_id])
        docs_per_word = add_to_dic(docs_per_word,
                                   {w: 1 for w in count_per_item[item_id].keys()})
    for item_id in count_per_item.keys():
        count_per_item_normalized[item_id] = calc_tfidf(
            str(item_id), count_per_item[item_id],
            len(count_per_item), all_words_count,
            docs_per_word,
            as_dict=True)
    return all_words_count, count_per_item, count_per_item_normalized


def get_textrank(model, fields):
    """ Returns the result of a textrank analysis on the fields of the instances
    of model. 
    
    Args:
        model: Model class which objects are to be analyzed.
        fields: list of strings listing the names of the fields which content
          will be analyzed.
    
    Returns a dictionary of lists:
    {item_id:[{"text":"expression", "rank":number}]} where item_id
    identifies the instance of model. The list of expressions contain the
    detected expression (text) and it's relevance in the text ("rank", where
    larger value means more relevance).    
    """

    if fields is None:
        fields = model.get_analyzable_fields()

    normalized_key_phrases_per_item = {}

    all_items = model.objects.all()
    item_count = 0
    total_item_count = len(all_items)
    for item in all_items:
        item_count += 1
        logger.info("TR({}/{}): {}".format(item_count, total_item_count, item))

        item_id = str(item)
        all_text = ""

        for field_name in fields:
            original_text = str(getattr(item, field_name))
            all_text += "\n\n" + original_text
        all_text = cleanup_text(all_text)
        (keywords, sentences, normalized_key_phrases_as_dic
         ) = process_text(all_text)
        del keywords
        del sentences
        del all_text
        normalized_key_phrases_per_item[item_id] = normalized_key_phrases_as_dic

    return normalized_key_phrases_per_item


def transform_textrank_to_list(normalized_key_phrases_per_item):
    """ Transforms a dict of textrank lists into a dic of (rank, text) lists.
    Args:
        normalized_key_phrases_per_item: a dictionary of the format:
            {item_id: [{"text":"texrank text", "rank":value_rank}, ..], ..}
    Returns:
        {item_id: [(valye:rank, "textrank text", ..], ..}
    
    """
    norm_list_dic = {}
    for (key, dic_n) in normalized_key_phrases_per_item.items():
        list_norm_keys = [(x["rank"], x["text"])
                          for x in dic_n]
        list_norm_keys.sort(key=lambda tup: tup[0], reverse=True)
        norm_list_dic[key] = list_norm_keys
    return norm_list_dic


def transform_textrank_to_dict(normalized_key_phrases_per_item):
    """ Transforms a dict of textrank lists into a dic of (rank, text) lists.
    Args:
        normalized_key_phrases_per_item: a dictionary of the format:
            {item_id: [{"text":"texrank text", "rank":value_rank}, ..], ..}
    Returns:
        {item_id: {"textrank text":rank, ..}, ..}
    
    """
    norm_list_dic = {}
    for (key, dic_n) in normalized_key_phrases_per_item.items():
        dict_norm = {x["text"]: x["rank"] for x in dic_n}
        norm_list_dic[key] = dict_norm
    return norm_list_dic


def adjust_textrank_tfidf(normalized_key_phrases_per_item, tfidf_dic_per_item,
                          language="en",
                          filter_non_tf_words=False):
    """ Adjusts the textranks in normalized_key_phrases_per_item according to
    the tfidfs in tfidf_dic_per_item
    
    Args:
        normalized_key_phrases_per_item:  dictionary of the format:
            {item_id: [{"text":"texrank text", "rank":value_rank}, ..], ..}
        tfidf_dic_per_item: dictionary of the format:
            {item_id: {"word": tfidf_rank,..}..}
        language: string identiyfing the language in the texts.
        filter_non_tf_words: if True, all thoses single word expressions which
          do not appear in the tfdif dictionary are discaraded.
    Returns: dictionary of the format:
            {item_id: [{"text":"texrank text", "rank":value_rank}, ..], ..}
        
    """
    norm_list_dic = {}
    for (key, dic_n) in normalized_key_phrases_per_item.items():
        tfidf_dic = correct_tfidf_dic(tfidf_dic_per_item[key])
        norm_list_dic[key] = adjust_textrank_with_tfidf(
            dic_n,
            tfidf_dic, language=language,
            filter_non_tf_words=filter_non_tf_words)
    return norm_list_dic
