"""
This module generates tags for models from word count on sources of semantics.

The TagGeneretor is a builder with get_generator to return the required
tag generator depending on tag counting method. Current supported methods
include: count (based on absolute word count), tfdif (count using tfdif),
textrank (expression counting), textranktfidf (expression couting weighted
with tfidf counting).
 

"""
import logging

from metadataserver.apps.ncem.models import Proposal, ScopeImage
from metadataserver.apps.ncemreldata.datamodel.write.genreport import (
    gen_report, get_textrank, transform_textrank_to_dict, adjust_textrank_tfidf
    )
from metadataserver.apps.papermanager.models import Paper
from metadataserver.dataexport.csv import save_to_csv
from metadataserver.seminfer.tagging.counttotags import (
    select_tags, flatten_dict_count, combine_tag_lists_simple_efficient
    )

logger = logging.getLogger(__name__)


def get_count_methods():
    """Returns the supported count methods"""
    return TagsGenerator.get_supported_methods()


def get_tags_relation_report(model, csv_file, instrument, word_types,
                             selection_method, max_number_of_tags=10,
                             importance_limits=(0, 10),
                             share_of_tags=0.5, fields=None,
                             count_method="tfidf",
                             include_title=False,
                             max_number_of_tags_title=10,
                             verbose=False,
                             include_scores=False):
    """Produces the tags for a model and save the results in a CSV file.
    
    Args: 
      model: class Proposal or ScopeImage.
      csv_file: string pointing to a file where to save the results.
      instrument: string used to select objects associated with a certain
        instrument. If set to None, the method does not save the file and
        only return the tags.
      word_types: list of single character strings ("N", "V") that identify
        the type of words to be used to produce tags. If set to None, all
        words are considered.
      selection_method: string identifying the words to tags selection
        method as defined in selected_tags: "uni_fixed", "progressive_fixed", 
        "relative.
      max_number_of_tags: maximum number of tags per item.
      importance_limits: minimum and maximum value of importance to attribute
        to a tag.
      share_of_tags: for the relative method, shared of the words found in
        an item to be considered.
      fields: List of fields to be analyzed in the objects to produce semantics.
        if set to None, default fields are used.
      count_method: string identifying the method to extract and count words
        or expressions in the semantic sources: count (based on absolute word
        count), tfdif (count using tfdif), textrank (expression counting),
        textranktfidf (expression couting weighted with tfidf counting).
      include_title: Tags are also generated from the title and merged with
        the general ones. Importance of title tagss is always higher.
      max_number_of_tags_title: max number of tags that can be extracted from
        the title.
      
    Returns: a dictionary of the format:
        {"model_item_id:" {"tag_name":"tag importance", ..}, ..-}
       
    
    """
    tg = TagsGenerator.get_generator(count_method)
    logger.info("Generating tags on fields: {}".format(fields))
    tags_for_model = tg.get_tags_for_model(model, instrument, word_types,
                                           selection_method, max_number_of_tags=max_number_of_tags,
                                           importance_limits=importance_limits,
                                           share_of_tags=share_of_tags, fields=fields,
                                           debug=verbose)
    if include_title:
        logger.info("Generating tags on title: {}".format("title"))
        title_importance_limits = (importance_limits[1] + 1,
                                   importance_limits[1] + importance_limits[1] -
                                   importance_limits[0] + 1)
        tags_from_title = tg.get_tags_for_model(model, instrument, word_types,
                                                selection_method,
                                                max_number_of_tags=max_number_of_tags_title,
                                                importance_limits=title_importance_limits,
                                                share_of_tags=share_of_tags, fields=["title"],
                                                debug=verbose)
        logger.info("Combining title and general tags!")
        for item_id in tags_for_model.keys():
            combine_tag_lists_simple_efficient(
                tags_for_model[item_id], [tags_from_title[item_id]]
                )
        del tags_from_title

    list_of_tagged_items = []
    for item_id, tags in tags_for_model.items():
        list_of_tagged_items.append([item_id] + flatten_dict_count(tags,
                                                                   include_count=include_scores))

    if not csv_file is None:
        save_to_csv(list_of_tagged_items, csv_file)
    return tags_for_model


class TagsGeneratorCount(object):
    """ Class that extracts tags as words and measure their importance based on
    the number of instances of that word in each text."""

    def check_model(self, model):
        """ Returns the class that is source of tags for another class. E.g.
        for ScopeImage is Paper. Also, it checks that Models are known.
        """
        if model in [ScopeImage]:
            return Proposal
        elif model in [Proposal, Paper]:
            return model
        else:
            raise ValueError("Tag analysis on model {} not supported.".format(
                model))

    def count_words(self, model, instrument, fields, word_types):
        """ This is the method that extracts tags and measure its importance.
        Children classes should redefine this method.
        
        Args:
            model: class Proposal.
            instrument: Instrument that the class is associated to.
            fields: Fields of the model to analyze.
            word_types: Can be set to a list including ["N", "V"] or None. If
              N is int he list, Nouns will be considered. if "V" is in the list
              verbs will be considede. If None is used, all words will be
              considered.
        Returns: a dictionary for each instance of the model in which 
          each one is a dictionar that lists the tags and their importance.
          Format: {"model_item_id:" {"tag_name":"tag importance", ..}, ..}     
             
        
        """
        model = self.check_model(model)
        all_words_count, count_per_item, count_per_item_normalized = (
            gen_report(model, fields, word_types=word_types))
        return count_per_item

    def finalize_tags_relationship(self, model, instrument, selected_tags):
        """ Associates the tags to the instances of model from the semantic
        genereate tags (selected_tags)"""
        if (model == Proposal or model == Paper):
            return selected_tags
        elif (model == ScopeImage):
            image_selected_tags = {}
            for proposal_serial_number, tags in selected_tags.items():
                p = Proposal.objects.get(serial_number=proposal_serial_number)
                candidate_images = ScopeImage.objects.filter(instrument=instrument,
                                                             proposal=p)
                for image in candidate_images:
                    image_selected_tags[image.original_route] = tags
            return image_selected_tags

    def get_tags_for_model(self, model, instrument, word_types,
                           selection_method, max_number_of_tags=10,
                           importance_limits=(0, 10), share_of_tags=0.5,
                           fields=None, debug=False):
        """ Returns tags associated to a model extracted from a semantic source
        in or associated to the model.
        
        Args:
          model: class Proposal or ScopeImage.
          instrument: string used to select objects associated with a certain
            instrument.
          word_types: list of single character strings ("N", "V") that identify
            the type of words to be used to produce tags. If set to None, all
            words are considered.
          selection_method: string identifying the words to tags selection
            method as defined in selected_tags: "uni_fixed", "progressive_fixed", 
            "relative.
          max_number_of_tags: maximum number of tags per item.
          importance_limits: minimum and maximum value of importance to attribute
            to a tag.
          share_of_tags: for the relative method, shared of the words found in
            an item to be considered.
          fields: List of fields to be analyzed in the objects to produce semantics.
            if set to None, default fields are used.
          tf_idf: If set to True, the tag counting used will be the tfidf
            normalize. It uses absoulout count otherwise.
        
        Returns: a dictionary of dictionaries of the type
          {object_id:{tag_id:importance}}   
        
        """
        if debug:
            logger.debug("Counting words from semantic sources")

        effective_model = self.check_model(model)
        word_count_per_item = self.count_words(effective_model, instrument,
                                               fields, word_types)

        selected_tags = {}
        if debug:
            logger.debug("Selecting tags")
        for item_id, word_count in word_count_per_item.items():
            selected_tags[item_id] = select_tags(selection_method,
                                                 word_count,
                                                 max_number_of_tags=max_number_of_tags,
                                                 importance_limits=importance_limits,
                                                 share_of_tags=share_of_tags)
            del word_count
        if debug:
            logger.debug("Associating related tags to items")
        return self.finalize_tags_relationship(model, instrument, selected_tags)


class TagsGeneratorTfidf(TagsGeneratorCount):
    """ Class that extracts tags as words and measure their importance based on
    the frequency of that word in each text vs all the texts."""

    def count_words(self, model, instrument, fields, word_types):
        all_words_count, count_per_item, count_per_item_normalized = (
            gen_report(model, fields, word_types=word_types))

        return count_per_item_normalized


class TagsGeneratorTextrank(TagsGeneratorCount):
    """ Class that extracts tags as textrank expressions and their ranks."""

    def count_words(self, model, instrument, fields, word_types):
        normalized_key_phrases_per_item = get_textrank(model, fields)
        text_rank_per_item = transform_textrank_to_dict(
            normalized_key_phrases_per_item)
        return text_rank_per_item


class TagsGeneratorTextrankTfidf(TagsGeneratorCount):
    """ Class that extracts tags as textrank expressions and their ranks. Ranks
    are modified depending on the TFIDF value of the words that compose them"""

    def count_words(self, model, instrument, fields, word_types):
        logger.info("Generating TF/IDF count")
        all_words_count, count_per_item, count_per_item_normalized = (
            gen_report(model, fields, word_types=word_types))
        logger.info("TF/IDF count completed {} different words found".format(
            len(all_words_count)))
        del all_words_count
        del count_per_item

        logger.info("Calculating TextRank")
        normalized_key_phrases_per_item = get_textrank(model, fields)
        logger.info("Calculated")
        logger.info("Adjusting TextRank with values TF/IDF")
        adjusted_key_phrases_per_item = adjust_textrank_tfidf(
            normalized_key_phrases_per_item, count_per_item_normalized,
            filter_non_tf_words=True)
        logger.info("Adjust Completed")
        del normalized_key_phrases_per_item
        del count_per_item_normalized
        logger.info("Transforming ranks into a dictionary")
        text_rank_per_item = transform_textrank_to_dict(
            adjusted_key_phrases_per_item)
        logger.info("Transformation completed.")

        return text_rank_per_item


class TagsGenerator(object):
    supported_methods_types = {
        "count": TagsGeneratorCount,
        "tfidf": TagsGeneratorTfidf,
        "textrank": TagsGeneratorTextrank,
        "textranktfidf": TagsGeneratorTextrankTfidf,
        }

    @classmethod
    def get_supported_methods(cls):
        return list(cls.supported_methods_types.keys())

    @classmethod
    def get_generator(cls, count_method="tfidf"):
        if count_method not in cls.supported_methods_types.keys():
            raise ValueError("Method {} not supported".format(count_method))

        return cls.supported_methods_types[count_method]()
