import inspect
import os
import logging

from django.test import TestCase
from metadataserver.apps.ncemreldata.datamodel.read.xlsparser import ModelPopulator
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.seminfer.textanalysis import nlp as analysis

logger = logging.getLogger(__name__)


# Create your tests here.
class TestAnalysis(TestCase):
    sample_proposal_file = "aux/ProposalTest-HTML.xls"

    def _get_test_dir(self):
        return os.path.dirname(os.path.abspath(
            inspect.getfile(inspect.currentframe())))

    def _get_file(self, file_route):
        return os.path.join(self._get_test_dir(), file_route)

    def test_annotate(self):
        mp = ModelPopulator()
        mp.parse_file(Proposal, self._get_file(self.sample_proposal_file))
        po = Proposal.objects.get(serial_number="42")

        tokens = analysis.tokenize(po.foundry_capabilities)

        annotated_text = analysis.annotate_text(tokens)
        logger.info(annotated_text)

        stopwords = analysis.load_stopwords("stopwords.txt")

        word_count = analysis.count_words(annotated_text, stopwords)

        docs_per_word = {x: 1 for x in word_count.keys()}

        logger.info("WC", word_count)

        word_count_norm = analysis.calc_tfidf("test.foundry",
                                              word_count,
                                              1.0,
                                              word_count.keys,
                                              docs_per_word)

        logger.info("WCn", word_count_norm)
