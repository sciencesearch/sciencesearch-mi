import datetime
import shutil

from django.core.management import call_command

from metadataserver.apps.ncem.datamodel.read.crawler import process_single_file
from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.apps.ncem.tests.test_crawler import _get_image_route
from metadataserver.apps.ncemreldata.datamodel.transform.explorerel import do_relation_exploration
from metadataserver.apps.ncemreldata.models import Proposal, CalendarEntry
from metadataserver.seminfer.tests import TestFileGeneric


class TestExploreRel(TestFileGeneric):
    def test_commands(self):
        self.test_do_relation_exploration_ser(as_command=True)

    def test_commands_gp(self):
        self.test_do_relation_exploration_ser_grace_period(as_command=True)

    def test_do_relation_exploration_ser(self, as_command=False):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))
        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview_1.ser"))
        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview.emi"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview.emi"))
        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "17mrad_conv-reference_image-overview_1.ser"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")

        shutil.copy(_get_image_route(
            "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
            _get_image_route("tmp/test_crawl/source/"
                             "03_-4um_FocalSeriesImages_Def_-5nm.dm3"))

        s_id_2 = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                      "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
                                     _get_image_route("tmp/test_crawl/dest"),
                                     img_format="png",
                                     base_folder=_get_image_route("tmp/test_crawl"),
                                     instrument="TEAM_II")

        p_1 = Proposal(serial_number="111", title="My proposal, related")
        p_1.save()

        p_2 = Proposal(serial_number="222", title="My proposal, unrelated")
        p_2.save()

        ce_1 = CalendarEntry(start=datetime.datetime(2015, 5, 4, 13, 0,
                                                     tzinfo=datetime.timezone.utc),
                             end=datetime.datetime(2015, 5, 4, 20, 0,
                                                   tzinfo=datetime.timezone.utc),
                             proposal_number="111",
                             instrument="TEAM_I")
        ce_1.proposal = p_1
        ce_1.save()

        ce_2 = CalendarEntry(start=datetime.datetime(2013, 8, 12, 15, 0, 0,
                                                     tzinfo=datetime.timezone.utc),
                             end=datetime.datetime(2013, 8, 12, 17, 0, 0,
                                                   tzinfo=datetime.timezone.utc),
                             proposal_number="222",
                             instrument="TEAM_II")
        ce_2.proposal = p_2
        ce_2.save()

        if not as_command:
            rel_list = do_relation_exploration()
        else:
            args = []
            opts = {}
            call_command('explorerel', *args, **opts)

        si = ScopeImage.objects.get(id=s_id)
        self.assertEqual(si.calendar_entry, ce_1)
        self.assertEqual(si.proposal, p_1)

        si_2 = ScopeImage.objects.get(id=s_id_2)
        self.assertEqual(si_2.calendar_entry, ce_2)
        self.assertEqual(si_2.proposal, p_2)
        if (not as_command):
            self.assertEqual(rel_list,
                             [dict(scope_image_id=s_id,
                                   calendar_entry_id=ce_1.id,
                                   proposal_serial_number=p_1.serial_number),
                              dict(scope_image_id=s_id_2,
                                   calendar_entry_id=ce_2.id,
                                   proposal_serial_number=p_2.serial_number)])

    def test_do_relation_exploration_ser_grace_period(self, as_command=False):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))
        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview_1.ser"))
        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview.emi"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview.emi"))
        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "17mrad_conv-reference_image-overview_1.ser"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")

        shutil.copy(_get_image_route(
            "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
            _get_image_route("tmp/test_crawl/source/"
                             "03_-4um_FocalSeriesImages_Def_-5nm.dm3"))

        s_id_2 = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                      "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
                                     _get_image_route("tmp/test_crawl/dest"),
                                     img_format="png",
                                     base_folder=_get_image_route("tmp/test_crawl"),
                                     instrument="TEAM_II")

        p_1 = Proposal(serial_number="111", title="My proposal, related")
        p_1.save()

        p_2 = Proposal(serial_number="222", title="My proposal, unrelated")
        p_2.save()

        ce_1 = CalendarEntry(start=datetime.datetime(2015, 5, 4, 18, 0,
                                                     tzinfo=datetime.timezone.utc),
                             end=datetime.datetime(2015, 5, 4, 20, 0,
                                                   tzinfo=datetime.timezone.utc),
                             proposal_number="111",
                             instrument="TEAM_I")
        ce_1.proposal = p_1
        ce_1.save()

        ce_2 = CalendarEntry(start=datetime.datetime(2013, 8, 12, 12, 0, 0,
                                                     tzinfo=datetime.timezone.utc),
                             end=datetime.datetime(2013, 8, 12, 15, 0, 0,
                                                   tzinfo=datetime.timezone.utc),
                             proposal_number="222",
                             instrument="TEAM_II")
        ce_2.proposal = p_2
        ce_2.save()

        if not as_command:
            rel_list = do_relation_exploration()
        else:
            args = []
            opts = {}
            call_command('explorerel', *args, **opts)

        si = ScopeImage.objects.get(id=s_id)
        self.assertEqual(si.calendar_entry, None)

        si_2 = ScopeImage.objects.get(id=s_id_2)
        self.assertEqual(si_2.calendar_entry, None)

        if not as_command:
            rel_list = do_relation_exploration(grace_h_before=3,
                                               grace_h_after=3)
        else:
            args = []
            opts = {
                "grace_h_before": 3,
                "grace_h_after": 3
            }
            call_command('explorerel', *args, **opts)

        si = ScopeImage.objects.get(id=s_id)
        self.assertEqual(si.calendar_entry, ce_1)
        self.assertEqual(si.proposal, p_1)

        si_2 = ScopeImage.objects.get(id=s_id_2)
        self.assertEqual(si_2.calendar_entry, ce_2)
        self.assertEqual(si_2.proposal, p_2)
        if (not as_command):
            self.assertEqual(rel_list,
                             [dict(scope_image_id=s_id,
                                   calendar_entry_id=ce_1.id,
                                   proposal_serial_number=p_1.serial_number),
                              dict(scope_image_id=s_id_2,
                                   calendar_entry_id=ce_2.id,
                                   proposal_serial_number=p_2.serial_number)])
