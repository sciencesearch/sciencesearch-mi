import csv
import os
import logging

from django.core.management import call_command

from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.apps.ncemreldata.datamodel.write.gentagrelreport import (
    TagsGenerator, TagsGeneratorTextrankTfidf
    )
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.papermanager.datamodel.read.paperimporter import paper_import
from metadataserver.apps.papermanager.models import Paper
from metadataserver.seminfer.tagging.counttotags import order_by_count
from metadataserver.seminfer.tests import TestFileGeneric

logger = logging.getLogger(__name__)


class TestGenTagRelReport(TestFileGeneric):

    def test_command(self):
        self._register_tmp_folder("test_tmp/")
        self._create_empty("test_tmp/", True)

        p1 = Proposal(serial_number="1234",
                      work_description="We like potatoes. Potatoes are the best. "
                                       "This work is all about potatoes.")
        p1.save()

        p2 = Proposal(serial_number="4321",
                      work_description="We like tomatoes. Tomatoes are the best. "
                                       "This work is all about tomatoes.")
        p2.save()

        args = ["proposal", "test_tmp/test_proposal.csv"]
        opts = {
            "max_number_of_tags": 2,
            "max_importance": 4,
            "min_importance": 2,
            "selection_method": "uni_fixed",
            "countmethod": "tfidf"
        }

        call_command('gentagrelreport', *args, **opts)

        self.assertTrue(os.path.isfile("test_tmp/test_proposal.csv"))

        with open("test_tmp/test_proposal.csv") as csv_file:
            tag_reader = csv.reader(csv_file, delimiter=",", quotechar="'")
            rows = []
            for row in tag_reader:
                rows.append(row)
            self.assertEqual(rows,
                             [["1234", "potato", "best"],
                              ["4321", "tomato", "best"]])

    def test_get_tags_for_model_proposals(self):
        p1 = Proposal(serial_number="1234",
                      work_description="We like potatoes. Potatoes are the best. "
                                       "This work is all about potatoes.")
        p1.save()

        p2 = Proposal(serial_number="4321",
                      work_description="We like tomatoes. Tomatoes are the best. "
                                       "This work is all about tomatoes.")
        p2.save()

        tg = TagsGenerator.get_generator("count")

        selected_tags = tg.get_tags_for_model(Proposal, None, ["N"], "uni_fixed",
                                              max_number_of_tags=4,
                                              importance_limits=(0, 4))

        p1_tags = order_by_count(selected_tags["1234"])
        p2_tags = order_by_count(selected_tags["4321"])

        self.assertEqual(p1_tags[0][1], "potato")
        self.assertEqual(p1_tags[1][1], "work")
        self.assertEqual(p2_tags[0][1], "tomato")
        self.assertEqual(p2_tags[1][1], "work")

    def test_get_tags_for_model_proposals_tfidf(self):
        p1 = Proposal(serial_number="1234",
                      work_description="We like potatoes. Potatoes are the best. "
                                       "This work is all about potatoes.")
        p1.save()

        p2 = Proposal(serial_number="4321",
                      work_description="We like tomatoes. Tomatoes are the best. "
                                       "This work is all about tomatoes.")
        p2.save()

        tg = TagsGenerator.get_generator("tfidf")
        selected_tags = tg.get_tags_for_model(Proposal, None, ["N"], "uni_fixed",
                                              max_number_of_tags=4,
                                              importance_limits=(0, 4))

        p1_tags = order_by_count(selected_tags["1234"])
        p2_tags = order_by_count(selected_tags["4321"])
        self.assertEqual(p1_tags[0][1], "potato")
        self.assertEqual(p2_tags[0][1], "tomato")

        self.assertEqual(selected_tags["1234"]["potato"], 4)
        self.assertEqual(selected_tags["4321"]["tomato"], 4)

        selected_tags = tg.get_tags_for_model(Proposal, None, ["N"], "uni_fixed",
                                              max_number_of_tags=2,
                                              importance_limits=(0, 4))

        p1_tags = order_by_count(selected_tags["1234"])
        p2_tags = order_by_count(selected_tags["4321"])
        self.assertEqual(len(p1_tags), 2)
        self.assertEqual(len(p2_tags), 2)

        self.assertEqual(selected_tags["1234"]["potato"], 4)
        self.assertEqual(selected_tags["4321"]["tomato"], 4)

    def test_get_tags_for_model_images_tfidf(self):
        p1 = Proposal(serial_number="1234",
                      work_description="We like potatoes. Potatoes are the best. "
                                       "This work is all about potatoes.")
        p1.save()

        p2 = Proposal(serial_number="4321",
                      work_description="We like tomatoes. Tomatoes are the best. "
                                       "This work is all about tomatoes.")
        p2.save()

        si_1 = ScopeImage(original_route="/route/f1.dm3", proposal=p1,
                          instrument="TEAM_I")
        si_1.save()

        si_2 = ScopeImage(original_route="/route/f2.dm3", proposal=p2,
                          instrument="TEAM_I")
        si_2.save()
        tg = TagsGenerator.get_generator("tfidf")
        selected_tags = tg.get_tags_for_model(ScopeImage, "TEAM_I", ["N"],
                                              "uni_fixed",
                                              max_number_of_tags=4,
                                              importance_limits=(0, 4))
        logger.info("selected_tags", selected_tags)
        p1_tags = order_by_count(selected_tags["/route/f1.dm3"])
        p2_tags = order_by_count(selected_tags["/route/f2.dm3"])
        self.assertEqual(p1_tags[0][1], "potato")
        self.assertEqual(p2_tags[0][1], "tomato")

        self.assertEqual(selected_tags["/route/f1.dm3"]["potato"], 4)
        self.assertEqual(selected_tags["/route/f2.dm3"]["tomato"], 4)

        selected_tags = tg.get_tags_for_model(ScopeImage, "TEAM_I", ["N"], "uni_fixed",
                                              max_number_of_tags=2,
                                              importance_limits=(0, 4))

        p1_tags = order_by_count(selected_tags["/route/f1.dm3"])
        p2_tags = order_by_count(selected_tags["/route/f2.dm3"])
        self.assertEqual(len(p1_tags), 2)
        self.assertEqual(len(p2_tags), 2)

        self.assertEqual(selected_tags["/route/f1.dm3"]["potato"], 4)
        self.assertEqual(selected_tags["/route/f2.dm3"]["tomato"], 4)

    def test_get_tags_for_model_papers_tfidf(self):
        p1 = Paper.paper_creator("One title", "OneTitle.pdf.json",
                                 "We like potatoes. Potatoes are the best. "
                                 "This work is all about potatoes.",
                                 ["one author"])

        p2 = Paper.paper_creator("One title2", "OneTitle2.pdf.json",
                                 "We like tomatoes. Tomatoes are the best. "
                                 "This work is all about tomatoes.",
                                 ["one author"])

        tg = TagsGenerator.get_generator("tfidf")

        selected_tags = tg.get_tags_for_model(Paper, "TEAM_I", ["N"],
                                              "uni_fixed",
                                              max_number_of_tags=4,
                                              importance_limits=(0, 4))
        p1_tags = order_by_count(selected_tags["OneTitle"])
        p2_tags = order_by_count(selected_tags["OneTitle2"])

        self.assertEqual(p1_tags[0][1], "potato")
        self.assertEqual(p2_tags[0][1], "tomato")

        self.assertEqual(selected_tags["OneTitle"]["potato"], 4)
        self.assertEqual(selected_tags["OneTitle2"]["tomato"], 4)

        selected_tags = tg.get_tags_for_model(Paper, "TEAM_I", ["N"], "uni_fixed",
                                              max_number_of_tags=2,
                                              importance_limits=(0, 4))

        p1_tags = order_by_count(selected_tags["OneTitle"])
        p2_tags = order_by_count(selected_tags["OneTitle2"])
        self.assertEqual(len(p1_tags), 2)
        self.assertEqual(len(p2_tags), 2)

        self.assertEqual(selected_tags["OneTitle"]["potato"], 4)
        self.assertEqual(selected_tags["OneTitle2"]["tomato"], 4)

    def test_get_tags_for_model_proposals_relative(self):
        p1 = Proposal(serial_number="1234",
                      work_description="We like potatoes. Potatoes are the best. "
                                       "This work is all about potatoes.")
        p1.save()

        p2 = Proposal(serial_number="4321",
                      work_description="We like tomatoes. Tomatoes are the best. "
                                       "This work is all about tomatoes.")
        p2.save()
        tg = TagsGenerator.get_generator("tfidf")
        selected_tags = tg.get_tags_for_model(Proposal, None, ["N"], "relative",
                                              max_number_of_tags=4,
                                              importance_limits=(0, 4),
                                              share_of_tags=0.5)

        p1_tags = order_by_count(selected_tags["1234"])
        p2_tags = order_by_count(selected_tags["4321"])

        self.assertEqual(p1_tags[0][1], "potato")
        self.assertEqual(p2_tags[0][1], "tomato")


class TestTagsGeneratorTextrankTfidf(TestFileGeneric):

    def test_count_words(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        paper_import("{}/{}".format(dir_path,
                                    "ACombinatorialAndComputationalApproachToDevelopingLigandsForUranylSequestrationFromSeawater.pdf.json"),
                     base_folder=dir_path)

        gen = TagsGeneratorTextrankTfidf()
        gen.count_words(Paper, "", None, ["N"])

    def test_bug_recrusion_depth(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        paper_import("{}/{}".format(dir_path,
                                    "AnAtlasOfCarbonNanotubeOpticalTransitions.pdf.json"),
                     base_folder=dir_path)

        gen = TagsGeneratorTextrankTfidf()
        gen.count_words(Paper, "", None, ["N"])

    def test_another_pytextrank_bug(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        paper_import("{}/{}".format(dir_path,
                                    "TowardsGrapheneLayersByAtmosphericPressureGraphitizationOfSiliconCarbide.pdf.json"),
                     base_folder=dir_path)

        gen = TagsGeneratorTextrankTfidf()
        gen.count_words(Paper, "", None, ["N"])
