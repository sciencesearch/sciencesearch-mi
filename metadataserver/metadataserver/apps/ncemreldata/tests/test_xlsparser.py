import datetime
import inspect
import os

from django.core.management import call_command
from django.test import TestCase
from metadataserver.apps.ncemreldata.datamodel.read.xlsparser import ModelPopulator
from metadataserver.apps.ncemreldata.models import (
    Proposal, CalendarEntry, INSTRUMENT_TEAM_I, get_null_date
    )


# Create your tests here.
class TestProposal(TestCase):
    sample_proposal_file = "aux/ProposalTest.xls"
    sample_calendar_file = "aux/CalendarTest.xls"

    def _get_test_dir(self):
        return os.path.dirname(os.path.abspath(
            inspect.getfile(inspect.currentframe())))

    def _get_file(self, file_route):
        return os.path.join(self._get_test_dir(), file_route)

    def test_parse_proposal_sample_file_command(self):
        self.test_parse_proposal_sample_file(as_command=True)

    def test_parse_proposal_sample_file(self, as_command=False):
        mp = ModelPopulator()
        if not as_command:
            mp.parse_file(Proposal, self._get_file(self.sample_proposal_file))
        else:
            args = ["proposal", self._get_file(self.sample_proposal_file)]
            opts = {}

            call_command('xlsimport', *args, **opts)

        p = Proposal.objects.get(serial_number="42")

        self.assertEqual(p.serial_number, "42")
        self.assertEqual(p.status, "COMPLETED")
        self.assertEqual(p.title, "A cool title")
        self.assertEqual(p.interaction_type, 2)
        self.assertEqual(p.project_leader, "Y")
        self.assertEqual(p.primary_researcher, "N")
        self.assertEqual(p.co_researcher, "N")
        self.assertEqual(p.first_name, "The name")
        self.assertEqual(p.last_name, "the last name")
        self.assertEqual(p.email, "a@b.c")
        self.assertEqual(p.date_project_started,
                         datetime.datetime(2009, 7, 17, 11, 4, 25,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(p.date_project_end,
                         datetime.datetime(2010, 8, 26, 11, 48, 33,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(p.assigned_scientist, "Full Name 1")
        self.assertEqual(p.pi_name, "Full Name 2")
        self.assertEqual(p.primary_researcher_name, "Full Name 3")
        self.assertEqual(p.contact_with_staff_name, "Full Name 4")
        self.assertEqual(p.significance, "SIGNIFICANCE")
        self.assertEqual(p.foundry_capabilities, "FOUNDRY_CAPABILITIES")
        self.assertEqual(p.work_description, "WORK_DESCRIPTION")
        self.assertEqual(p.project_duration, "PROJECT_DURATION text")
        self.assertEqual(p.relevant_experience, "RELEVANT_EXPERIENCE")
        self.assertEqual(p.project_summary, "PROJECT_SUMMARY")
        self.assertEqual(p.follow_on_progress_effort,
                         "FOLLOW_ON_PROGRESS_REPORT")

        p = Proposal.objects.get(serial_number="75")
        self.assertEqual(p.title, "A cool title 2")

        self.assertEqual(len(Proposal.objects.all()), 2)

    def test_parse_calendar_file_command(self):
        self.test_parse_calendar_file(True)

    def test_parse_calendar_file(self, as_command=False):
        mp = ModelPopulator()

        if not as_command:
            mp.parse_file(CalendarEntry,
                          self._get_file(self.sample_calendar_file),
                          extra_fields_dic=dict(instrument="TEAM_I"))
        else:
            args = ["calendar", self._get_file(self.sample_calendar_file),
                    "--instrument=TEAM_I"]
            opts = {}

            call_command('xlsimport', *args, **opts)

        ne = CalendarEntry.objects.all().first()
        self.assertEqual(ne.instrument, INSTRUMENT_TEAM_I)
        self.assertEqual(ne.summary, "summary")
        self.assertEqual(ne.start, datetime.datetime(2017, 9, 11, 9, 0, 0, 0,
                                                     tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.end, datetime.datetime(2017, 9, 11, 18, 0, 0, 0,
                                                   tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.location, "4357")
        self.assertEqual(ne.duration_h, 9)
        self.assertEqual(ne.created_by, "gservice")
        self.assertEqual(ne.attendees, "at1,at2")
        self.assertEqual(ne.proposal_number, "4357")
        self.assertEqual(ne.proposal_type, "Standard")
        self.assertEqual(ne.lead_facility, "NCEM")
        self.assertEqual(ne.support_facilities, "Fabrication")
        self.assertEqual(ne.pi_affiliation, "UCB")
        self.assertEqual(ne.pi_employer_type, "UC_Berkeley")
        self.assertEqual(ne.pi_affiliation_type, "UC_Berkeley")
        self.assertEqual(ne.pi_funding_agency, "NSF")
        self.assertEqual(ne.proposal_submmited,
                         datetime.datetime(2017, 1, 2, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.proposal_last_submmited,
                         datetime.datetime(2017, 1, 3, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.proposal_authorized, get_null_date())
        self.assertEqual(ne.proposal_started,
                         datetime.datetime(2017, 2, 27, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.proposal_ended, get_null_date())
        self.assertEqual(ne.proposal_expires,
                         datetime.datetime(2018, 2, 27, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))
