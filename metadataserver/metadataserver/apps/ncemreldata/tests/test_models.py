import datetime

from django.test import TestCase
from metadataserver.apps.ncemreldata.models import (
    Proposal, PROPOSAL_STATE_ACTIVE, curate_date, CalendarEntry, YES, INSTRUMENT_TEAM_I
    )


# Create your tests here.

class TestCurateDate(TestCase):
    def test_curate_date(self):
        self.assertEqual(curate_date(10),
                         datetime.datetime(1970, 1, 1,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(curate_date("01/20/2017 10:01:11 AM"),
                         datetime.datetime(2017, 1, 20, 10, 1, 11,
                                           tzinfo=datetime.timezone.utc))

        self.assertEqual(curate_date("01/20/2017"),
                         datetime.datetime(2017, 1, 20,
                                           tzinfo=datetime.timezone.utc))

        self.assertEqual(curate_date("09/10/2017 9:10:14 PM"),
                         datetime.datetime(
                             2017, 9, 10, 21, 10, 14,
                             tzinfo=datetime.timezone.utc)
                         )

        self.assertEqual(curate_date("09/10/2017 21:10:14"),
                         datetime.datetime(
                             2017, 9, 10, 21, 10, 14,
                             tzinfo=datetime.timezone.utc)
                         )


class TestProposal(TestCase):

    def test_create(self):
        pr = Proposal(1, PROPOSAL_STATE_ACTIVE, "Title", 1,
                      YES, YES, YES,
                      "First Name", "Last Name", "one@email.com", "bin",
                      datetime.datetime(2017, 1, 28, 10, 12, 12,
                                        tzinfo=datetime.timezone.utc),
                      datetime.datetime(2018, 2, 27, 11, 10, 9,
                                        tzinfo=datetime.timezone.utc),
                      "Assigned sci",
                      "PI name",
                      "PR name",
                      "Staff contact name",
                      "Signif",
                      "capab",
                      "work desc",
                      "prokect duration",
                      "relevant exp",
                      "summary",
                      "effort follow progress")
        pr.save()

        p = Proposal.objects.get(serial_number="1")

        self.assertEqual(p.serial_number, "1")
        self.assertEqual(p.status, PROPOSAL_STATE_ACTIVE)
        self.assertEqual(p.title, "Title")
        self.assertEqual(p.interaction_type, 1)
        self.assertEqual(p.project_leader, YES)
        self.assertEqual(p.primary_researcher, YES)
        self.assertEqual(p.co_researcher, YES)
        self.assertEqual(p.first_name, "First Name")
        self.assertEqual(p.last_name, "Last Name")
        self.assertEqual(p.email, "one@email.com")
        self.assertEqual(p.date_project_started,
                         datetime.datetime(2017, 1, 28, 10, 12, 12,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(p.date_project_end,
                         datetime.datetime(2018, 2, 27, 11, 10, 9,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(p.assigned_scientist, "Assigned sci")
        self.assertEqual(p.pi_name, "PI name")
        self.assertEqual(p.primary_researcher_name, "PR name")
        self.assertEqual(p.contact_with_staff_name, "Staff contact name")
        self.assertEqual(p.significance, "Signif")
        self.assertEqual(p.foundry_capabilities, "capab")
        self.assertEqual(p.work_description, "work desc")
        self.assertEqual(p.project_duration, "prokect duration")
        self.assertEqual(p.relevant_experience, "relevant exp")
        self.assertEqual(p.project_summary, "summary")
        self.assertEqual(p.follow_on_progress_effort,
                         "effort follow progress")

        arg_list = [2, PROPOSAL_STATE_ACTIVE, "Title2", 1,
                    YES, YES, YES,
                    "First Name", "Last Name", "one@email.com", "bin",
                    datetime.datetime(2017, 1, 28, 10, 12, 12,
                                      tzinfo=datetime.timezone.utc),
                    datetime.datetime(2018, 2, 27, 11, 10, 9,
                                      tzinfo=datetime.timezone.utc),
                    "Assigned sci",
                    "PI name",
                    "PR name",
                    "Staff contact name",
                    "Signif",
                    "capab",
                    "work desc",
                    "prokect duration",
                    "relevant exp",
                    "summary",
                    "effort follow progress"]

        pr = Proposal(*arg_list)
        pr.save()

        p = Proposal.objects.get(serial_number="2")

        self.assertEqual(p.serial_number, "2")
        self.assertEqual(p.status, PROPOSAL_STATE_ACTIVE)
        self.assertEqual(p.title, "Title2")
        self.assertEqual(p.interaction_type, 1)
        self.assertEqual(p.project_leader, YES)
        self.assertEqual(p.primary_researcher, YES)
        self.assertEqual(p.co_researcher, YES)
        self.assertEqual(p.first_name, "First Name")
        self.assertEqual(p.last_name, "Last Name")
        self.assertEqual(p.email, "one@email.com")
        self.assertEqual(p.date_project_started,
                         datetime.datetime(2017, 1, 28, 10, 12, 12,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(p.date_project_end,
                         datetime.datetime(2018, 2, 27, 11, 10, 9,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(p.assigned_scientist, "Assigned sci")
        self.assertEqual(p.pi_name, "PI name")
        self.assertEqual(p.primary_researcher_name, "PR name")
        self.assertEqual(p.contact_with_staff_name, "Staff contact name")
        self.assertEqual(p.significance, "Signif")
        self.assertEqual(p.foundry_capabilities, "capab")
        self.assertEqual(p.work_description, "work desc")
        self.assertEqual(p.project_duration, "prokect duration")
        self.assertEqual(p.relevant_experience, "relevant exp")
        self.assertEqual(p.project_summary, "summary")
        self.assertEqual(p.follow_on_progress_effort,
                         "effort follow progress")

    def test_null_fields(self):
        p = Proposal(10)
        p.save()

    def test_str(self):
        p = Proposal(serial_number="33")
        self.assertEqual(str(p), "33")


class TestCalendarEntry(TestCase):

    def test_null_fields(self):
        p = Proposal(serial_number="1234")
        p.save()
        ce = CalendarEntry(instrument=INSTRUMENT_TEAM_I,
                           start=datetime.datetime(2017, 10, 10, 15, 0,
                                                   tzinfo=datetime.timezone.utc),
                           end=datetime.datetime(2017, 10, 11, 15, 0,
                                                 tzinfo=datetime.timezone.utc),
                           location="1234",
                           proposal_number="1234")
        ce.save()

    def test_create(self):
        p = Proposal(serial_number="1234")
        p.save()
        ce = CalendarEntry(None,
                           "summary",
                           datetime.datetime(2017, 10, 10, 15, 0,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 11, 15, 0,
                                             tzinfo=datetime.timezone.utc),
                           "1234",
                           9,
                           "googleC",
                           "attendess",
                           "1234",
                           "Proposal type",
                           "NCEM",
                           "UC Berkeley",
                           "Affi PI",
                           "Employ PI",
                           "Affi Type PI",
                           "funding PI",
                           datetime.datetime(2017, 10, 13,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 14,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 15,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 16,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 17,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 18,
                                             tzinfo=datetime.timezone.utc),
                           None,
                           INSTRUMENT_TEAM_I, )

        ce.save()

        ne = CalendarEntry.objects.get(id=ce.id)
        self.assertEqual(ne.instrument, INSTRUMENT_TEAM_I)
        self.assertEqual(ne.summary, "summary")
        self.assertEqual(ne.start, datetime.datetime(2017, 10, 10, 15, 0, 0, 0,
                                                     tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.end, datetime.datetime(2017, 10, 11, 15, 0, 0, 0,
                                                   tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.location, "1234")
        self.assertEqual(ne.duration_h, 9)
        self.assertEqual(ne.created_by, "googleC")
        self.assertEqual(ne.attendees, "attendess")
        self.assertEqual(ne.proposal_number, "1234")
        self.assertEqual(ne.proposal_type, "Proposal type")
        self.assertEqual(ne.lead_facility, "NCEM")
        self.assertEqual(ne.support_facilities, "UC Berkeley")
        self.assertEqual(ne.pi_affiliation, "Affi PI")
        self.assertEqual(ne.pi_employer_type, "Employ PI")
        self.assertEqual(ne.pi_affiliation_type, "Affi Type PI")
        self.assertEqual(ne.pi_funding_agency, "funding PI")
        self.assertEqual(ne.proposal_submmited,
                         datetime.datetime(2017, 10, 13, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.proposal_last_submmited,
                         datetime.datetime(2017, 10, 14, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.proposal_authorized,
                         datetime.datetime(2017, 10, 15, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.proposal_started,
                         datetime.datetime(2017, 10, 16, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.proposal_ended,
                         datetime.datetime(2017, 10, 17, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))
        self.assertEqual(ne.proposal_expires,
                         datetime.datetime(2017, 10, 18, 0, 0, 0,
                                           tzinfo=datetime.timezone.utc))

    def test_create_rel(self):
        p = Proposal(serial_number="1234")
        p.save()
        ce = CalendarEntry(None,
                           "summary",
                           datetime.datetime(2017, 10, 10, 15, 0,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 11, 15, 0,
                                             tzinfo=datetime.timezone.utc),
                           "1234",
                           9,
                           "googleC",
                           "attendess",
                           "1234",
                           proposal=p)

        ce.save()

        p = Proposal(serial_number="12344")
        p.save()
        ce = CalendarEntry(None,
                           "summary",
                           datetime.datetime(2017, 10, 10, 15, 0,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 11, 15, 0,
                                             tzinfo=datetime.timezone.utc),
                           "12344",
                           9,
                           "googleC",
                           "attendess",
                           "12344",
                           "Proposal type",
                           "NCEM",
                           "UC Berkeley",
                           "Affi PI",
                           "Employ PI",
                           "Affi Type PI",
                           "funding PI",
                           datetime.datetime(2017, 10, 13,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 14,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 15,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 16,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 17,
                                             tzinfo=datetime.timezone.utc),
                           datetime.datetime(2017, 10, 18,
                                             tzinfo=datetime.timezone.utc),
                           p.serial_number,
                           INSTRUMENT_TEAM_I, )

        ce.save()
