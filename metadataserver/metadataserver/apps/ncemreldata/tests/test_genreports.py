import os

from django.core.management import call_command

from metadataserver.apps.ncemreldata.datamodel.write.genreport import gen_report
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.seminfer.tagging.counttotags import flatten_dict_count
from metadataserver.seminfer.tests import TestFileGeneric


class TestGenReport(TestFileGeneric):
    def test_command(self):
        self._register_tmp_folder("test_tmp/")
        self._create_empty("test_tmp/", True)

        p1 = Proposal(serial_number="1234",
                      work_description="We like potatoes. Potatoes are the best. "
                                       "This work is all about potatoes.")
        p1.save()

        p2 = Proposal(serial_number="4321",
                      work_description="We like tomatoes. Tomatoes are the best. "
                                       "This work is all about tomatoes.")
        p2.save()

        args = ["proposal", "test_tmp/test_word_count.csv",
                "--word_types=N"]
        opts = {}

        call_command('wordcountreport', *args, **opts)

        self.assertTrue(os.path.isfile("test_tmp/test_word_count.csv"))

    def test_gen_report(self):
        p1 = Proposal(serial_number="71234",
                      work_description="We like potatoes at work."
                                       " Potatoes are the best."
                                       " This work is all about potatoes.")
        p1.save()

        p2 = Proposal(serial_number="4321",
                      work_description="We like tomatoes at work."
                                       " Tomatoes are the best."
                                       " This work is all about tomatoes.")
        p2.save()

        p3 = Proposal(serial_number="1234b",
                      work_description="We like oranges at work."
                                       " Oranges are the best."
                                       " This work is all about orange.")
        p3.save()

        all_words_count, count_per_item, count_per_item_normalized = (
            gen_report(Proposal, None, word_types=None))

        p1_counts = count_per_item[p1.serial_number]
        p2_counts = count_per_item[p2.serial_number]
        p3_counts = count_per_item[p3.serial_number]

        self.assertEqual(p1_counts["potato"], 3)
        self.assertEqual(p1_counts["work"], 2)
        self.assertEqual(p1_counts["best"], 1)
        self.assertEqual(p2_counts["tomato"], 3)
        self.assertEqual(p2_counts["work"], 2)
        self.assertEqual(p2_counts["best"], 1)
        self.assertEqual(p3_counts["orange"], 3)
        self.assertEqual(p3_counts["work"], 2)
        self.assertEqual(p3_counts["best"], 1)

        p1_counts_norm = count_per_item_normalized[p1.serial_number]
        p2_counts_norm = count_per_item_normalized[p2.serial_number]
        p3_counts_norm = count_per_item_normalized[p3.serial_number]

        self.assertEqual(p1_counts_norm["potato"], max(p1_counts_norm.values()))
        self.assertEqual(p2_counts_norm["tomato"], max(p2_counts_norm.values()))
        self.assertEqual(p3_counts_norm["orange"], max(p3_counts_norm.values()))

        self.assertEqual(flatten_dict_count(p1_counts_norm),
                         ["potato", "work", "best"])
        self.assertEqual(flatten_dict_count(p2_counts_norm),
                         ["tomato", "work", "best"])
        self.assertEqual(flatten_dict_count(p3_counts_norm),
                         ["orange", "work", "best"])
