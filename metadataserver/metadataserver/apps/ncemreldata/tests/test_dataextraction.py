import inspect
import os

from django.test import TestCase

from metadataserver.apps.ncemreldata.datamodel.read.xlsparser import ModelPopulator
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.seminfer.textanalysis import extract_html_content


# Create your tests here.
class TestExtraction(TestCase):
    sample_proposal_file = "aux/ProposalTest-HTML.xls"

    def _get_test_dir(self):
        return os.path.dirname(os.path.abspath(
            inspect.getfile(inspect.currentframe())))

    def _get_file(self, file_route):
        return os.path.join(self._get_test_dir(), file_route)

    def test_parse_proposal_sample_file(self):
        mp = ModelPopulator()
        mp.parse_file(Proposal, self._get_file(self.sample_proposal_file))
        po = Proposal.objects.get(serial_number="42")
        self.assertEqual(extract_html_content(po.significance), "")
        self.assertEqual(extract_html_content(po.foundry_capabilities),
                         """ In order to untangle the questions regarding size effects, in particular the question of the role of FIB damage, it is necessary to move past FIB-based preparation techniques. Moreover, new techniques must be found to make even smaller samples, as focused ion beams are limited to samples about 50nm in diameter.  The Molecular Foundry offers a unique opportunity to address both of those issues, as it not only has lithographic facilities, but the facilities are dedicated to nanofabrication.   Moreover, in tandem with the National Centerfor Electron Microscopy, it is possible to perform both the sample preparationand the in-situ electron microscopy necessary to address such a problem. The Molecular Foundry houses the necessary equipment to perform e-beam lithography,which is required to define the smallest nanopillars we would like to make. Moreover, the Foundry has the capacity to perform high-resolution plasma etching, which is necessary to produce pillars with specific size and feature requirements. More specifically, in order to produce useful samples for in-situ compression tests, it is necessary to perform highly anisotropic etches over very specifc diameters. In addition, a possible sample preparation scheme using the nanoimprinting facility combined with electrodeposition has been proposed to us previously by Dr. Cabrini of the Foundry.  Lastly, the Foundry has the characterization tools necessary to complete the process (inspection SEM), as the technique will need to be refined as samples are produced and analyzed. In total, the Foundry is exceptionally well-suited to produce these nanopillars."""
                         )
        self.assertEqual(extract_html_content(po.work_description), "")

    def test_parse_proposal_sample_file_cleanhtml(self):
        mp = ModelPopulator()
        mp.parse_file(Proposal, self._get_file(self.sample_proposal_file))
        po = Proposal.objects.get(serial_number="77")
        self.assertEqual(extract_html_content(po.work_description),
                         """We propose to pattern the nitride windows using the opticalcontact printer at the Foundry and then wet etching with KOH from the backsideof a nitrided wafer. The rest of the work will be performed at NCEM: We would then perforate these windows with micron-sized holes and deposit the electrodesusing the FIB at NCEM. Next we would spin cast ahomogeneous layer of block copolymer electrolyte sample onto the holey siliconnitride windows (a process we are experienced with at NCEM). Thus, ourfabrication of the substrates would greatly benefit from the opticallithography and wet etching (KOH) capabilities available at the Foundry. """
                         )
