from django.apps import AppConfig


class NcemreldataConfig(AppConfig):
    name = 'metadataserver.apps.ncemreldata'
    label = 'ncemreldata'
