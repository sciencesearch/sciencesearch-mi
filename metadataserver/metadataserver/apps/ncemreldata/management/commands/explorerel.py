import logging

from django.core.management.base import BaseCommand
from metadataserver.apps.ncemreldata.datamodel.transform.explorerel import do_relation_exploration

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """ Command to an explore the potential relationshipo between ScopeImages
    and calendar entries and proposals.
    
    Usage
    
    python manage.py explorerel
    
    """

    def add_arguments(self, parser):
        parser.add_argument(
            '--grace_h_before', '-b',
            dest='grace_h_before',
            nargs="?",
            action="store",
            default=0,
            type=int,
            help='Increases the time span of each calendar entry by X hours'
                 ' before the star time,'
            )
        parser.add_argument(
            '--grace_h_after', '-a',
            dest='grace_h_after',
            nargs="?",
            action="store",
            default=0,
            type=int,
            help='Increases the time span of each calendar entry by X hours'
                 ' after the star time,'
            )

    def handle(self, *args, **options):
        grace_h_before = options["grace_h_before"]
        grace_h_after = options["grace_h_after"]
        logger.info("Doing relational analysis. Grace period size: {}h, {}h"
              "".format(grace_h_before, grace_h_after))
        do_relation_exploration(grace_h_before=grace_h_before,
                                grace_h_after=grace_h_after)
