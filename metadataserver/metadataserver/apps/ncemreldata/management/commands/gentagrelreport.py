from django.core.management.base import BaseCommand, CommandError

from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.apps.ncemreldata.datamodel.write.gentagrelreport import (
    get_tags_relation_report, get_count_methods
    )
from metadataserver.apps.ncemreldata.models import (
    Proposal, INSTRUMENTS, INSTRUMENT_TEAM_I
    )
from metadataserver.apps.papermanager.models import Paper


class Command(BaseCommand):
    """ Produces a list of NLP-based tags associated with proposals or images.
    It generates NLP based word count analysis for proposals. Then, it
    transforms the word count dictionaries into lists of tuples
    (word, importance). Then, following the selection policy, n words are
    chosen, stripped of the importance value, and associated to the
    corresponding image or proposal.  
    
    Usage:
    
    python manage.pt gentagrelreport [options] (model) (csvfile)
    
    Args:
        model: string "proposal" if analysis is for the proposals. "image"
          if analysis is for scope images.
        csvfile: sting poiting to a file system location where the tags will
          be dunpled in CSV format.  
    Options:
        --word_types: If set to "N" NLP analysis only considers nouns. If set
          to "V", only verbs. If not set, it considers all words. 
        --instrument: If the analysis is for images, this flag specifies
          what instrument images should be considered.
        --selection_method: string of the values "uni_fixed",
          "progressive_fixed" and "relative". Selects method to be used to
           transform the word counting dictionaries into tag importance lists.  
        --max_tags: number of max tags per item to be considered.
        --share: float  number that represents the share number of tags to be
          considered per item in the relative method.
        --min_importance number representing the min importance value to be
          considered per tag.
        --max_importance number representing the max importance value to be
          considered per tag.
        --countmethod: selects the method to extract words and measure their
          importance
        --include_title: (from proposals) Title text is also analyzed and its
          tags added with higher importance than the ones of the text.
        --max_title_tags: Maximum number of tags to be derived from the title.
        --include_scores: If present the confidence scores of each tag will be
          also exported.
    """

    def _get_instruments(self):
        return [x[0] for x in INSTRUMENTS]

    def add_arguments(self, parser):

        parser.add_argument("model", help="Model to generate the semantic"
                                          " report about", choices=["proposal", "image",
                                                                    "paper"])

        parser.add_argument("csvfile", help="file.csv where report will be"
                                            " recorded")

        parser.add_argument(
            '--countmethod', '-c',
            dest='count_method',
            nargs="?",
            choices=get_count_methods(),
            default="textranktfidf",
            help='Selects the method to count :{}'
                 ''.format(get_count_methods())
            )

        parser.add_argument(
            '--instrument', '-n',
            dest='instrument',
            nargs="?",
            choices=self._get_instruments(),
            default=INSTRUMENT_TEAM_I,
            help='If set, only tags for certain instrument will be generated:{}'
                 ''.format(self._get_instruments())
            )

        parser.add_argument(
            '--word_types', '-w',
            dest='word_types',
            nargs='+',
            default=None,
            choices=["N", "V"],
            help='Types of words to be considered tags.'
            )

        parser.add_argument(
            '--selection_method', '-s',
            dest='selection_method',
            nargs='?',
            default="relative",
            choices=["uni_fixed", "progressive_fixed", "relative"],
            help='Method to select and weight tags.'
            )

        parser.add_argument(
            '--max_tags', '-m',
            dest='max_number_of_tags',
            nargs='?',
            default=10,
            help=('Maximum number of tags per item.')
            )

        parser.add_argument(
            '--share', '-a',
            dest='share_of_tags',
            nargs='?',
            default=0.5,
            help=('Share of tags to keep in the relative method.')
            )

        parser.add_argument(
            '--min_importance', '-i',
            dest='min_importance',
            nargs='?',
            default=0,
            help='Minimum importance value to be associated to a tag.'
            )

        parser.add_argument(
            '--max_importance', '-I',
            dest='max_importance',
            nargs='?',
            default=10,
            help='Maximum importance value to be associated to a tag.'
            )

        parser.add_argument(
            '--include_title', "-t",
            action='store_true',
            dest='include_title',
            default=False,
            help='If present the tags will be generated including the title'
                 ' field.',
            )

        parser.add_argument(
            '--max_title_tags', '-M',
            dest='max_number_of_tags_title',
            nargs='?',
            default=10,
            help=('Maximum number of tags per item coming from the title')
            )

        parser.add_argument(
            '--include_scores', "-S",
            action='store_true',
            dest='include_scores',
            default=False,
            help='If present the confidence scores of each tag will be also'
                 ' exported.',
            )

    def handle(self, *args, **options):
        model_name = options["model"]
        csv_file = options["csvfile"]
        instrument = options["instrument"]
        word_types = options["word_types"]
        selection_method = options["selection_method"]
        max_number_of_tags = int(options["max_number_of_tags"])
        share_of_tags = float(options["share_of_tags"])
        count_method = options["count_method"]
        include_title = options["include_title"]
        max_number_of_tags_title = int(options["max_number_of_tags_title"])

        importance_limits = (float(options["min_importance"]),
                             float(options["max_importance"]))
        include_scores = options["include_scores"]

        if model_name == "proposal":
            model = Proposal
        elif model_name == "image":
            model = ScopeImage
        elif model_name == "paper":
            model = Paper
        else:
            raise CommandError("Only Proposal or ScopeImage analysis is "
                               "supported.")

        get_tags_relation_report(model, csv_file, instrument, word_types,
                                 selection_method,
                                 max_number_of_tags=max_number_of_tags,
                                 importance_limits=importance_limits,
                                 share_of_tags=share_of_tags, fields=None,
                                 count_method=count_method,
                                 include_title=include_title,
                                 max_number_of_tags_title=max_number_of_tags_title,
                                 verbose=True,
                                 include_scores=include_scores)
