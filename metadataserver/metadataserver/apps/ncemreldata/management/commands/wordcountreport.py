
from metadataserver.apps.ncemreldata.datamodel.write.genwordcount import write_word_count_report
from metadataserver.apps.ncemreldata.models import Proposal

from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    """ Command to analyze the importance and presence of words within the
    text fields of the models.
    
    Usage:
    
    python manage.pt wordcountreport (model) (cvsffile.cvs) 
        [--fields=field_name] [--word_types={N, V}}
        
    Args:
        model: model which objects will be analyzed.
        cvsfile.cvs: file where the report will be writeng in CSV format,
          including 1) a list of words and total count for all the objects; 2)
          per object word count and term frequency–inverse document frequency.
        --fields: List of fields to be processed. if not set, the class provides
          a default list through get_analyzable_fields.
        --word_types: word types to be included in the analysis. If not set, all
          words are considered. If set to N, only Nouns will be considered. If
          set to V, only Verbs are considered.
    """

    def add_arguments(self, parser):
        parser.add_argument("model", help="Model to generate the semantic"
                            " report about",  choices=["proposal", "calendar"])
        
        parser.add_argument("csvfile", help="file.csv where report will be"
                            " recorded")
        
        parser.add_argument(
            '--fields', '-i',
            dest='fields',
            nargs='+',
            default=None,
            help='Entity fields to be analyzed. If not set, all analyzable'
                ' fields will be analyzed.'
        )
        
        parser.add_argument(
            '--word_types', '-w',
            dest='word_types',
            nargs='+',
            default=None,
            choices=["N", "V"],
            help='Types of words to be analyzed.'
        )
        
        parser.add_argument(
            '--textrank', '-t',
            action='store_true',
            dest='text_rank',
            default=False,
            help='If present the report includes text_rank analysis.',
        )
        
        parser.add_argument(
            '--tfidftextrank', '-f',
            action='store_true',
            dest='adjusted_text_rank',
            default=False,
            help='If present the report includes text_rank analysis, which'
            ' ranks are adjusted with the tfidf values of its words.',
        )
        
    def handle(self, *args, **options):
            
        model_name =  options["model"]
        fields = options["fields"]
        csv_file = options["csvfile"]
        word_types = options["word_types"]
        text_rank = options["text_rank"]
        adjusted_text_rank = options["adjusted_text_rank"]
        model = None
        if model_name=="proposal":
            model = Proposal
        else:
            raise CommandError("Only Proposal analysis is supported.")
        
        write_word_count_report(csv_file, model, fields=fields,
                                word_types=word_types,
                                include_text_rank=text_rank,
                                include_test_rank_adjusted=adjusted_text_rank)
        
