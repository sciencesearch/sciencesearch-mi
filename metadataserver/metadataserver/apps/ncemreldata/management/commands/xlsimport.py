from django.core.management.base import BaseCommand

from metadataserver.apps.ncemreldata.datamodel.read.xlsparser import ModelPopulator
from metadataserver.apps.ncemreldata.models import (
    Proposal, INSTRUMENTS, INSTRUMENT_TEAM_I, CalendarEntry
    )


class Command(BaseCommand):
    """ Command to import data in CSV format into the models of ncemreldata.
    
    Usage:
    
    python manage.py csvimport (proposal|calendar) (file_name.csv)
    
    Args:
        model: string identifying the model that is being parsed.
        xls_file: string pointing to a XLS (excel) file which rows will
          be inserted as model objects. Excel file must be formated: Each
          column represents a data column, and each row an object. All
          rows must have the same number of columns interleave blank rows are
          not supported.
        --instrument: string identifying the NCEM instrument which the data
          corresponds to.
    """

    def add_arguments(self, parser):
        parser.add_argument("model", help="Model the XLS will be be imported"
                                          " into", choices=["proposal", "calendar"])

        parser.add_argument("xls_file", help="XLS files which data will be"
                                             " imported into the model. Each column represents"
                                             " a data column, and each row an object. All"
                                             " rows must have the same number of columns"
                                             " interleave blank rows are not supported.")
        parser.add_argument(
            '--instrument', '-i',
            dest='instrument',
            nargs="?",
            choices=self._get_instruments(),
            default=INSTRUMENT_TEAM_I,
            help='NCEM instrument associated to the calendar entries parsed:{}'
                 ''.format(self._get_instruments())
            )

    def _get_instruments(self):
        return [x[0] for x in INSTRUMENTS]

    def handle(self, *args, **options):
        self.stdout.write("Importing CSV file {} into model {}".format(
            options["xls_file"], options["model"]))
        populator = ModelPopulator()
        if options["model"] == "proposal":
            populator.parse_file(Proposal, options["xls_file"])
        elif options["model"] == "calendar":
            populator.parse_file(CalendarEntry, options["xls_file"],
                                 extra_fields_dic=dict(instrument=options["instrument"]))
