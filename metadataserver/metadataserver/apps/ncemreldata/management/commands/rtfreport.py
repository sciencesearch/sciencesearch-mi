import logging

from django.core.management.base import CommandError

from metadataserver.apps.ncemreldata.datamodel.write.gentagrelreport import get_tags_relation_report
from metadataserver.apps.ncemreldata.management.commands import gentagrelreport as gt
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.dataexport.rtf import export_report

logger = logging.getLogger(__name__)


class Command(gt.Command):
    """ Parses all proposals in the daabase, generates associated tags and
    saves them in an RTF file together with the source text of the proposal.
    
    Usage:
    
    python manage.py rtfreport [options] proposal (report_file.rtf)
    
    Args:
        proposal: has to be "proposal". Future implementations might support
          other models.
        report_file.rtf: file where the proposal will be stored.
    
    Options: 
        --word_types: If set to "N" NLP analysis only considers nouns. If set
          to "V", only verbs. If not set, it considers all words. 
        --instrument: If the analysis is for images, this flag specifies
          what instrument images should be considered.
        --selection_method: string of the values "uni_fixed",
          "progressive_fixed" and "relative". Selects method to be used to
           transform the word counting dictionaries into tag importance lists.  
        --max_tags: number of max tags per item to be considered.
        --share: float  number that represents the share number of tags to be
          considered per item in the relative method.
        --min_importance number representing the min importance value to be
          considered per tag.
        --max_importance number representing the max importance value to be
          considered per tag.
        --countmethod: selects the method to extract words and measure their
          importance
        --include_title: (from proposals) Title text is also analyzed and its
          tags added with higher importance than the ones of the text.
        --max_title_tags: Maximum number of tags to be derived from the title.
    """

    def handle(self, *args, **options):
        model_name = options["model"]
        csv_file = options["csvfile"]
        instrument = options["instrument"]
        word_types = options["word_types"]
        selection_method = options["selection_method"]
        max_number_of_tags = int(options["max_number_of_tags"])
        share_of_tags = float(options["share_of_tags"])
        count_method = options["count_method"]
        include_title = options["include_title"]
        max_number_of_tags_title = int(options["max_number_of_tags_title"])

        importance_limits = (float(options["min_importance"]),
                             float(options["max_importance"]))
        if model_name == "proposal":
            model = Proposal
        else:
            raise CommandError("Only Proposal analysis is "
                               "supported.")

        logger.info("Generating Tags")
        tags_for_model = get_tags_relation_report(
            model, csv_file, instrument, word_types,
            selection_method,
            max_number_of_tags=max_number_of_tags,
            importance_limits=importance_limits,
            share_of_tags=share_of_tags, fields=None,
            count_method=count_method,
            include_title=include_title,
            max_number_of_tags_title=max_number_of_tags_title,
            verbose=True)
        logger.info("Retrieving proposals")
        proposals = {x.serial_number: x for x in Proposal.objects.all()}

        logger.info("Writing RTF")
        export_report(csv_file, proposals,
                      ["title"] + Proposal.get_analyzable_fields(),
                      tags_for_model)
