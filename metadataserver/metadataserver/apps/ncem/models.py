"""
NCEM application data model to represent images, user tags, their
relationships, and captured user feed back (new tags, validation/invalidation).

The model can represent:
1) System data
- ScopeImage: Metadata of a microscope image.
- Tag: Tags.
- Tagged: Relationship of tags with an image: if they are associated or the
  system suggests them as possible candidates.
2) User feedback on the data:
- TagUserAddition: Proposal for new tags of an image.
- TagModification: Feed back on the validity of the relationship of tag with
  an image
  
Sessions: The model captures the concept of session, evaluations of tag
user additions done by the same user together are considered in the same
session. THe session is represented through a session id.

Examples:
    # Creates and stores image metadata.
    si = ScopeImage(file_name="fn.jpg",
                    original_route="/dir1/dir2/fn.jog",
                    bitmap_url="http://image_server/f1.jpg")
    si.save()
    
    # Creates and stores a tag:
    t = Tag(tag="new_tag")
    t.save()
    
    # Creates a tag if it does not exist:
    t2 = Tag.add_tag_safe("new_tag2") 
    
    # Associate a tag to an image
    rel = Tagged(tag=tag, scope_image=si, TAG_ASSOCIATED)
    
    # Associate a tag to an image as suggested
    rel_sug = Tagged(tag=tag, scope_image=si, TAG_SUGGETED)
   
    # Mark a tag of an image as valid
    si.user_validates_tag("new tag")
    
    # Mark a tag of an image as invalid
    si.user_invalidates_tag("new_tag")
        
"""
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

# TODO(gonzalorodrigo): add operation to mark all images as un-reviewed.
# TODO(gonzalorodrigo): Add the concept of "review round".
from metadataserver.apps.ncemreldata.models import Proposal, CalendarEntry, INSTRUMENTS
from metadataserver.apps.searchengine.configuration import METADATA_TYPES, UNKNOWN_SOURCE

# Create your models here.
TAG_ACTION_VALIDATE = "v"
TAG_ACTION_INVALIDATE = "i"
TAG_ACTIONS = [(TAG_ACTION_VALIDATE, "validate"), (TAG_ACTION_INVALIDATE,
                                                   "invalidate")]

TAG_ASSOCIATED = "a"
TAG_SUGGESTED = "s"
TAG_TYPE = [(TAG_ASSOCIATED, "associated"), (TAG_SUGGESTED, "suggested")]

DEFAULT_HISTORIC_INDEX = -1


class SyntheticData(models.Model):
    """
    Metaclass to parent all the model objects that represent data that is
    generated by humans or systems that might change in the time.
    
    Properties:
        historic_index: integer representing what set of data an object belongs
          to. Index -1 corresponds to live data.
    """
    historic_index = models.IntegerField(default=DEFAULT_HISTORIC_INDEX,
                                         blank=False)

    @classmethod
    def inheritors(klass):
        """ Returns a list of the classes that are descendant of this class."""
        subclasses = set()
        work = [klass]
        while work:
            parent = work.pop()
            for child in parent.__subclasses__():
                if child not in subclasses:
                    subclasses.add(child)
                    work.append(child)
        return subclasses

    @classmethod
    def get_max_historic_index_subclasses(cls):
        """ Returns the larger historic_index in all the subclasses."""
        results_list = []
        for child in SyntheticData.inheritors():
            results_list.append(child.get_max_historic_index())
        if results_list:
            return max(results_list)
        else:
            return -1

    @classmethod
    def get_max_historic_index(cls):
        """ Returns the larger historic_index in the table """
        result = cls.objects.all().aggregate(models.Max('historic_index'))
        if result["historic_index__max"] is None:
            return -1
        else:
            return result["historic_index__max"]

    @classmethod
    def update_historic_index(cls, new_id, old_id=-1):
        """
        Updates the historic_index to new_id in all objects that 
        historic_index==old_id.
        
        Args:
          new_id: integer historic_index to be set in all objects of the class.
          old_id: integer historic_index to select objects to be modified.
        
        """
        cls.objects.filter(historic_index=old_id).update(historic_index=new_id)

    @classmethod
    def update_historic_index_subclasses(cls, new_id, old_id=-1):
        """
        Updates the historic_index to new_id in all objects of subclasses such as 
        historic_index==old_id.
        
        Args:
          new_id: integer historic_index to be set in all objects of the class.
          old_id: integer historic_index to select objects to be modified.
        """
        for child in cls.inheritors():
            child.update_historic_index(new_id=new_id, old_id=old_id)

    @classmethod
    def filter_by_historic_index(cls, historic_index=-1, **kwargs):
        """Returns instances of the object according to a historic index"""
        return cls.objects.filter(historic_index=historic_index, **kwargs)

    @classmethod
    def get_by_historic_index(cls, historic_index=-1, **kwargs):
        """Returns an instance of the object according to a historic index"""
        return cls.objects.get(historic_index=historic_index, **kwargs)

    class Meta:
        abstract = True
        unique_together = (('historic_index',),)


class Tag(SyntheticData):
    """
    Represents a text string that can be associated with other elements in the
    model.
    
    Properties:
        tag: String with the tag content.
    """
    tag = models.CharField(max_length=256, primary_key=True, blank=False,
                           default='')

    class Meta:
        ordering = ('tag',)
        unique_together = (SyntheticData.Meta.unique_together[0] + ('tag',))

    def __str__(self):
        return str(self.tag)

    @classmethod
    def add_tag_safe(cls, tag_str, raise_error=False):
        """
        Creates, stores, and returns a new tag, returns the already existing one
        otherwise.        
        
        Args:
            tag_str: string to create the tag with. 
            raise_error: if true, if the object did not exist, it
              raises ObjectDoesNotExist exception
        Returns:
            Tag object
            
        """
        try:
            t = Tag.objects.get(tag=tag_str)
        except ObjectDoesNotExist:
            if raise_error:
                raise ObjectDoesNotExist("tag does not exist")
            t = Tag(tag=tag_str)
            t.save()
        return t


class ScopeImage(models.Model):
    """
    Metadata directly associated with an NCEM image.

    Properties:
        file_name: the name of the file as a string.
        original_route: string of the full directory route (including filename)
          where the original image file was placed.
        file_path: path to file, used for quickly finding all files in the same directory
        bitmap_url: string URL pointing to a location from where a PNG/JPG/TIFF
          version of the image can be downloaded.
        file_metadata: string containing a JSON dictionary that represents the
          file format metadata of the image.
        reviewed: boolean, if True, the image tagging metadata has been
          reviewed or altered.
        acquire_date: timestamp with time zone
        instrument: the name of the ncem instrument
        disabled: boolean
        calendar_entry_id: integer
        proposal_id: integer
        dimensions: integer
    """
    file_name = models.CharField(max_length=1024, blank=False)
    original_route = models.CharField(max_length=2048, blank=False,
                                      db_index=True)
    file_path = models.CharField(max_length=2048, blank=True, db_index=True)
    bitmap_url = models.URLField(max_length=2083, blank=False)
    file_metadata = JSONField(blank=True, default='')
    reviewed = models.BooleanField(default=False)
    tags = models.ManyToManyField(Tag, through='tagged')

    calendar_entry = models.ForeignKey(CalendarEntry, blank=True, null=True,
                                       on_delete=models.SET_NULL)
    proposal = models.ForeignKey(Proposal, blank=True, null=True,
                                 on_delete=models.SET_NULL)
    acquire_date = models.DateTimeField(blank=True, null=True)
    dimensions = models.IntegerField(blank=True, default=0)

    """ Synthetic field Indicating what instrument was used."""
    instrument = models.CharField(max_length=128, choices=INSTRUMENTS,
                                  blank=True, default="TEAM_I")

    """ Controls if image should be shown for review """
    disabled = models.BooleanField(blank=True, default=False)

    class Meta:
        ordering = ('original_route',)

    def _original_route_to_file_path(self):
        return self.original_route.rsplit('/', 1)[0]

    def save(self, *args, **kwargs):
        if not self.file_path or len(self.file_path) == 0:
            self.file_path = self._original_route_to_file_path()
        super().save(*args, **kwargs)

    def _get_tags(self, tag_type):
        return list(Tag.objects.filter(scopeimage=self,
                                       tagged__strength=tag_type,
                                       tagged__historic_index=-1).distinct())

    def get_tags(self):
        """ Returns Tag objects marked as associated with this Image."""
        return self._get_tags(TAG_ASSOCIATED)

    def get_suggested_tags(self):
        """ Returns Tag objects marked suggested for this Image."""
        return self._get_tags(TAG_SUGGESTED)

    def mark_reviewed(self):
        """ Mark this image as reviewed."""
        self.reviewed = True
        self.save()

    def mark_unreviewed(self):
        """ Mark this image as reviewed."""
        self.reviewed = False
        self.save()

    def user_suggests_tag(self, tag_name, session_id=''):
        """ Registers that a user suggests tag tag_name for this image. 
        Suggesting the same tag in the same session_id is ignored. If the tag
        is not in the database, it is added.
        
        Args:
            tag_name: string identifying the tag.
            session_id: string identifying the client session.
        """
        tag = Tag.add_tag_safe(tag_name)
        user_add = TagUserAddition.add_user_addition_safe(scope_image=self,
                                                          tag=tag,
                                                          session_id=session_id)

    def _user_acts_on_tag(self, action, tag_name, session_id="",
                          metadata_type=None):
        tag = Tag.add_tag_safe(tag_name, raise_error=True)
        tagged_list = Tagged.objects.filter(tag=tag, scope_image=self)
        if metadata_type:
            tagged_list = tagged_list.filter(metadata_type=metadata_type)
        if len(tagged_list) == 0:
            raise ObjectDoesNotExist()
        for tagged in tagged_list:
            try:
                tm = TagModification.objects.get(tagged=tagged,
                                                 session_id=session_id)
                tm.delete()
            except ObjectDoesNotExist:
                pass
            tm = TagModification(action=action, tagged=tagged,
                                 session_id=session_id)
            tm.save()

    def user_validates_tag(self, tag_name, session_id="",
                           metadata_type=None):
        """ Registers that a user marks a the tag tag_name of this image as
        valid. It can be called multiple times, it the session_id is the same
        only one validation is registered.
        
        Args:
            tag_name: string identifying the tag.
            session_id: string identifying the client session.
            metadata_type: metadata type of the tag that the user validates.
              if None, all tags named tag_name are validated.
        
        Raises:
            ObjectDoesNotExist: if the tag tag_name does not exist or it is not
              associated with this image. 
        """
        self._user_acts_on_tag(TAG_ACTION_VALIDATE, tag_name, session_id,
                               metadata_type=metadata_type)

    def user_invalidates_tag(self, tag_name, session_id="",
                             metadata_type=None):
        """ Registers that a user marks a the tag tag_name of this image as
        invalid. 
        
        Args:
            tag_name: string identifying the tag.
            session_id: string identifying the client session.
            metadata_type: metadata type of the tag that the user invalidates.
              if None, all tags named tag_name are invalidated.
        
        Raises:
            ObjectDoesNotExist: if the tag tag_name does not exist or it is not
              associated with this image. 
        """
        self._user_acts_on_tag(TAG_ACTION_INVALIDATE, tag_name, session_id,
                               metadata_type=metadata_type)

    def strobe_tags(self, tag_list, session_id=""):
        """ Sets a list of tags as the tags of the class after post-processing. 
                
        Args:
            tag_list: list of tags as string. For each tag: 1) If the tag was
              suggested or assigned, it will be marked as validated. 2) If the
              tag was not suggested or assigned, it will registered as added by
              the user. 3) If a tag was suggested or assigned but is no in the
              list, it will be marked as invalidated.
             session_id: string identifying the client session.
        """
        tag_set = set(tag_list)
        associated_tags = set([str(x) for x in self.get_tags()])
        suggested_tags = set([str(x) for x in self.get_suggested_tags()])

        new_tags = list(
            tag_set.difference(suggested_tags).difference(associated_tags))
        for new_tag in new_tags:
            self.user_suggests_tag(new_tag, session_id)

        validated_tags = list(tag_set.intersection(associated_tags).union(
            tag_set.intersection(suggested_tags)))
        for validated_tag in validated_tags:
            self.user_validates_tag(validated_tag, session_id)

        invalidated_tags = list(associated_tags.union(
            suggested_tags).difference(tag_set))
        for invalidated_tag in invalidated_tags:
            self.user_invalidates_tag(invalidated_tag, session_id)

    def __str__(self):
        return str(self.id) + "-" + str(self.file_name)

    @classmethod
    def get_next_unreviewed(cls, random_order=False, only_related=False,
                            route_filter=None):
        """Returns the first ScopeImage of the database (alpha order on the file
        route) that is not marked as reviewed. 
        
        Args:
            random_order: If True get_next_unreviewed returns a random
              ScopeImage that is not reviewed.
            only_related: If True, get_next_unreviewed only returns those
              images associated to a calendar entry or proposal.
        """
        filter_args = dict(reviewed=False, disabled=False)
        if only_related:
            filter_args["proposal__isnull"] = False
        if route_filter:
            filter_args["original_route__startswith"] = route_filter

        if random_order:
            return ScopeImage.objects.order_by('?').filter(**filter_args)[:1]
        else:
            return ScopeImage.objects.filter(**filter_args)[:1]

    @classmethod
    def get_reviewed_images(cls):
        """Returns the ScopeImage of the database (alpha order on the file
        route) that are marked as reviewed. 
        """
        return ScopeImage.objects.filter(reviewed=True)


class Tagged(SyntheticData):
    """
    Relationship between Tag and ScopeImage as 'associated' or 'suggested' 
    (strength). Independently of the strength, a tag can be related to the
    same image only once.
    
    Properties:
        scope_image: ScopeImage object the tag is connected to.
        tag: Tag object  associated to the ScopeImage.
        strength: TAG_TYPE to indicates the type of association between the
          Tag and the ScopeImage.
    
    """
    scope_image = models.ForeignKey(ScopeImage, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    strength = models.CharField(max_length=4, choices=TAG_TYPE, blank=False,
                                default=TAG_ASSOCIATED)

    metadata_type = models.CharField(choices=METADATA_TYPES, max_length=4,
                                     blank=False,
                                     default=UNKNOWN_SOURCE)

    class Meta(SyntheticData.Meta):
        ordering = ('scope_image', 'metadata_type', 'tag')
        unique_together = (SyntheticData.Meta.unique_together[0] +
                           ('scope_image', 'tag', 'metadata_type'),)


class TagModification(SyntheticData):
    """
    User evaluation on the relationship of a ScopeImage and a Tag. There can
    be only one evaluation per relationship and session.
    
    Properties:
        action: TAG_ACTIONS, if set TAG_ACTION_VALIDATE the user validates the
            relationship. if set to TAG_ACTION_INVALIDATE the user invalidates
            the relationship.
        tagged: Tagged object that this modification judges.
        session_id: string representing the session in which the user judgment
            was captured.
    """
    action = models.CharField(choices=TAG_ACTIONS, max_length=4, blank=False)
    tagged = models.ForeignKey(Tagged, on_delete=models.CASCADE)
    session_id = models.CharField(max_length=1024, blank=True, default='')

    class Meta(SyntheticData.Meta):
        ordering = ('tagged',)
        unique_together = (SyntheticData.Meta.unique_together[0] +
                           ('tagged', 'session_id'),)


class TagUserAddition(SyntheticData):
    """
    User suggestion of a tag for an ImageScope. The same tag can be only 
    suggested once for the same imageScrope in the same session.
    
    Properties:
        scope_image: ScopeImage object that the tag is suggested for.
        tag: Tag object to be suggested for the image.
        session_id: string representing the session in which the user judgment
            was captured.
    """
    scope_image = models.ForeignKey(ScopeImage, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    session_id = models.CharField(max_length=1024, blank=True, default='')

    @classmethod
    def add_user_addition_safe(cls, scope_image, tag, session_id):
        """ Creates a user addition if it does not exist already, returns the
        one exiting otherwise.
        """
        try:
            tm = TagUserAddition.objects.get(scope_image=scope_image,
                                             tag=tag,
                                             session_id=session_id)
        except ObjectDoesNotExist:
            tm = TagUserAddition(scope_image=scope_image,
                                 tag=tag,
                                 session_id=session_id)
            tm.save()
        return tm

    class Meta(SyntheticData.Meta):
        ordering = ('scope_image', 'tag', 'session_id')
        unique_together = (SyntheticData.Meta.unique_together[0] +
                           ('scope_image', 'tag', 'session_id'),)


class ScopeImagesFolder(models.Model):
    """This model stores the observed routes for the files.
    
    Properties:
        route: folder route where images are contained. It has to be unique
          in the database.
        image_count: number of images contained in such folder.
    
    """
    route = models.CharField(max_length=1024,
                             primary_key=True, blank=False)
    image_count = models.IntegerField(default=0)

    class Meta:
        ordering = ('route',)


def add_historic_index(func):
    def wrapper(*args, **kwargs):
        if "historic_index" not in kwargs.keys():
            kwargs["historic_index"] = DEFAULT_HISTORIC_INDEX
        result = func(*args, **kwargs)
        return result

    return wrapper


def add_decorators_class(cls):
    cls.objects.filter = add_historic_index(cls.objects.filter)
    cls.objects.get = add_historic_index(cls.objects.get)


def add_decorators():
    for cls in SyntheticData.inheritors():
        add_decorators_class(cls)


add_decorators()
