"""
This module includes functions to import or export values in models
in a generic way
"""

import csv
import logging
from concurrent.futures import ProcessPoolExecutor, wait

from django.db import models, connection, transaction
from metadataserver.dataexport.csv import save_to_csv

logger = logging.getLogger(__name__)


class WorkerD:

    def __init__(self, model, key_fields, value_fields, create=False):
        """Worker class to map operations on the processed rows
        Args:
          model(Model class): class to operate on
          key_fields(list(str)): name of the key fields used to identify
            the object to write on. Their values must be passed later
            as the first elements in row_data
          value_fiedls(list(str)): name of the value fields to write in
            the object.
          create(bool): if true, the worker will try to create an object
            if it does not exist (identified by key_fields). 
        """

        self._model = model
        self._key_fields = key_fields
        self._value_fields = value_fields
        self._create = create
        self._closed = False

    def do(self, row_data):
        """Action of the worker, finds an object of the model and sets the
        values. The values for the key fields and value fields must be in
        row_data.        
        """
        if not self._closed:
            connection.close()
            self._closed = True
        logger.info("Setting data for {}".format(row_data))
        return DataExporter.set_object_from_data_row(
            self._model, self._key_fields,
            self._value_fields,
            row_data, create=self._create)


class DataExporter(object):
    """This class provides methods to export the values of the objects of a
    model to a CSV file. Also, it can set the values of the objects in
    a model from a CSV file.
    """
    key_field_name = "key-field_"

    @classmethod
    def check_fields(cls, model, fields):
        """Checks that certain fields exist in a model:
        Args:
          model(class of subclass of Model): model to check if the field is
            there or not.
          fields(list(str)): List of the field names to check
        Returns: (Ok, field_name)
          Ok(bool): will be True if all field names in argument fields are
            in the model. False otherwise.
          field_name(str): If Ok is True, it is None, otherwise it contains
            the first field name in fields that is not in model.
        """
        for field in fields:
            try:
                model._meta.get_field(field)
            except models.FieldDoesNotExist:
                return (False, field)
        return True, None

    @classmethod
    def get_key_field(cls, model):
        """Returns a default key field name for a model."""
        return model._meta.pk.name

    @classmethod
    def produce_header(cls, key_fields, fields):
        """Transform and combine keys for the CSV headers.
        Args:
          key_fields(list(str)): list of names of key fields.
          fields(list(str)): lisf of names of value fields.
        Returns(list(str)): a list composed by the key_fields with a prefix
          that ident it as prefix. Followed but the value fields          
        
        """
        return ["{}{}".format(cls.key_field_name, k)
                for k in key_fields] + fields

    @classmethod
    def parse_header_fields(cls, row):
        """Parses a list of header tags from the CSV file and extract key fields
        and value fields. This is the inverse operation of "produce_header"
        Args:
          row(list(str)): list o field names, first key field (with a prefix wot indicaate
            that they are key) followed by value keys.
        Returns: (key_fields, fields):
          key_fields: list of the field names in row that are idenitified as key.
          fields: fields in row that are value fields. 
        """
        key_fields = []
        fields = []
        for field_name in row:
            if field_name.startswith(cls.key_field_name):
                key_fields.append(field_name[len(cls.key_field_name):])
            else:
                fields.append(field_name)

        return key_fields, fields

    @classmethod
    def produce_data_row(self, model_obj, key_fields, fields):
        """Extracts the values of the fields "fields" and "key_fields"
        from a model object and return them as a list. Field validity is not
        checked and will raise and exception if any of the fields do not
        exist in the model obj.
        Args:
          model_obj(model obj): 
          key_fields(list(str)): list of the key fields. Values of these
            fields will be first in the returned list.
          fields(list(str)): list of value fields. . Values of these
            fields will be second in the returned list
        Returns: row_data, a list of the values of fields in model object. 
          First the ones corresponding to key_fields (preserving order),
          followed by the ones corresponding to vields (preserving order).
        """
        row_data = []
        for field in key_fields + fields:
            row_data.append(getattr(model_obj, field))
        return row_data

    @classmethod
    def get_args_dict(cls, keys, values):
        return {x: y for (x, y) in zip(keys, values)}

    @classmethod
    def set_object_from_data_row(self, model, key_fields, fields,
                                 row_data, create=False):
        """Retrieves or creates a object of type model identified by the
        key_fields and sets the values for fields. 
        Args:
          model(type of model subclass): Class of the object to be set.
          key_fields(list(str)): list of names of fields to be used
            to identify the object.
          fields(list(str)): list of names of fields to be set.
          row_data(list): List of values corresponding to the key_fields first
            an fields second. values are in the same order than in those
            lists.
          create: if True, an object will be created if it does not exist.
        Returns(bool): True if the values could be set. False, if the object
          was not found.        
        """
        key_values = row_data[:len(key_fields)]
        values = row_data[len(key_fields):]
        key_dict = self.get_args_dict(key_fields, key_values)
        if create:
            obj, created = model.objects.get_or_create(**key_dict)
            if created:
                logger.info("Object created, keys: {}".format(key_dict))
        else:
            try:
                obj = model.objects.get(**key_dict)
            except models.ObjectDoesNotExist:
                return False
        for (field_name, value) in zip(fields, values):
            setattr(obj, field_name, value)

        obj.save()
        return True

    def export(self, file_name, model, fields, key_fields=[],
               buffer_size=30000):
        """Exports the values of fields of the objects of a model into a CSV
        file. The first row is the name of the fields followed by the data.
        Args:
          file_name(str): file system locatio to store the data in CSV format.
            If file exists, it will be over-written.
          model(type): type of a subclass of django model. All data of the
            objects of the model will be exported.
          fields(list(str)): names of the fields which values must be exported
            to the csv file.
          key_fields(list(str)): list of fields used to identify each object in
            each row. If not set, it will use the model's primary key.
          buffer_size(int): size of the write buffer used to dump on a CSV file.
        Returns(int): Number of data rows writen in the file. 
        """
        fields_ok, bad_field = DataExporter.check_fields(model, fields)
        if not fields_ok:
            raise ValueError("Field {} does not exist in {}".format(
                bad_field, str(model)))

        if not key_fields:
            key_fields = [DataExporter.get_key_field(model)]

        data_rows = []
        data_rows.append(DataExporter.produce_header(key_fields, fields))
        save_to_csv(data_rows, file_name, append=False)
        written_lines = 0
        all_objects = model.objects.all().order_by(
            DataExporter.get_key_field(model))
        num_objects = all_objects.count()
        percent = -1
        with transaction.atomic():
            first_obj = 0
            while first_obj < num_objects:
                last_obj = min(num_objects, first_obj + buffer_size)
                data_rows = []
                objects = all_objects[first_obj:last_obj]
                objects = list(objects)

                for model_obj in objects:
                    data_rows.append(DataExporter.produce_data_row(model_obj,
                                                                   key_fields, fields))

                save_to_csv(data_rows, file_name, append=True)
                written_lines += len(data_rows)
                first_obj = last_obj
                new_percent = int(100 * last_obj / num_objects)
                if new_percent != percent:
                    logger.info("{}% completed".format(new_percent))
                    percent = new_percent
                del objects

        return written_lines

    def get_csv_headers(self, file_name, buffer_size):
        """Extract the first header row from a CSV files.
        Args:
          file_name(str): file system route poitning to the CSV file.
          buffer_size(int): bytes to read to detect the first row. If the first
           row is larger than buffer_size, it raises an Exception.
        Returns(list(str)): first values of the file in a list.  
          
        """
        with open(file_name, "r") as f:
            buff = f.read(buffer_size)
            reader = csv.reader(buff.split('\n'), delimiter=',')
            for row in reader:
                return row
        raise Exception("Could not read header of CSV file {}".format(
            file_name))

    def import_data(self, file_name, model, create=False,
                    buffer_size=300000, num_workers=2, chunksize=10000):
        """Sets model objects fields from the values in a CSV file. The first
        row must contain the fields names. First fields must be key fields 
        (prepended with "key-field_") to identify the object. The following
        one must the values of the fields to be set. It can create the objects
        if they don't exist.
        Args:
          file_name(str): file system route pointed to the location of the
            input csv file.
          model(type): subclass of Model. Class which objects will be set.
          create(bool): if True, objects are created if they do not exist.
          buffer_size(int): size of the reading buffer from the file.
          num_workers(int): number of parallel workers to insert in the
            database.
          chunksize(int): number of rows to give to a worker.
          
        """

        header_fields = self.get_csv_headers(file_name, buffer_size)
        key_fields, value_fields = DataExporter.parse_header_fields(
            header_fields)
        fields_ok, bad_field = DataExporter.check_fields(model,
                                                         key_fields + value_fields)
        if not fields_ok:
            raise ValueError("CSV file field {} does not exist in {}".format(
                bad_field, str(model)))
        logger.info("Detected key fields: {}".format(key_fields))
        logger.info("Detected data fields: {}".format(value_fields))

        futures = []
        w = WorkerD(model, key_fields, value_fields, create=create)
        iterable_reader = self.data_reader_yield(file_name,
                                                 buffer_size=buffer_size)
        with ProcessPoolExecutor(max_workers=num_workers) as executor:
            executor.map(w.do, iterable_reader, chunksize=chunksize)
        wait(futures)

        connection.close()

    def data_reader_yield(self, file_name, buffer_size=300000):
        """Generator to read the rows in a CSV file. It discards the first
        row and yields the rest one row at a time.
        Args:
          file_name(str): file system location where the CSV file sites.
          buffer_size(int): size of thereading buffer used to read the file.
        Yields: one row of the CSV file at a time.        
        """

        with open(file_name, "r") as f:
            buff = f.read(buffer_size)
            first_row = True
            while buff:
                lines = buff.split("\n")
                if buff[-1] == "\n":
                    buff = ""
                else:
                    buff = lines[-1]
                    lines = lines[:-1]
                if lines and not lines[-1]:
                    lines = lines[:-1]

                logger.info("Reader: Extracting lines of CSV files")
                reader = csv.reader(lines, delimiter=',',
                                    quotechar="'", quoting=csv.QUOTE_MINIMAL)
                for row in reader:
                    if not first_row:
                        yield row
                    else:
                        first_row = False
                buff += f.read(buffer_size)
            logger.info("All file read, reader is done.")
