""" Classes and methods to parse CSV tag files from NCEM. """

import csv
import logging

from metadataserver.apps.ncem.models import (
    ScopeImage, Tagged, Tag, TAG_ASSOCIATED, ObjectDoesNotExist, TAG_SUGGESTED
    )
from metadataserver.apps.searchengine.models import UNKNOWN_SOURCE
from metadataserver.seminfer.fileanalysis import simplify_route


logger = logging.getLogger(__name__)

class NCEMDataExtractor(object):
    """Class to parse NCEM image tags CSV files with the following format:
    idx,file_idx,sub_image_idx,file_route,,tag1,tag2,tag3,,,,,,tagn
    
    New parsers can be done by extending this class and re-defining parse_row
    """
    LABEL_TAG_INDEX = 4
    FIRST_LABEL_INDEX = 6
    FILE_ROUTE_INDEX = 3
    REMOVE_DUPLICATES = True

    def _parse_row(self, row, base_route=None):
        if len(row) <= 1:
            return None
        if len(row) <= self.FIRST_LABEL_INDEX:
            raise ValueError("Row is too short, it misses fields: {}".format(
                row))
        if row[0] == "#idx":
            return None
        if (self.LABEL_TAG_INDEX is not None
                and row[self.LABEL_TAG_INDEX] != "LABELS"):
            raise ValueError("Missing LABELS tag in row: {}".format(row))
        file_route = row[self.FILE_ROUTE_INDEX]
        if base_route:
            file_route = simplify_route(base_route, file_route)
        if self.REMOVE_DUPLICATES:
            the_tags = list(
                set([x.strip() for x in row[self.FIRST_LABEL_INDEX:]]))
        else:
            the_tags = [x.strip() for x in row[self.FIRST_LABEL_INDEX:]]
        if "" in the_tags:
            the_tags.remove("")
        return self._create_dict_from_row(row,
                                          file_route=file_route,
                                          the_tags=the_tags)

    def _create_dict_from_row(self, row, file_route, the_tags):
        return dict(idx=int(row[0]),
                    file_idx=int(row[1]),
                    sub_image_idx=int(row[2]),
                    file_route=file_route,
                    tags=the_tags)

    def extract_tags(self, file_route, base_route=None, support_duplicates=True):
        """ Extracts tags for the images present in the file_route CSV file.
        Args:
            - file_route: string pointing to the file system location where the CSV file
              is.
            - base_route: string that should substracted from the parsed file routes
              in the CSV file to identiy an image. e.g. base_route="/foo/", image route
              "/foo/foo2/File1" will be parsed as "foo2/File1".
            - support_duplicates: If true, the system will not break if the same tag
              appears in the same image twice.
        """
        self._data_list = []
        self._data_dic = {}
        with open(file_route) as csv_file:
            label_reader = csv.reader(csv_file, delimiter=",", quotechar="'")
            for row in label_reader:
                item = self._parse_row(row, base_route)
                if item:
                    self._data_list.append(item)
                    self._add_to_dic(item, support_duplicates=support_duplicates)

    def _add_to_dic(self, item, support_duplicates=True):
        key = item["file_route"]
        if key in self._data_dic.keys():
            if not support_duplicates:
                raise ValueError("Duplicated file_route in the labeling process: {}"
                                 "".format(key))

            dic_item = self._data_dic[key]
        else:
            dic_item = []

        self._data_dic[key] = list(set(dic_item + item["tags"]))

    def get_data_list(self):
        """ Gets a list of each data row as a dictionary."""
        return self._data_list

    def get_data_dic(self):
        """ Gets a dictionary if the tags indexed by image route."""
        return self._data_dic

    def get_file_list(self):
        """Returbs a list of all the file_routes parsed in the CSV"""
        return [x["file_route"] for x in self._data_list]


class FlatNCEMDataExtractor(NCEMDataExtractor):
    """ FlatNCEMDataExtractor is parser for a simpler version of the files
    parsed by NCEMDataExtractor. In this format, the CSV rows include the
    file route in the first value and the labels in the rest.
    
    """
    FIRST_LABEL_INDEX = 1
    LABEL_TAG_INDEX = None
    FILE_ROUTE_INDEX = 0

    def _create_dict_from_row(self, row, file_route, the_tags):
        return dict(file_route=file_route,
                    tags=the_tags)


def extract_tags(file_route, base_route=None, list_only=True,
                 support_duplicates=True, suggested=False, simple_format=False,
                 metadata_type=UNKNOWN_SOURCE):
    """ Extracts tags for the images present in the file_route CSV file and stores
    them in the database. Images that are not in the database are skipped.
        Args:
            - file_route: string pointing to the file system location where the CSV file
              is.
            - base_route: string that should substracted from the parsed file routes
              in the CSV file to identiy an image. e.g. base_route="/foo/", image route
              "/foo/foo2/File1" will be parsed as "foo2/File1".
            - list_only: if True, extract_tags only list the files and tags detected
              and does not store the data.
            - support_duplicates: If true, the system will not break if the same tag
              appears in the same image twice.
            - suggested: if True, tags are registered as suggested.
            - simple_format: if True, the csv expected format is: 
                file_route, tag1, tag2, tag3...
        """
    if not simple_format:
        extractor = NCEMDataExtractor()
    else:
        extractor = FlatNCEMDataExtractor()
    extractor.extract_tags(file_route, base_route=base_route,
                           support_duplicates=support_duplicates)
    data_dict = extractor.get_data_dic()
    if list_only:
        logger.info("Detected files")
        for file_name in extractor.get_file_list():
            logger.info(file_name)
        logger.info("")
        logger.info("Files and tags")
        for item in data_dict.keys():
            logger.info("{}: {}".format(item, data_dict[item]))
    else:
        logger.info("Storing data")
        relationship = TAG_ASSOCIATED
        if suggested:
            relationship = TAG_SUGGESTED
        for item in data_dict.keys():
            try:
                si = ScopeImage.objects.get(original_route=item)
            except ObjectDoesNotExist:
                logger.warning("Image {} is not in the database, skipping".format(item))
                continue
            for tag_str in data_dict[item]:
                tag = Tag.add_tag_safe(tag_str)
                try:
                    tg = Tagged.objects.get(scope_image=si, tag=tag,
                                            metadata_type=metadata_type)
                except ObjectDoesNotExist:
                    tg = Tagged(scope_image=si, tag=tag, strength=relationship,
                                metadata_type=metadata_type)
                    tg.save()
