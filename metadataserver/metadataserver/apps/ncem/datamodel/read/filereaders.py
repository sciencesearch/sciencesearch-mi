""" Functions to read and extract info on NCEM files of SER, DM3/DM4 format."""
import ntpath
import os
import logging

import matplotlib
from PIL import Image

matplotlib.rcParams["backend"] = "Agg"

from matplotlib import cm
from matplotlib.image import imsave
import ncempy.io.dm
import ncempy.io.ser

import hyperspy.api as hs
import matplotlib.pyplot as plt
import numpy as np

from metadataserver.apps.ncemreldata.models import curate_date

SUPPORTED_EXTENSIONS = ['ser', 'dm3', 'dm4']
logger = logging.getLogger(__name__)


def get_reader(file_route, no_image_caching=False):
    extension = file_route.split('.')[-1]
    if not extension:
        raise ValueError("File has no extension, it is not possible to "
                         "determine appropriate reader.")
    if extension not in SUPPORTED_EXTENSIONS:
        raise ValueError("Extension {} of {} is not supported"
                         "".format(extension, file_route))
    if extension in ["ser"]:
        return SerFile(file_route, no_image_caching=no_image_caching)
    elif extension in ["dm3", "dm4"]:
        return Dm3File(file_route, no_image_caching=no_image_caching)


class SerFile(object):
    """ Reads and extracts metadata and bitmap of a NCEM SER image file. If
    the SER file comes with an EMI file, it will try to open it and parse it
    too.
    
    File name conventions. If the SER file is named "whatever1_whatever2.ser",
    the class will try to find an emi file called "whatever1.emi" in the same
    folder. 
    
    Metadata format: dictionary with three keys:
        - 'header': a dictionary corresponding to the header of the file
        - 'metadata': a dictionary corresponding to the file sub metadata.
        - 'emi': it contains the emi information as dictionary if present,
           None otherwise. 
    """

    def __init__(self, file_route, no_image_caching=False):

        self._file_route = file_route
        self._metadata = None
        self._img = None
        self._no_image_caching = no_image_caching
        self._dimensions = self.process_file(self._file_route)

    @staticmethod
    def _get_dimensions(img):
        dimensions = len(img.shape)
        for d in img.shape:
            if d == 1:
                dimensions -= 1
        dimensions = max(dimensions, 1)
        return dimensions

    def get_dimensions(self):
        return self._dimensions

    @classmethod
    def _discover_emi(cls, file_route):
        # we assume the file is called something like:
        # 17mrad_conv-reference_image-overview_1.ser
        # and a equivalent to 
        # 17mrad_conv-reference_image-overview.emi
        # exists.
        file_name = ntpath.basename(file_route)
        folder = os.path.dirname(file_route)
        parts = file_name.split("_")
        if len(parts) == 1:
            # No "_" in the filename.
            return None
        emi_file_name = "{}.emi".format("_".join(parts[:-1]))
        emi_file_route = os.path.join(folder, emi_file_name)
        if not os.path.isfile(emi_file_route):
            # file does not exist
            return None

        return emi_file_route

    def process_file(self, file_route):
        emi_route = SerFile._discover_emi(file_route)
        f = ncempy.io.ser.fileSER(file_route, emi_route)
        dataset = f.getDataset(0)

        self._metadata = dict(header=f.head,
                              emi=f._emi,
                              metadata=dataset[1])
        self._img = dataset[0]
        del f
        return self._get_dimensions(self._img)

    def get_metadata(self):
        """ Returns a dictionary containing the file metadata."""
        if self._metadata is None:
            raise ValueError("Metadata has not been parsed yet.")
        return self._metadata

    def save_png(self, target_route, do_contrast_scale=None):
        """Saves a PNG of the scope image in target_route"""
        if self.get_dimensions() > 1:
            self._save_png(target_route, do_contrast_scale=do_contrast_scale)
        else:
            logger.info("dimensions {}".format(self.get_dimensions()))
            self.save_png_spectrum(self._file_route, target_route)

    @staticmethod
    def save_png_spectrum(file_route, target_route):
        """Saves the image as an spectrum file: A plot of energy vs. electors.
        Args:
          file_route(str): route in the file system to read the ser/dm3/dm4
            file from.
          target_route(str): route in the the file system to save the
            png plot to.
        """
        try:
            signal = hs.load(file_route)
            if isinstance(signal, list):
                signal = signal[0]
            signal.plot(navigator=None)
            plt.savefig(target_route)
            plt.close()
        except AttributeError as e:
            logger.error("Error producing spectrum for 1D img {}".format(
                target_route))
            logger.exception(e)
            return False
        return True

    def _save_png(self, target_route, do_contrast_scale=None):
        """Saves a PNG of the scope image in target_route"""
        if self._img is None:
            raise ValueError("Image has not been extracted yet.")
        if len(self._img.shape) == 1:
            logger.warning("WARNING, uni-dimensional image detected ({}), "
                  "reshaping to {}."
                  "".format(self._img.shape, (self._img.shape[0], 1)))
            self._img = self._img.reshape(self._img.shape[0], 1)
        imsave(target_route, self._img, format="png",
               cmap=cm.get_cmap("viridis"))

    @staticmethod
    def make_thumbnail_from_png(orig_png, width, height, small_png=None):
        if not small_png:
            small_png = ".".join(orig_png.split(".")[:-1]) + ".tb.png"

        img = Image.open(orig_png)

        scale_size = max(width, height)
        img.thumbnail((scale_size, scale_size))
        h_pad = (scale_size - img.size[0]) / 2
        v_pad = (scale_size - img.size[1]) / 2
        img = img.crop((-h_pad, -v_pad, img.size[0] + h_pad, img.size[1] + v_pad))

        img.save(small_png)

    def get_acquire_date(self):
        if (self._metadata is None or "emi" not in self._metadata
                or self._metadata["emi"] is None
                or self._metadata["emi"]["AcquireDate"] is None):
            return None

        return curate_date(self._metadata["emi"]["AcquireDate"])


class Dm3File(SerFile):
    """ Reads and extracts metadata and bitmap of a NCEM dm3 image file
        
    Metadata format: dictionary with three keys:
        - 'dimensions': integer with the number of dimensions in the image.
        - 'header': a dictionary corresponding to the header of the file.
        - 'metadata': a dictionary corresponding to the file sub metadata.
         
    """

    def process_file(self, file_route, bin_text_dump=False):
        f = ncempy.io.dm.fileDM(file_route,
                                on_memory=not self._no_image_caching)
        if bin_text_dump:
            logger.info("[DATA BIN DUMP START: {}]".format(file_route))
            data_bin = f.fromfile(f.fid, dtype="<u1", count=-1)
            logger.info(f._bin2str(data_bin))
            logger.info("[DATA BIN DUMP END: {}]".format(file_route))
            # TODO(gonzalorodrigo): Transform prints into log calls.
        f.parseHeader()
        ds = f.getDataset(0)
        img3D = ds['data']
        true_dimensions = self._get_dimensions(img3D)
        orig_dimensions = len(img3D.shape)
        dimensions = orig_dimensions
        if dimensions == 2:
            self._img = img3D
        else:
            while dimensions > 2:
                img3D = img3D[int(img3D.shape[0] / 2), :, :]
                dimensions = len(img3D.shape)
            self._img = img3D
        self._metadata = dict(dimensions=orig_dimensions,
                              header=f.allTags,
                              metadata={x: ds[x]
                                        for x in ds.keys()
                                        if x != "data"})
        del f
        return true_dimensions

    @staticmethod
    def contrast_scale(img):
        """
        % scale image to have mean of 0, standard deviation of 1
        image = image - mean(image(:));
        image = image / sqrt(mean(image(:).^2));
        
        % scale image to designed contrast level, for example -3 to +3 standard devs, clamp to values between 0 and 1
        image = (image - (-3)) / ( 3 - (-3));
        image = min(max(image,0),1);
        
        %  output 256 colour map indices, assuming 0 to 255 range:
        image = uint8(round(255*image));
        """
        # scale image to have mean of 0, standard deviation of 1
        orig_mean = np.mean(img)
        orig_stdev = np.std(img)
        new_mean = 0.0
        new_stdev = 1.0
        img = new_mean + (img - orig_mean) * (new_stdev / orig_stdev)

        # scale image to designed contrast level, for example -3 to +3 standard
        #  devs, clamp to values between 0 and 1
        scale_contrast = 3.0
        img = (img - (-scale_contrast)) / (scale_contrast - (-scale_contrast))
        img = np.maximum(img, np.zeros(shape=img.shape))
        img = np.minimum(img, np.ones(shape=img.shape))

        return img

    def _save_png(self, target_route, do_contrast_scale=True):
        """Saves a PNG of the scope image in target_route. If the image has
        more than three dimensions it is flatten to (n1/2,n2,n3)."""

        if self._img is None:
            raise ValueError("Image has not been extracted yet.")
        img = self._img
        if do_contrast_scale:
            img = self.contrast_scale(img)
        imsave(target_route, img, format="png",
               cmap=cm.get_cmap("gray"))

    def get_acquire_date(self):
        date_value = None
        time_value = None

        if self._metadata and "header" in self._metadata:
            for key in self._metadata["header"]:
                if not date_value and "Acquisition Date" in key:
                    date_value = self._metadata["header"][key]
                if not time_value and "Acquisition Time" in key:
                    time_value = self._metadata["header"][key]
                if date_value and time_value:
                    break
        if not date_value or not time_value:
            return None
        date_text = ("{} {}".format(date_value, time_value))
        return curate_date(date_text)


class SpectrumReader(object):

    def __init__(self, file_route, no_image_caching=False):

        self._file_route = file_route
        self._signals = []

    def process_file(self, file_route):
        self._signals = hs.load(file_route)

    def save_png(self, target_route, do_contrast_scale=None):
        if self._signals:
            plt.savefig(target_route)
        else:
            logger.warning("1D img {} cannot be written: no signals detected ".format(
                target_route))
