"""
Things this module has to do:
- Read and open dm3 and dm4 files:
    - extract the metainfo
    - produce a JPG/PNG/TIFF
    - produce also the FFT?
- realize what is the real folder
"""
import urllib.parse
import logging

from django.utils import timezone

from metadataserver.apps.ncem.datamodel.read.filereaders import get_reader, SerFile
from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.dataexport import make_dict_serializeable
from metadataserver.seminfer.fileanalysis import ensure_folder, simplify_route
from metadataserver.seminfer.fileanalysis.filefinders import do_on_all_files

logger = logging.getLogger(__name__)


def process_single_file(image_route, img_folder, base_folder='', img_format='png',
                        url_prefix=None, allow_duplicates=False, instrument="",
                        no_contrast_scale=False, thumbnail_res=(300, 300),
                        no_image_caching=False):
    """ Extracts ScopeImage data from NCEM image and stores it the database.
    It also produces a PNG and stores it in a destination folder.
    Args:
        image_route: string pointing to the location of the file.
        img_folder: destination folder where the generated PNG will be stored.
        base_folder: folder route to substract from image_route to calculate
          the original_route.
        img_format: string identifying the image format to transform the NCEM
          image to. Values supported 'png'.
        url_prefix: if set with a string, its value is prepended to the
          original_route to construct the ScopeImage bitmap url.
        allow_duplicates: if False, a file which original_route is already
          present in the DB will not be processed.
        instrument: string identifying the instrument this file relates to
        no_contrast_scale:
        thumbnail_res: thumbnail width, height
        no_image_caching:
    Returns:
        Id of the created ScopeImage entry.
    
    """
    logger.info("Processing: {}".format(image_route))
    file_name = image_route.split("/")[-1]
    original_route = simplify_route(base_folder, image_route)
    file_path = original_route.rsplit("/", 1)[0]
    bitmap_url = None

    if (not allow_duplicates and
            len(ScopeImage.objects.filter(original_route=original_route)) > 0):
        logger.info("SKIPPING, original route already present in DB: {}".format(
            original_route))
        return None
    try:
        image_processor = get_reader(image_route,
                                     no_image_caching=no_image_caching)
    except Exception as e:
        # TODO(gonzalorodrigo): Refine exceptions
        logger.warning("SKIPPING, error processing file: {}. Detail: ({}){}".format(
            image_route,
            type(e), e))
        return None

    file_metadata = image_processor.get_metadata()
    file_metadata = make_dict_serializeable(file_metadata)

    dimensions = image_processor.get_dimensions()

    acquire_date = image_processor.get_acquire_date()

    if img_format == 'png':
        img_route = "{}/{}.png".format(img_folder,
                                       original_route).replace("//",
                                                               "/")
        ensure_folder(img_route)
        image_processor.save_png(img_route,
                                 do_contrast_scale=not no_contrast_scale)
        bitmap_url = "{}.png".format(original_route)
        if thumbnail_res:
            SerFile.make_thumbnail_from_png(img_route, thumbnail_res[0],
                                            thumbnail_res[1])
        if url_prefix:
            bitmap_url = urllib.parse.urljoin(url_prefix, bitmap_url)
    elif img_format:
        raise ValueError("Not supported image format: {}".format(
            img_format))

    si = ScopeImage(file_name=file_name,
                    original_route=original_route,
                    file_path=file_path,
                    bitmap_url=bitmap_url,
                    file_metadata=file_metadata,
                    acquire_date=acquire_date,
                    instrument=instrument,
                    dimensions=dimensions)
    si.save()

    return si.id


num_processed_images = 0
num_acquired_dates = 0
last_num_processed_images = 0
last_date = None


class DateStatsMaker(object):
    """ This class wraps a method of with the signature of process_single_file.
    It is used to perform stats on the the date parsing code for image
    crawling.
    """

    def __init__(self, reporting_period=100):
        self._reporting_period = reporting_period
        self._num_processed_images = 0
        self._num_acquired_dates = 0
        self._last_num_processed_images = 0
        self._last_date = None

    def acquire_date_stats(self, image_route, img_folder=None,
                           base_folder='', img_format='png',
                           url_prefix=None,
                           allow_duplicates=False,
                           instrument=""):
        """ Extracts ScopeImage data from NCEM image and keeps track of the
        well extracted dates. Uses global variables.
        Args:
            image_route: string pointing to the location of the file.
            img_folder: destination folder where the generated PNG will be stored.
            base_folder: folder route to substract from image_route to calculate
              the original_route.
            img_format: string identifying the image format to transform the NCEM
              image to. Values supported 'png'.
            url_prefix: if set with a string, its value is prepended to the
              original_route to construct the ScopeImage bitmap url.
            allow_duplicates: if False, a file which original_route is already
              present in the DB will not be processed.
        Returns:
            Id of the created ScopeImage entry.
        
        """
        try:
            image_processor = get_reader(image_route)
        except Exception as e:
            # TODO(gonzalorodrigo): Refine exceptions
            logger.warning("SKIPPING, error processing file: {}. Detail: ({}){}".format(
                image_route,
                type(e), e))
            return None

        file_metadata = image_processor.get_metadata()
        file_metadata = make_dict_serializeable(file_metadata)

        acquire_date = image_processor.get_acquire_date()

        self._num_processed_images += 1
        if acquire_date is not None:
            self._num_acquired_dates += 1
        else:
            logger.warning("acquire_date is None for :{}".format(image_route))

        if self._num_processed_images % self._reporting_period == 0:
            self.print_stats_crawl()
        return 0

    def print_stats_crawl(self, pre_text=""):
        """ Prints stats on the dates correctly parsed (absolute number
        and percentage) and the number of images processed per second since
        the last call.
        Args:
            pre_text: string is preprended to the stats messsage.
        """
        current_time = timezone.now()
        percent = "N/A"
        perf_str = ""
        if self._num_processed_images:
            percent = (100.0 * float(self._num_acquired_dates) /
                       float(self._num_processed_images))
            if self._last_date is not None:
                span = current_time - self._last_date
                rate = (float(self._num_processed_images -
                              self._last_num_processed_images) /
                        float(span.total_seconds()))
                perf_str = "Images/s. {}".format(rate)
        self._last_date = current_time
        self._last_num_processed_images = num_processed_images

        logger.info("{}:{}{} processed images. {} dates. {}% correct dates. {}".format(
            current_time,
            pre_text,
            self._num_processed_images,
            self._num_acquired_dates,
            percent,
            perf_str
            ))


def crawl(source_folder, imgs_target_folder, base_folder="",
          extensions=list(["ser", "dm3", "dm4"]), img_format="png", url_prefix=None,
          recursive=True, allow_duplicates=False,
          instrument="", only_measure=False, no_contrast_scale=False,
          thumbnail_res=(300, 300),
          no_image_caching=False):
    """ Extracts ScopeImage data from NCEM images and stores it the database.
    It also produces a PNGs and stores them in a destination folder.
    Args:
        source_folder: string pointing to a folder where crawl will look for
           NCEM images.
        imgs_target_folder: destination folder where the generated PNGs will be
            stored.
        base_folder: folder route to substract from image_route to calculate
          the original_route.
        extensions: NCEM extension files to considered.            
        img_format: string identifying the image format to transform the NCEM
          image to. Values supported 'png'.
        url_prefix: if set with a string, its value is prepended to the
          original_route to construct the ScopeImage bitmap url.
        recursive: If True, crawl will search also subdirectories for images.
        allow_duplicates: if False, files which original_route is already
          present in the DB will not be processed.
    Returns:
        List of Ids of the created ScopeImage entries.
    
    
    """
    work_function = process_single_file

    if only_measure:
        date_stats = DateStatsMaker()
        work_function = date_stats.acquire_date_stats

    result = do_on_all_files(source_folder,
                             extensions,
                             work_function,
                             recursive,
                             imgs_target_folder,
                             base_folder=base_folder,
                             img_format=img_format,
                             url_prefix=url_prefix,
                             allow_duplicates=allow_duplicates,
                             instrument=instrument,
                             no_contrast_scale=no_contrast_scale,
                             thumbnail_res=thumbnail_res,
                             no_image_caching=no_image_caching)

    if only_measure:
        date_stats.print_stats_crawl("END: ")
    return result
