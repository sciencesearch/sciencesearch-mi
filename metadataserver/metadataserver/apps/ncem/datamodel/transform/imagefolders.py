from metadataserver.apps.ncem.models import ScopeImage, ScopeImagesFolder


class ScopeImageFolderGenerator:
    """ScopeImageFolderGenerator class is capable of detecting the different
    directories that contain or are parents to folders containing ScopeImages.
    It reviews the images in the database, extracts the folders, and group them.
    It also counts the images that are pending from a folder.
    """

    def __init__(self, delimiter="/"):
        self._route_list = {}
        self._delimiter = delimiter

    def _transform_folder(self, folder, depth,
                          is_file=True):

        parts = folder.split(self._delimiter)

        if parts and is_file:
            parts = parts[:-1]
        parts = parts[:depth]

        if len(parts) == 1 and folder and folder[0] == "/":
            parts.append("")

        return self._delimiter.join(parts)

    def _add_folder(self, folder, depth, is_file=True):
        p_folder = None
        for i in range(1, depth + 1):
            p_folder = self._add_folder_d(folder, i, avoid=p_folder,
                                          is_file=is_file)

    def _add_folder_d(self, folder, depth, avoid=None, is_file=True):
        clean_folder = self._transform_folder(folder, depth,
                                              is_file=is_file)
        if clean_folder != avoid:
            if not clean_folder in self._route_list:
                self._route_list[clean_folder] = 1
            else:
                self._route_list[clean_folder] += 1
        return clean_folder

    def scan_folders(self, depth=3, is_file=True):
        """Extracts the list of folders from the ScopeImages in the database.
        It will detect any folder that has images in underlaying folders. Only
        folders to depth "depth" from the root. List is stored in the class.
        
        Args:
          depth (int): folders depth to be considered (including"/").
          is_file (bool): if True, it means that the file routes include the
            file name.
        
        Returns(int): Number of different folders detected.   
        """
        self._route_list = {}
        for image in ScopeImage.objects.filter(disabled=False):
            self._add_folder(image.original_route, depth,
                             is_file=is_file)
        return len(self._route_list)

    def dump_to_model(self, clean_existing=True):
        """Stores detected folders and number of files pending from them in
        the ScopeImagesFolder model."""
        if clean_existing:
            ScopeImagesFolder.objects.all().delete()
        for (folder, count) in self._route_list.items():
            f = ScopeImagesFolder(route=folder,
                                  image_count=count)
            f.save()
