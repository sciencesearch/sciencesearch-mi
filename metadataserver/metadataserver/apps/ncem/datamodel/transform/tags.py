import logging

from metadataserver.apps.ncem.models import (
    ScopeImage, TagUserAddition, TagModification, Tagged, Tag, SyntheticData, TAG_ACTION_VALIDATE
    )

logger = logging.getLogger(__name__)


def simple_from_user_feedback():
    """ Applies user modifications to images that are reviewed using a simple
    model:
    - Modification for images that are not reviewed are ignored and stored.
    - Tags associated with an Scope Image are:
      - Those marked suggested or associated and marked by the user as valid.
      - Those new proposed by the user.  
    """
    logger.info("Starting user feedback processing: Simple model. Only reviewed"
          " images will be processed.")
    max_id = SyntheticData.get_max_historic_index_subclasses()
    new_historic_id = max_id + 1
    SyntheticData.update_historic_index_subclasses(new_historic_id)

    for si in ScopeImage.get_reviewed_images():
        new_tags = [str(x.tag)
                    for x in TagUserAddition.objects.filter(scope_image=si,
                                                            historic_index=new_historic_id)]

        validated_tags = [str(x.tagged.tag)
                          for x in TagModification.objects.filter(
                tagged__scope_image=si,
                action=TAG_ACTION_VALIDATE,
                historic_index=new_historic_id)]

        all_tags = list(set(new_tags + validated_tags))

        logger.info("Processing IMAGE({},{}). New tags: {}".format(si.id,
                                                             si.original_route,
                                                             all_tags))

        for tag_name in all_tags:
            tag = Tag.add_tag_safe(tag_name)
            tagged = Tagged(scope_image=si, tag=tag)
            tagged.save()
        si.mark_unreviewed()
