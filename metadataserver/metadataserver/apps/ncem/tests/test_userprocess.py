from django.core.management import call_command
from django.test import TestCase

from metadataserver.apps.ncem.datamodel.transform.tags import simple_from_user_feedback
from metadataserver.apps.ncem.models import (
    Tag, ScopeImage, Tagged, TagUserAddition, TAG_SUGGESTED, TagModification
    )


class TestUserProcessSimple(TestCase):

    def test_user_process_command(self):
        self.test_user_process(True)

    def test_user_process(self, as_command=False):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()

        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()
        tag3 = Tag(tag="tag3")
        tag3.save()
        tag4 = Tag(tag="tag4")
        tag4.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3, strength=TAG_SUGGESTED)
        tg3.save()
        tg4 = Tagged(scope_image=si, tag=tag4, strength=TAG_SUGGESTED)
        tg4.save()

        session_id = "1234"
        si.strobe_tags(["tag5", "tag6", "tag1", "tag3"], session_id=session_id)
        si.mark_reviewed()
        if not as_command:
            simple_from_user_feedback()
        else:
            args = []
            opts = {}

            call_command('userprocess', *args, **opts)

        si_after = ScopeImage.objects.get(id=si.id)

        self.assertEqual(set([str(tag) for tag in si_after.get_tags()]),
                         {"tag5", "tag6", "tag1", "tag3"})

        self.assertEqual(list(TagUserAddition.filter_by_historic_index()), [])
        self.assertEqual(list(TagModification.filter_by_historic_index()), [])

    #         print (TagModification.objects.all())
    #         self.assertNotEqual(list(
    #             TagModification.filter_by_historic_index(historic_index=0)), [])
    #         self.assertNotEqual(list(
    #             TagUserAddition.filter_by_historic_index(historic_index=0)), [])
    #

    def test_user_process_bug(self):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()

        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()
        tag3 = Tag(tag="tag3")
        tag3.save()
        tag4 = Tag(tag="tag4")
        tag4.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3, strength=TAG_SUGGESTED)
        tg3.save()
        tg4 = Tagged(scope_image=si, tag=tag4, strength=TAG_SUGGESTED)
        tg4.save()

        session_id = "1234"
        si.strobe_tags(["tag1", "tag2", "tag3", "tag4",
                        "tag_new"], session_id=session_id)
        si.mark_reviewed()

        si_2 = ScopeImage(file_name="fn2.jpg",
                          original_route="/dir1/dir2/fn2.jog",
                          bitmap_url="http://image_server/f2.jpg")
        si_2.save()

        tg1 = Tagged(scope_image=si_2, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si_2, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si_2, tag=tag3, strength=TAG_SUGGESTED)
        tg3.save()
        tg4 = Tagged(scope_image=si_2, tag=tag4, strength=TAG_SUGGESTED)
        tg4.save()
        si_2.strobe_tags(["tag_new"], session_id=session_id)
        si_2.mark_reviewed()

        simple_from_user_feedback()
        si_after = ScopeImage.objects.get(id=si.id)

        self.assertEqual(set([str(tag) for tag in si_after.get_tags()]),
                         {"tag1", "tag2", "tag3", "tag4", "tag_new"})

        si_2_after = ScopeImage.objects.get(id=si_2.id)
        self.assertEqual(set([str(tag) for tag in si_2_after.get_tags()]),
                         {"tag_new"})

        self.assertEqual(list(TagUserAddition.filter_by_historic_index()), [])
        self.assertEqual(list(TagModification.filter_by_historic_index()), [])
