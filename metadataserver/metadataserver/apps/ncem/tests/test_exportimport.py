from metadataserver.apps.ncem.datamodel.read.exportimport import DataExporter
from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.apps.papermanager.models import Paper
from metadataserver.seminfer.tests import TestFileGeneric, TestFileGenericTransaction


class TestExport(TestFileGeneric):
    _tmp_folder = "/tmp/test_NCEM"

    def setUp(self):
        self._register_tmp_folder(self._tmp_folder)
        self._create_empty(self._tmp_folder, is_folder=True)

    def test_check_fields(self):
        self.assertEqual(DataExporter.check_fields(ScopeImage, []),
                         (True, None))

        self.assertEqual(DataExporter.check_fields(ScopeImage, ["id",
                                                                "file_name", "original_route"]),
                         (True, None))

        self.assertEqual(DataExporter.check_fields(ScopeImage, ["id2",
                                                                "file_name", "original_route"]),
                         (False, "id2"))

    def test_get_key_field(self):
        self.assertEqual(DataExporter.get_key_field(ScopeImage), "id")
        self.assertEqual(DataExporter.get_key_field(Paper), "simple_title")

    def test_produce_headers(self):
        self.assertEqual(DataExporter.produce_header(["f1"], ["f2", "f3"]),
                         ["key-field_f1", "f2", "f3"])
        self.assertEqual(DataExporter.produce_header([], ["f2", "f3"]),
                         ["f2", "f3"])

    def test_parse_header_fields(self):
        self.assertEqual(DataExporter.parse_header_fields(["key-field_title",
                                                           "text_body"]),
                         (["title"], ["text_body"]))

        self.assertEqual(DataExporter.parse_header_fields(
            DataExporter.produce_header(["f1"], ["f2", "f3"])),
            (["f1"], ["f2", "f3"]))

    def test_produce_data_row(self):
        p = Paper(simple_title="simple", title="title_c", text_body="body_c")

        self.assertEqual(DataExporter.produce_data_row(
            p, ["simple_title"], ["title", "text_body"]),
            ["simple", "title_c", "body_c"])

    def test_set_object_from_data_row(self):
        p = Paper(simple_title="simple_title_1", title="title_1_o",
                  text_body="text_body_1_o")
        p.save()

        self.assertTrue(DataExporter.set_object_from_data_row(Paper,
                                                              ["simple_title"],
                                                              ["title", "text_body"],
                                                              ["simple_title_1", "title_1", "text_body_1"],
                                                              create=False))
        p1 = Paper.objects.get(simple_title="simple_title_1")
        self.assertEqual(p1.title, "title_1")
        self.assertEqual(p1.text_body, "text_body_1")

        self.assertFalse(DataExporter.set_object_from_data_row(Paper,
                                                               ["simple_title"],
                                                               ["title", "text_body"],
                                                               ["simple_title_2", "title_1", "text_body_1"],
                                                               create=False))

    def test_set_object_from_data_row_create(self):
        self.assertTrue(DataExporter.set_object_from_data_row(Paper,
                                                              ["simple_title"],
                                                              ["title", "text_body"],
                                                              ["simple_title_1", "title_1", "text_body_1"],
                                                              create=True))
        p1 = Paper.objects.get(simple_title="simple_title_1")
        self.assertEqual(p1.title, "title_1")
        self.assertEqual(p1.text_body, "text_body_1")

    def test_export(self):
        p1 = Paper(simple_title="simple_title_1",
                   title="title_1",
                   text_body="text_body_1")
        p1.save()

        p2 = Paper(simple_title="simple_title_2",
                   title="title_2",
                   text_body="text_body_2")
        p2.save()

        file_name = self._create_test_file("exported.csv", "")
        de = DataExporter()

        self.assertEqual(de.export(file_name, Paper, ["title", "text_body"]),
                         2)

        with open(file_name, "r") as f:
            content = f.read()
            self.assertEqual(content,
                             "key-field_simple_title,title,text_body\n" +
                             "simple_title_1,title_1,text_body_1\n" +
                             "simple_title_2,title_2,text_body_2\n")

    def test_export_buffer_size(self):
        p1 = Paper(simple_title="simple_title_1",
                   title="title_1",
                   text_body="text_body_1")
        p1.save()

        p2 = Paper(simple_title="simple_title_2",
                   title="title_2",
                   text_body="text_body_2")
        p2.save()
        file_name = self._create_test_file("exported_2.csv", "")
        de = DataExporter()

        self.assertEqual(de.export(file_name, Paper, ["title"],
                                   buffer_size=1),
                         2)

        with open(file_name, "r") as f:
            content = f.read()
            self.assertEqual(content,
                             "key-field_simple_title,title\n" +
                             "simple_title_1,title_1\n" +
                             "simple_title_2,title_2\n")

    def test_export_key_field(self):
        p1 = Paper(simple_title="simple_title_1",
                   title="title_1",
                   text_body="text_body_1")
        p1.save()

        p2 = Paper(simple_title="simple_title_2",
                   title="title_2",
                   text_body="text_body_2")
        p2.save()

        file_name = self._create_test_file("exported.csv", "")
        de = DataExporter()

        self.assertEqual(de.export(file_name, Paper, ["text_body"],
                                   key_fields=["title"]),
                         2)

        with open(file_name, "r") as f:
            content = f.read()
            self.assertEqual(content,
                             "key-field_title,text_body\n" +
                             "title_1,text_body_1\n" +
                             "title_2,text_body_2\n")

    def test_get_args_dict(self):
        self.assertEqual(DataExporter.get_args_dict(["key1", "key2"],
                                                    ["value1", "value2"]),
                         {"key1": "value1", "key2": "value2"})

    def test_import_data(self):
        file_name = self._create_test_file("exported.csv",
                                           "key-field_simple_title,title,text_body\n" +
                                           "simple_title_1,title_1,text_body_1\n" +
                                           "simple_title_2,title_2,text_body_2\n")

        de = DataExporter()
        objs = list(de.data_reader_yield(file_name))
        self.assertEqual(objs[0],
                         ["simple_title_1", "title_1", "text_body_1"])
        self.assertEqual(objs[1],
                         ["simple_title_2", "title_2", "text_body_2"])
        self.assertEqual(len(objs), 2)

    def test_import_data_cut_buffer(self):
        file_name = self._create_test_file("exported.csv",
                                           "key-field_simple_title,title,text_body\n" +
                                           "simple_title_1,title_1,text_body_1\n" +
                                           "simple_title_2,title_2,text_body_2\n")

        de = DataExporter()
        objs = list(de.data_reader_yield(file_name, buffer_size=80))
        self.assertEqual(objs[0],
                         ["simple_title_1", "title_1", "text_body_1"])
        self.assertEqual(objs[1],
                         ["simple_title_2", "title_2", "text_body_2"])
        self.assertEqual(len(objs), 2)

    def test_get_csv_headers(self):
        file_name = self._create_test_file("exported.csv",
                                           "key-field_simple_title,title,text_body\n" +
                                           "simple_title_1,title_1,text_body_1\n" +
                                           "simple_title_2,title_2,text_body_2\n")
        de = DataExporter()
        self.assertEqual(de.get_csv_headers(file_name, 300000),
                         ["key-field_simple_title", "title", "text_body"])


class TestExportTransac(TestFileGenericTransaction):
    _tmp_folder = "/tmp/test_NCEM"

    def setUp(self):
        self._register_tmp_folder(self._tmp_folder)
        self._create_empty(self._tmp_folder, is_folder=True)

    def test_import(self):
        file_name = self._create_test_file("exported.csv",
                                           "key-field_simple_title,title,text_body\n" +
                                           "simple_title_1,title_1,text_body_1\n" +
                                           "simple_title_2,title_2,text_body_2\n")

        de = DataExporter()
        de.import_data(file_name, Paper, create=True, buffer_size=80,
                       num_workers=2, chunksize=5)

        p_t = Paper.objects.get(simple_title="simple_title_1")
        self.assertEqual(p_t.title, "title_1")
        self.assertEqual(p_t.text_body, "text_body_1")

        p_t = Paper.objects.get(simple_title="simple_title_2")
        self.assertEqual(p_t.title, "title_2")
        self.assertEqual(p_t.text_body, "text_body_2")
