import os

from django.core.management import call_command

from metadataserver.apps.ncem.datamodel.read.tagparser import (
    NCEMDataExtractor, extract_tags, FlatNCEMDataExtractor
    )
from metadataserver.apps.ncem.models import ScopeImage, Tagged
from metadataserver.apps.searchengine.configuration import USER_PROVIDED
from metadataserver.seminfer.tests import TestFileGeneric


class TestTagParser(TestFileGeneric):
    _tmp_folder = "/tmp/test_NCEM"

    def setUp(self):
        self._register_tmp_folder(self._tmp_folder)
        self._create_empty(self._tmp_folder, is_folder=True)

    def _create_test_file(self, file_name, content):
        file_route = os.path.join(self._tmp_folder, file_name)
        self._register_tmp_file(file_route)

        cvs_file = open(file_route, 'w')
        cvs_file.write(content)
        cvs_file.close()

        return file_route

    def test_extract_tags_simple(self):
        text = """


'TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser',STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis ,annular dark field,perovskite,,

'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis,annular dark field,perovskite,,

        """
        file_route = self._create_test_file("myFile.csv", text)

        extractor = FlatNCEMDataExtractor()

        extractor.extract_tags(file_route)

        data_dict = extractor.get_data_dic()

        self.assertEqual(set(data_dict["TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser"]),
                         {
                             "STEM", "crystal lattice", "atomic resolution",
                             "strontium titanate", "flat", "zone axis",
                             "annular dark field", "perovskite"
                         })
        self.assertEqual(set(data_dict["TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser"]),
                         {
                             "STEM", "crystal lattice", "atomic resolution",
                             "strontium titanate", "flat", "zone axis",
                             "annular dark field", "perovskite"
                         })

    def test_extract_tags(self):
        text = """
#idx,file_idx,sub_image_idx,filename,labels,,tags_manual,,,,,,,,,

0,1,11,'TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser',LABELS,2,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis ,annular dark field,perovskite,,

1,3,45,'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',LABELS,8,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis,annular dark field,perovskite,,

        """
        file_route = self._create_test_file("myFile.csv", text)

        extractor = NCEMDataExtractor()

        extractor.extract_tags(file_route)

        data_dict = extractor.get_data_dic()

        self.assertEqual(set(data_dict["TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser"]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))
        self.assertEqual(set(data_dict["TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser"]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))

    def test_extract_tags_route(self):
        text = """
#idx,file_idx,sub_image_idx,filename,labels,,tags_manual,,,,,,,,,

0,1,11,'TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser',LABELS,2,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis ,annular dark field,perovskite,,

1,3,45,'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',LABELS,8,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis,annular dark field,perovskite,,

        """
        file_route = self._create_test_file("myFile.csv", text)

        extractor = NCEMDataExtractor()

        extractor.extract_tags(file_route, base_route="TEAM_I/ZhangJ")

        data_dict = extractor.get_data_dic()

        self.assertEqual(set(data_dict["9-17-14_STO/1_256_5.1Mx_2.ser"]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))
        self.assertEqual(set(data_dict["9-17-14_STO/3_256_10Mx_1.ser"]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))

    def test_extract_tags_duplicated(self):
        text = """
#idx,file_idx,sub_image_idx,filename,labels,,tags_manual,,,,,,,,,

0,1,11,'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',LABELS,2,STEM,,flat,zone axis ,annular dark field,perovskite,,

1,3,45,'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',LABELS,8,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis,perovskite,,

        """
        file_route = self._create_test_file("myFile.csv", text)

        extractor = NCEMDataExtractor()

        extractor.extract_tags(file_route, base_route="TEAM_I/ZhangJ")

        data_dict = extractor.get_data_dic()

        self.assertEqual(set(data_dict["9-17-14_STO/3_256_10Mx_1.ser"]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))
        self.assertRaises(ValueError, extractor.extract_tags,
                          file_route, base_route="TEAM_I/ZhangJ",
                          support_duplicates=False)

    def test_get_file_list(self):
        text = """
#idx,file_idx,sub_image_idx,filename,labels,,tags_manual,,,,,,,,,

0,1,11,'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',LABELS,2,STEM,,flat,zone axis ,annular dark field,perovskite,,

1,3,45,'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_2.ser',LABELS,8,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis,perovskite,,

        """
        file_route = self._create_test_file("myFile.csv", text)

        extractor = NCEMDataExtractor()

        extractor.extract_tags(file_route, base_route="TEAM_I/ZhangJ")

        self.assertEqual(set(["9-17-14_STO/3_256_10Mx_1.ser",
                              "9-17-14_STO/3_256_10Mx_2.ser"]),
                         set(extractor.get_file_list()))

    def test_extract_tags_full_command(self):
        self.test_extract_tags_full(True)

    def test_extract_tags_full(self, as_command=False):
        text = """
#idx,file_idx,sub_image_idx,filename,labels,,tags_manual,,,,,,,,,

0,1,11,'TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser',LABELS,2,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis ,annular dark field,perovskite,,

1,3,45,'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',LABELS,8,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis,annular dark field,,

        """
        file_route = self._create_test_file("myFile.csv", text)

        # IT should not fail if the iamges are not there, it should just sli`themm
        extract_tags(file_route, list_only=False)

        si_1 = ScopeImage(file_name="fn.jpg",
                          original_route="TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser",
                          bitmap_url="http://image_server/f1.jpg")
        si_1.save()
        si_2 = ScopeImage(file_name="fn.jpg",
                          original_route="TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser",
                          bitmap_url="http://image_server/f1.jpg")
        si_2.save()
        if not as_command:
            extract_tags(file_route, list_only=False)
        else:
            args = [file_route]
            opts = {}

            call_command('tagparse', *args, **opts)

        self.assertEqual(set([str(x) for x in si_1.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))
        self.assertEqual(set([str(x) for x in si_2.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field"]))

        # Repeating should not fail
        if not as_command:
            extract_tags(file_route, list_only=False)
        else:
            args = [file_route]
            opts = {}

            call_command('tagparse', *args, **opts)
        self.assertEqual(set([str(x) for x in si_1.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))
        self.assertEqual(set([str(x) for x in si_2.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field"]))

    def test_extract_tags_full_type_command(self):
        self.test_extract_tags_full_type(True)

    def test_extract_tags_full_type(self, as_command=False):
        text = """
#idx,file_idx,sub_image_idx,filename,labels,,tags_manual,,,,,,,,,

0,1,11,'TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser',LABELS,2,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis ,annular dark field,perovskite,,

1,3,45,'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',LABELS,8,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis,annular dark field,,

        """
        file_route = self._create_test_file("myFile.csv", text)

        # IT should not fail if the iamges are not there, it should just sli`themm
        extract_tags(file_route, list_only=False)

        si_1 = ScopeImage(file_name="fn.jpg",
                          original_route="TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser",
                          bitmap_url="http://image_server/f1.jpg")
        si_1.save()
        si_2 = ScopeImage(file_name="fn.jpg",
                          original_route="TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser",
                          bitmap_url="http://image_server/f1.jpg")
        si_2.save()
        if not as_command:
            extract_tags(file_route, list_only=False, metadata_type="user")
        else:
            args = [file_route]
            opts = {"metadatatype": "user"}

            call_command('tagparse', *args, **opts)

        self.assertEqual(set([str(x) for x in si_1.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))
        self.assertEqual(set([str(x) for x in si_2.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field"]))

        self.assertEqual(Tagged.objects.get(scope_image=si_1,
                                            tag="STEM").metadata_type,
                         USER_PROVIDED)
        self.assertEqual(Tagged.objects.get(scope_image=si_2,
                                            tag="atomic resolution").metadata_type,
                         USER_PROVIDED)

        # Repeating should not fail
        if not as_command:
            extract_tags(file_route, list_only=False)
        else:
            args = [file_route]
            opts = {}

            call_command('tagparse', *args, **opts)
        self.assertEqual(set([str(x) for x in si_1.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))
        self.assertEqual(set([str(x) for x in si_2.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field"]))

    def test_extract_tags_full_simple_command(self):
        self.test_extract_tags_full_simple(True)

    def test_extract_tags_full_simple(self, as_command=False):
        text = """
#idx,file_idx,sub_image_idx,filename,labels,,tags_manual,,,,,,,,,

'TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser',STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis ,annular dark field,perovskite,,

'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis,annular dark field,,

        """
        file_route = self._create_test_file("myFile.csv", text)

        si_1 = ScopeImage(file_name="fn.jpg",
                          original_route="TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser",
                          bitmap_url="http://image_server/f1.jpg")
        si_1.save()
        si_2 = ScopeImage(file_name="fn.jpg",
                          original_route="TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser",
                          bitmap_url="http://image_server/f1.jpg")
        si_2.save()
        if not as_command:
            extract_tags(file_route, list_only=False, simple_format=True)
        else:
            args = [file_route, "--simple"]
            opts = {}

            call_command('tagparse', *args, **opts)
        self.assertEqual(set([str(x) for x in si_1.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))
        self.assertEqual(set([str(x) for x in si_2.get_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field"]))

    def test_extract_tags_full_suggested_command(self):
        self.test_extract_tags_full_suggested(True)

    def test_extract_tags_full_suggested(self, as_command=False):
        text = """
#idx,file_idx,sub_image_idx,filename,labels,,tags_manual,,,,,,,,,

0,1,11,'TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser',LABELS,2,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis ,annular dark field,perovskite,,

1,3,45,'TEAM_I/ZhangJ/9-17-14_STO/3_256_10Mx_1.ser',LABELS,8,STEM,crystal lattice,atomic resolution,strontium titanate,flat,zone axis,annular dark field,,

        """
        file_route = self._create_test_file("myFile.csv", text)

        # IT should not fail if the iamges are not there, it should just sli`themm
        if not as_command:
            extract_tags(file_route, list_only=False, suggested=True)
        else:
            args = [file_route, "--suggestedtags"]
            opts = {}

            call_command('tagparse', *args, **opts)

        si_1 = ScopeImage(file_name="fn.jpg",
                          original_route="TEAM_I/ZhangJ/9-17-14_STO/1_256_5.1Mx_2.ser",
                          bitmap_url="http://image_server/f1.jpg")
        si_1.save()

        self.assertFalse(si_1.get_suggested_tags())

        if not as_command:
            extract_tags(file_route, list_only=False, suggested=True)
        else:
            args = [file_route, "--suggestedtags"]
            opts = {}

            call_command('tagparse', *args, **opts)
        self.assertEqual(set([str(x) for x in si_1.get_suggested_tags()]),
                         set(["STEM", "crystal lattice", "atomic resolution",
                              "strontium titanate", "flat", "zone axis",
                              "annular dark field", "perovskite"]))
