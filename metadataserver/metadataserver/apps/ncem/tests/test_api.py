import json
import logging

from django.contrib.auth.models import User
from django.test import Client
from rest_framework import status
from rest_framework.test import APITestCase, APIClient, APITransactionTestCase

from metadataserver.apps.ncem.models import (
    Tag, ScopeImage, Tagged, TagUserAddition, TAG_SUGGESTED, TagModification,
    TAG_ACTION_VALIDATE, TAG_ACTION_INVALIDATE
    )
from metadataserver.apps.ncemreldata.models import Proposal

API_BASE = '/api/image-tag/'

url_login = '/api-token-auth/'
next_unreviewed_endpoint = API_BASE + 'images/next-unreviewed/'
next_unreviewed_random_endpoint = API_BASE + 'images/next-unreviewed-random/'
next_unreviewed_random_related_endpoint = (API_BASE +
                                           'images/next-unreviewed-random-related/')
next_unreviewed_related_endpoint = (API_BASE +
                                    'images/next-unreviewed-related/')
images_endpoint = API_BASE + 'images/'
single_image_endpoint = API_BASE + 'images/{}/'
tags_endpoint = API_BASE + 'images/{}/tags/'
suggested_tags_endpoint = API_BASE + 'images/{}/suggested-tags/'
single_tag_endpoint = API_BASE + 'images/{}/tags/{}/'
reviewed_endpoint = API_BASE + 'images/{}/reviewed/'
validate_tag_endpoint = API_BASE + 'images/{}/tags/{}/valid/'
invalidate_tag_endpoint = API_BASE + 'images/{}/tags/{}/invalid/'
validate_suggested_tag_endpoint = API_BASE + 'images/{}/suggested-tags/{}/valid/'
invalidate_suggested_tag_endpoint = (API_BASE +
                                     'images/{}/suggested-tags/{}/invalid/')
strobe_tags_endpoint = (API_BASE +
                        'images/{}/strobe/')
logger = logging.getLogger(__name__)


class AbstractGenericAPITest(object):

    def _do_login_token(self, is_admin=False, username='user1',
                        password='pass1'):
        if is_admin:
            user = User.objects.create_superuser(username, username + '@mail.com',
                                                 password)
            user.is_staff = is_admin
        else:
            user = User.objects.create_user(username, username + '@mail.com',
                                            password)
        user.save()
        response = self.client.post(url_login, data=
        {
            "username": username,
            "password": password
            })
        self.assertEqual(status.HTTP_200_OK,
                         response.status_code)
        self.assertTrue("token" in response.data.keys())
        self.client.credentials(HTTP_AUTHORIZATION='Token ' +
                                                   response.data["token"])
        return response.data["token"]

    def _configure_url(self, url, *args):
        return url.format(*args)

    def _get_session_id(self):
        return self.client.session.session_key


class GenericAPITestTransaction(APITransactionTestCase, AbstractGenericAPITest):
    def setUp(self):
        self._id_string = "idstring"
        self.client = APIClient()
        self._do_login_token()


class GenericAPITest(APITestCase, AbstractGenericAPITest):
    def setUp(self):
        self._id_string = "idstring"
        self.client = APIClient()
        self._do_login_token()


class TestNCEMAPI(GenericAPITest):

    def test_client_get_root_base_uri(self):
        with self.settings(DJANGO_BASE_URI='alt/alt2'):
            url = self._configure_url('/alt/alt2' + API_BASE)
            logger.info("U {}".format(url))
            response = self.client.get(url)
            self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_client_get_root(self):
        url = self._configure_url(API_BASE)
        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         {
                             "images":
                                 "http://testserver/api/image-tag/images/",
                             "folders":
                                 "http://testserver/api/image-tag/folders/",
                             })

    def test_session_id(self):
        url = self._configure_url(API_BASE)
        self.client.get(url)
        session_id_1 = self._get_session_id()
        self.assertEqual(session_id_1, self.client.session.session_key)
        self.assertTrue(session_id_1)
        self.client.get(url)
        session_id_2 = self._get_session_id()
        self.assertEqual(session_id_1, session_id_2)

        new_client = Client()
        new_client.get(url)
        self.assertNotEqual(self.client.session.session_key,
                            new_client.session.session_key)

    def test_strobe(self):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()

    def test_user_validates_invalidates_suggested_tag(self):

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()
        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s2.save()
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()

        tg = Tagged(scope_image=s2, tag=tag1, strength=TAG_SUGGESTED)
        tg.save()

        tg = Tagged(scope_image=si, tag=tag1, strength=TAG_SUGGESTED)
        tg.save()

        url_validate = self._configure_url(validate_suggested_tag_endpoint,
                                           si.id, "tag1")
        url_invalidate = self._configure_url(invalidate_suggested_tag_endpoint,
                                             si.id, "tag1")

        response = self.client.post(url_validate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id=self._get_session_id()).action,
                         TAG_ACTION_VALIDATE)

        response = self.client.post(url_invalidate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id=self._get_session_id()).action,
                         TAG_ACTION_INVALIDATE)
        response = self.client.post(url_validate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id=self._get_session_id()).action,
                         TAG_ACTION_VALIDATE)

    def test_user_validates_invalidates_tag(self):

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()
        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s2.save()
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()

        tg = Tagged(scope_image=s2, tag=tag1)
        tg.save()

        tg = Tagged(scope_image=si, tag=tag1)
        tg.save()

        url_validate = self._configure_url(validate_tag_endpoint, si.id,
                                           "tag1")
        url_invalidate = self._configure_url(invalidate_tag_endpoint, si.id,
                                             "tag1")

        response = self.client.post(url_validate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id=self._get_session_id()).action,
                         TAG_ACTION_VALIDATE)

        response = self.client.post(url_invalidate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id=self._get_session_id()).action,
                         TAG_ACTION_INVALIDATE)
        response = self.client.post(url_validate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id=self._get_session_id()).action,
                         TAG_ACTION_VALIDATE)

    def test_tag_valid(self):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()

        tg = Tagged(scope_image=si, tag=tag1)
        tg.save()

        url_validate = self._configure_url(validate_tag_endpoint, si.id,
                                           "tag1")
        response = self.client.post(url_validate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        modifications = TagModification.objects.filter(
            action=TAG_ACTION_VALIDATE,
            tagged=tg,
            session_id=self._get_session_id())
        self.assertEqual(len(modifications), 1)
        self.assertEqual(modifications[0].action, TAG_ACTION_VALIDATE)

        url_validate = self._configure_url(validate_tag_endpoint, si.id,
                                           "tag1")

        url_validate = self._configure_url(validate_tag_endpoint, si.id,
                                           "tag3")
        response = self.client.post(url_validate)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_tag_invalid(self):
        """ Regular cases """
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()
        tag1 = Tag(tag="tag1")
        tag1.save()

        tg = Tagged(scope_image=si, tag=tag1)
        tg.save()

        tag2 = Tag(tag="tag2")
        tag2.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()

        url_validate = self._configure_url(validate_tag_endpoint, si.id,
                                           "tag1")
        response = self.client.post(url_validate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        url_invalidate = self._configure_url(invalidate_tag_endpoint, si.id,
                                             "tag2")
        response = self.client.post(url_invalidate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        modifications = TagModification.objects.filter(
            action=TAG_ACTION_VALIDATE,
            tagged=tg,
            session_id=self._get_session_id())
        self.assertEqual(len(modifications), 1)

        modifications = TagModification.objects.filter(
            action=TAG_ACTION_INVALIDATE,
            tagged=tg2,
            session_id=self._get_session_id())
        self.assertEqual(len(modifications), 1)

        url_validate = self._configure_url(validate_tag_endpoint, si.id,
                                           "tag1")
        response = self.client.post(url_validate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        url_invalidate = self._configure_url(invalidate_tag_endpoint, si.id,
                                             "tag2")
        response = self.client.post(url_invalidate)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_mark_reviewed(self):
        s2 = ScopeImage(file_name="fn2.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f2.jpg",
                        file_metadata="Metadata2")
        s2.save()
        url = self._configure_url(reviewed_endpoint, s2.id)
        self.assertFalse(s2.reviewed)
        self.client.post(url)
        s2_after = ScopeImage.objects.get(id=s2.id)
        self.assertTrue(s2_after.reviewed)

    def test_add_user_tag(self):
        s2 = ScopeImage(file_name="fn2.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f2.jpg",
                        file_metadata="Metadata2")
        s2.save()
        url = self._configure_url(single_tag_endpoint, s2.id, "newtag")
        self.client.put(url)
        ua = TagUserAddition.objects.get(tag=Tag.objects.get(tag="newtag"),
                                         scope_image=s2,
                                         session_id=self._get_session_id())
        self.assertEqual(ua.tag.tag, "newtag")
        self.assertEqual(ua.scope_image.id, s2.id)
        self.assertEqual(ua.session_id, self._get_session_id())
        self.assertTrue(Tag.objects.get(tag="newtag"))
        self.client.put(url)

    def test_get_tags(self):
        s2 = ScopeImage(file_name="fn2.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f2.jpg",
                        file_metadata="Metadata2")
        s2.save()
        s1 = ScopeImage(file_name="fn1.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata="Metadata1")
        s1.save()
        tag1 = Tag(tag="tag1")
        tag1.save()

        tg = Tagged(scope_image=s1, tag=tag1)
        tg.save()

        tag2 = Tag(tag="tag2")
        tag2.save()
        tg2 = Tagged(scope_image=s1, tag=tag2)
        tg2.save()

        tag3 = Tag(tag="tag3")
        tag3.save()
        tg3 = Tagged(scope_image=s1, tag=tag3, strength=TAG_SUGGESTED)
        tg3.save()

        url = self._configure_url(tags_endpoint, s1.id)
        response = self.client.get(url)
        candidate_data = json.loads(
            """[{"tag":"tag1"},{"tag":"tag2"}]
            """)
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

        url = self._configure_url(suggested_tags_endpoint, s1.id)

        response = self.client.get(url)
        candidate_data = json.loads(
            """[{"tag":"tag3"}]""")
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

        url = self._configure_url(suggested_tags_endpoint, s2.id)
        response = self.client.get(url)
        candidate_data = json.loads("""[]""")
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

    def test_images(self):
        url = self._configure_url(images_endpoint)
        s2 = ScopeImage(file_name="fn2.jpg",
                        dimensions=2,
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f2.jpg",
                        file_metadata="Metadata2")
        s2.save()

        s1 = ScopeImage(file_name="fn1.jpg",
                        dimensions=2,
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata="Metadata1")
        s1.save()
        tag1 = Tag(tag="tag1")
        tag1.save()

        tg = Tagged(scope_image=s1, tag=tag1)
        tg.save()

        tag2 = Tag(tag="tag2")
        tag2.save()
        tg2 = Tagged(scope_image=s1, tag=tag2)
        tg2.save()

        tag3 = Tag(tag="tag3")
        tag3.save()
        tg3 = Tagged(scope_image=s1, tag=tag3, strength=TAG_SUGGESTED)
        tg3.save()

        response = self.client.get(url)

        candidate_data = json.loads(
            """[{"id":""" + str(s1.id) + """,
            "dimensions": 2,
            "file_metadata":"Metadata1",
            "file_name":"fn1.jpg",
            "file_path":"/dir1/dir2",
            "original_route":"/dir1/dir2/fn1.jog",
            "data_url":"/dir1/dir2/fn1.jog",
            "bitmap_url":"http://image_server/f1.jpg",
            "reviewed": false,
            "tags":["tag1", "tag2"],
            "suggested_tags":["tag3"],
            "proposal":null},
            {"id":""" + str(s2.id) + """,
            "dimensions": 2,
            "file_metadata":"Metadata2",
            "file_name":"fn2.jpg",
            "file_path":"/dir1/dir2",
            "original_route":"/dir1/dir2/fn2.jog",
            "data_url":"/dir1/dir2/fn2.jog",
            "bitmap_url":"http://image_server/f2.jpg",
            "reviewed": false,
            "tags":[],
            "suggested_tags":[],
            "proposal":null}]
            """)
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

    def test_single_image(self):
        s2 = ScopeImage(file_name="fn2.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f2.jpg",
                        file_metadata="Metadata2")
        s2.save()

        s1 = ScopeImage(file_name="fn1.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata={'field1': 'field2'})
        s1.save()
        tag1 = Tag(tag="tag1")
        tag1.save()

        tg = Tagged(scope_image=s1, tag=tag1)
        tg.save()

        tag2 = Tag(tag="tag2")
        tag2.save()
        tg2 = Tagged(scope_image=s1, tag=tag2)
        tg2.save()

        tag3 = Tag(tag="tag3")
        tag3.save()
        tg3 = Tagged(scope_image=s1, tag=tag3, strength=TAG_SUGGESTED)
        tg3.save()

        url = self._configure_url(single_image_endpoint, s1.id)
        response = self.client.get(url)
        candidate_data = json.loads(
            """{"id":""" + str(s1.id) + """,
            "dimensions": 0,
            "file_metadata":{"field1": "field2"},
            "file_name":"fn1.jpg",
            "file_path":"/dir1/dir2",
            "original_route":"/dir1/dir2/fn1.jog",
            "data_url":"/dir1/dir2/fn1.jog",
            "bitmap_url":"http://image_server/f1.jpg",
            "reviewed": false,
            "tags":["tag1", "tag2"],
            "suggested_tags":["tag3"],
            "proposal":null}
            """)
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

    def test_next_unreviewed(self):
        url = self._configure_url(next_unreviewed_endpoint)

        s2 = ScopeImage(file_name="fn2.jpg",
                        dimensions=2,
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f2.jpg",
                        file_metadata="Metadata2")
        s2.save()

        s1 = ScopeImage(file_name="fn1.jpg",
                        dimensions=2,
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata="Metadata1")
        s1.save()

        snext = ScopeImage.get_next_unreviewed().first()
        self.assertEqual(s1.id, snext.id)
        response = self.client.get(url)
        candidate_data = json.loads(
            """{"id":""" + str(s1.id) + """,
            "dimensions": 2,
            "file_metadata":"Metadata1",
            "file_name":"fn1.jpg",
            "file_path":"/dir1/dir2",
            "original_route":"/dir1/dir2/fn1.jog",
            "data_url":"/dir1/dir2/fn1.jog",
            "bitmap_url":"http://image_server/f1.jpg",
            "reviewed": false,
            "tags":[],
            "suggested_tags":[],
            "proposal":null}
            """)
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

        snext.mark_reviewed()
        snext.save()
        snext = ScopeImage.get_next_unreviewed().first()
        self.assertEqual(s2.id, snext.id)

        response = self.client.get(url)
        candidate_data = json.loads(
            """{"id":""" + str(s2.id) + """,
            "dimensions": 2,
            "file_metadata":"Metadata2",
            "file_name":"fn2.jpg",
            "file_path":"/dir1/dir2",
            "original_route":"/dir1/dir2/fn2.jog",
            "data_url":"/dir1/dir2/fn2.jog",
            "bitmap_url":"http://image_server/f2.jpg",
            "reviewed": false,
            "tags":[],
            "suggested_tags":[],
            "proposal":null}
            """)
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

        snext.mark_reviewed()
        snext.save()
        snext = ScopeImage.get_next_unreviewed().first()
        self.assertFalse(snext)

        response = self.client.get(url)
        candidate_data = json.loads(
            """[]
            """)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        candidate_data = json.loads("""{"detail": "Not found."}""")
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

    def test_next_unreviewed_random(self):
        url = self._configure_url(next_unreviewed_random_endpoint)

        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s2.save()

        s1 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s1.save()

        s3 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir3/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s3.save()

        response = self.client.get(url)
        snext = json.loads(response.content.decode('utf-8'))

        self.assertIn(snext["id"], [s1.id, s2.id, s3.id])

        id_list = []
        for i in range(10):
            id_list.append(json.loads(
                self.client.get(url).content.decode('utf-8'))['id'])
        self.assertTrue(len(set(id_list)) > 1)

        s1.mark_reviewed()
        response = self.client.get(url)
        snext = json.loads(response.content.decode('utf-8'))
        self.assertIn(snext["id"], [s2.id, s3.id])

        s2.mark_reviewed()
        response = self.client.get(url)
        snext = json.loads(response.content.decode('utf-8'))
        self.assertIn(snext["id"], [s3.id])

        s3.mark_reviewed()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        candidate_data = json.loads("""{"detail": "Not found."}""")
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

    def test_next_router_query(self):
        url = self._configure_url(next_unreviewed_random_endpoint)

        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s2.save()

        s1 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s1.save()

        s3 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir2/dir3/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s3.save()

        response = self.client.get(url, {'route': '/dir1'})
        snext = json.loads(response.content.decode('utf-8'))

        self.assertIn(snext["id"], [s1.id, s2.id])

        id_list = []
        for i in range(10):
            id_list.append(json.loads(
                self.client.get(url).content.decode('utf-8'))['id'])
        self.assertTrue(len(set(id_list)) > 1)

        s1.mark_reviewed()
        response = self.client.get(url, {'route': '/dir1'})
        snext = json.loads(response.content.decode('utf-8'))
        self.assertIn(snext["id"], [s2.id])

        s2.mark_reviewed()
        candidate_data = json.loads("""{"detail": "Not found."}""")
        response = self.client.get(url, {'route': '/dir1'})
        snext = json.loads(response.content.decode('utf-8'))
        self.assertEqual(snext, candidate_data)

        s1.mark_reviewed()
        response = self.client.get(url, {'route': '/dir2'})
        snext = json.loads(response.content.decode('utf-8'))
        self.assertIn(snext["id"], [s3.id])
        s3.mark_reviewed()

        response = self.client.get(url, {'route': '/dir2'})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        candidate_data = json.loads("""{"detail": "Not found."}""")
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

    def test_next_unreviewed_random_related(self):
        url = self._configure_url(next_unreviewed_random_related_endpoint)
        p1 = Proposal(serial_number="1234")
        p1.save()
        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        proposal=p1)
        s2.save()

        s1 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s1.save()

        s3 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir3/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        proposal=p1)
        s3.save()

        response = self.client.get(url)
        snext = json.loads(response.content.decode('utf-8'))

        self.assertIn(snext["id"], [s2.id, s3.id])

        id_list = []
        for i in range(20):
            id_list.append(json.loads(
                self.client.get(url).content.decode('utf-8'))['id'])
        self.assertTrue(len(set(id_list)) == 2)

        s2.mark_reviewed()
        response = self.client.get(url)
        snext = json.loads(response.content.decode('utf-8'))
        self.assertIn(snext["id"], [s3.id])

        s3.mark_reviewed()
        response = self.client.get(url)
        snext = json.loads(response.content.decode('utf-8'))
        self.assertIn(snext["id"], [s1.id])

        s1.mark_reviewed()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        candidate_data = json.loads("""{"detail": "Not found."}""")
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

    def test_next_unreviewed_related(self):
        url = self._configure_url(next_unreviewed_related_endpoint)
        p1 = Proposal(serial_number="1234")
        p1.save()
        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        proposal=p1)
        s2.save()

        s1 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s1.save()

        s3 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir3/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        proposal=p1)
        s3.save()

        response = self.client.get(url)
        snext = json.loads(response.content.decode('utf-8'))

        self.assertIn(snext["id"], [s2.id])

        id_list = []
        for i in range(20):
            id_list.append(json.loads(
                self.client.get(url).content.decode('utf-8'))['id'])
        self.assertTrue(len(set(id_list)) == 1)

        s2.mark_reviewed()
        response = self.client.get(url)
        snext = json.loads(response.content.decode('utf-8'))
        self.assertIn(snext["id"], [s3.id])

        s3.mark_reviewed()
        response = self.client.get(url)
        snext = json.loads(response.content.decode('utf-8'))
        self.assertIn(snext["id"], [s1.id])

        s1.mark_reviewed()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        candidate_data = json.loads("""{"detail": "Not found."}""")
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         candidate_data)

    def test_strobe_tags(self):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()

        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()
        tag3 = Tag(tag="tag3")
        tag3.save()
        tag4 = Tag(tag="tag4")
        tag4.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3, strength=TAG_SUGGESTED)
        tg3.save()
        tg4 = Tagged(scope_image=si, tag=tag4, strength=TAG_SUGGESTED)
        tg4.save()

        url = self._configure_url(strobe_tags_endpoint, si.id)
        json_data = json.dumps({"tags": ["tag5", "tag6", "tag1", "tag3"]})
        self.assertFalse(si.reviewed)

        response = self.client.post(url, json_data,
                                    content_type="application/json")
        self.assertEqual(status.HTTP_200_OK,
                         response.status_code)

        si_after = ScopeImage.objects.get(id=si.id)
        self.assertTrue(si_after.reviewed)

        session_id = self._get_session_id()
        self.assertEqual(set([str(x.tag)
                              for x in TagUserAddition.objects.filter(scope_image=si,
                                                                      session_id=session_id)]),
                         {"tag5", "tag6"})

        self.assertEqual(TagModification.objects.get(tagged=tg1,
                                                     session_id=session_id).action,
                         TAG_ACTION_VALIDATE)
        self.assertEqual(TagModification.objects.get(tagged=tg2,
                                                     session_id=session_id).action,
                         TAG_ACTION_INVALIDATE)
        self.assertEqual(TagModification.objects.get(tagged=tg3,
                                                     session_id=session_id).action,
                         TAG_ACTION_VALIDATE)
        self.assertEqual(TagModification.objects.get(tagged=tg4,
                                                     session_id=session_id).action,
                         TAG_ACTION_INVALIDATE)

        json_data = json.dumps({"tags2": ["tag5", "tag6", "tag1", "tag3"]})
        response = self.client.post(url, json_data,
                                    content_type="application/json")
        self.assertEqual(status.HTTP_500_INTERNAL_SERVER_ERROR,
                         response.status_code)

        json_data = json.dumps(["tag5", "tag6", "tag1", "tag3"])
        response = self.client.post(url, json_data,
                                    content_type="application/json")
        self.assertEqual(status.HTTP_500_INTERNAL_SERVER_ERROR,
                         response.status_code)

        json_data = {"a": "HI!"}
        response = self.client.post(url, json_data)
        self.assertEqual(status.HTTP_500_INTERNAL_SERVER_ERROR,
                         response.status_code)
