import datetime
import inspect
import os
import shutil
import logging

from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone

from metadataserver.apps.ncem.datamodel.read.crawler import (
    process_single_file, crawl, DateStatsMaker, do_on_all_files
    )
from metadataserver.apps.ncem.datamodel.read.filereaders import SerFile, Dm3File
from metadataserver.apps.ncem.models import ScopeImage
from metadataserver.seminfer.tests import TestFileGeneric

# from docutils.parsers.rst.directives import get_measure


_curren_file = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))
_images_folder = "{}/image_files".format(_curren_file)
logger = logging.getLogger(__name__)


def _get_image_route(file_name):
    return os.path.join(_images_folder, file_name)


def get_measure(start=None, operation=None):
    """Auxiliary function to measure time spans.
    
    Args:
        start: if set to a datetime object, function returns a timespan
          object measurring the difference between start and now().
        operation: if set to a string, (and start is a datetime object),
          the function prints a message indicating the process PID, the
          string and the time_sona value.
    
    Returns: a datetime object (now()) or a timespan object.    
    """
    if start is None:
        return timezone.now()
    else:
        end = timezone.now()
        delta = end - start
        if operation:
            logger.info("PID({})-Operation ({}): {}".format(os.getpid(), operation,
                                                      delta))
        return delta


class TestNCEMCrawler(TestCase):

    def test_ncem_library(self):
        pass


class TestCrawler(TestFileGeneric):

    def test_infinity_json_bug(self):
        """dm2 file parsing fails when data is going to be inserted in the DB.
        Message: DETAIL:  Token "-Infinity" is invalid.
        Example file triggering this problem: FocalSeriesImages_tip3.dm4
        """
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        s_id = process_single_file(
            _get_image_route("FocalSeriesImages_tip3.dm4"),
            _get_image_route("tmp/test_crawl/dest"),
            img_format="png",
            base_folder=_get_image_route(""),
            instrument="TEAM_I")
        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/"
                "FocalSeriesImages_tip3.dm4.png")))

    def test_crawl_command(self):
        self.test_crawl(as_command=True)

    def test_crawl(self, as_command=False):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl/"))

        self._create_empty(_get_image_route("tmp/test_crawl/source/d1"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/source/d2"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/source/d2/d3"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/bitmaps"),
                           is_folder=True)
        """ intended fileroutes
        d1/file1.ser
        d2/file2.ser
        d2/d3/file3.ser
        d2/d3/file4.dm3
        """
        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"),
            _get_image_route("tmp/test_crawl/source/d1/"
                             "file1.ser"))
        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"),
            _get_image_route("tmp/test_crawl/source/d2/"
                             "file2.ser"))
        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"),
            _get_image_route("tmp/test_crawl/source/d2/d3/"
                             "file3.ser"))
        shutil.copy(_get_image_route(
            "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
            _get_image_route("tmp/test_crawl/source/d2/d3/"
                             "file4.dm3"))

        base_url = "http://myserver.com/imgs/"

        source_folder = _get_image_route("tmp/test_crawl/source/")
        imgs_target_folder = _get_image_route("tmp/test_crawl/bitmaps/")
        base_folder = _get_image_route("tmp/test_crawl/source/")
        url_prefix = base_url

        if not as_command:
            id_list = crawl(source_folder, imgs_target_folder,
                            base_folder=base_folder,
                            url_prefix=url_prefix,
                            instrument="TEAM_I")
            self.assertEqual(len(id_list), 4)
        else:
            args = [source_folder, imgs_target_folder,
                    "--basefolder={}".format(base_folder),
                    "--prefixurl={}".format(url_prefix),
                    "--instrument={}".format("TEAM_I"),
                    "--recursive"]
            opts = {}

            call_command('crawl', *args, **opts)

        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d1/file1.ser.png")))
        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d2/file2.ser.png")))
        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d2/d3/file3.ser.png")))
        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d2/d3/file4.dm3.png")))

        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d1/file1.ser.tb.png")))
        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d2/file2.ser.tb.png")))
        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d2/d3/file3.ser.tb.png")))
        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d2/d3/file4.dm3.tb.png")))

        urls = [x.bitmap_url for x in ScopeImage.objects.all()]
        original_routes = [x.original_route for x in ScopeImage.objects.all()]

        self.assertEqual(set(urls), {
            "http://myserver.com/imgs/d1/file1.ser.png",
            "http://myserver.com/imgs/d2/file2.ser.png",
            "http://myserver.com/imgs/d2/d3/file3.ser.png",
            "http://myserver.com/imgs/d2/d3/file4.dm3.png",
            })

        self.assertEqual(set(original_routes), {
            "d1/file1.ser",
            "d2/file2.ser",
            "d2/d3/file3.ser",
            "d2/d3/file4.dm3"
            })

        si = ScopeImage.objects.get(original_route="d1/file1.ser")

        metadata = si.file_metadata
        self.assertEquals(metadata["header"]["ByteOrder"], 18761)
        self.assertEquals(metadata["metadata"]["DataType"], 2)
        self.assertEqual(si.instrument, "TEAM_I")

    def test_crawl_no_cache_command(self):
        self.test_crawl_no_cache(as_command=True)

    def test_crawl_no_cache(self, as_command=False):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl/"))

        self._create_empty(_get_image_route("tmp/test_crawl/source/d1"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/source/d2"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/source/d2/d3"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/bitmaps"),
                           is_folder=True)
        """ intended fileroutes
        d1/file1.ser
        d2/d3/file4.dm3
        """
        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"),
            _get_image_route("tmp/test_crawl/source/d1/"
                             "file1.ser"))

        shutil.copy(_get_image_route(
            "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
            _get_image_route("tmp/test_crawl/source/d2/d3/"
                             "file4.dm3"))

        base_url = "http://myserver.com/imgs/"

        source_folder = _get_image_route("tmp/test_crawl/source/")
        imgs_target_folder = _get_image_route("tmp/test_crawl/bitmaps/")
        base_folder = _get_image_route("tmp/test_crawl/source/")
        url_prefix = base_url

        m1_start = get_measure()
        if not as_command:
            id_list = crawl(source_folder, imgs_target_folder,
                            base_folder=base_folder,
                            url_prefix=url_prefix,
                            instrument="TEAM_I",
                            no_image_caching=False)
            self.assertEqual(len(id_list), 2)
        else:
            args = [source_folder, imgs_target_folder,
                    "--basefolder={}".format(base_folder),
                    "--prefixurl={}".format(url_prefix),
                    "--instrument={}".format("TEAM_I"),
                    "--recursive"]
            opts = {}

            call_command('crawl', *args, **opts)
        m1_delta = get_measure(m1_start, "Crawl WITH image caching")
        ScopeImage.objects.all().delete()

        m2_start = get_measure()
        if not as_command:
            id_list = crawl(source_folder, imgs_target_folder,
                            base_folder=base_folder,
                            url_prefix=url_prefix,
                            instrument="TEAM_I",
                            no_image_caching=True)
            self.assertEqual(len(id_list), 2)
        else:
            args = [source_folder, imgs_target_folder,
                    "--basefolder={}".format(base_folder),
                    "--prefixurl={}".format(url_prefix),
                    "--instrument={}".format("TEAM_I"),
                    "--recursive",
                    "--noimagecaching"]
            opts = {}

            call_command('crawl', *args, **opts)

        m2_delta = get_measure(m2_start, "Crawl WITHOUT image caching")

        # current ncempy code incorporates performance improvements
        # which makes this test no longer required
        #self.assertGreater(m2_delta, m1_delta)
        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d1/file1.ser.png")))

        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d2/d3/file4.dm3.png")))

        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d1/file1.ser.tb.png")))
        self.assertTrue(os.path.isfile(_get_image_route(
            "tmp/test_crawl/bitmaps/"
            "d2/d3/file4.dm3.tb.png")))

        urls = [x.bitmap_url for x in ScopeImage.objects.all()]
        original_routes = [x.original_route for x in ScopeImage.objects.all()]

        self.assertEqual(set(urls), {
            "http://myserver.com/imgs/d1/file1.ser.png",
            "http://myserver.com/imgs/d2/d3/file4.dm3.png"
            })

        self.assertEqual(set(original_routes), {
            "d1/file1.ser",
            "d2/d3/file4.dm3",
            })

        si = ScopeImage.objects.get(original_route="d1/file1.ser")

        metadata = si.file_metadata
        self.assertEquals(metadata["header"]["ByteOrder"], 18761)
        self.assertEquals(metadata["metadata"]["DataType"], 2)
        self.assertEqual(si.instrument, "TEAM_I")

    def test_process_single_file(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview_1.ser"))
        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview.emi"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview.emi"))

        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "17mrad_conv-reference_image-overview_1.ser"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "17mrad_conv-reference_image-overview_1.ser.png")))

        si = ScopeImage.objects.get(id=s_id)

        self.assertEqual(si.file_name,
                         "17mrad_conv-reference_image-overview_1.ser")
        self.assertEqual(si.original_route,
                         "source/17mrad_conv-reference_image-overview_1.ser")
        self.assertEqual(si.bitmap_url,
                         "source/17mrad_conv-reference_image-overview_1.ser.png")
        self.assertEqual(si.instrument, "TEAM_I")

        self.assertEqual(si.acquire_date,
                         datetime.datetime(2015, 5, 4, 15, 28, 32,
                                           tzinfo=datetime.timezone.utc))

        metadata = si.file_metadata
        self.assertEquals(metadata["header"]["ByteOrder"], 18761)
        self.assertEquals(metadata["metadata"]["DataType"], 2)
        self.assertEquals(metadata["emi"]["Emission [uA]"], 267.0)

        self.assertEqual(si.dimensions, 2)

    def test_process_single_file_acquire_date_bug(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "chiral4_Hour_00_Minute_00_Second_31_Frame_0178.dm4"),
            _get_image_route("tmp/test_crawl/source/"
                             "chiral4_Hour_00_Minute_00_Second_31_Frame_0178.dm4"))

        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "chiral4_Hour_00_Minute_00_Second_31_Frame_0178.dm4"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "chiral4_Hour_00_Minute_00_Second_31_Frame_0178.dm4.png")))

        si = ScopeImage.objects.get(id=s_id)

        self.assertEqual(si.file_name,
                         "chiral4_Hour_00_Minute_00_Second_31_Frame_0178.dm4")
        self.assertEqual(si.original_route,
                         "source/chiral4_Hour_00_Minute_00_Second_31_Frame_0178.dm4")
        self.assertEqual(si.bitmap_url,
                         "source/chiral4_Hour_00_Minute_00_Second_31_Frame_0178.dm4.png")
        self.assertEqual(si.instrument, "TEAM_I")

        self.assertEqual(si.acquire_date,
                         datetime.datetime(2016, 9, 1, 0, 59, 28,
                                           tzinfo=datetime.timezone.utc))

    def test_hyperspy_bug(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "11_Si_contaminated.emi"),
            _get_image_route("tmp/test_crawl/source/"
                             "11_Si_contaminated.emi"))
        shutil.copy(_get_image_route(
            "11_Si_contaminated_1.ser"),
            _get_image_route("tmp/test_crawl/source/"
                             "11_Si_contaminated_1.ser"))

        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "11_Si_contaminated_1.ser"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")

        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "11_Si_contaminated_1.ser.png")))

        si = ScopeImage.objects.get(id=s_id)

        self.assertEqual(si.file_name,
                         "11_Si_contaminated_1.ser")

    def test_process_single_file_acquire_date_synthetic(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "chiral4_mean.dm4"),
            _get_image_route("tmp/test_crawl/source/"
                             "chiral4_mean.dm4"))

        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "chiral4_mean.dm4"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "chiral4_mean.dm4.png")))

        si = ScopeImage.objects.get(id=s_id)

        self.assertEqual(si.file_name,
                         "chiral4_mean.dm4")
        self.assertEqual(si.original_route,
                         "source/chiral4_mean.dm4")
        self.assertEqual(si.bitmap_url,
                         "source/chiral4_mean.dm4.png")
        self.assertEqual(si.instrument, "TEAM_I")

        self.assertEqual(si.acquire_date,
                         None)

    def test_process_single_file_dimension_bug_v2(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "Diffraction SI.dm4"),
            _get_image_route("tmp/test_crawl/source/"
                             "Diffraction SI.dm4"))

        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "Diffraction SI.dm4"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "Diffraction SI.dm4.png")))

        si = ScopeImage.objects.get(id=s_id)

        self.assertEqual(si.file_name,
                         "Diffraction SI.dm4")

    def test_process_single_file_dimension_bug(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "STEM_EELS1_1.ser"),
            _get_image_route("tmp/test_crawl/source/"
                             "STEM_EELS1_1.ser"))
        shutil.copy(_get_image_route(
            "STEM_EELS1.emi"),
            _get_image_route("tmp/test_crawl/source/"
                             "STEM_EELS1.emi"))

        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "STEM_EELS1_1.ser"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "STEM_EELS1_1.ser.png")))

        si = ScopeImage.objects.get(id=s_id)

        self.assertEqual(si.file_name,
                         "STEM_EELS1_1.ser")
        self.assertEqual(si.original_route,
                         "source/STEM_EELS1_1.ser")
        self.assertEqual(si.bitmap_url,
                         "source/STEM_EELS1_1.ser.png")
        self.assertEqual(si.instrument, "TEAM_I")
        self.assertEqual(si.dimensions, 1)

    def test_process_single_file_one_dimension_dm3(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "Particle1-EELS_box3.dm3"),
            _get_image_route("tmp/test_crawl/source/"
                             "Particle1-EELS_box3.dm3"))

        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "Particle1-EELS_box3.dm3"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "Particle1-EELS_box3.dm3.png")))

        si = ScopeImage.objects.get(id=s_id)

        self.assertEqual(si.file_name,
                         "Particle1-EELS_box3.dm3")
        self.assertEqual(si.original_route,
                         "source/Particle1-EELS_box3.dm3")
        self.assertEqual(si.bitmap_url,
                         "source/Particle1-EELS_box3.dm3.png")
        self.assertEqual(si.instrument, "TEAM_I")
        self.assertEqual(si.dimensions, 1)

    def test_process_single_file_dm3(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
            _get_image_route("tmp/test_crawl/source/"
                             "03_-4um_FocalSeriesImages_Def_-5nm.dm3"))
        shutil.copy(_get_image_route(
            "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
            _get_image_route("tmp/test_crawl/source/"
                             "03_-4um_FocalSeriesImages_Def_-5nm.dm3"))

        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "03_-4um_FocalSeriesImages_Def_-5nm.dm3.png")))

        si = ScopeImage.objects.get(id=s_id)
        self.assertEqual(si.instrument,
                         "TEAM_I")

        self.assertEqual(si.file_name,
                         "03_-4um_FocalSeriesImages_Def_-5nm.dm3")
        self.assertEqual(si.original_route,
                         "source/03_-4um_FocalSeriesImages_Def_-5nm.dm3")
        self.assertEqual(si.bitmap_url,
                         "source/03_-4um_FocalSeriesImages_Def_-5nm.dm3.png")

        self.assertEqual(si.acquire_date,
                         datetime.datetime(2013, 8, 12, 15, 20, 21,
                                           tzinfo=datetime.timezone.utc))

    def test_process_single_file_duplicates(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview_1.ser"))
        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview.emi"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview.emi"))
        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "17mrad_conv-reference_image-overview_1.ser"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "17mrad_conv-reference_image-overview_1.ser"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        self.assertIsNone(s_id)
        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "17mrad_conv-reference_image-overview_1.ser"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   allow_duplicates=True,
                                   instrument="TEAM_I")
        self.assertIsNotNone(s_id)

    def test_process_single_file_image_contrast(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "11_FocalSeriesImages_Def_-1nm.dm3"),
            _get_image_route("tmp/test_crawl/source/"
                             "11_FocalSeriesImages_Def_-1nm.dm3"))

        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "11_FocalSeriesImages_Def_-1nm.dm3"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I")
        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "11_FocalSeriesImages_Def_-1nm.dm3.png")))

    def test_process_single_file_image_no_cache(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "11_FocalSeriesImages_Def_-1nm.dm3"),
            _get_image_route("tmp/test_crawl/source/"
                             "11_FocalSeriesImages_Def_-1nm.dm3"))

        m1_start = get_measure()
        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "11_FocalSeriesImages_Def_-1nm.dm3"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I",
                                   no_image_caching=True)
        m1_delta = get_measure(m1_start, "Crawl with No image cache")

        si = ScopeImage.objects.get(id=s_id).delete()

        m2_start = get_measure()
        s_id = process_single_file(_get_image_route("tmp/test_crawl/source/"
                                                    "11_FocalSeriesImages_Def_-1nm.dm3"),
                                   _get_image_route("tmp/test_crawl/dest"),
                                   img_format="png",
                                   base_folder=_get_image_route("tmp/test_crawl"),
                                   instrument="TEAM_I",
                                   no_image_caching=False)
        m2_delta = get_measure(m2_start, "Crawl with image cache")

        self.assertGreater(m1_delta, m2_delta)

        self.assertTrue(os.path.isfile(
            _get_image_route(
                "tmp/test_crawl/dest/source/"
                "11_FocalSeriesImages_Def_-1nm.dm3.png")))

    def test_date_crawl(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))

        self._create_empty(_get_image_route("tmp/test_crawl/source"),
                           is_folder=True)
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)

        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview_1.ser"))
        shutil.copy(_get_image_route(
            "17mrad_conv-reference_image-overview.emi"),
            _get_image_route("tmp/test_crawl/source/"
                             "17mrad_conv-reference_image-overview.emi"))

        shutil.copy(_get_image_route(
            "03_-4um_FocalSeriesImages_Def_-5nm.dm3"),
            _get_image_route("tmp/test_crawl/source/"
                             "03_-4um_FocalSeriesImages_Def_-5nm.dm3"))

        shutil.copy(_get_image_route(
            "chiral4_Hour_00_Minute_00_Second_31_Frame_0178.dm4"),
            _get_image_route("tmp/test_crawl/source/"
                             "chiral4_Hour_00_Minute_00_Second_31_Frame_0178.dm4"))

        shutil.copy(_get_image_route(
            "chiral4_mean.dm4"),
            _get_image_route("tmp/test_crawl/source/"
                             "chiral4_mean.dm4"))
        date_stats = DateStatsMaker()
        work_function = date_stats.acquire_date_stats

        source_folder = _get_image_route("tmp/test_crawl/source/")
        imgs_target_folder = _get_image_route("tmp/test_crawl/bitmaps/")
        base_folder = _get_image_route("tmp/test_crawl/source/")
        url_prefix = "http://loscalhost"
        extensions = ["ser", "dm3", "dm4"]
        img_format = "png"
        recursive = True
        allow_duplicates = False,
        instrument = "REAM_I"

        result = do_on_all_files(source_folder,
                                 extensions,
                                 work_function,
                                 recursive,
                                 imgs_target_folder,
                                 base_folder=base_folder,
                                 img_format=img_format,
                                 url_prefix=url_prefix,
                                 allow_duplicates=allow_duplicates,
                                 instrument=instrument)
        self.assertEqual(date_stats._num_acquired_dates, 3)
        self.assertEqual(date_stats._num_processed_images, 4)


class TestSERFile(TestFileGeneric):

    def test_discover_emi(self):
        ser_file_route = _get_image_route(
            "17mrad_conv-reference_image-overview_1.ser")
        emi_file_route = SerFile._discover_emi(ser_file_route)
        self.assertEqual(emi_file_route, _get_image_route(
            "17mrad_conv-reference_image-overview.emi"))

    def test_constructor(self):
        ser = SerFile(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"))
        metadata = ser.get_metadata()
        self.assertEquals(metadata["header"]["ByteOrder"], 18761)
        self.assertEquals(metadata["metadata"]["DataType"], 2)
        self.assertEquals(metadata["emi"]["Emission [uA]"], 267.0)

        ser_flat = SerFile(_get_image_route(
            "flatname.ser"))
        metadata_flat = ser_flat.get_metadata()
        self.assertEquals(metadata_flat["header"]["ByteOrder"], 18761)
        self.assertEquals(metadata_flat["metadata"]["DataType"], 2)
        self.assertEquals(metadata_flat["emi"], None)

    def test_save_png(self):
        ser = SerFile(_get_image_route(
            "17mrad_conv-reference_image-overview_1.ser"))
        out_png = _get_image_route("test.png")
        self._register_tmp_file(out_png)
        ser.save_png(out_png)
        self.assertTrue(os.path.isfile(out_png))

    def test_make_thumbnail_from_png(self):
        self._register_tmp_folder(_get_image_route("tmp/test_crawl"))
        self._create_empty(_get_image_route("tmp/test_crawl/dest"),
                           is_folder=True)
        small_png = _get_image_route("tmp/test_crawl/dest/thumbnail.png")
        orig_png = _get_image_route(
            "Capture5_Hour_00_Minute_02_Second_40_Frame_0323.dm4.png")

        SerFile.make_thumbnail_from_png(orig_png, 100, 300, small_png)
        self.assertTrue(os.path.isfile(small_png))

        SerFile.make_thumbnail_from_png(orig_png, 100, 300)

        self.assertTrue(os.path.isfile(_get_image_route(
            "Capture5_Hour_00_Minute_02_Second_40_Frame_0323.dm4.tb.png")))


class TestDmFile(TestFileGeneric):

    def test_constructor(self):
        dm3 = Dm3File(_get_image_route("Frame_115K_spot4_300V.dm3"))
        metadata = dm3.get_metadata()
        self.assertEqual(metadata["dimensions"], 2)
        self.assertEqual(metadata["metadata"]["pixelOrigin"], [0.0, 0.0])
        self.assertEqual(metadata["header"]['.ImageSourceList.1.ImageRef'], 1)

    def test_save_image(self):
        dm3 = Dm3File(_get_image_route("Frame_115K_spot4_300V.dm3"))
        out_png = _get_image_route("testdm3.png")
        self._register_tmp_file(out_png)
        dm3.save_png(out_png)

    def test_save_image_3D(self):
        dm3 = Dm3File(_get_image_route(
            "03_-4um_FocalSeriesImages_Def_-5nm.dm3"))
        out_png = _get_image_route("testdm3-3D.png")
        self._register_tmp_file(out_png)
        dm3.save_png(out_png)
