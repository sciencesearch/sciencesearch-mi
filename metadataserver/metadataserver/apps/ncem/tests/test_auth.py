import json

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

url_login = '/api-token-auth/'
url_login_session = '/api-auth/login/'
images_endpoint = "/api/image-tag/images/"
debug_tags_endpoint = "/api/image-tag/debug/tags/"

class TestAuth(APITestCase):
    def setUp(self):
        self.client = APIClient()

    def test_failed_login(self, is_admin=False, username='user1',
                        password='pass1'):
        if is_admin:
            user = User.objects.create_superuser(username, username+'@mail.com',
                                                 password)
            user.is_staff=is_admin
        else:
            user = User.objects.create_user(username, username+'@mail.com',
                                        password)
        user.save()
        response = self.client.post(url_login, data=
                                    {"username":username,
                                     "password":"wrong!"})
        self.assertEqual(status.HTTP_400_BAD_REQUEST,
                         response.status_code)
        
        some_url=images_endpoint
        response = self.client.post(url_login_session, data=
                                    {"username":username,
                                     "password":"wong!",
                                     "next":some_url})
        self.assertEqual(status.HTTP_200_OK,
                         response.status_code)
        return True
        
    def _do_login_token(self, is_admin=False, username='user1',
                        password='pass1'):
        if is_admin:
            user = User.objects.create_superuser(username, username+'@mail.com',
                                                 password)
            user.is_staff=is_admin
        else:
            user = User.objects.create_user(username, username+'@mail.com',
                                        password)
        user.save()
        response = self.client.post(url_login, data=
                                    {"username":username,
                                     "password":password})
        self.assertEqual(status.HTTP_200_OK,
                         response.status_code)
        self.assertTrue("token" in response.data.keys())
        self.client.credentials(HTTP_AUTHORIZATION='Token ' +
                                response.data["token"])
        return response.data["token"]
    
    def _do_login_session(self, is_admin=False, username='user_session',
                        password='pass1'):
        if is_admin:
            user = User.objects.create_superuser(username, username+'@mail.com',
                                                 password)
            user.is_staff=is_admin
        else:
            user = User.objects.create_user(username, username+'@mail.com',
                                        password)
        user.save()
        some_url=images_endpoint
        response = self.client.post(url_login_session, data=
                                    {"username":username,
                                     "password":password,
                                     "next":some_url})
        self.assertEqual(status.HTTP_302_FOUND,
                         response.status_code)
        return True
    
    def test_login(self):
        self.assertTrue(self._do_login_token())
        self.assertTrue(self._do_login_session())
    
    def test_check_authorization(self):
        url = images_endpoint
        url_admin = debug_tags_endpoint
        
        response = self.client.get(url)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED,
                       response.status_code)
        # Test without credentials
        self._do_login_token()
                
        # Test without credentials of a normal user on a normal view
        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK,
                       response.status_code)
        
        # Test without credentials of a normal user on an admin view
        response = self.client.get(url_admin)
        self.assertEqual(status.HTTP_403_FORBIDDEN,
                       response.status_code)
        
        # Test without credentials of an admin user on an admin view
        self.client = APIClient()
        self._do_login_session(username="user2", is_admin=True)
        response = self.client.get(url_admin)
        self.assertEqual(status.HTTP_200_OK,
                       response.status_code)
        
        
        
    
        