from django.test import TestCase

from metadataserver.apps.ncem.datamodel.transform.imagefolders import ScopeImageFolderGenerator
from metadataserver.apps.ncem.models import ScopeImage, ScopeImagesFolder


class TestScopeImageFolderGenerator(TestCase):

    def test_transform_folder_is_file(self):

        fg = ScopeImageFolderGenerator()

        self.assertEqual(fg._transform_folder("file.txt", 1),
                         "")
        self.assertEqual(fg._transform_folder("one/file.txt", 1),
                         "one")

        self.assertEqual(fg._transform_folder("/one/file.txt", 1),
                         "/")

        self.assertEqual(fg._transform_folder("/one/two/tree/file.txt", 1),
                         "/")

        self.assertEqual(fg._transform_folder("/one/two/tree/file.txt", 2),
                         "/one")

        self.assertEqual(fg._transform_folder("one/two/tree/file.txt", 1),
                         "one")

        self.assertEqual(fg._transform_folder("one/two/tree/file.txt", 2),
                         "one/two")

        self.assertEqual(fg._transform_folder("/file.txt", 1),
                         "/")

    def test_transform_folder_is_not_file(self):

        fg = ScopeImageFolderGenerator()

        self.assertEqual(fg._transform_folder("/", 1,
                                              is_file=False),
                         "/")

        self.assertEqual(fg._transform_folder("", 1, is_file=False),
                         "")

        self.assertEqual(fg._transform_folder("one", 1, is_file=False),
                         "one")

        self.assertEqual(fg._transform_folder("/one", 1, is_file=False),
                         "/")

        self.assertEqual(fg._transform_folder("/one/two/tree", 1,
                                              is_file=False),
                         "/")

        self.assertEqual(fg._transform_folder("/one/two/tree", 2,
                                              is_file=False),
                         "/one")

        self.assertEqual(fg._transform_folder("one/two/tree", 1,
                                              is_file=False),
                         "one")

        self.assertEqual(fg._transform_folder("one/two/tree", 2,
                                              is_file=False),
                         "one/two")

    def test_add_folder(self):
        fg = ScopeImageFolderGenerator()

        fg._add_folder("/a.txt", 2)
        fg._add_folder("b.txt", 2)
        fg._add_folder("/one/a.txt", 2)
        fg._add_folder("/one/two/c.txt", 2)
        fg._add_folder("/one/three/d.txt", 2)
        fg._add_folder("/four/three/e.txt", 2)

        self.assertEqual(fg._route_list,
                         {
                             "/": 5,
                             "": 1,
                             "/one": 3,
                             "/four": 1
                         }
                         )

    def test_scan_folders(self):

        for route in ["/a.txt", "b.txt", "/one/a.txt", "/one/two/c.txt",
                      "/one/three/d.txt", "/four/three/e.txt"]:
            si = ScopeImage(original_route=route)
            si.save()

        fg = ScopeImageFolderGenerator()
        fg.scan_folders(depth=2)
        self.assertEqual(fg._route_list,
                         {
                             "/": 5,
                             "": 1,
                             "/one": 3,
                             "/four": 1
                         }
                         )

        fg.scan_folders(depth=3)
        self.assertEqual(fg._route_list,
                         {
                             "/": 5,
                             "": 1,
                             "/one": 3,
                             "/one/two": 1,
                             "/one/three": 1,
                             "/four": 1,
                             "/four/three": 1
                         }
                         )

    def test_dump_to_model(self):
        fg = ScopeImageFolderGenerator()
        fg._route_list = {"one": 1, "two": 2, "four": 4}

        fg.dump_to_model()

        dir_list = []
        for folder in ScopeImagesFolder.objects.all():
            dir_list.append((folder.route, folder.image_count))
        self.assertEqual(dir_list,
                         [("four", 4),
                          ("one", 1),
                          ("two", 2)])

        fg.dump_to_model()
        dir_list = []
        for folder in ScopeImagesFolder.objects.all():
            dir_list.append((folder.route, folder.image_count))
        self.assertEqual(dir_list,
                         [("four", 4),
                          ("one", 1),
                          ("two", 2)])
        fg._route_list = {"one": 1, "two": 2, "five": 5}
        fg.dump_to_model(clean_existing=False)
        dir_list = []
        for folder in ScopeImagesFolder.objects.all():
            dir_list.append((folder.route, folder.image_count))
        self.assertEqual(dir_list,
                         [("five", 5),
                          ("four", 4),
                          ("one", 1),
                          ("two", 2)])
