import json

from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase
from metadataserver.apps.ncem.models import (
    Tag, ScopeImage, Tagged, TagUserAddition, TAG_SUGGESTED, TagModification,
    TAG_ACTION_VALIDATE, TAG_ACTION_INVALIDATE, SyntheticData
    )
from metadataserver.apps.ncem.serializers import ScopeImageSerializer
from metadataserver.apps.ncemreldata.models import Proposal
from metadataserver.apps.searchengine.configuration import UNKNOWN_SOURCE, USER_PROVIDED

from rest_framework.renderers import JSONRenderer


# Create your tests here.
class TestScopeImage(TestCase):

    def test_get_reviewed_images(self):
        s1 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s1.save()

        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s2.save()

        s3 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s3.save()
        s1.mark_reviewed()
        s2.mark_reviewed()
        self.assertEqual({s1.id, s2.id},
                         set([x.id for x in ScopeImage.get_reviewed_images()]))

    def test_strobe_tags(self):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()

        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()
        tag3 = Tag(tag="tag3")
        tag3.save()
        tag4 = Tag(tag="tag4")
        tag4.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3, strength=TAG_SUGGESTED)
        tg3.save()
        tg4 = Tagged(scope_image=si, tag=tag4, strength=TAG_SUGGESTED)
        tg4.save()

        tg5 = Tagged(scope_image=si, tag=tag4, strength=TAG_SUGGESTED,
                     metadata_type=USER_PROVIDED)
        tg5.save()

        session_id = "1234"
        si.strobe_tags(["tag5", "tag6", "tag1", "tag3"], session_id=session_id)

        self.assertEqual(set([str(x.tag)
                              for x in TagUserAddition.objects.filter(scope_image=si,
                                                                      session_id=session_id)]),
                         {"tag5", "tag6"})

        self.assertEqual(TagModification.objects.get(tagged=tg1,
                                                     session_id=session_id).action,
                         TAG_ACTION_VALIDATE)
        self.assertEqual(TagModification.objects.get(tagged=tg2,
                                                     session_id=session_id).action,
                         TAG_ACTION_INVALIDATE)
        self.assertEqual(TagModification.objects.get(tagged=tg3,
                                                     session_id=session_id).action,
                         TAG_ACTION_VALIDATE)
        self.assertEqual(TagModification.objects.get(tagged=tg4,
                                                     session_id=session_id).action,
                         TAG_ACTION_INVALIDATE)
        self.assertEqual(TagModification.objects.get(tagged=tg5,
                                                     session_id=session_id).action,
                         TAG_ACTION_INVALIDATE)

    def test_user_validates_invalidates_tag(self):

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()
        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s2.save()
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()

        tg = Tagged(scope_image=s2, tag=tag1)
        tg.save()

        tg = Tagged(scope_image=si, tag=tag1)
        tg.save()

        tg2 = Tagged(scope_image=si, tag=tag1, metadata_type=USER_PROVIDED)
        tg2.save()

        si.user_validates_tag("tag1", "ses1", metadata_type=USER_PROVIDED)
        self.assertEqual(TagModification.objects.get(tagged=tg2,
                                                     session_id="ses1").action,
                         TAG_ACTION_VALIDATE)

        self.assertEqual(len(TagModification.objects.filter(tagged=tg,
                                                            session_id="ses1")),
                         0)

        si.user_validates_tag("tag1", "ses1")
        self.assertEqual(TagModification.objects.get(tagged=tg2,
                                                     session_id="ses1").action,
                         TAG_ACTION_VALIDATE)

        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id="ses1").action,
                         TAG_ACTION_VALIDATE)

        si.user_invalidates_tag("tag1", "ses1")
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id="ses1").action,
                         TAG_ACTION_INVALIDATE)
        si.user_validates_tag("tag1", "ses1")
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id="ses1").action,
                         TAG_ACTION_VALIDATE)
        si.user_invalidates_tag("tag1", "ses2")
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id="ses1").action,
                         TAG_ACTION_VALIDATE)
        self.assertEqual(TagModification.objects.get(tagged=tg,
                                                     session_id="ses2").action,
                         TAG_ACTION_INVALIDATE)

    def test_user_validates_tag(self):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()
        tag1 = Tag(tag="tag1")
        tag1.save()

        tg = Tagged(scope_image=si, tag=tag1)
        tg.save()

        si.user_validates_tag("tag1", "ses1")

        modifications = TagModification.objects.filter(
            action=TAG_ACTION_VALIDATE,
            tagged=tg,
            session_id="ses1")
        self.assertEqual(len(modifications), 1)
        self.assertEqual(modifications[0].action, TAG_ACTION_VALIDATE)

    def test_user_invalidates_tag(self):
        """ Regular cases """
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()
        tag1 = Tag(tag="tag1")
        tag1.save()

        tg = Tagged(scope_image=si, tag=tag1)
        tg.save()

        tag2 = Tag(tag="tag2")
        tag2.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()

        si.user_validates_tag("tag1", "ses1")
        si.user_invalidates_tag("tag2", "ses1")

        modifications = TagModification.objects.filter(
            action=TAG_ACTION_VALIDATE,
            tagged=tg,
            session_id="ses1")
        self.assertEqual(len(modifications), 1)

        modifications = TagModification.objects.filter(
            action=TAG_ACTION_INVALIDATE,
            tagged=tg2,
            session_id="ses1")
        self.assertEqual(len(modifications), 1)

        """ Tags and asscociations that do not exist """
        tag3 = Tag(tag="tag4")
        tag3.save()

        self.assertRaises(ObjectDoesNotExist,
                          si.user_validates_tag,
                          "tag10")
        self.assertRaises(ObjectDoesNotExist,
                          si.user_validates_tag,
                          "tag4")

        """ Repeating calls with the same tag. """

        si.user_validates_tag("tag1", "ses1")
        si.user_validates_tag("tag1", "ses2")

    def test_user_suggests_tag(self):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()
        si.user_suggests_tag("tag1", "s1")
        si.user_suggests_tag("tag1", "s2")
        si.user_suggests_tag("tag2", "s3")

        additions = TagUserAddition.objects.filter(scope_image=si)
        additions_list = [{
                              "i": x.scope_image.id,
                              "t": x.tag.tag,
                              "s": x.session_id
                          } for x in additions]
        self.assertEqual(additions_list,
                         [{"i": si.id, "t": "tag1", "s": "s1"},
                          {"i": si.id, "t": "tag1", "s": "s2"},
                          {"i": si.id, "t": "tag2", "s": "s3"}])

        si.user_suggests_tag("tag1", "s1")
        additions = TagUserAddition.objects.filter(scope_image=si)
        additions_list = [{
                              "i": x.scope_image.id,
                              "t": x.tag.tag,
                              "s": x.session_id
                          } for x in additions]
        self.assertEqual(additions_list,
                         [{"i": si.id, "t": "tag1", "s": "s1"},
                          {"i": si.id, "t": "tag1", "s": "s2"},
                          {"i": si.id, "t": "tag2", "s": "s3"}])

    def test_mark_reviewed(self):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()
        si2 = ScopeImage.objects.get(id=si.id)
        self.assertFalse(si2.reviewed)

        si.mark_reviewed()
        si.save()
        si3 = ScopeImage.objects.get(id=si.id)
        self.assertTrue(si3.reviewed)

    def test_get_next_unreviewed(self):

        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s2.save()

        s1 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s1.save()

        snext = ScopeImage.get_next_unreviewed().first()
        self.assertEqual(s1.id, snext.id)

        snext.mark_reviewed()
        snext.save()
        snext = ScopeImage.get_next_unreviewed().first()
        self.assertEqual(s2.id, snext.id)

        snext.mark_reviewed()
        snext.save()
        snext = ScopeImage.get_next_unreviewed().first()
        self.assertFalse(snext)

    def test_get_next_unreviewed_and_related(self):

        p1 = Proposal(serial_number="1234")
        p1.save()

        s2 = ScopeImage(file_name="fn2.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        proposal=p1)
        s2.save()

        s1 = ScopeImage(file_name="fn1.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s1.save()

        snext = ScopeImage.get_next_unreviewed().first()
        self.assertEqual(s1.id, snext.id)

        snext = ScopeImage.get_next_unreviewed(only_related=True).first()
        self.assertEqual(s2.id, snext.id)

        snext.mark_reviewed()
        snext.save()

        snext = ScopeImage.get_next_unreviewed(only_related=True).first()
        self.assertFalse(snext)

    def test_get_next_unreviewed_random(self):

        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s2.save()

        s1 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s1.save()

        s3 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir3/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s3.save()

        snext = ScopeImage.get_next_unreviewed(random_order=True).first()

        self.assertIn(snext.id, [s1.id, s2.id, s3.id])

        id_list = []
        for i in range(10):
            id_list.append(ScopeImage.get_next_unreviewed(
                random_order=True).first().id)

        self.assertTrue(len(set(id_list)) > 1)

        s1.mark_reviewed()
        snext = ScopeImage.get_next_unreviewed(random_order=True).first()
        self.assertIn(snext.id, [s2.id, s3.id])

        s2.mark_reviewed()
        snext = ScopeImage.get_next_unreviewed(random_order=True).first()
        self.assertIn(snext.id, [s3.id])

        s3.mark_reviewed()
        snext = ScopeImage.get_next_unreviewed(random_order=True).first()
        self.assertFalse(snext)

    def test_get_next_unreviewed_disabled(self):

        s2 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn2.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        disabled=True)
        s2.save()

        s1 = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn1.jog",
                        bitmap_url="http://image_server/f1.jpg")
        s1.save()

        snext = ScopeImage.get_next_unreviewed().first()
        self.assertEqual(s1.id, snext.id)

        snext.mark_reviewed()
        snext.save()
        snext = ScopeImage.get_next_unreviewed().first()
        self.assertFalse(snext)

        s2.disabled = False
        s2.save()

        snext = ScopeImage.get_next_unreviewed().first()
        self.assertEqual(s2.id, snext.id)

    def test_get_tags(self):
        tag1 = Tag(tag="tag1")
        tag2 = Tag(tag="tag2")
        tag3 = Tag(tag="tag3")
        stag1 = Tag(tag="stag1")
        stag2 = Tag(tag="stag2")

        tags = [tag1, tag2, tag3, stag1, stag2]
        for tag in tags:
            tag.save()

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3)
        tg3.save()
        tg5 = Tagged(scope_image=si, tag=tag3, metadata_type=USER_PROVIDED)
        tg5.save()

        stg4 = Tagged(scope_image=si, tag=stag1,
                      strength=TAG_SUGGESTED)
        stg4.save()

        tags = si.get_tags()
        text_tags = [str(x) for x in tags]
        self.assertEqual(set(text_tags), {"tag1", "tag2", "tag3"})

    def test_suggested_tags(self):
        tag1 = Tag(tag="tag1")
        tag2 = Tag(tag="tag2")
        tag3 = Tag(tag="tag3")
        stag1 = Tag(tag="stag1")
        stag2 = Tag(tag="stag2")

        tags = [tag1, tag2, tag3, stag1, stag2]
        for tag in tags:
            tag.save()

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3)
        tg3.save()

        stg4 = Tagged(scope_image=si, tag=stag1,
                      strength=TAG_SUGGESTED)
        stg4.save()

        stg5 = Tagged(scope_image=si, tag=stag2,
                      strength=TAG_SUGGESTED)
        stg5.save()

        tags = si.get_tags()
        text_tags = [str(x) for x in tags]
        self.assertEqual(set(text_tags), {"tag1", "tag2", "tag3"})

        tags = si.get_suggested_tags()
        text_tags = [str(x) for x in tags]
        self.assertEqual(set(text_tags), {"stag1", "stag2"})


class TestScopeImageSerializer(TestCase):

    def test_bitmap_url_settings(self):
        si_http = ScopeImage(file_name="fn.jpg",
                             original_route="/dir1/dir2/fn.jog",
                             bitmap_url="http://image_server/f1.jpg",
                             file_metadata=json.loads('{"key":"value"}'))
        si_no_http = ScopeImage(file_name="fn.jpg",
                                original_route="/dir1/dir2/fn.jog",
                                bitmap_url="/f1.jpg",
                                file_metadata=json.loads('{"key":"value"}'))
        with self.settings(NCEM_IMAGE_SERVER_URL='http://other_server'):
            serializer = ScopeImageSerializer(si_http)
            self.assertEqual(serializer.data["bitmap_url"],
                             "http://image_server/f1.jpg")
            serializer = ScopeImageSerializer(si_no_http)
            self.assertEqual(serializer.data["bitmap_url"],
                             "http://other_server/f1.jpg")

    def test_bitmap_url_settings_complex(self):
        si_http = ScopeImage(file_name="fn.jpg",
                             original_route="/dir1/dir2/fn.jog",
                             bitmap_url="http://image_server/f1.jpg",
                             file_metadata=json.loads('{"key":"value"}'))
        si_no_http = ScopeImage(file_name="fn.jpg",
                                original_route="/dir1/dir2/fn.jog",
                                bitmap_url="/f1.jpg",
                                file_metadata=json.loads('{"key":"value"}'))
        with self.settings(NCEM_IMAGE_SERVER_URL='http://other_server/URL/'):
            serializer = ScopeImageSerializer(si_http)
            self.assertEqual(serializer.data["bitmap_url"],
                             "http://image_server/f1.jpg")
            serializer = ScopeImageSerializer(si_no_http)
            self.assertEqual(serializer.data["bitmap_url"],
                             "http://other_server/URL/f1.jpg")

    def test_data_url_settings(self):
        si_http = ScopeImage(file_name="fn.jpg",
                             original_route="/dir1/dir2/fn.jog",
                             bitmap_url="http://image_server/f1.jpg",
                             file_metadata=json.loads('{"key":"value"}'))
        with self.settings(NCEM_DATA_SERVER_URL='http://other_server'):
            serializer = ScopeImageSerializer(si_http)
            self.assertEqual(serializer.data["data_url"],
                             "http://other_server/dir1/dir2/fn.jog")

    def test_serialization(self):
        tag1 = Tag(tag="tag1")
        tag2 = Tag(tag="tag2")
        tag3 = Tag(tag="tag3")
        stag1 = Tag(tag="stag1")
        stag2 = Tag(tag="stag2")

        tags = [tag1, tag2, tag3, stag1, stag2]
        for tag in tags:
            tag.save()

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata=json.loads('{"key":"value"}'))
        si.save()
        si_id = si.id
        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3)
        tg3.save()

        stg4 = Tagged(scope_image=si, tag=stag1,
                      strength=TAG_SUGGESTED)
        stg4.save()

        stg5 = Tagged(scope_image=si, tag=stag2,
                      strength=TAG_SUGGESTED)
        stg5.save()

        serializer = ScopeImageSerializer(si)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)

        candidate_data = json.loads(
            """{"id":""" + str(si_id) + """,
            "file_metadata":{"key":"value"},
            "file_name":"fn.jpg",
            "dimensions": 0,
            "file_path":"/dir1/dir2",
            "original_route":"/dir1/dir2/fn.jog",
            "data_url":"/dir1/dir2/fn.jog",
            "bitmap_url":"http://image_server/f1.jpg",
            "reviewed": false,
            "tags":["tag1","tag2","tag3"],
            "suggested_tags":["stag1","stag2"],
            "proposal": null
            }
            """)
        self.assertEqual(candidate_data, produced_data)

    def test_serialization_with_proposal(self):
        tag1 = Tag(tag="tag1")
        tag2 = Tag(tag="tag2")
        tag3 = Tag(tag="tag3")
        stag1 = Tag(tag="stag1")
        stag2 = Tag(tag="stag2")

        tags = [tag1, tag2, tag3, stag1, stag2]
        for tag in tags:
            tag.save()

        si = ScopeImage(file_name="fn.jpg",
                        dimensions=2,
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata=json.loads('{"key":"value"}'))
        si.save()
        si_id = si.id
        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3)
        tg3.save()

        stg4 = Tagged(scope_image=si, tag=stag1,
                      strength=TAG_SUGGESTED)
        stg4.save()

        stg5 = Tagged(scope_image=si, tag=stag2,
                      strength=TAG_SUGGESTED)
        stg5.save()

        p = Proposal(serial_number="1234")
        p.save()
        si.proposal = p
        p.save()

        serializer = ScopeImageSerializer(si)
        content = JSONRenderer().render(serializer.data)
        produced_data = json.loads(content)

        candidate_data = json.loads(
            """{"id":""" + str(si_id) + """,
            "file_metadata":{"key":"value"},
            "file_name":"fn.jpg",
            "file_path":"/dir1/dir2",
            "original_route":"/dir1/dir2/fn.jog",
            "data_url":"/dir1/dir2/fn.jog",
            "dimensions": 2,
            "bitmap_url":"http://image_server/f1.jpg",
            "reviewed": false,
            "tags":["tag1","tag2","tag3"],
            "suggested_tags":["stag1","stag2"],
            "proposal": "1234"
            }
            """)
        self.assertEqual(candidate_data, produced_data)


class TestTag(TestCase):
    def test_add_tag_safe(self):
        new_tag_1 = Tag.add_tag_safe("MyTag", raise_error=False)
        self.assertEqual(new_tag_1.tag, "MyTag")
        new_tag_2 = Tag.objects.get(tag="MyTag")
        self.assertEqual(new_tag_2.tag, "MyTag")

        self.assertRaises(ObjectDoesNotExist,
                          Tag.add_tag_safe,
                          "MyTag2", True)


class TestTagged(TestCase):
    def test_tagged_field(self):
        new_tag_1 = Tag.add_tag_safe("MyTag", raise_error=False)
        si = ScopeImage(original_route="img1.ser")
        si.save()
        tg1 = Tagged(scope_image=si, tag=new_tag_1)
        tg1.save()
        self.assertEqual(tg1.metadata_type, UNKNOWN_SOURCE)

        tg2 = Tagged(scope_image=si, tag=new_tag_1,
                     metadata_type=USER_PROVIDED)
        tg2.save()
        self.assertEqual(tg2.metadata_type, USER_PROVIDED)


class TestTagModification(TestCase):
    def test_get_max_historic_index(self):
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata=json.loads('{"key":"value"}'))
        si.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()

        self.assertEqual(TagModification.get_max_historic_index(), -1)

        tm1 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg1,
                              historic_index=1)
        tm1.save()

        self.assertEqual(TagModification.get_max_historic_index(), 1)

        tm2 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg2,
                              historic_index=2)
        tm2.save()

        self.assertEqual(TagModification.get_max_historic_index(), 2)

    def test_get_max_historic_index_sub_classes(self):
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()

        tag3 = Tag(tag="tag3")
        tag3.save()

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata=json.loads('{"key":"value"}'))
        si.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()

        self.assertEqual(SyntheticData.get_max_historic_index_subclasses(), -1)

        tm1 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg1,
                              historic_index=1)
        tm1.save()

        self.assertEqual(SyntheticData.get_max_historic_index_subclasses(), 1)

        tm2 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg2,
                              historic_index=2)
        tm2.save()

        self.assertEqual(SyntheticData.get_max_historic_index_subclasses(), 2)

        ta1 = TagUserAddition(scope_image=si, tag=tag3, historic_index=4)
        ta1.save()

        self.assertEqual(SyntheticData.get_max_historic_index_subclasses(), 4)

    def test_update_historic_index(self):
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()
        tag3 = Tag(tag="tag3")
        tag3.save()

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata=json.loads('{"key":"value"}'))
        si.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3)
        tg3.save()

        self.assertEqual(TagModification.get_max_historic_index(), -1)

        tm1 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg1,
                              historic_index=2)
        tm1.save()

        tm2 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg2,
                              historic_index=2)
        tm2.save()

        tm3 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg3)
        tm3.save()

        self.assertEqual({"tag1", "tag2"},
                         set([str(x.tagged.tag) for x in
                              TagModification.objects.filter(historic_index=2)]))
        self.assertEqual({"tag3"},
                         set([str(x.tagged.tag) for x in
                              TagModification.objects.filter(historic_index=-1)]))

        TagModification.update_historic_index(4, 2)
        self.assertEqual({"tag1", "tag2"},
                         set([str(x.tagged.tag) for x in
                              TagModification.objects.filter(historic_index=4)]))
        TagModification.update_historic_index(5)
        self.assertEqual({"tag3"},
                         set([str(x.tagged.tag) for x in
                              TagModification.objects.filter(historic_index=5)]))

    def test_update_historic_index_subclasses(self):
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()
        tag3 = Tag(tag="tag3")
        tag3.save()

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata=json.loads('{"key":"value"}'))
        si.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()

        self.assertEqual(TagModification.get_max_historic_index(), -1)

        tm1 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg1,
                              historic_index=2)
        tm1.save()

        tm2 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg2,
                              historic_index=2)
        tm2.save()

        ta1 = TagUserAddition(scope_image=si, tag=tag3)
        ta1.save()

        self.assertEqual({"tag1", "tag2"},
                         set([str(x.tagged.tag) for x in
                              TagModification.objects.filter(historic_index=2)]))
        self.assertEqual({"tag3"},
                         set([str(x.tag) for x in
                              TagUserAddition.objects.filter(historic_index=-1)]))

        SyntheticData.update_historic_index_subclasses(4, 2)
        self.assertEqual({"tag1", "tag2"},
                         set([str(x.tagged.tag) for x in
                              TagModification.objects.filter(historic_index=4)]))

        SyntheticData.update_historic_index_subclasses(5)
        self.assertEqual({"tag3"},
                         set([str(x.tag) for x in
                              TagUserAddition.objects.filter(historic_index=5)]))

    def test_update_historic_index_subclasses_extended(self):
        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg")
        si.save()

        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()
        tag3 = Tag(tag="tag3")
        tag3.save()
        tag4 = Tag(tag="tag4")
        tag4.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()
        tg3 = Tagged(scope_image=si, tag=tag3, strength=TAG_SUGGESTED)
        tg3.save()
        tg4 = Tagged(scope_image=si, tag=tag4, strength=TAG_SUGGESTED)
        tg4.save()

        self.assertEqual(si.get_tags(), [tag1, tag2])
        self.assertEqual(si.get_suggested_tags(), [tag3, tag4])

        SyntheticData.update_historic_index_subclasses(1)

        self.assertFalse(si.get_tags())
        self.assertFalse(si.get_suggested_tags())

        for t in [tag1, tag2, tag3]:
            tag1_after = Tag.objects.get(tag=t.tag, historic_index=1)
            self.assertTrue(tag1_after)
            tg1_after = Tagged.objects.get(scope_image=si, tag=tag1_after,
                                           historic_index=1)
            self.assertTrue(tg1_after)

    def test_filter_by_historic_index(self):
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()
        tag3 = Tag(tag="tag3")
        tag3.save()

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata=json.loads('{"key":"value"}'))
        si.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()
        tg2 = Tagged(scope_image=si, tag=tag2)
        tg2.save()

        self.assertEqual(TagModification.get_max_historic_index(), -1)

        tm1 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg1,
                              historic_index=2)
        tm1.save()

        tm2 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg2,
                              historic_index=2)
        tm2.save()

        ta1 = TagUserAddition(scope_image=si, tag=tag3)
        ta1.save()

        self.assertEqual({"tag1"},
                         set([str(x.tagged.tag) for x in
                              TagModification.filter_by_historic_index(historic_index=2,
                                                                       tagged=tg1)]))
        self.assertEqual({"tag3"},
                         set([str(x.tag) for x in
                              TagUserAddition.filter_by_historic_index(
                                  historic_index=-1,
                                  scope_image=si)]))
        self.assertEqual("tag3",
                         str(TagUserAddition.get_by_historic_index(
                             historic_index=-1,
                             scope_image=si).tag))

    def test_filter_history_aware(self):
        tag1 = Tag(tag="tag1")
        tag1.save()
        tag2 = Tag(tag="tag2")
        tag2.save()

        si = ScopeImage(file_name="fn.jpg",
                        original_route="/dir1/dir2/fn.jog",
                        bitmap_url="http://image_server/f1.jpg",
                        file_metadata=json.loads('{"key":"value"}'))
        si.save()

        tg1 = Tagged(scope_image=si, tag=tag1)
        tg1.save()

        self.assertEqual(SyntheticData.get_max_historic_index_subclasses(), -1)

        tag3 = Tag(tag="tag3", historic_index=1)
        tag3.save()

        tg2 = Tagged(scope_image=si, tag=tag2, historic_index=1)
        tg2.save()

        self.assertEqual(SyntheticData.get_max_historic_index_subclasses(), 1)

        self.assertEqual(list(Tagged.objects.filter(scope_image=si)),
                         [tg1, tg2])
        self.assertEqual(list(Tagged.objects.filter(scope_image=si,
                                                    historic_index=1)),
                         [tg2])

        self.assertEqual(si.get_tags(), [tag1])

        tm1 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg1,
                              historic_index=2)
        tm1.save()

        tm2 = TagModification(action=TAG_ACTION_VALIDATE, tagged=tg2,
                              historic_index=2)
        tm2.save()

        ta1 = TagUserAddition(scope_image=si, tag=tag3)
        ta1.save()

        self.assertEqual({"tag1"},
                         set([str(x.tagged.tag) for x in
                              TagModification.filter_by_historic_index(
                                  historic_index=2,
                                  tagged=tg1)]))
        self.assertEqual({"tag3"},
                         set([str(x.tag) for x in
                              TagUserAddition.filter_by_historic_index(
                                  historic_index=-1,
                                  scope_image=si)]))
        self.assertEqual("tag3",
                         str(TagUserAddition.get_by_historic_index(
                             historic_index=-1,
                             scope_image=si).tag))
