from django.apps import AppConfig


class NcemConfig(AppConfig):
    name = 'metadataserver.apps.ncem'
    label = 'ncem'
