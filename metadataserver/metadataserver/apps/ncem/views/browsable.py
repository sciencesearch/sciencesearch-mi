"""
Browsable views for the NCEM interface. ScopeImageViewSet if set to present
the flat data of images. TagsViewSet presents the tags associated with a
particular image. SuggestedTagsViewSet presents the tags suggested to a
particular image.
"""

import json
from metadataserver.apps.ncem.models import (
    ScopeImage, Tag, Tagged, TAG_ASSOCIATED, TAG_SUGGESTED,
    TagUserAddition, ScopeImagesFolder
    )
from metadataserver.apps.ncem.serializers import (
    ScopeImageSerializer, TagSerializer, ScopeImagesFolderSerializer
    )

from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponseServerError, HttpResponse
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authentication import (TokenAuthentication,
                                           SessionAuthentication)
from rest_framework.decorators import detail_route
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


class ScopeImageViewSet(viewsets.ModelViewSet):
    """
    View derived from the ModelViewSetfor the ScopeImage model. End-points
    offered:
    - images/: GET returns a list of the serialized ScopeImages in the database.
    - images/<id>/: GET returns a serialized ScopeImage identified by <id>
    - images/next-unreviewed/: GET returns the serialized version of the first
      ScopeImage with reviewed=False
    - images/next-unreviewed-random/: GET returns the serialized version of the
      a random ScopeImage with reviewed=False
    - images/next-unreviewed-random-related/: GET returns the serialized version
       of the a random ScopeImage that is related to a proposal.
    """
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    
    queryset = ScopeImage.objects.all()
    serializer_class = ScopeImageSerializer
    
    def dispatch(self, request, *args, **kwargs):
        self.route_query = request.GET.get('route', None)
        return super(ScopeImageViewSet, self).dispatch(
            request, *args, **kwargs)
    
    def get_object(self):
        """
        Returns the object the view is displaying or the next unreviewed
        if pl=´next-unreviewed
        """
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        if (filter_kwargs == {"pk" : "next-unreviewed"} or 
            filter_kwargs == {"pk" : "next-unreviewed-random"} or
            filter_kwargs == {"pk" : "next-unreviewed-random-related"} or
            filter_kwargs == {"pk" : "next-unreviewed-related"}):
            random_order=filter_kwargs["pk"] in ["next-unreviewed-random",
                                             "next-unreviewed-random-related"]
            related = filter_kwargs["pk"] in ["next-unreviewed-random-related",
                                              "next-unreviewed-related"]
            
            if related:
                if not ScopeImage().get_next_unreviewed(
                    random_order=random_order, only_related=related,
                    route_filter=self.route_query):
                    related=False
                    random_order=True
            
            obj = get_object_or_404(ScopeImage.get_next_unreviewed(
                    random_order=random_order, only_related=related,
                    route_filter=self.route_query))
            self.check_object_permissions(self.request, obj)
            return obj
        return super(ScopeImageViewSet, self).get_object()    

    @detail_route(methods=['post'], url_path='reviewed')
    def reviewed(self, request, pk, id_string=None, session_id=None):
        image = self.get_object()
        image.mark_reviewed()
        serialized_image = ScopeImageSerializer(image)
        return Response(serialized_image.data)
    
    @detail_route(methods=['post'], url_path='strobe')
    def strobe_tags(self, request, pk):
        try:
            json_data = json.loads(request.body)
        except ValueError:
            return HttpResponseServerError("Expected JSON dictionary with tags field.")
        if not type(json_data) is dict or not 'tags' in json_data.keys():
            return HttpResponseServerError("Expected JSON dictionary with tags field.")
        tag_list=json_data['tags']
        
        if (not type(tag_list) is list or 
            not all(type(item) is str for item in tag_list)):
            return HttpResponseServerError("Malformed data: Expected list of strings in"
                                           " tags field!")
        image = self.get_object()
        image.strobe_tags(tag_list, session_id=self.get_session_id())
        image.mark_reviewed()
        return HttpResponse("List of Tags registered")
    
    def get_session_id(self):
        """Makes sure that the request has a session and returns its id string"""
        if not self.request.session.session_key:
            self.request.session.create()
        return self.request.session.session_key

class ScopeImagesFolderViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    
    queryset = ScopeImagesFolder.objects.all()
    serializer_class = ScopeImagesFolderSerializer

class SuggestedTagsViewSet(viewsets.ModelViewSet):
    """
    View derived from the ModelViewSet for the Tags model. It presents
    the tags suggested for an image. End-points offered:
    - images/<imageid>/suggested-tags/: GET returns a list of the serialized Tag
      suggested for a ScopeImage of id=<id>.
    - images/<imageid>/tags/<tagid>/: POST registers a tagid as a user
      suggestion of ScopeImage imageid.
    - images/<imageid>/tags/<tagid>/valid/: POST registers validation of tagid
      as a valid tag ofr ScopeImage imageid.
    - images/<imageid>/tags/<tagid>/invalid/: POST registers invalidation of
      tagid as a valid tag ofr ScopeImage imageid.  
    
    Requirements: It must be routed including two URL parameters:
    - imageid: id of a valid ScopeImage.
    - session_id: string identifying the session_id 
    """
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    def get_query_image(self):
        image_id = self.kwargs['imageid']
        return ScopeImage.objects.get(id=image_id)
    def get_queryset(self):        
        queryset = self.get_query_image().get_suggested_tags()
        return queryset
    def get_session_id(self):
        """Makes sure that the request has a session and returns its id string"""
        if not self.request.session.session_key:
            self.request.session.create()
        return self.request.session.session_key
    def check_tag(self, tag_name):
        image  = self.get_query_image()
        tg = Tagged.objects.get(tag=Tag.objects.get(tag=tag_name),
                                    scope_image=image,
                                    strength=TAG_SUGGESTED)
        return tg
    @detail_route(methods=['post'], url_path='valid')
    def valid(self, request, pk, imageid=None, id_string=None,
              session_id=None):
        session_id = self.get_session_id()
        image  = self.get_query_image()
        try:
            self.check_tag(pk)
            image.user_validates_tag(pk, session_id=session_id)
        except ObjectDoesNotExist:
            raise Http404("Tag ({}) is not associated with this image({})"
                          "".format(pk, imageid))
        return Response("Valid")
    
    @detail_route(methods=['post'], url_path='invalid')
    def invalid(self, request, pk, imageid=None, id_string=None,
                session_id=None):
        session_id = self.get_session_id()
        image  = self.get_query_image()
        try:
            image.user_invalidates_tag(pk, session_id=session_id)
        except ObjectDoesNotExist:
            raise Http404("Tag ({}) is not associated with this image({})"
                          "".format(pk, imageid))
        return Response("Invalid")
    
class TagsViewSet(SuggestedTagsViewSet):
    """
    View derived from the ModelViewSet for the Tags model. It presents
    the tags associated to an image. End-points offered:
    - images/<imageid>/suggested-tags/: GET returns a list of the serialized Tag
      suggested for a ScopeImage of id=<id>.
    - images/<imageid>/tags/<tagid>/: POST registers a tagid as a user
      suggestion of ScopeImage imageid.
    - images/<imageid>/tags/<tagid>/valid/: POST registers validation of tagid
      as a valid tag ofr ScopeImage imageid.
    - images/<imageid>/tags/<tagid>/invalid/: POST registers invalidation of
      tagid as a valid tag ofr ScopeImage imageid.  
    
    Requirements: It must be routed including two URL parameters:
    - imageid: id of a valid ScopeImage.
    - session_id: string identifying the session_id 
    """
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    
    def get_queryset(self):        
        queryset = self.get_query_image().get_tags()
        return queryset
    def check_tag(self, tag_name):
        image  = self.get_query_image()
        tg = Tagged.objects.get(tag=Tag.objects.get(tag=tag_name),
                                scope_image=image,
                                    strength=TAG_ASSOCIATED,
                                    historic_index=-1)
        return tg
    def update(self, request, *args, **kwargs):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        tag_name = filter_kwargs['pk']
        session_id = self.get_session_id()
        scope_image = self.get_query_image()
        tag=Tag.add_tag_safe(tag_name)
        TagUserAddition.add_user_addition_safe(scope_image, tag,
                                               session_id)
        headers = self.get_success_headers(TagSerializer(tag).data)
        return Response("tag_name", status=status.HTTP_201_CREATED,
                        headers=headers)

