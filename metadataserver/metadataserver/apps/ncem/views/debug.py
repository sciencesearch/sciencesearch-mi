""" 
ModelViewSet Classes that are required to expose all the database objects
"""

from rest_framework import viewsets
from rest_framework.authentication import (TokenAuthentication,
                                           SessionAuthentication)
from rest_framework.permissions import IsAdminUser

from metadataserver.apps.ncem.models import (
    Tag, Tagged, TagModification, TagUserAddition
    )
from metadataserver.apps.ncem.serializers import (
    TagSerializer, TaggedSerializer, TagModificationSerializer,
    TagUserAdditionSerializer
    )


class TagsViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    
class TagRelationshipSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)
    queryset = Tagged.objects.all()
    serializer_class = TaggedSerializer
    
class TagModificationSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)
    queryset = TagModification.objects.all()
    serializer_class = TagModificationSerializer

class TagUserAdditionSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)
    queryset = TagUserAddition.objects.all()
    serializer_class = TagUserAdditionSerializer
