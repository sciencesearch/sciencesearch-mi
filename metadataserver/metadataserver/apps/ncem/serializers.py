import logging

from django.conf import settings
from rest_framework import serializers

from metadataserver.apps.ncem.models import (
    Tag, Tagged, ScopeImage, TagModification,
    TagUserAddition, ScopeImagesFolder
    )

logger = logging.getLogger(__name__)


class CustomURLField(serializers.CharField):
    def to_representation(self, value, base_url=None):
        base_rep = super().to_representation(value)
        if base_url is None or base_rep.startswith("http"):
            return base_rep
        else:
            base_cad = base_url
            if base_cad[-1] == "/":
                base_cad = base_cad[:-1]
            if base_rep and base_rep[0] == "/":
                base_rep = base_rep[1:]
            return base_cad + "/" + base_rep


class CustomImageURLField(CustomURLField):
    def to_representation(self, value, base_url=None):
        return super().to_representation(value, settings.NCEM_IMAGE_SERVER_URL)


class CustomDataURLField(CustomURLField):
    def to_representation(self, value, base_url=None):
        return super().to_representation(value, settings.NCEM_DATA_SERVER_URL)


class ScopeImageSerializer(serializers.ModelSerializer):
    """
    Serializer for the ncem.models.ScopeImage
    
    A serialized ScopeImage object is  a dictionary that includes
    object attributes id, file_name, original_route, bitmpa_url, and reviewed.
    It also includes two extra fields: 
      - tags: a list of strings representing the tags associated with the image.
      - suggested_tag: a list of strings suggested by the systema as candidate
        tags for the image.
    
    Example of JSON render of an ScopeImage object:
        {"id": 2,
        "file_name":"fn.jpg",
        "file_path":"/dir1/dir2",
        "original_route":"/dir1/dir2/fn.jog",
        "bitmap_url":"http://image_server/f1.jpg",
        "reviewed": false,
        "tags":["tag1","tag2","tag3"],
        "suggested_tags":["stag1","stag2"]}   
    """
    tags = serializers.SerializerMethodField('_get_tags')
    suggested_tags = serializers.SerializerMethodField('_get_suggested_tags')
    bitmap_url = CustomImageURLField()
    data_url = CustomDataURLField(source='original_route')

    class Meta:
        model = ScopeImage
        fields = ('id', 'file_name', 'file_path', 'original_route', 'data_url', 'bitmap_url',
                  'dimensions', 'file_metadata', 'reviewed', 'tags', 'suggested_tags',
                  'proposal')

    def _get_tags(self, scope_image):
        return self._tranform_tags(scope_image.get_tags())

    def _get_suggested_tags(self, scope_image):
        return self._tranform_tags(scope_image.get_suggested_tags())

    def _tranform_tags(self, tags):
        return [x.tag for x in tags]


class ScopeImagesFolderSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScopeImagesFolder
        fields = ('route', 'image_count')


class TagSerializer(serializers.ModelSerializer):
    """
    Serializer for the ncem.models.Tag. It transforms the object in a string
    containing the Tag id.
    """

    class Meta:
        model = Tag
        fields = ('tag',)


class TaggedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tagged
        fields = ('id', 'tag', "scope_image", "strength")


class TagModificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagModification
        fields = ('id', 'action', "tagged", "session_id", "historic_index")


class TagUserAdditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagUserAddition
        fields = ('id', 'scope_image', "tag", "session_id", "historic_index")
