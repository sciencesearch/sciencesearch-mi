from django.core.management.base import BaseCommand
from metadataserver.apps.ncem.datamodel.transform.tags import simple_from_user_feedback


class Command(BaseCommand):
    """ Processes the user feedback in the database and reproduces the tags of
    an image accordingly. Feedback is stored as older.
    
    Applies user modifications to images that are reviewed using a simple
    model:
    - Modification for images that are not reviewed are ignored and stored.
    - Tags associated with an Scope Image are:
      - Those marked suggested or associated and marked by the user as valid.
      - Those new proposed by the user.
    
    Usage:
        
    python manage.py userprocess
        
    """

    def handle(self, *args, **options):
        simple_from_user_feedback()
