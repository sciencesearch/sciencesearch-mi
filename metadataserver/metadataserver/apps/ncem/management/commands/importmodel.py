from django.core.management.base import BaseCommand
from metadataserver.apps.ncem.datamodel.read.exportimport import DataExporter
from metadataserver.apps.ncem.management.commands.exportmodel import MODEL_TYPES


class Command(BaseCommand):
    """Imports the values of model objects from a CSV file. First row
    must be the name of the fields. Any field with the prefix 'key-field_'
    will be considered a key field. Each following row will correspond to an
    object that will be identified by the values of the key fields. Value fields
    will be written accordingly.
    
    Usage
    
    python manage.py importmodel [options] (model) (csv_file)
    
    Args:
      model: Name of the class model in lower case.
      csv_file: filename of the CSV where to read the values from.
    Options:
      --create: if no object identified by the key fields exist for a row, it
        tries to create it.
      --buffserize: byte size of the read buffer on the CSV file.
      --queuesize: size of the buffer betweent the file reader and the database
        writers.
      --numworkes: number of database writer processes to run in parallel.
        Default(1)
    
    
    """

    def add_arguments(self, parser):

        parser.add_argument(
            '--create', '-c',
            action="store_true",
            dest="create",
            default=False,
            help='If model object does not exist, it will try to create it',
            )

        parser.add_argument(
            '--buffersize', '-b',
            dest='buffer_size',
            default=100000000,
            help='Read buffer size',
            )

        parser.add_argument(
            '--queuesize', '-q',
            dest='queue_size',
            default=10000,
            help='Max buffer between DB reader and write.',
            )

        parser.add_argument(
            '--numworkers', '-n',
            dest='num_workers',
            default=1,
            help='Number of parallel workers.',
            )

        parser.add_argument('model', help="Model class to export",
                            choices=[x[1] for x in MODEL_TYPES])

        parser.add_argument('csv_file', help="Filename of output CSV file")

    def handle(self, *args, **options):
        processed_model = None
        for m in MODEL_TYPES:
            if m[1] == options["model"]:
                processed_model = m[0]
        self.stdout.write(
            """"Model export script:
            - Model to be exported: {}
            - CSV tags file: {}
            - Create objects: {}
            - Buffer size: {}
            - Intermediate buffer: {}
            - Num workers: {} 
            """.format(processed_model,
                           options["csv_file"],
                           options["create"],
                           options["buffer_size"],
                           options["queue_size"],
                           options["num_workers"]))

        de = DataExporter()
        de.import_data(options["csv_file"], processed_model,
                       create=options["create"],
                       buffer_size=int(options["buffer_size"]),
                       chunksize=int(options["queue_size"]),
                       num_workers=int(options["num_workers"]))
