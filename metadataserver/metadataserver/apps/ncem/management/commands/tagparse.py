from django.core.management.base import BaseCommand, CommandError
from metadataserver.apps.ncem.datamodel.read.tagparser import extract_tags
from metadataserver.apps.searchengine.configuration import METADATA_TYPES


class Command(BaseCommand):
    """ Commend to crawl over filesystem directories listing files, generating
    and storing their corresponding ScopeImage object and a bitpmap file that
    is browser readable.
    
    Usage:
        
    python manage.py tagparse [options] (csv_tags_file)
        
    Args:
        csv_tags_file: csv file containing tags for files.
    
    Options:
        --listonly: if present, tags are not imported but a list of the image
          full routes is printed.
        --basefolder (router): file system route to be considered the root of
          the files referenced in the CSV file. This route will be subtracted
          when producing the image id to matach with an image in the DB.
        --noduplicates: If set, the system will not allow for tag appears more
          than once for image in the CSV file.
        --suggestedtags: If included, tags in the CSV file are inserted as
          suggeted.     
        --simple: If present, the expected CSV format will be:
          file_route, tag1, tag2, ...
    """
    help = 'Imports tags from a file'

    def add_arguments(self, parser):

        parser.add_argument(
            '--metadatatype', '-m',
            dest='metadata_type',
            default="unknown",
            choices=[x[1] for x in METADATA_TYPES],
            help='Sets the type of source where the tags were extracted from.'
            )

        parser.add_argument(
            '--listonly', '-l',
            action='store_true',
            dest='list_only',
            default=False,
            help='If present the tags are not inserted in the database but'
                 ' a list of referenced files is produced.',
            )

        parser.add_argument(
            '--basefolder', '-b',
            dest='base_folder',
            default="",
            help='Folder to be considered "root" of the files in the csv.'
                 ' It has to be a parent route to the location in the csv. It will'
                 ' be substracted from the parsed routes to identify the images in'
                 ' the db,'
            )
        parser.add_argument(
            '--noduplicates', '-n',
            dest='support_duplicates',
            default=True,
            help='If set, the system will not allow for tag appears more than once for'
                 ' image in the CSV file.'
            )

        parser.add_argument(
            '--suggestedtags', '-s',
            action='store_true',
            dest='suggested',
            default=False,
            help='If present the tags are inserted in the database as suggested tags.'
            )

        parser.add_argument(
            '--simple', '-i',
            action='store_true',
            dest='simple_format',
            default=False,
            help='If present, the expected CSV format will be: file_route, '
                 'tag1, tag2, ...'
            )

        parser.add_argument('csv_tags_file', help="File system"
                                                  " route where the tags csv file is.")

    def handle(self, *args, **options):
        self.stdout.write(
            """"NCEM crawling script:
            - CSV tags file: {}
            - List only: {}
            - Base folder: {}
            - Suggested tags: {}
            - Metadata Type: {}
            """.format(options["csv_tags_file"],
                           options["list_only"],
                           options["base_folder"],
                           options["suggested"],
                           options["metadata_type"]))
        metadata_type_processed = None
        for m in METADATA_TYPES:
            if m[1] == options["metadata_type"]:
                metadata_type_processed = m[0]
        if metadata_type_processed is None:
            raise CommandError("Poblem parsing the metadata type: {}".format(
                options["metadata_type"]))

        extract_tags(options["csv_tags_file"],
                     base_route=options["base_folder"],
                     list_only=options["list_only"],
                     support_duplicates=options["support_duplicates"],
                     suggested=options["suggested"],
                     simple_format=options["simple_format"],
                     metadata_type=metadata_type_processed)
