from django.core.management.base import BaseCommand, CommandError

from metadataserver.apps.ncem.datamodel.read.exportimport import DataExporter
from metadataserver.apps.ncem.models import (
    ScopeImage, Tag, Tagged, TagModification, TagUserAddition
    )
from metadataserver.apps.ncemreldata.models import (Proposal, CalendarEntry)
from metadataserver.apps.papermanager.models import (Author, Paper)
from metadataserver.apps.searchengine.models import (
    ScopeImageMetadata, ProposalMetadata, PaperMetadata, SearchResultArchive,
    SearchResultSummaryArchive, SearchResultSummary, SearchResult
    )

MODEL_TYPES = [(ScopeImage, "scopeimage"),
               (Tag, "tag"),
               (Tagged, "tagged"),
               (TagModification, "tagmodification"),
               (TagUserAddition, "taguseraddition"),
               (Proposal, "proposal"),
               (CalendarEntry, "calendarentry"),
               (ScopeImageMetadata, "scopeimagemetadata"),
               (ProposalMetadata, "proposalmetadata"),
               (PaperMetadata, "papermetadata"),
               (SearchResultArchive, "searchResultarchive"),
               (SearchResultSummaryArchive, "searchresultsummaryarchive"),
               (SearchResultSummary, "searchresultsummary"),
               (SearchResult, "searchresult"),
               (Author, "author"),
               (Paper, "paper")]

"""
We need to be able "key fields" for the import export

"""


class Command(BaseCommand):
    """ Exports the values of the objects of a model to CSV file. 
    The file will include a first row with the name of the columns. Then, 
    one row per object. Columns are the key fields and value fields of
    each object.
    
    Usage 
    
    python manage.py exportcmodel [options] (model) (csv_file)
    
    Args:
      model: Name of the class model in lower case.
      csv_file: filename of the CSV where to write the values.
    Options:
      --fields f1 f2 3: name of the value fields to export in the CSV.
      --keyfield f1, f2: name of the fields that will be used to identify the
        objects. If not specified it uses the primary key.
      --rowsperwrite n: will write n rows at the same time in the CSV file
        (buffering options)
    """

    def add_arguments(self, parser):
        parser.add_argument(
            '--fields', '-f',
            dest='fields',
            default=[],
            help='List of data fields to be exported',
            nargs='+',
            )

        parser.add_argument(
            '--keyfields', '-k',
            dest='key_fields',
            default=[],
            help='List of key fields to be exported',
            nargs='+',
            )

        parser.add_argument(
            '--rowsperwrite', '-r',
            dest='rows_per_write',
            default=10000,
            help='Number of rows to read before dumping them in the file',
            )

        parser.add_argument('model', help="Model class to export",
                            choices=[x[1] for x in MODEL_TYPES])

        parser.add_argument('csv_file', help="Filename of output CSV file")

    def handle(self, *args, **options):
        processed_model = None
        for m in MODEL_TYPES:
            if m[1] == options["model"]:
                processed_model = m[0]
        self.stdout.write(
            """"Model export script:
            - Model to be exported: {}
            - CSV tags file: {}
            - key fields: {}
            - Data fields: {}
            - Rows per write: {}
            """.format(processed_model,
                           options["csv_file"],
                           options["key_fields"],
                           options["fields"],
                           options["rows_per_write"]))

        if not options["fields"]:
            raise CommandError("At least one data field' must"
                               " be specified.")

        de = DataExporter()
        de.export(options["csv_file"], processed_model, options["fields"],
                  options["key_fields"],
                  buffer_size=int(options["rows_per_write"]))
