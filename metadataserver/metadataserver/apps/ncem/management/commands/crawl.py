from django.core.management.base import BaseCommand, CommandError

from metadataserver.apps.ncem.datamodel.read.crawler import crawl
from metadataserver.apps.ncemreldata.models import (INSTRUMENTS, INSTRUMENT_TEAM_I)


class Command(BaseCommand):
    """ Commend to crawl over filesystem directories listing files, generating
    and storing their corresponding ScopeImage object and a bitpmap file that
    is browser readable.
    
    Usage:
        
    python manage.py crawl [options] (crawling_source) (target_folder)
    
    Args:
        crawling_source: file or directory to parse.
        target_folder: directory where to store the bitmaps created from
            the original files.
    Options:
        --recursive: if crawling_source is a directory crawling continues in
          sub-folders.
        --extensions (ext list): if crawling_source is a directory, list of file
          extensions to process.
        --basefolder (router): file system route to be considered the root of
          the crawl. e.g., python manage.py dir1/dir2/dir3 --based_folder dir1/
              implies that the full router of files of dir3 is "dir2/dir3/x"
        --outformat (format): format of the ouptut images. 
        --prefixurl (URL): string to prepend to the bitmap url in the
          ImageScope object.
        --duplicates: if present, duplicated origin_routes are allowed and 
          inserted.  
        --instrument: string identifying the NCEM instrument which the data
          corresponds to.  
        --statsonly: if set, it crawls the images, do not store any data in the
          db or create preview images, but produces stats on
          the number of images which date was parsed correctly.
        --nocontrastscale: If present DM3/DM4 images contrast will not be
          scaled. Contrast scaling improves images with single pixels that are
          much more brighter than the rest.
        --thumbnailres: Sets the resolution for image thumbnails (not the
          extracted images). If set to 0, no thumbnails are generated
    """
    help = 'Explores a folder extract information of images'

    def _get_default_exts(self):
        return self._get_supported_exts()

    @staticmethod
    def _get_supported_exts():
        return ["dm3", "dm4", "ser"]

    @staticmethod
    def _get_supported_image_formats():
        return ["png", "jpg", "tiff"]

    def _get_default_image_format(self):
        return self._get_supported_image_formats()[0]

    def add_arguments(self, parser):
        parser.add_argument(
            '--recursive', '-r',
            action='store_true',
            dest='recursive',
            default=False,
            help='If present the crawl will process sub-directories'
                 ' recursively.',
            )

        parser.add_argument(
            '--duplicates', '-d',
            action='store_true',
            dest='allow_duplicates',
            default=False,
            help='If present the crawl will allow to insert NCEM images with'
                 'with the same original route as other image present in the db',
            )

        parser.add_argument(
            '--basefolder', '-b',
            dest='base_folder',
            default="",
            help='Folder to be considered "root" of the crawl. It has to be'
                 ' a parent route to the location of the "crawling folder". Default'
                 ' is the PWD.'
            )

        parser.add_argument(
            '--extensions', '-e',
            dest='extensions',
            nargs='+',
            choices=self._get_supported_exts(),
            default=self._get_default_exts(),
            help='Extensions of the files to be processed. Default: {}'
                 ''.format(self._get_default_exts())
            )

        parser.add_argument(
            '--outformat', '-o',
            dest='image_format',
            choices=self._get_supported_image_formats(),
            default=self._get_default_image_format(),
            help='Image format of the output images. Default "{}".'
                 ' Supported formats: {}'.format(self._get_default_image_format(),
                                                 self._get_supported_image_formats())
            )

        parser.add_argument(
            '--prefixurl', '-p',
            dest='urlprefix',
            default=None,
            help='The string will be added as a prefix to the image urls',
            )

        parser.add_argument(
            '--instrument', '-i',
            dest='instrument',
            nargs="?",
            choices=self._get_instruments(),
            default=INSTRUMENT_TEAM_I,
            help='NCEM instrument associated to the crawled images.'
                 ''.format(self._get_instruments())
            )

        parser.add_argument('--thumbnailres', '-t',
                            dest='thumbnail_res',
                            default="300x300",
                            help='Sets the resolution for image thumbnails'
                                 ' (not the extracted images). If set to 0, no'
                                 ' thumbnails are generated.')

        parser.add_argument(
            '--statsonly', '-s',
            action='store_true',
            dest='stats_only',
            default=False,
            help='If present the crawl only parses images and checks how many'
                 ' Acquisition Dates can be extracted.',
            )

        parser.add_argument('--nocontrastscale', '-n',
                            action='store_true',
                            dest='no_contrast_scale',
                            default=False,
                            help='If present, DM3/DM4 images contrast will'
                                 ' not be scaled. Contrast scaling improves images'
                                 ' with single pixels that are much more brighter'
                                 ' than the rest.')

        parser.add_argument('--noimagecaching', '-c',
                            action='store_true',
                            dest='no_image_caching',
                            default=False,
                            help='If present, DM3/DM4 images will not be'
                                 ' preloaded in memory for processing.')

        parser.add_argument('crawling_source', help="File system"
                                                    " route where the file crawl will happen.")

        parser.add_argument('target_folder', help="File system"
                                                  " route where the file output images will be stored")

    @staticmethod
    def _get_instruments():
        return [x[0] for x in INSTRUMENTS]

    @staticmethod
    def parse_thumbnail_res(thumbnail_res):
        if thumbnail_res == 0:
            return None

        thumbnail_res = thumbnail_res.lower()
        res = thumbnail_res.split("x")
        if len(res) != 2:
            raise CommandError("Bad format in thumbnail resolution")
        try:
            res[0] = int(res[0])
            res[1] = int(res[1])
            if res[0] <= 0 or res[1] <= 0:
                raise CommandError("Zero or negative resolution not allowed")
            return res[0], res[1]
        except ValueError:
            raise CommandError("Bad format in thumbnail resolution")

    def handle(self, *args, **options):
        thumbnail_res = self.parse_thumbnail_res(options["thumbnail_res"])

        self.stdout.write(
            """"NCEM crawling script:
            - Input folder: {}
            - Output imgs folder: {}
            - Base folder: {}
            - Extensions processed: {}
            - Output image format: {}
            - Url Prefix: {}
            - Recursive search: {}
            - Allow duplicates: {}
            - Thumbail res: {}
            - Disable contrast scaling: {}
            - Disable dm3/4 image caching: {}""".format(
                options["crawling_source"],
                options["target_folder"],
                options["base_folder"],
                options["extensions"],
                options["image_format"],
                options["urlprefix"],
                options["recursive"],
                options["allow_duplicates"],
                thumbnail_res,
                options["no_contrast_scale"],
                options["no_image_caching"]))

        crawl(options["crawling_source"],
              options["target_folder"],
              base_folder=options["base_folder"],
              extensions=options["extensions"],
              img_format=options["image_format"],
              url_prefix=options["urlprefix"],
              recursive=options["recursive"],
              allow_duplicates=options["allow_duplicates"],
              instrument=options["instrument"],
              only_measure=options["stats_only"],
              no_contrast_scale=options["no_contrast_scale"],
              thumbnail_res=thumbnail_res,
              no_image_caching=options["no_image_caching"]
              )

#         for poll_id in options['poll_id']:
#             try:
#                 poll = Poll.objects.get(pk=poll_id)
#             except Poll.DoesNotExist:
#                 raise CommandError('Poll "%s" does not exist' % poll_id)
# 
#             poll.opened = False
#             poll.save()
# 
#             self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
