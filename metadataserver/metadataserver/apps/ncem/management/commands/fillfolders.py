import logging
from django.core.management.base import BaseCommand

from metadataserver.apps.ncem.datamodel.transform.imagefolders import ScopeImageFolderGenerator

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--depth', '-d',
            dest='depth',
            default=3,
            type=int,
            help='Number of folder to analyze in the routes',
            )

        parser.add_argument(
            '--update', '-u',
            action='store_true',
            dest='update',
            default=False,
            help='If present the prexisting folders are not delete and but are '
                 'updated.',
            )

    def handle(self, *args, **options):
        clean_existing = not options["update"]
        depth = options["depth"]

        fg = ScopeImageFolderGenerator()
        number_folders = fg.scan_folders(depth)
        logger.info("{} distinct folders detected".format(number_folders))
        fg.dump_to_model(clean_existing)
        logger.info("Data stored in database")
