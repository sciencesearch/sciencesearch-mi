from metadataserver.apps.ncem.views import browsable as browsable_views
from metadataserver.apps.ncem.views import debug as debug_views

from django.conf.urls import url, include
from rest_framework import routers


ncem_router = routers.DefaultRouter()
ncem_router.register(r'images', browsable_views.ScopeImageViewSet,
                     base_name='images')

ncem_tags_router = routers.DefaultRouter()
ncem_tags_router.register(r'tags',                     
                      browsable_views.TagsViewSet, base_name='tags')
ncem_tags_router.register(r'suggested-tags',                     
                      browsable_views.SuggestedTagsViewSet,
                      base_name='suggested_tags')

ncem_router.register(r'folders', browsable_views.ScopeImagesFolderViewSet,
                     base_name='folders')


ncem_debug_router  = routers.DefaultRouter()
ncem_debug_router.register(r'scope_images',                     
                      browsable_views.ScopeImageViewSet,
                      base_name='debug-images')
ncem_debug_router.register(r'tags',                     
                      debug_views.TagsViewSet, base_name='debug-tags')
ncem_debug_router.register(r'reltagsimgs',                     
                      debug_views.TagRelationshipSet,
                      base_name='debug-reltagsimgs')
ncem_debug_router.register(r'tagmodifications',                     
                      debug_views.TagModificationSet,
                      base_name='debug-tagmodifications')
ncem_debug_router.register(r'taguseradditions',                     
                      debug_views.TagUserAdditionSet,
                      base_name='debug-taguseradditions')

                     
# #ncem_router.register(r'alt/.(?P<imageid>[a-z0-9]+)/tags/',



urlpatterns = [
    url(r'', include(ncem_router.urls)), 
    url(r'images/(?P<imageid>[a-z0-9]+)/',
        include(ncem_tags_router.urls)),
    url(r'debug/', include(ncem_debug_router.urls)), 
]