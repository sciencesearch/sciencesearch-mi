import datetime
import logging
import cProfile
import pstats
import io


class TM(object):

    def __init__(self, msg="", w=None, stats=False):
        self._logger = logging.getLogger()
        self._profiler = cProfile.Profile()
        self._start_time = None
        self._stop_time = None
        self._msg = msg
        self._worker = w
        self._stats = stats

    def start(self):
        self._stop_time = None
        self._start_time = datetime.datetime.now()
        if self._stats:
            self._profiler.enable()
        return self

    def stop(self, report=True):
        if self._stats:
            self._profiler.disable()
        self._stop_time = datetime.datetime.now()
        seconds = self.get_time_s()
        if report:
            worker_s = ""
            if self._worker is not None:
                worker_s = "W({}): ".format(self._worker)
            self._logger.info("{}Op({}) took {}s ".format(worker_s, self._msg, seconds))

            if self._stats:
                formatted_stats = io.StringIO()
                sortby = ['time', 'cumulative', 'calls']
                profiler_stats = pstats.Stats(self._profiler, stream=formatted_stats).sort_stats(*sortby)
                profiler_stats.print_stats(.1)
                self._logger.debug(formatted_stats.getvalue())
        return seconds

    def get_time_s(self):
        if self._start_time is None or self._stop_time is None:
            return False
        delta = (self._stop_time - self._start_time)
        return float(delta.seconds) + float(delta.microseconds) / 1000000
