import csv
from itertools import zip_longest


def transform_values_to_columns(dic_of_lists,
                                dic_order=None, do_headers=True,
                                value_size=2, space_size=1):
    """ Maps a dictionary of lists in a list of rows that can be
    written as a CSV values:
      - Mapping each dictionary vertically in the rows of rows.
      - Leaving spaces between the columns corresponding to each dictionary.
      - Filling columns with empty values so all rows have the same length.
      - The first row of the list of rows includes the name(key) of each list.
      
    
    Args:
        dic_of_lists: dictionary of lists of values of the same shape. 
        dic_order: Is set to None, items of the dictionary are listed in
          random oder. If set to a list of the keys, dictionary elements
          will be mapped in the indicated order.
        do_headers: if true, the first row includes the name (key) of the
          items in the dictionary.
        value_size = number if sub items in each item of the lists.
        space_size = number of empty values to separate columns in rows.
    Returns: A list of lists including the elemeents of the the dict_of_lists
      as vertical rows and a first row with the name of the items.        
    """

    fill_value = ()
    for i in range(value_size):
        fill_value += ("",)

    space_value = ()
    for i in range(space_size):
        space_value += ("",)
    space_value = list(space_value)

    space_value_first_row = space_value + ([""] * (value_size - 1))

    if dic_order is None:
        dic_order = dic_of_lists.keys()
    count_dic_list = [dic_of_lists[x] for x in dic_order]

    rows = []
    if do_headers:
        first_row = []
        for d_name in dic_order:
            first_row.append(d_name)
            first_row += space_value_first_row
        rows.append(first_row)

    for (item) in zip_longest(*count_dic_list, fillvalue=fill_value):
        row = []
        for sub_item in item:
            for part in sub_item:
                row.append(part)
            row += space_value
        rows.append(row)
    return rows


def save_to_csv(rows, file_name, append=False):
    """ Saves a list of rows of rows to a CSV file.
    """
    open_string = 'w'
    if append:
        open_string = 'a'
    with open(file_name, open_string) as csvfile:
        cwriter = csv.writer(csvfile, delimiter=',',
                             quotechar="'", quoting=csv.QUOTE_MINIMAL)
        for row in rows:
            cwriter.writerow(row)
