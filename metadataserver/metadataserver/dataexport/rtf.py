from PyRTF.Elements import Document
from PyRTF.PropertySets import ParagraphPropertySet
from PyRTF.Renderer import Renderer
from PyRTF.document.paragraph import Paragraph
from PyRTF.document.section import Section


def export_report(filename, model_instances, fields, tags, head_field="title"):
    """ Writes a report on the tags of model instances.
    Args:
        file_name: string pointing to the location where the file should
          be written at.
        model_instances: Dictionary of model instances indexed by their key
          attribute. {"item_id":<model object>, ..}
        fields: list of strings with the names of fields to export in ORDER.
        tags: Dictionary of lists of strings indexed by the ids of the
          model_instances. {"item_id": ["tag1", "tag2"..}, ..}
        
    """
    doc = Document()
    ss = doc.StyleSheet
    is_first_page = True
    for key, instance in model_instances.items():
        section = Section()
        doc.Sections.append(section)

        if not is_first_page:
            p = Paragraph(ParagraphPropertySet().SetPageBreakBefore(True))
            section.append(p)
        else:
            is_first_page = False

        if head_field in fields:
            p = Paragraph(ss.ParagraphStyles.Heading2,
                          str(getattr(instance, head_field)))
            section.append(p)
        p = Paragraph(ss.ParagraphStyles.Normal, "ID: {}".format(key))
        section.append(p)
        for field_name in fields:
            if field_name == head_field:
                continue
            text = str(getattr(instance, field_name))
            section.append("- {}: {}".format(field_name, text))

        p = Paragraph(ss.ParagraphStyles.Heading2,
                      "Detected labels (from more to less important):")
        section.append(p)
        section.append(Paragraph(ss.ParagraphStyles.Normal))
        section.append(", ".join(tags[key]))
        section.append("")
        section.append("")

    with open(filename, "w") as f:
        DR = Renderer()
        DR.Write(doc, f)
