import json

import numpy


class SafeJSONEncodeNumpy(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.integer):
            return int(obj)
        elif isinstance(obj, numpy.floating):
            return float(obj)
        elif isinstance(obj, numpy.ndarray):
            return obj.tolist()
        elif isinstance(obj, numpy.bytes_):
            return str(obj)
        else:
            try:
                return super(SafeJSONEncodeNumpy, self).default(obj)
            except TypeError as e:
                return str(obj)


def make_dict_serializeable(source_dict):
    transformed_text = json.dumps(source_dict, cls=SafeJSONEncodeNumpy,
                                  ensure_ascii=False)
    for (orig, dest) in [(': -Infinity', ': "-Infinity"'),
                         (': Infinity', ': "Infinity"'),
                         (': Nan', ': "NaN"'),
                         ("\\u0000", "")]:
        transformed_text = transformed_text.replace(orig, dest)
    return json.loads(transformed_text)
