import os
from collections import namedtuple

from metadataserver.dataexport.rtf import export_report
from metadataserver.seminfer.tests import TestFileGeneric


class TestExportReport(TestFileGeneric):
    def setUp(self):
        self._register_tmp_folder("tmp_rtf")
        self._create_empty("tmp_rtf", True)

    def test_export_report(self):
        MyObj = namedtuple('MyObj', ['title', 'field1', 'field2'])

        model_instaces = {
            "id1": MyObj(title="Title 1",
                         field1="Some text 1-1",
                         field2="Some text 1-2"),
            "id2": MyObj(title="Title 2",
                         field1="Some text 2-1",
                         field2="Some text 2-2"),
            "id3": MyObj(title="Title 3",
                         field1="Some text 3-1",
                         field2="Some text 3-2")
        }
        tags = {
            "id1": ["tagid1-1", "tagid1-2"],
            "id2": ["tagid2-1", "tagid2-2", "tagid2-3"],
            "id3": ["tagid3-1", "tagid3-2", "tagid3-3"]
        }

        export_report("tmp_rtf/test_file.rtf",
                      model_instaces, ["title", "field1", "field2"], tags)
        self.assertTrue(os.path.isfile("tmp_rtf/test_file.rtf"))
