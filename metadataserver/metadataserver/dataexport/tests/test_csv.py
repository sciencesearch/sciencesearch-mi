from django.test import TestCase

from metadataserver.dataexport.csv import transform_values_to_columns


class TestTransform(TestCase):

    def test_transform_values_to_columns(self):
        count_dic_list = {
            "d1": [("a", 1), ("b", 2), ("c", 3)],
            "d2": [("a", 4)]
        }

        result = transform_values_to_columns(count_dic_list, do_headers=False)

        self.assertEqual(result,
                         [["a", 1, "", "a", 4, ""],
                          ["b", 2, "", "", "", ""],
                          ["c", 3, "", "", "", ""],
                          ]
                         )

        result = transform_values_to_columns(count_dic_list, do_headers=True)

        self.assertEqual(result,
                         [["d1", "", "", "d2", "", ""],
                          ["a", 1, "", "a", 4, ""],
                          ["b", 2, "", "", "", ""],
                          ["c", 3, "", "", "", ""],
                          ])

        result = transform_values_to_columns(count_dic_list, do_headers=True,
                                             space_size=2)

        self.assertEqual(result,
                         [["d1", "", "", "", "d2", "", "", ""],
                          ["a", 1, "", "", "a", 4, "", ""],
                          ["b", 2, "", "", "", "", "", ""],
                          ["c", 3, "", "", "", "", "", ""],
                          ])

        count_dic_list = {
            "d1": [("a", 1, 0), ("b", 2, 0), ("c", 3, 0)],
            "d2": [("a", 4, 0)]
        }

        result = transform_values_to_columns(count_dic_list, do_headers=False,
                                             value_size=3)

        self.assertEqual(result,
                         [["a", 1, 0, "", "a", 4, 0, ""],
                          ["b", 2, 0, "", "", "", "", ""],
                          ["c", 3, 0, "", "", "", "", ""],
                          ]
                         )

    def test_transform_values_order(self):
        count_dic_list = {
            "d1": [("a", 1), ("b", 2), ("c", 3)],
            "d2": [("a", 4)]
        }
        result = transform_values_to_columns(count_dic_list, do_headers=True,
                                             dic_order=["d2", "d1"])

        self.assertEqual(result,
                         [["d2", "", "", "d1", "", ""],
                          ["a", 4, "", "a", 1, ""],
                          ["", "", "", "b", 2, ""],
                          ["", "", "", "c", 3, ""],
                          ]
                         )
