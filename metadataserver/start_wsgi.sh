#!/bin/bash
#
# Start metadata server in WSGI mode.

site_name="metadataserver"

if [ -z "$WSGI_TIMEOUT" ]
then 
    export WSGI_TIMEOUT=90
fi

if [ -z "$WSGI_PROCESSES" ]
then 
    export WSGI_PROCESSES=5
fi

uwsgi --chdir=/code \
--module=${site_name}.wsgi:application \
--env DJANGO_SETTINGS_MODULE=${site_name}.settings \
--master --pidfile=/tmp/project-master.pid \
--reaper \
--http=0.0.0.0:8000 \
--http-timeout=3600 \
--processes="${WSGI_PROCESSES}" \
--harakiri="${WSGI_TIMEOUT}" \
--max-requests=5000 \
--vacuum \
--enable-threads \
--lazy-apps

#--uid=1000 --gid=2000 \
#--daemonize=/var/log/uwsgi/yourproject.log      # background the process
