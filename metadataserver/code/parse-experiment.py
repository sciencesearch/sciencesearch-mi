import sys
import pandas as pd
import numpy as np
import multiprocessing as mp
from datetime import datetime

MSG = "Usage: python3 parse-experiment.py <experiment dir> <output dir>"

def calc_time(timestr):
    timeint = 0
    
    hhmmss = timestr.split(":")
    timeint += int(hhmmss[0]) * 60 * 60
    timeint += int(hhmmss[1]) * 60
    timeint += int(hhmmss[2])

    return timeint


def test_iteration(line2, iteration_seq, iteration_test):
    num_workers = int(line2[5])
    
    res_iteration_seq = iteration_seq
    res_iteration_test =  iteration_test
    if iteration_test == 1 and num_workers != 1:
        res_iteration_test = 0
    elif iteration_test == 0 and num_workers == 1:
        res_iteration_seq = iteration_seq + 1
        res_iteration_test = 1

    return (res_iteration_seq, res_iteration_test)


def aggregate_main_lines(line1, line2, line3, experiment_id, iteration_seq):
    query_term = line2[3][1:-1]
    num_workers = int(line2[5])
    elapsed_time = float(line2[7])
    
    start_time = calc_time(line1[6])
    end_time = calc_time(line3[6])

    agg_data = [experiment_id, iteration_seq, query_term, num_workers, elapsed_time, start_time, end_time]
    
    return agg_data


def append_data(data, agg_data, columns):
    for i in range(0, len(columns)):
        data[columns[i]].append(agg_data[i])


def extract_main_data(inputdir, outputdir):
    columns = ["experiment_id", "iteration_seq", "query_term", "num_workers", "elapsed_time", "start_time", "end_time"]
    main_data_pd = pd.DataFrame(columns=columns)

    data = dict()
    for column in columns:
        data[column] = []

    filename = inputdir + "/info.main.log.clean"
    with open(filename, "r") as filein:
        experiment_id = 1
        iteration_seq = 1
        iteration_test = 1
        line_id = 0
        line_id_reset = 3
        
        line1 = None
        line2 = None
        line3 = None
        for line in filein:
            if line_id == 0:
                line1 = line.replace("\n", "").split()
                line_id = (line_id + 1) % line_id_reset
            elif line_id == 1:
                line2 = line.replace("\n", "").split()
                line_id = (line_id + 1) % line_id_reset
            elif line_id == 2:
                line3 = line.replace("\n", "").split()
                line_id = (line_id + 1) % line_id_reset
                (iteration_seq, iteration_test) = test_iteration(line2, iteration_seq, iteration_test)
                agg_data = aggregate_main_lines(line1, line2, line3, experiment_id, iteration_seq)
                append_data(data, agg_data, columns)
                experiment_id += 1

        if line_id != 0:
            print("WARNING: The number of lines in info.main.log is not divisible by 3!")
    
    filename = outputdir + "/info.main.csv"
    main_data_pd = main_data_pd.append(pd.DataFrame(data))
    main_data_pd.to_csv(filename)

    return main_data_pd


def extract_cpumem_data(inputdir, outputdir):
    return None


def extract_one_query_data(inputdir, main_data_pd, experiment, detailed_columns):
    detailed_query_columns = detailed_columns[0]
    detailed_worker_columns = detailed_columns[1]

    data_query = dict()
    for column in detailed_query_columns:
        data_query[column] = []
    
    data_worker = dict()
    for column in detailed_worker_columns:
        data_worker[column] = []
    
    filename = inputdir + "/info.queries." + str(experiment[1]) + "t.log"
    print(filename)
    with open(filename, "r") as filein:
        for line in filein:
            if line.find("Op(Workers Section") != -1:
                words = line.replace("\n", "").split()

                timestamp = calc_time(words[2].split(',')[0])
                ids = main_data_pd[(main_data_pd.start_time <= timestamp) & (timestamp <= main_data_pd.end_time)]\
                        ['experiment_id'].values
                
                if len(ids) == 0:
                    print("INFO: " + line.replace("\n", ""))
                    print("WARNING: timestamp " + str(timestamp) + " outside of any experiment ID!")
                    continue
                
                if len(ids) > 1:
                    print("WARNING: timestamp matching multiple experiment IDs!")
                
                experiment_id = ids[0]
                query_term = words[7][1:-1]
                value = float(words[11][:-1])

                append_data(data_query, 
                        [timestamp, experiment_id, query_term, 'workersT', value], 
                        detailed_query_columns)
            elif line.find("Op(Combine results") != -1:
                words = line.replace("\n", "").split()
                
                timestamp = calc_time(words[2].split(',')[0])
                ids = main_data_pd[(main_data_pd.start_time <= timestamp) & (timestamp <= main_data_pd.end_time)]\
                        ['experiment_id'].values
                
                if len(ids) == 0:
                    print("INFO: " + line.replace("\n", ""))
                    print("WARNING: timestamp " + str(timestamp) + " outside of any experiment ID!")
                    continue
                
                if len(ids) > 1:
                    print("WARNING: timestamp matching multiple experiment IDs!")
                
                experiment_id = ids[0]
                query_term = words[8][1:-1]
                value = float(words[12][:-1])

                append_data(data_query, 
                        [timestamp, experiment_id, query_term, 'combineT', value], 
                        detailed_query_columns)
            elif line.find("Op(Flatten for query") != -1:
                words = line.replace("\n", "").split()
                
                timestamp = calc_time(words[2].split(',')[0])
                ids = main_data_pd[(main_data_pd.start_time <= timestamp) & (timestamp <= main_data_pd.end_time)]\
                        ['experiment_id'].values
                
                if len(ids) == 0:
                    print("INFO: " + line.replace("\n", ""))
                    print("WARNING: timestamp " + str(timestamp) + " outside of any experiment ID!")
                    continue
                
                if len(ids) > 1:
                    print("WARNING: timestamp matching multiple experiment IDs!")
                
                experiment_id = ids[0]
                query_term = words[7][1:-1]
                value = float(words[11][:-1])

                append_data(data_query, 
                        [timestamp, experiment_id, query_term, 'flattenT', value], 
                        detailed_query_columns)
            elif line.find("Op(Full Search Parallel") != -1:
                words = line.replace("\n", "").split()

                timestamp = calc_time(words[2].split(',')[0])
                ids = main_data_pd[(main_data_pd.start_time <= timestamp) & (timestamp <= main_data_pd.end_time)]\
                        ['experiment_id'].values
                
                if len(ids) == 0:
                    print("INFO: " + line.replace("\n", ""))
                    print("WARNING: timestamp " + str(timestamp) + " outside of any experiment ID!")
                    continue
                
                if len(ids) > 1:
                    print("WARNING: timestamp matching multiple experiment IDs!")
                
                experiment_id = ids[0]
                query_term = words[8][1:-1]
                value = float(words[12][:-1])

                append_data(data_query, 
                        [timestamp, experiment_id, query_term, 'totalT', value], 
                        detailed_query_columns)
            elif line.find("Op(Retrieve data for query") != -1:
                words = line.replace("\n", "").split()
                
                timestamp = calc_time(words[2].split(',')[0])
                ids = main_data_pd[(main_data_pd.start_time <= timestamp) & (timestamp <= main_data_pd.end_time)]\
                        ['experiment_id'].values
                
                if len(ids) == 0:
                    print("INFO: " + line.replace("\n", ""))
                    print("WARNING: timestamp " + str(timestamp) + " outside of any experiment ID!")
                    continue
                
                if len(ids) > 1:
                    print("WARNING: timestamp matching multiple experiment IDs!")
                
                experiment_id = ids[0]
                worker_id = int(words[3][2:-2])
                value = float(words[13][:-1])

                append_data(data_worker, 
                        [timestamp, experiment_id, worker_id, 'indexRetrT', value], 
                        detailed_worker_columns)
            elif line.find("Op(Actual Data Retrieval") != -1:
                words = line.replace("\n", "").split()
                
                timestamp = calc_time(words[2].split(',')[0])
                ids = main_data_pd[(main_data_pd.start_time <= timestamp) & (timestamp <= main_data_pd.end_time)]\
                        ['experiment_id'].values
                
                if len(ids) == 0:
                    print("INFO: " + line.replace("\n", ""))
                    print("WARNING: timestamp " + str(timestamp) + " outside of any experiment ID!")
                    continue
                
                if len(ids) > 1:
                    print("WARNING: timestamp matching multiple experiment IDs!")
                
                experiment_id = ids[0]
                worker_id = int(words[3][2:-2])
                value = float(words[16][:-1])

                append_data(data_worker, 
                        [timestamp, experiment_id, worker_id, 'indexActT', value], 
                        detailed_worker_columns)
            elif line.find("Op(Query.get_hits for query") != -1:
                words = line.replace("\n", "").split()
                
                timestamp = calc_time(words[2].split(',')[0])
                ids = main_data_pd[(main_data_pd.start_time <= timestamp) & (timestamp <= main_data_pd.end_time)]\
                        ['experiment_id'].values
                
                if len(ids) == 0:
                    print("INFO: " + line.replace("\n", ""))
                    print("WARNING: timestamp " + str(timestamp) + " outside of any experiment ID!")
                    continue
                
                if len(ids) > 1:
                    print("WARNING: timestamp matching multiple experiment IDs!")
                
                experiment_id = ids[0]
                worker_id = int(words[3][2:-2])
                value = float(words[12][:-1])

                append_data(data_worker, 
                        [timestamp, experiment_id, worker_id, 'indexHitsT', value], 
                        detailed_worker_columns)
            elif line.find("Op(Recreate hits for query") != -1:
                words = line.replace("\n", "").split()
                
                timestamp = calc_time(words[2].split(',')[0])
                ids = main_data_pd[(main_data_pd.start_time <= timestamp) & (timestamp <= main_data_pd.end_time)]\
                        ['experiment_id'].values
                
                if len(ids) == 0:
                    print("INFO: " + line.replace("\n", ""))
                    print("WARNING: timestamp " + str(timestamp) + " outside of any experiment ID!")
                    continue
                
                if len(ids) > 1:
                    print("WARNING: timestamp matching multiple experiment IDs!")
                
                experiment_id = ids[0]
                worker_id = int(words[3][2:-2])
                value = float(words[13][:-1])

                append_data(data_worker, 
                        [timestamp, experiment_id, worker_id, 'expandT', value], 
                        detailed_worker_columns)
            elif line.find("Op(SearchableMark.rank_hits") != -1 and line.find("for query") != -1:
                words = line.replace("\n", "").split()
                
                timestamp = calc_time(words[2].split(',')[0])
                ids = main_data_pd[(main_data_pd.start_time <= timestamp) & (timestamp <= main_data_pd.end_time)]\
                        ['experiment_id'].values
                
                if len(ids) == 0:
                    print("INFO: " + line.replace("\n", ""))
                    print("WARNING: timestamp " + str(timestamp) + " outside of any experiment ID!")
                    continue
                
                if len(ids) > 1:
                    print("WARNING: timestamp matching multiple experiment IDs!")
                
                experiment_id = ids[0]
                worker_id = int(words[3][2:-2])
                value = float(words[14][:-1])

                append_data(data_worker, 
                        [timestamp, experiment_id, worker_id, 'rankT', value], 
                        detailed_worker_columns)
    
    return (data_query, data_worker)


def execute_extract_queries_data(inputdir, main_data_pd, input_queue, output_queue, detailed_columns):
    while True:
        experiment = input_queue.get()
        if experiment[0] == "terminate" and experiment[1] == -1:
            break
        detailed_data = extract_one_query_data(inputdir, main_data_pd, experiment, detailed_columns)
        output_queue.put(detailed_data)


def extract_queries_data(inputdir, outputdir, main_data_pd):
    #detailed_query_types = ['workersT', 'combineT', 'flattenT', 'totalT']
    #detailed_worker_types = ['indexRetrT', 'indexActT', 'indexHitsT', 'expandT', 'rankT']
    detailed_query_columns = ['timestamp', 'experiment_id', 'query_term', 'type', 'value']
    detailed_worker_columns = ['timestamp', 'experiment_id', 'worker_id', 'type', 'value']
    detailed_columns = (detailed_query_columns, detailed_worker_columns)
    detailed_query_data_pd = pd.DataFrame(columns=detailed_query_columns)
    detailed_worker_data_pd = pd.DataFrame(columns=detailed_worker_columns)

    experiments = main_data_pd[['num_workers']].drop_duplicates().values
    experiments = [('query_term', experiment[0]) for experiment in experiments]
    #experiments = experiments[:8]

    num_procs = 9
    procs = []
    input_queue = mp.Queue()
    output_queue = mp.Queue()
    
    for _ in range(0, num_procs):
        procs.append(mp.Process(target=execute_extract_queries_data, 
                args=(inputdir, main_data_pd, input_queue, output_queue, detailed_columns,)))
    
    for experiment in experiments:
        input_queue.put(experiment)
    
    for _ in range(0, num_procs):
        input_queue.put(["terminate", -1])
    
    for i in range(0, num_procs):
        procs[i].start()

    for _ in experiments:
        (detailed_query_data, detailed_worker_data) = output_queue.get()
        detailed_query_data_pd = detailed_query_data_pd.append(pd.DataFrame(detailed_query_data))
        detailed_worker_data_pd = detailed_worker_data_pd.append(pd.DataFrame(detailed_worker_data))

    for i in range(0, num_procs):
        procs[i].join()
    
    filename = outputdir + "/info.queries.detailed.csv"
    detailed_query_data_pd.to_csv(filename)

    filename = outputdir + "/info.workers.detailed.csv"
    detailed_worker_data_pd.to_csv(filename)
    
    return (detailed_query_data_pd, detailed_worker_data_pd)


def restructure_query_data(dedup_data_pd, experiment, data_columns):
    data = dict()
    for column in data_columns:
        data[column] = []
    
    experiment_id = experiment[0]
    experiment_query_term = experiment[1]
    experiment_num_workers = experiment[2]

    entries = dedup_data_pd[(dedup_data_pd.experiment_id == experiment_id)]\
            [['experiment_id', 'type', 'value']].values
    
    if len(entries) == 0:
        print("WARNING: zero query values match experiment!")
        return {}
    
    if len(entries) < (len(data_columns) - 3):
        print("WARNING: less than " + str(len(data_columns) - 3) + " query values match experiment!")
        return {}
    
    data['experiment_id'].append(experiment_id)
    data['query_term'].append(experiment_query_term)
    data['num_workers'].append(experiment_num_workers)
    for entry in entries:
        if len(data[entry[1]]) == 0:
            data[entry[1]].append(entry[2])
    
    for column in data_columns:
        if len(data[column]) == 0:
            return {}
    
    return data


def restructure_worker_data(dedup_data_pd, experiment, data_columns):
    data = dict()
    for column in data_columns:
        data[column] = []
    
    experiment_id = experiment[0]
    experiment_query_term = experiment[1]
    experiment_num_workers = experiment[2]

    entries = dedup_data_pd[(dedup_data_pd.experiment_id == experiment_id)]\
            [['experiment_id', 'worker_id', 'type', 'value']].values
    
    if len(entries) == 0:
        print("WARNING: zero worker values match experiment!")
        return {}
    
    if len(entries) < (len(data_columns) - 4):
        print("WARNING: less than " + str(len(data_columns) - 4) + " worker values match experiment!")
        return {}
    
    worker_ids = []
    for entry in entries:
        if entry[1] not in worker_ids:
            worker_ids.append(entry[1])
    
    worker_data = dict()
    for worker_id in worker_ids:
        worker_data[str(worker_id)] = dict()
        for column in data_columns[4:]:
            worker_data[str(worker_id)][column] = None

    for entry in entries:
        if worker_data[str(entry[1])][entry[2]] == None:
            worker_data[str(entry[1])][entry[2]] = entry[3]

    for worker_id in worker_ids:
        data['experiment_id'].append(experiment_id)
        data['query_term'].append(experiment_query_term)
        data['num_workers'].append(experiment_num_workers)
        data['worker_id'].append(worker_id)
        for column in data_columns[4:]:
            data[column].append(worker_data[str(worker_id)][column])
    
    return data


def restructure_queries_data(outputdir, main_data_pd, inter_data_pd):
    #detailed_query_types = ['experiment_id', 'query_term', 'num_workers', 'workersT', 'combineT', 'flattenT', 'totalT']
    #detailed_worker_types = ['experiment_id', 'query_term', 'num_workers', 'worker_id', 'indexRetrT', 'indexActT', 'indexHitsT', 'expandT', 'rankT']
    data_query_columns = ['experiment_id', 'query_term', 'num_workers', 'workersT', 'combineT', 'flattenT', 'totalT']
    data_worker_columns = ['experiment_id', 'query_term', 'num_workers', 'worker_id', 'indexRetrT', 'indexActT', 'indexHitsT', 'expandT', 'rankT']
    query_data_pd = pd.DataFrame(columns=data_query_columns)
    worker_data_pd = pd.DataFrame(columns=data_worker_columns)
    dedup_query_data_pd = inter_data_pd[0].drop_duplicates()
    dedup_worker_data_pd = inter_data_pd[1].drop_duplicates()

    experiments = main_data_pd[['experiment_id', 'query_term', 'num_workers']].drop_duplicates().values
    #experiments = experiments[:114]

    for experiment in experiments:
        print("experiment:"  + str(experiment[0]) + " (" + experiment[1] + " " + str(experiment[2]) + ")" )
        query_data = restructure_query_data(dedup_query_data_pd, experiment, data_query_columns)
        worker_data = restructure_worker_data(dedup_worker_data_pd, experiment, data_worker_columns)
        if query_data != {}:
            query_data_pd = query_data_pd.append(pd.DataFrame(query_data))
        if worker_data != {}:
            worker_data_pd = worker_data_pd.append(pd.DataFrame(worker_data))

    filename = outputdir + "/info.queries.csv"
    query_data_pd.to_csv(filename)

    filename = outputdir + "/info.workers.csv"
    worker_data_pd.to_csv(filename)

    return (query_data_pd, worker_data_pd)


def main(args):
    if len(args) != 3:
        print(MSG)
        sys.exit(1)

    inputdir = args[1]
    outputdir = args[2]

    mp.set_start_method('spawn')

    tstart = datetime.now()
    
    main_data_pd = extract_main_data(inputdir, outputdir)
    #cpumem_data_pd = extract_cpumem_data(inputdir, outputdir)
    inter_data_pd = extract_queries_data(inputdir, outputdir, main_data_pd)
    
    #inter_data_pd = []
    #filename = outputdir + "/info.queries.detailed.csv"
    #inter_data_pd.append(pd.read_csv(filename))
    #filename = outputdir + "/info.workers.detailed.csv"
    #inter_data_pd.append(pd.read_csv(filename))

    (query_data_pd, worker_data_pd) = restructure_queries_data(outputdir, main_data_pd, inter_data_pd)

    #print(query_data_pd)
    #print(worker_data_pd)
    
    tend = datetime.now()
    tdelta = tend - tstart

    print("Execution time: " + str(tdelta.seconds + (tdelta.microseconds / 1000000)) + " seconds")

    return 0


if __name__ == "__main__":
    main(sys.argv)
