import sys

MSG = "Usage: python3 correct-query-log.py <input file> <output file>"

def adjust(line, first_time, filout):
    words=line.replace("\n", "").split()
    if line.find("INFO 2020") != -1:
        if first_time == None:
            first_time = int(words[2].split(':')[0])
        else:
            if first_time > int(words[2].split(':')[0]):
                words[2] = str(int(words[2].split(':')[0]) + 24) \
                        + ':' + words[2].split(':')[1] \
                        + ':' + words[2].split(':')[2]
    outline = ""
    for word in words[:-1]:
        outline += word + " "
    if len(words) > 0:
        outline += words[-1] + "\n"
    filout.write(outline)
    return first_time


def main(args):
    if len(args) != 3:
        print(MSG)
        sys.exit(1)

    inputfile = args[1]
    outputfile = args[2]

    first_time = None

    with open(inputfile, 'r') as filin, open(outputfile, 'w') as filout:
        for line in filin:
            if line.find(")INFO") != -1:
                pos = line.find(")INFO")
                line1 = line[:pos+1]
                line2 = line[pos+1:]
                first_time = adjust(line1, first_time, filout)
                first_time = adjust(line2, first_time, filout)
            else:
                first_time = adjust(line, first_time, filout)

    return 0


if __name__ == "__main__":
    main(sys.argv)