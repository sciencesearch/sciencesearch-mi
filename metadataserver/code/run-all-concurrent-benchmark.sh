#!/bin/bash

# set query list and number of worker threads list
CONTAINER="metadataserver_web-backend_1"
QUERY_FILE="terms.txt"
CONCURRENCY_LIST="1 4 8 12 16 20 24 28 32"
ITERATIONS="1 2 3 4 5"
LOG_EXT="t.log"

for iteration in $ITERATIONS
do
    for concurrency in $CONCURRENCY_LIST
    do
        echo -n "Execution start date: "
        date -u
        
        # 1 run queries
        docker exec -it $CONTAINER bash -c "python snipet.py localhost $QUERY_FILE $concurrency"
        sleep 2

        echo -n "Execution end date: "
        date -u
    done
done

