import sys
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

from statistics import mean
from collections import defaultdict

import pandas as pd
import numpy as np

MSG = "Usage: python3 plot-throughput.py <input dir> <label> <output dir>"
NUM_QUERIES = 96
FACTOR = 60

def get_data(inputdir):
    data = dict()
    data["throughput"] = dict()
    data["throughput"]["max"] = []
    data["throughput"]["avg"] = []
    data["throughput"]["min"] = []
    
    throughput_data = pd.read_csv(inputdir + "/info.main.csv")
    
    procs = throughput_data['concurrency'].drop_duplicates().values.tolist()
    procs.sort()
    data["procs"] = procs
    
    for proc in procs:
        latency_max = throughput_data[(throughput_data.concurrency == proc)]['execution_time'].max()
        latency_avg = throughput_data[(throughput_data.concurrency == proc)]['execution_time'].mean()
        latency_min = throughput_data[(throughput_data.concurrency == proc)]['execution_time'].min()
        data["throughput"]["max"].append((NUM_QUERIES / latency_min) *  FACTOR)
        data["throughput"]["avg"].append((NUM_QUERIES / latency_avg) * FACTOR)
        data["throughput"]["min"].append((NUM_QUERIES / latency_max) * FACTOR)
    
    return data


def plot_throughput(data, label, outputdir):
    xdata = data["procs"]
    ydata_min = data["throughput"]["min"]
    ydata_avg = data["throughput"]["avg"]
    ydata_max = data["throughput"]["max"]

    yerror = [[], []]
    for i in range(0, len(ydata_avg)):
        yerror[0].append(ydata_avg[i] - ydata_min[i])
        yerror[1].append(ydata_max[i] - ydata_avg[i])
    
    plt.figure(1)
    ax1 = plt.subplot()
    
    ax1.plot(xdata, ydata_avg, zorder=2, color="black", alpha=1.0, linestyle="-", marker='o', label=label + " queries")
    #ax1.bar(xdata, ydata_avg, 2, zorder=2, edgecolor="black", color="gray", alpha=1.0, label="targeted queries")
    ax1.errorbar(xdata, ydata_avg, yerr=yerror, fmt="none", zorder=3, capsize=6, ecolor="black")
    
    ax1.set_xlabel("Number of Concurrent Queries")
    ax1.set_xticks(xdata)
    ax1.set_ylabel("Throughput (queries / minute)")
    ax1.legend(loc="lower right")
    plt.grid(axis='y', b=True, zorder=1)
    plt.savefig(outputdir + "/throughput-" + label + ".png", dpi=300, bbox_inches='tight')

    with open(outputdir + "/throughput-" + label + ".txt", "w") as fin:
        for i in range(0, len(xdata)):
            fin.write(str(xdata[i]) + " " 
                    + str(ydata_min[i]) + " " 
                    + str(ydata_avg[i]) + " " 
                    + str(ydata_max[i]) + "\n")


def main(args):
    if len(args) != 4:
        print(MSG)
        sys.exit(1)

    inputdir = args[1]
    label = args[2]
    outputdir = args[3]
    
    
    data = get_data(inputdir)
    plot_throughput(data, label, outputdir)

    exit(0)


if __name__ == "__main__":
    main(sys.argv)
