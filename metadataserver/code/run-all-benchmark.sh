#!/bin/bash

# set query list and number of worker threads list
CONTAINER="metadataserver_web-backend_1"
QUERY_FILE="terms.txt"
THREADS_LIST="1 4 8 12 16 20 24 28 32"
ITERATIONS="1 2 3 4 5"
LOG_EXT="t.log"

declare -a query_array
i=0
while read query
do
    query_array[$i]=$query
    i=$(($i + 1))
done < $QUERY_FILE
n=$i

for iteration in $ITERATIONS
do
    for workers in $THREADS_LIST
    do
        logfile="qprofile-logs/info.queries.$workers$LOG_EXT"
        
        if [ $iteration -eq 1 ]
        then
            echo -n "" > $logfile
        fi

        # 1. modify the number of workers of the backend in the docker-compose file
        sed -i "/SEARCH_CONFIG_NUM_WORKERS/c\      - SEARCH_CONFIG_NUM_WORKERS=$workers" docker-compose.yml
        sleep 1
            
        # 2. upgrade the stack
        docker-compose up -d
        sleep 10
        
        # 3. Log output
        docker logs -f $CONTAINER &>> $logfile &
        sleep 20
        
        for ((i=0;i<$n;i++))
        do
            query=${query_array[$i]}
            
            echo -n "Execution start date: "
            date -u

            # 4. run queries
            docker exec -it $CONTAINER bash -c "python snipet.py localhost $query $workers"

            echo -n "Execution end date: "
            date -u

            sleep 2
        done
        #sleep 10
    done
done

