import requests
import requests.auth
import sys
import time
from datetime import datetime

def test_search(q, token, page_start=0, num_results=10, serverip="localhost"):
    base_url = "http://" + serverip + ":8000/backend/api/search/do_search/papers/"
    
    response = requests.get(
        base_url + '?query={}&page_start={}&num_results={}'.format(q, page_start, num_results),
        headers={"Authorization": "Token {}".format(token)}
    )
    
    if response.ok:
        print(response.json())
    else:
        response.raise_for_status()


if __name__ == "__main__":
    serverip = "localhost"
    query = "graphene"
    workers = ""
    if len(sys.argv) == 2:
        serverip = sys.argv[1]
    if len(sys.argv) == 3:
        serverip = sys.argv[1]
        query = sys.argv[2]
    if len(sys.argv) == 4:
        serverip = sys.argv[1]
        query = sys.argv[2]
        workers = sys.argv[3]
    with requests.Session():
        auth_response = requests.post(
            'http://' + serverip + ':8000/backend/api-token-auth/',
            data={"username": 'userncem', "password": 'p4ssw3rd'}
        )
        
        if not auth_response.ok:
            auth_response.raise_for_status()
    
        token = auth_response.json()["token"]
        
        tstart = datetime.now()
        test_search(query, token, serverip=serverip)
        tend = datetime.now()
        tdelta = tend - tstart

        print("Execution time for [" + query + "] with " + workers + " workers: " 
                + str(tdelta.seconds + (tdelta.microseconds / 1000000)))
