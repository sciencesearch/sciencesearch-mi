import sys
import pandas as pd
import numpy as np

MSG = "Usage: python3 parse-main-log-tp.py <input file> <output CSV file>"

def extract_data(outputfile, inputfile):
    columns = ["concurrency", "execution_time"]
    data_pandas = pd.DataFrame(columns=columns)
    
    data = dict()
    for column in columns:
        data[column] = []
    
    with open(inputfile, "r") as fin:
        while True:
            line = fin.readline()
            if line == "":
                break
            
            line = line.replace("\n", "").split()
            data["concurrency"].append(int(line[4]))
            data["execution_time"].append(float(line[6]))

    data_pandas = data_pandas.append(pd.DataFrame(data))
    data_pandas.to_csv(outputfile)


def main(args):
    if len(args) < 3:
        print(MSG)
        sys.exit(1)

    inputfile = args[1]
    outputfile = args[2]

    extract_data(outputfile, inputfile)


if __name__ == "__main__":
    main(sys.argv)
