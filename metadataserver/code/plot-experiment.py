import sys
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
#from matplotlib.ticker import ScalarFormatter, MultipleLocator, AutoMinorLocator
#from statistics import mean

import pandas as pd
import numpy as np

MSG = "Usage: python3 plot-experiment.py <input dir> <output dir> <label>"


def plot_latency1(outputdir, main_data_pd, label):
    #["experiment_id", "iteration_seq", "query_term", "num_workers", "elapsed_time", "start_time", "end_time"]

    min_experiment_id = None
    if label == "general":
        min_experiment_id = 6
    elif label == "targeted":
        min_experiment_id = 73
    else:
        print("ERROR: Unrecognized label!")
        return
    
    #data_pd = main_data_pd[(main_data_pd.experiment_id > min_experiment_id)]
    
    data_pd = main_data_pd

    workers = data_pd['num_workers'].drop_duplicates().sort_values().values

    mean_values = data_pd[['num_workers', 'query_term', 'elapsed_time']].groupby(['num_workers', 'query_term']).mean()\
            .groupby(['num_workers']).mean().values
    
    max_values = data_pd[['num_workers', 'query_term', 'elapsed_time']].groupby(['num_workers', 'query_term']).max()\
            .groupby(['num_workers']).max().values
    
    min_values = data_pd[['num_workers', 'query_term', 'elapsed_time']].groupby(['num_workers', 'query_term']).min()\
            .groupby(['num_workers']).min().values
    
    mean_values = [elem[0] for elem in mean_values]
    max_values = [elem[0] for elem in max_values]
    min_values = [elem[0] for elem in min_values]
    
    filename = outputdir + "/query-latency-" + label + ".png"
    text = label + " queries"
    
    xdata = workers
    ydata = mean_values
    yerror = [[], []]
    for i in range(0, len(workers)):
        yerror[0].append(mean_values[i] - min_values[i])
        yerror[1].append(max_values[i] - mean_values[i])
    
    plt.figure(1)
    ax1 = plt.subplot()
    
    ax1.bar(xdata, ydata, 2, zorder=2, edgecolor="black", color="white", alpha=1.0, label=text)
    ax1.errorbar(xdata, ydata, yerr=yerror, fmt="none", zorder=3, capsize=6, ecolor="black")
    
    ax1.set_xlabel("Number of Workers")
    ax1.set_xticks(xdata)
    ax1.set_ylabel("Latency (s)")
    ax1.legend(loc="upper right")
    plt.grid(axis='y', b=True, zorder=1)
    plt.savefig(filename, dpi=300, bbox_inches='tight')

    with open(outputdir + "/query-latency-" + label + ".txt", "w") as fin:
        for i in range(0, len(xdata)):
            fin.write(str(xdata[i]) + " " 
                    + str(min_values[i]) + " " 
                    + str(mean_values[i]) + " " 
                    + str(max_values[i]) + "\n")
    

def plot_latency2(outputdir, main_data_pd, queries_data_pd, label):
    #['experiment_id', 'workersT', 'combineT', 'flattenT', 'totalT']

    min_experiment_id = None
    if label == "general":
        min_experiment_id = 6
    elif label == "targeted":
        min_experiment_id = 73
    else:
        print("ERROR: Unrecognized label!")
        return
    
    data_pd = pd.merge(main_data_pd[(main_data_pd.experiment_id > min_experiment_id)], 
            queries_data_pd[(queries_data_pd.experiment_id > min_experiment_id)], on="experiment_id")
    
    workers = data_pd['num_workers'].drop_duplicates().sort_values().values

    totalt_vals = data_pd[['num_workers', 'query_term', 'totalT']].groupby(['num_workers', 'query_term']).mean()\
            .groupby(['num_workers']).mean().values
    
    flattent_vals = data_pd[['num_workers', 'query_term', 'flattenT']].groupby(['num_workers', 'query_term']).mean()\
            .groupby(['num_workers']).mean().values
    
    combinet_vals = data_pd[['num_workers', 'query_term', 'combineT']].groupby(['num_workers', 'query_term']).mean()\
            .groupby(['num_workers']).mean().values
    
    workerst_vals = data_pd[['num_workers', 'query_term', 'workersT']].groupby(['num_workers', 'query_term']).mean()\
            .groupby(['num_workers']).mean().values
    
    totalt_vals = [elem[0] for elem in totalt_vals]
    flattent_vals = [elem[0] for elem in flattent_vals]
    combinet_vals = [elem[0] for elem in combinet_vals]
    workerst_vals = [elem[0] for elem in workerst_vals]

    filename = outputdir + "/query-latency-detailed-" + label + ".png"

    xdata = workers
    ydata1 = totalt_vals
    ydata2 = [(elem[0] + elem[1] + elem[2]) for elem in zip(flattent_vals, combinet_vals, workerst_vals)]
    ydata3 = [(elem[0] + elem[1]) for elem in zip(combinet_vals, workerst_vals)]
    ydata4 = workerst_vals
    
    plt.figure(2)
    ax1 = plt.subplot()
    
    ax1.bar(xdata, ydata1, 2, zorder=2, edgecolor="black", color="white", alpha=1.0, 
        label="total time")
    ax1.bar(xdata, ydata2, 2, zorder=3, edgecolor="black", color="white", alpha=1.0, 
        label="flatten time", hatch="\\\\\\")
    ax1.bar(xdata, ydata3, 2, zorder=4, edgecolor="black", color="white", alpha=1.0, 
        label="combine time", hatch="///")
    ax1.bar(xdata, ydata4, 2, zorder=5, edgecolor="black", color="white", alpha=1.0, 
        label="workers time", hatch="xxx")
    
    ax1.set_xlabel("Number of Workers")
    ax1.set_xticks(xdata)
    ax1.set_ylabel("Latency (s)")
    ax1.legend(loc="upper right")
    plt.grid(axis='y', b=True, zorder=1)
    plt.savefig(filename, dpi=300, bbox_inches='tight')


def plot_latency3(outputdir, main_data_pd, workers_data_pd, label):
    #['experiment_id', 'worker_id', 'indexRetrT', 'indexActT', 'indexHitsT', 'expandT', 'rankT']

    min_experiment_id = None
    if label == "general":
        min_experiment_id = 6
    elif label == "targeted":
        min_experiment_id = 73
    else:
        print("ERROR: Unrecognized label!")
        return
    
    #data_pd = pd.merge(main_data_pd[(main_data_pd.experiment_id > min_experiment_id)], 
    #        workers_data_pd[(workers_data_pd.experiment_id > min_experiment_id)], on="experiment_id")
    
    data_pd = pd.merge(main_data_pd, workers_data_pd, on="experiment_id")

    workers = data_pd['num_workers_x'].drop_duplicates().sort_values().values

    total_vals = data_pd[['num_workers_x', 'query_term_x', 'elapsed_time']].groupby(['num_workers_x', 'query_term_x']).mean()\
            .groupby(['num_workers_x']).mean().values
    
    indexretrt_vals = data_pd[['num_workers_x', 'query_term_x', 'worker_id', 'indexRetrT']]\
            .groupby(['num_workers_x', 'query_term_x', 'worker_id']).mean()\
            .groupby(['num_workers_x', 'query_term_x']).max()\
            .groupby(['num_workers_x']).mean().values
    
    indexact_vals = data_pd[['num_workers_x', 'query_term_x', 'worker_id', 'indexActT']]\
            .groupby(['num_workers_x', 'query_term_x', 'worker_id']).mean()\
            .groupby(['num_workers_x', 'query_term_x']).max()\
            .groupby(['num_workers_x']).mean().values
    
    indexhits_vals = data_pd[['num_workers_x', 'query_term_x', 'worker_id', 'indexHitsT']]\
            .groupby(['num_workers_x', 'query_term_x', 'worker_id']).mean()\
            .groupby(['num_workers_x', 'query_term_x']).max()\
            .groupby(['num_workers_x']).mean().values
    
    expand_vals = data_pd[['num_workers_x', 'query_term_x', 'worker_id', 'expandT']]\
            .groupby(['num_workers_x', 'query_term_x', 'worker_id']).mean()\
            .groupby(['num_workers_x', 'query_term_x']).max()\
            .groupby(['num_workers_x']).mean().values
    
    rank_vals = data_pd[['num_workers_x', 'query_term_x', 'worker_id', 'rankT']]\
            .groupby(['num_workers_x', 'query_term_x', 'worker_id']).mean()\
            .groupby(['num_workers_x', 'query_term_x']).max()\
            .groupby(['num_workers_x']).mean().values
    
    total_vals = [elem[0] for elem in total_vals]
    indexretrt_vals = [elem[0] for elem in indexretrt_vals]
    indexact_vals = [elem[0] for elem in indexact_vals]
    indexhits_vals = [elem[0] for elem in indexhits_vals]
    expand_vals = [elem[0] for elem in expand_vals]
    rank_vals = [elem[0] for elem in rank_vals]

    filename = outputdir + "/query-latency-deep-" + label + ".png"

    xdata = workers
    ydata1 = total_vals
    ydata2 = [(elem[0] + elem[1]) for elem in zip(indexretrt_vals, indexact_vals)]
    ydata3 = [(elem[0] + elem[1]) for elem in zip(indexhits_vals, ydata2)]
    ydata4 = [(elem[0] + elem[1]) for elem in zip(expand_vals, ydata3)]
    ydata5 = [(elem[0] + elem[1]) for elem in zip(rank_vals, ydata4)]
    
    plt.figure(3)
    ax1 = plt.subplot()
    
    ax1.bar(xdata, ydata1, 2, zorder=2, edgecolor="black", color="white", alpha=1.0, label="other")
    ax1.bar(xdata, ydata5, 2, zorder=3, edgecolor="black", color="white", alpha=1.0, 
            label="ranking", hatch="ooo")
    ax1.bar(xdata, ydata4, 2, zorder=4, edgecolor="black", color="white", alpha=1.0, 
            label="recreate metadata", hatch="///")
    ax1.bar(xdata, ydata3, 2, zorder=5, edgecolor="black", color="white", alpha=1.0, 
            label="index filter", hatch="xxx")
    ax1.bar(xdata, ydata2, 2, zorder=6, edgecolor="black", color="white", alpha=1.0, 
            label="index retrieval", hatch="\\\\\\")
    
    ax1.set_xlabel("Number of Workers")
    ax1.set_xticks(xdata)
    ax1.set_ylabel("Latency (s)")
    ax1.legend(loc="upper right")
    plt.grid(axis='y', b=True, zorder=1)
    plt.savefig(filename, dpi=300, bbox_inches='tight')


def main(args):
    if len(args) != 4:
        print(MSG)
        sys.exit(1)

    inputdir = args[1]
    outputdir = args[2]
    label = args[3]

    filename = inputdir + "/info.main.csv"
    main_data_pd = pd.read_csv(filename)

    filename = inputdir + "/info.queries.csv"
    queries_data_pd = pd.read_csv(filename)

    filename = inputdir + "/info.workers.csv"
    workers_data_pd = pd.read_csv(filename)

    plot_latency1(outputdir, main_data_pd, label)
    #plot_latency2(outputdir, main_data_pd, queries_data_pd, label)
    plot_latency3(outputdir, main_data_pd, workers_data_pd, label)

    return 0


if __name__ == "__main__":
    main(sys.argv)