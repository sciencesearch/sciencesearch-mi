import requests
import requests.auth
import sys
import time
import multiprocessing
from datetime import datetime

def test_search(q, token, page_start=0, num_results=10, serverip="localhost"):
    base_url = "http://" + serverip + ":8000/backend/api/search/do_search/papers/"
    
    response = requests.get(
        base_url + '?query={}&page_start={}&num_results={}'.format(q, page_start, num_results),
        headers={"Authorization": "Token {}".format(token)}
    )

    return response
    

def func(params):
    #tstart = datetime.now()
    rc = test_search(params[0], params[1], serverip=params[2])
    #tend = datetime.now()
    #tdelta = tend - tstart
    #print("Execution time with concurrency " + str(concurrency) + " for [" + params[0] + "] was: " 
    #        + str(tdelta.seconds + (tdelta.microseconds / 1000000)))
    #time.sleep(4)
    return rc


if __name__ == "__main__":
    serverip = "localhost"
    queries = ["graphene"]
    concurrency = 1
    if len(sys.argv) == 2:
        serverip = sys.argv[1]
    elif len(sys.argv) == 3:
        serverip = sys.argv[1]
        with open(sys.argv[2], "r") as filein:
            queries = []
            for line in filein:
                query = line.replace("\n", "")
                queries.append(query)
    elif len(sys.argv) == 4:
        serverip = sys.argv[1]
        with open(sys.argv[2], "r") as filein:
            queries = []
            for line in filein:
                query = line.replace("\n", "")
                queries.append(query)
        concurrency = int(sys.argv[3])

    with requests.Session():
        auth_response = requests.post(
            'http://' + serverip + ':8000/backend/api-token-auth/',
            data={"username": 'userncem', "password": 'p4ssw3rd'}
        )
        
        if not auth_response.ok:
            auth_response.raise_for_status()
    
        token = auth_response.json()["token"]
        
        pool = multiprocessing.Pool(processes=concurrency)
        tstart = datetime.now()
        responses = pool.map(func, [(query, token, serverip) for query in queries])
        tend = datetime.now()
        tdelta = tend - tstart
        
        num_errors=0
        for response in responses:
            if response.ok:
                print(response.json())
            else:
                num_errors += 1
                #response.raise_for_status()
        print("Total number of errors:  " + str(num_errors))
        print("Execution time with concurrency " + str(concurrency) + " was: " 
                + str(tdelta.seconds + (tdelta.microseconds / 1000000)))

