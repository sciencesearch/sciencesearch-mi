import sys

MSG = "Usage: python3 correct-main-log.py <input file> <output file>"


def main(args):
    if len(args) != 3:
        print(MSG)
        sys.exit(1)

    inputfile = args[1]
    outputfile = args[2]

    first_time = None

    with open(inputfile, 'r') as filin, open(outputfile, 'w') as filout:
        skip = 0
        for line in filin:
            words=line.replace("\n", "").split()
            if skip != 1:
                if first_time == None:
                    first_time = int(words[6].split(':')[0])
                else:
                    if first_time > int(words[6].split(':')[0]):
                        words[6] = str(int(words[6].split(':')[0]) + 24) \
                                + ':' + words[6].split(':')[1] \
                                + ':' + words[6].split(':')[2]
            outline = ""
            for word in words[:-1]:
                outline += word + " "
            outline += words[-1] + "\n"
            filout.write(outline)
            skip = (skip + 1) % 3

    return 0


if __name__ == "__main__":
    main(sys.argv)