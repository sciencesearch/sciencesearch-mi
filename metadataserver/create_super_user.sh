#!/bin/bash
#
# Script to create and a superuser to access the Metadataserver admin interface.
#

echo "Starting services..."
docker-compose up -d

echo "About to create the superuser to administrate the django server."
echo "Note down the user and password and use [site]/admin to create new users."
docker-compose run web python manage.py createsuperuser

echo "Stopping services..."
docker-compose down
