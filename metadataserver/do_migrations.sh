#!/bin/bash

docker-compose exec web-backend sh -c 'POSTGRES_PASSWORD=`cat /run/secrets/postgres_password` python manage.py makemigrations ncem ncemreldata searchengine papermanager'
docker-compose exec web-backend sh -c 'POSTGRES_PASSWORD=`cat /run/secrets/postgres_password` python manage.py migrate'
