#!/bin/bash
#
# Entry point to start the metadata server, it will start it in WSGI mode if
# WSGGI_ACTIVATED is set to any value. It will start it in stand alone mode
# otherwise.

if [ -z "$WSGI_ACTIVATED" ]
then 
    python3 manage.py runserver 0.0.0.0:8000
else
	./start_wsgi.sh
fi
