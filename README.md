# README #

## Authors ##

* **Lavanya Ramakrishnan** - (lramakrishnan@lbl.gov)
* **Katie Antypas** - (kantypas@lbl.gov)
* **Anna Giannakou** - (agiannakou@lbl.gov)
* **Alexandru Iulian Orhean** - (aiorhean@lbl.gov)

Contributors:

* **Matthew Henderson** - (mhenderson@lbl.gov)
* **Gonzalo Rodrigo Alvarez** - (gprodrigoalvarez@lbl.gov)
* **Gunther Weber** - (ghweber@lbl.gov)

## Description ##

This document describes the necessary steps that need to be taken in order to deploy S-Search on a single node machine, run the query latency and query throughput experiments, and collect, parse and plot the results the same way they are presented in the Experimental Results section.

**OBSERVATION 1**: Since the dataset that we used during the evaluation of the system is not maintained in a public repository and is not available under an OSI-approved license, this artifact comes packed with a different dataset. The dataset provided by us is a smaller subset of the initial dataset, containing a database of research papers, metadata tags generated from the research paper and indexes assembled from the metadata tags.

**OBSERVATION 2**: The provided dataset does not contain all of the terms that we used during the experimental evaluation, so we only provided with this release the terms that returned a non-empty list of results when initiating queries. The reason behind this decision has to do with the fact the queries that return a empty list of results run very fast and can skew results, especially when applying averages, minimums and maximums on the collected execution times.

**OBSERVATION 3**: Because the provided dataset is smaller, terms from query set 2 (open-ended queries) determine S-Search to execute queries with similar latency to the terms from query set 1 (targeted queries). Thus we only included terms from query set 1 in this release. The dataset is provided as a psql script that inserts the data in the database of S-Search.

**OBSERVATION 4**: Since the dataset provided with this artifact is different than the one in the paper and to keep the results consistent, we ran the query latency and query throughput experiments over the provided dataset on the SC-Platform and Testbed B (systems mentioned in the paper) and included the results in their raw form, but also as CSV files and plots. The results can be compared to your runs and can be found under 'metadataserver/qprofile-results/'

## Requirements ##

You need to have docker and docker-compose installed on your system. For reference we tested the artifact on Ubuntu 18.04.4 LTS, Docker 19.03.8, docker-compose 1.17.1. After you install docker you can add your used to the docker group in order to avoid using sudo, but the scripts assume that your user is not part of that group and use sudo anyway.

If you want to parse the results and plot them you will also need to install Python3, matplotlib, numpy and pandas on your system. When we tested the artifact all of the libraries were installed from Ubuntu's package manager:

~~~
sudo apt install python3 python3-matplotlib python3-numpy python3-pandas
~~~

Make sure that you are in the 'metadataserver' directory when running any of the following steps:

~~~
cd metadataserver
~~~

## Configuration and Deployment ##

Configure 'Dockerfile' with your username, userid, groupname and groupid (lines 3, 4, 5 and 6). You can get this information by running the 'id' command.

```
ENV USERID <userid>
ENV USERNAME <username>
ENV GROUPID <groupid>
ENV GROUPNAME <groupname>
```

Deploy the database and the backend containers together as an application:

```
sudo docker-compose up
```

Wait until the application is built and deployed, then press Ctrl + C to exit. .

Start the application in detached mode:

```
sudo docker-compose up -d
```

Import the database

```
sudo docker exec -it metadataserver_db_1 bash
psql -U metadataserver < /docker-entrypoint-initdb.d/metadataserver.psql
exit
```

## Query latency experiments ##

### Setup ###

Put the query latency terms in the 'terms.txt' file, that will be read during the experiments:

```
cat terms-targeted.txt > terms.txt
```

Change line 59 in 'docker-compose.yml' to point to the latency experiment python client script:

```
      - ./code/snippet.py:/code/snipet.py
```

Clear the logs directory:

```
sudo bash -c "rm qprofile-logs/*.log"
```

Restart the docker application

```
sudo docker-compose down
sudo docker-compose up -d
```

Run a suite of 3105 experiments (69 queries x 9 number of parallel worker process configurations x 5 iterations). This will take some time to run, but if you want to avoid waiting too much time, you can change line 7 in the **code/run-all-benchmark.sh** script to only run iteration 1:

```
ITERATIONS="1"
```

To start running the queries run this script like this (you might want to run this command in a screen or tmux session, if you are running the experiments remotely and especially if you are running all 3105 experiments):

```
sudo bash -c "./code/run-all-benchmark.sh | tee qprofile-logs/info.main.log"
```

You can change the number of parallel worker processes configurations by editing the **THREADS_LIST** variable in the **code/run-all-benchmark.sh** script.

### Parse logs and plot figure ###

Copy the log files to another directory:

```
mkdir latency-experiments
cp -r qprofile-logs latency-experiments/.
```

Clean the info.main.log file (this will filter out the result list for each query and only keep the timestamp and execution time information):

```
cat latency-experiments/qprofile-logs/info.main.log | grep "Execution" > latency-experiments/qprofile-logs/info.main.log.clean
```

The parse scripts use timestamp information to correlate the execution time logged by the backend and the time collected by the client script. The timestamp is in UTC date format and for the parse scripts to work correctly the time must not cycle back to 00:00 at midnight (if the experiments were run over multiple days). Run the following scripts to fix any issues with timestamps:

```
mkdir latency-experiments/qprofile-logs-adjusted
python3 code/correct-main-log.py latency-experiments/qprofile-logs/info.main.log.clean latency-experiments/qprofile-logs-adjusted/info.main.log.clean
for i in 1 4 8 12 16 20 24 28 32; do python3 code/correct-query-log.py latency-experiments/qprofile-logs/info.queries.${i}t.log latency-experiments/qprofile-logs-adjusted/info.queries.${i}t.log; done
```

Parse and extract CSV files from the experiment logs:

```
python3 code/parse-experiment.py latency-experiments/qprofile-logs-adjusted latency-experiments
```

Plot average latency and min-max variation with increasing number of parallel worker processes and the latency breakdown for each search phase:

```
python3 code/plot-experiment.py latency-experiments latency-experiments targeted
```

**query-latency-targeted.png** and **query-latency-deep-targeted.png** contain the plotted results.

## Measuring query throughput ##

### Setup ###

Put the query throughput terms in the 'terms.txt' file, that will be read during the experiments:

```
cat terms-targeted.concurrent.txt > terms.txt
```

Change line 59 in docker-compose.yml to point to the throughput experiment python client script:

```
      - ./code/concurrent_snippet.py:/code/snipet.py
```

Change line 82 in docker-compose.yml and fix the number of parallel worker processes to 4:

```
      - SEARCH_CONFIG_NUM_WORKERS=4
```
    

Clear the logs directory:

```
sudo bash -c "rm qprofile-logs/*.log"
```

Restart the docker application

```
sudo docker-compose down
sudo docker-compose up -d
```

Run a suite of 45 experiements (9 number of concurrent queries configurations x 5 iterations) This will take some time to run, but if you want to avoid waiting too much time, you can change line 7 in the **code/run-all-concurrent-benchmark.sh** script to only run iteration 1:

```
ITERATIONS="1"
```

To start running the queries run this script like this (you might want to run this command in a screen or tmux session, if you are running the experiments remotely and especially if you are running all 45 experiments):

```
sudo bash -c "./code/run-all-concurrent-benchmark.sh | tee qprofile-logs/info.main.log"
```

### Parse logs and plot figure ###

Copy the log files to another directory:

```
mkdir throughput-experiments
cp -r qprofile-logs throughput-experiments/.
```

Clean the info.main.log file (this will filter out the result list for each query and only keep the timestamp and execution time information):

```
cat throughput-experiments/qprofile-logs/info.main.log | grep "Execution time" > throughput-experiments/qprofile-logs/info.main.log.clean
```

Parse and extract CSV files from the experiment logs:

```
python3 code/parse-main-log-tp.py throughput-experiments/qprofile-logs/info.main.log.clean throughput-experiments/info.main.csv
```

Plot average query througput with increasing levels of concurrency:

```
python3 code/plot-throughput.py throughput-experiments targeted throughput-experiments
```

**throughput-targeted.png** contains the plotted results.

